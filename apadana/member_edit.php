<?php 
$action = filter_input(INPUT_GET, 'action', $filter = FILTER_SANITIZE_STRING);

if ($action == "charge_balance"){
	require_once 'inc/db_connect.php';
	require_once 'inc/functions.php';
	ob_start();
	sec_session_start();
	require_once 'inc/information.php';
	require_once('inc/statistics.php');
	
	$username = filter_input(INPUT_GET, 'username', $filter = FILTER_SANITIZE_STRING);
	$balance = filter_input(INPUT_GET, 'balance', $filter = FILTER_SANITIZE_STRING);
	
	mysql_update('members', array('username'=>$username), array('balance'=>$balance));
	
	echo "موجودی به‌روز شد.";
	return;
	
}

require_once('header.php'); 
?>

<?php if ($role != "admin"){header("Location: login.php?redirect=".urlencode("http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']));} ?>

<?php
if (!empty($err)) {
	switch ($err) {
		
	case "invalid-username" : $err_msg = "نام کاربری باید شامل حروف انگلیسی و اعداد باشد!"; break;
	case "username-exists" : $err_msg = "کاربری با این نام کاربری وجود دارد!"; break;
	case "mobile_number-length" : $err_msg = "طول شماره موبایل باید 10 حرف باشد!"; break;
	case "invalid-activation" : $err_msg = "درخواست نامعتبر است!"; break;
	case "invalid-reset" : $err_msg = "درخواست نامعتبر است!"; break;
	case "invalid" : $err_msg = "فرمت تصویر قابل پشتیبانی نیست!"; break;
	case "fill" : $err_msg = "موارد الزامی را پر کنید!"; break;
	case "empty" : $err_msg = "تصویری را انتخاب کنید!"; break;
	case "size" : $err_msg = "حجم فایل رعایت نشده است.!"; break;
	case "type" : $err_msg = "پسوند فایل رعایت نشده است!"; break;
	case "upload" : $err_msg = "خطا در آپلود فایل. مجوزها را بررسی کنید!"; break;
	case "con-oldpsw" : $err_msg = "گذرواژه قبلی شما درست نیست!"; break;
	case "invalid-email" : $err_msg = "ایمیل وارد شده معتبر نیست!"; break;
	case "email-exists" : $err_msg = "کاربری با این ایمیل وجود دارد!"; break;
	case "database" : $err_msg = "خطای پایگاه داده!"; break;
	case "insert" : $err_msg = "خطا در ویرایش پایگاه داده!"; break;
	case "edit" : $err_msg = "خطا در نوشتن دیتابیس!"; break;
	case "username-exists" : $err_msg = "کاربری با این نام کاربری وجود دارد!"; break;
	case "mobile_number-exists" : $err_msg = "کاربری با این شماره موبایل وجود دارد!"; break;
	case "min-length" : $err_msg = "طول نام کاربری و گذواژه باید حداقل 6 کاراکتر باشد!"; break;
	case "max-username-length" : $err_msg = "طول نام کاربری باید حداکثر 50 کاراکتر باشد!"; break;
	case "max-email-length" : $err_msg = "طول آدرس ایمیل باید حداکثر 50 کاراکتر باشد!"; break;
	
	default : $err_msg = "خطای غیر منتظره‌ای پیش آمده!"; break;
	}
	
}
?> 
<?php
if (!empty($suc)) {
	switch ($suc) {
	case "password" : $suc_msg = "گذرواژه تغییر کرد!"; break;
	case "check-email" : $suc_msg = "ایمیل فرستاده شد."; break;
	case "upload" : $suc_msg = "تصویر آپلود شد!"; break;
	case "delete" : $suc_msg = "تصویر حذف شد!"; break;
	case "update" : $suc_msg = "نمایه به‌روز شد!"; break;
	case "edit" : $suc_msg = "کاربر ویرایش شد!"; break;

	default : $suc_msg = "عملیات با موفقیت انجام شد!"; break;
	}
	
}
?> 
<?php
if (!empty($err_msg)) {
	echo '<div class="alert alert-error no-print"><a href="'.str_replace('err='.$err,"",implode('&',array_unique(explode('&',$_SERVER['REQUEST_URI'])))).'"><button type="button" class="close">&times;</button></a><p>'. $err_msg .'</p></div>';
}
?>
<?php
if (!empty($suc_msg)) {
	echo '<div class="alert alert-success no-print"><a href="'.str_replace('suc='.$suc,"",implode('&',array_unique(explode('&',$_SERVER['REQUEST_URI'])))).'"><button type="button" class="close">&times;</button></a><p>'. $suc_msg .'</p></div>';
}
?>
<?php

if (!isset($_SESSION['members_redirect'])){$_SESSION['members_redirect'] = "members.php";}

// ------

$id = filter_input(INPUT_GET, 'id', $filter = FILTER_SANITIZE_STRING);

// ------

$stmt = 'SELECT username, email, password, role, level, status, profile_image, first_name, last_name, mobile_number, mobile_number_status, home_number, fax_number, office, shaba, address, presenter, balance, reward_percentage, about, extras FROM members WHERE id="'.$id.'"';

//echo $stmt;exit;

if ($mysqli->query($stmt)->num_rows<1){ header('Location: members.php?err=not-exists'); exit; }

$stmt = $mysqli->prepare($stmt);
$stmt->execute();
$stmt->store_result();

$stmt->bind_result($username, $email, $password, $role, $level, $status, $profile_image, $first_name, $last_name, $mobile_number, $mobile_number_status, $home_number, $fax_number, $office, $shaba, $address, $presenter, $balance, $reward_percentage, $about, $extras);
$stmt->fetch();
$stmt->close();

?>
<div class="container">
	<div class="pull-left">
		<a class="pull-right" style="margin-left: 3px;" href="member_delete.php?<?php echo 'id='.$id ?>"><button class='btn btn-danger'><span>حذف</span> <i class="icon-trash icon-white"></i></button></a>
		<form action="<?php echo $options["url"] ?>/inc/forget.php" method="post" name="forget_form" class="pull-right" style="margin-left: 3px;">
			<input type="hidden" value="member_edit.php?id=<?php echo $id ?>&" id="redirect" name="redirect" />
			<input type="hidden" placeholder="" class="span2" id="username" name="username" value="<?php echo $username ?>" />
			<button class="btn btn-success pull-left normal" type="submit"><span>بازنشانی رمز عبور</span> <i class="icon-lock icon-white"></i></button>  
		</form>
		<a href="<?php echo $options["url"].'/inc/send_activation.php?username='.$username.'&password='.$password.'&redirect=member_edit.php?id='.$id ?>"><button class="btn btn-primary normal"><span>درخواست فعال‌سازی</span> <i class="icon-user icon-white"></i></button></a>
		<a href="<?php echo $_SESSION['members_redirect'] ?>"><button class='btn'><span>بازگشت</span> <i class="icon-chevron-left"></i></button></a>
	</div>
	<button class="btn disabled"><span id="subtitle">ویرایش عضو</span></button><br /><br />
	<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert">&times;</button>
	<p>مواردی که با علامت ستاره‌ی قرمز مشخص شده‌ است، الزامی است.</p></div>
	<div id="right" class="span4 pull-right text-right">
		<ul class="nav nav-tabs">
			<li class="pull-right active"><a href="#basic-data" data-toggle="tab">اطلاعات پایه</a></li>
			<li class="pull-right"><a href="#change-password" data-toggle="tab">گذرواژه</a></li>
            <li class="pull-right"><a href="#profile-picture" data-toggle="tab">تصویر</a></li>
            <li class="pull-right"><a href="#wallet" data-toggle="tab">کیف پول</a></li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane active" id="basic-data">
				<form action="<?php echo $options["url"] ?>/inc/edit_member.php" method="post">
					<input type="hidden" value="member_edit.php?id=<?php echo $id ?>&" name="redirect" id="redirect"/>
					<input type="hidden" value="<?php echo $email ?>" name="former_email" id="former_email"/>
                    <input type="hidden" value="<?php echo $mobile_number ?>" id="former_mobile_number" name="former_mobile_number" />
					<table class="table table-striped table-right">
			<tr>
				<td class="span2"><h5 class="normal">نام کاربری <span class="red">*</span></h5></td>
							<td style="padding: 5px 0 0 0;"><input type="text" maxlength="30" style="font: normal 11px tahoma; width:100px;" value="<?php echo $username ?>" name="username" id="username" /></td>
						</tr>
						<tr>
							<td><h5 class="normal">ایمیل <span class="red">*</span></h5></td>
							<td style="padding: 5px 0 0 0;"><input type="email" style="font: normal 11px tahoma; width:100px;" value="<?php echo $email ?>" name="email" id="email"/></td>
						</tr>
                        <tr>
							<td><h5 class="normal">نقش</h5></td>
							<td style="padding: 5px 0 0 0;">
								<select class="tahoma size-11" style="width: 115px;" name="role" id="role">
									<?php
									foreach ($member_roles as $role1=>$role1_value){
										echo '<option ';
										if ($role1==$role){ echo 'selected="selected" ';}
										echo 'value="'.$role1.'">'.$role1_value.'</option>';
									}
									?>
									<?php
				
									$stmt = $mysqli->prepare("select count(1) FROM members_roles");
													
									$stmt->execute();
									$stmt->bind_result($rows);
									$stmt->fetch();
									$stmt->close();
				
									for($i=0;$i<$rows;$i++){
										
										$stmt = $mysqli->prepare('SELECT slug, role_title FROM members_roles LIMIT 1 OFFSET '.$i);
										$stmt->execute();	
										$stmt->bind_result($slug, $role_title);
										$stmt->fetch();
										$stmt->close();
										
										echo '<option ';
										if ($role==$slug){echo 'selected="selected" '; }
										echo 'value="'.$slug.'">'.$role_title.'</option>';
					
									}
									?>
								</select>
							</td>
						</tr>
                        <tr>
                            <td><h5 class="normal">سطح</h5></td>
                            <td style="padding-top: 5px;">
                                <select class="tahoma size-11" style="width: 100px;" name="level" id="level">
                                    <?php
                                    foreach ($member_levels as $level1=>$level1_value){
                                        echo '<option ';
                                        if ($level1==$level){ echo 'selected="selected" ';}
                                        echo 'value="'.$level1.'">'.$level1_value.'</option>';
                                    }
                                    ?>
                                </select>
                            </td> 
                        </tr>
						<tr>
							<td><h5 class="normal">وضعیت</h5></td>
							<td style="padding: 5px 0 0 0;">
								<select class="tahoma size-11" style="width: 115px;" name="status" id="status">
									<?php
									foreach ($member_statuses as $status1=>$status1_value){
										echo '<option ';
										if ($status1==$status){ echo 'selected="selected" ';}
										echo 'value="'.$status1.'">'.$status1_value.'</option>';
									}
									?>
								</select>
							</td> 
						</tr>
						<tr>
							<td><h5 class="normal">نام</h5></td>
							<td style="padding: 5px 0 0 0;"><input type="text" maxlength="100" style="font: normal 11px tahoma; width:100px;" value="<?php echo $first_name ?>" name="first_name" id="first_name"/></td>
						</tr>
						<tr>
							<td><h5 class="normal">نام خانوادگی</h5></td>
							<td style="padding: 5px 0 0 0;"><input type="text" maxlength="100" style="font: normal 11px tahoma; width:100px;" value="<?php echo $last_name ?>" name="last_name" id="last_name"/></td>
						</tr>
						<tr>
							<td><h5 class="normal">شماره موبایل</h5></td>
							<td style="padding: 5px 0 0 0;"><input type="number" placeholder="" maxlength="10" value="<?php echo $mobile_number ?>" style="font: normal 11px tahoma; width:100px;" id="mobile_number" name="mobile_number"/> 98+</td>
						</tr>
                        <tr>
							<td><h5 class="normal">وضعیت شماره موبایل</h5></td>
							<td style="padding: 5px 0 0 0;">
								<select class="tahoma size-11" style="width: 115px;" name="mobile_number_status" id="mobile_number_status">
									<?php
									foreach ($mobile_number_statuses as $status1=>$status1_value){
										echo '<option ';
										if ($status1==$mobile_number_status){ echo 'selected="selected" ';}
										echo 'value="'.$status1.'">'.$status1_value.'</option>';
									}
									?>
								</select>
							</td> 
						</tr>
                        <tr>
							<td><h5 class="normal">شماره منزل</h5></td>
							<td style="padding: 5px 0 0 0;"><input type="number" placeholder="" maxlength="10" value="<?php echo $home_number ?>" style="font: normal 11px tahoma; width:100px;" id="home_number" name="home_number"/> 98+</td>
						</tr>
                        <tr>
							<td><h5 class="normal">شماره فکس</h5></td>
							<td style="padding: 5px 0 0 0;"><input type="number" placeholder="" maxlength="10" value="<?php echo $fax_number ?>" style="font: normal 11px tahoma; width:100px;" id="fax_number" name="fax_number"/> 98+</td>
						</tr>
                        <tr>
							<td><h5 class="normal">شرکت</h5></td>
							<td style="padding: 5px 0 0 0;"><input type="text" placeholder="شرکت" maxlength="100" value="<?php echo $office ?>" class="normal text-right span2" id="office" name="office" style="font: normal 12px 'WYekan',B Yekan; width:100px;" /></td>
						</tr>
                        <tr>
							<td><h5 class="normal">شماره شبا</h5></td>
							<td style="padding: 5px 0 0 0;"><input type="text" placeholder="شماره شبا" maxlength="100" value="<?php echo $shaba ?>" class="normal text-right span2" id="shaba" name="shaba" style="font: normal 12px 'WYekan',B Yekan; width:100px;" /></td>
						</tr>
                        <tr>
							<td><h5 class="normal">آدرس</h5></td>
							<td style="padding: 5px 0 0 0;"><textarea maxlength="500" class="normal text-right span2" id="address" name="address" style="font: normal 12px 'WYekan',B Yekan; width:150px;"><?php echo $address ?></textarea></td>
						</tr>
						<tr>
							<td><h5 class="normal">معرف</h5></td>
							<td style="padding: 5px 0 0 0;">
                            	<select class="chosen-select chosen-rtl" name="presenter" id="presenter" data-placeholder="کاربری را انتخاب کنید.">
                                    <option value="" selected="selected"></option>
                                    <?php
                                    
                                    $result = $mysqli->query("SELECT * FROM members WHERE id!=\"".$id."\"");
                                    $rows = $result->num_rows;
                                    
                                    for ($i=0;$i<$rows;$i++){
                                        $stmt = $mysqli->prepare("SELECT id, role, username, first_name, last_name FROM members WHERE id!=\"".$id."\" LIMIT 1 OFFSET ?");
                                        $stmt->bind_param('s', $i);
                                        $stmt->execute();
                                        $stmt->store_result();
                                 
                                        $stmt->bind_result($presenter_id, $presenter_role, $presenter_username, $presenter_first_name, $presenter_last_name);
                                        $stmt->fetch();
                                        $stmt->close();
                                        
                                        
										$presenter_display_name = $presenter_first_name." ".$presenter_last_name;
										if ($presenter_display_name == " "){
											$presenter_display_name = $presenter_username;
										}
										echo '<option value="'.$presenter_id.'" ';
										if ($presenter == $presenter_id){echo 'selected="selected"';}
										echo '>';
										echo $presenter_display_name.'</option>';
                                            
                                      
                                    }
                                    ?>
                                </select>
                                <script>
                                $("#presenter").chosen({
                                    width: "165px !important",
                                    allow_single_deselect: true,
                                    no_results_text : "کاربری یافت نشد.",
                                });
                                </script>
                            </td>
						</tr>
                        <tr>
							<td><h5 class="normal">درصد جایزه</h5></td>
							<td style="padding: 5px 0 0 0;"><input type="number" placeholder="" maxlength="3" value="<?php echo $reward_percentage ?>" class="normal text-right span2" id="reward_percentage" name="reward_percentage" style="font: normal 12px 'WYekan',B Yekan; width:50px;" /></td>
						</tr>
                        <tr>
							<td><h5 class="normal">درباره</h5></td>
							<td style="padding: 5px 0 0 0;"><textarea maxlength="500" class="normal text-right span2" id="about" name="about" style="font: normal 12px 'WYekan',B Yekan; width:150px;"><?php echo $about ?></textarea></td>
						</tr>
                        <tr>
                            <td><h5 class="normal">تنظیمات اضافی</h5></td>
                            <td style="padding: 5px 0 0 0;">
                                <?php
                                foreach ($member_extras as $extra=>$extra_text){
                                    echo '<h6 class="normal" id="'.$extra.'"><input id="'.$extra.'-check" name="extras[]" type="checkbox" value="'.$extra.'" ';
                                    if (in_array($extra, explode(";", $extras))){ echo 'checked="checked"'; }
                                    echo '>&ensp;'.$extra_text.'</option></h6>';
                                }
                                ?>
                            </td>
                        </tr>
					</table>
					<button class='btn btn-info pull-left' type="submit"><span>ویرایش</span> <i class="icon-edit icon-white"></i></button>
				</form>
			</div>
            <script type="text/javascript" src="<?php echo $options['url'] ?>/js/pwstrength.js"></script>
			<script type="text/javascript">
			jQuery(document).ready(function () {
				"use strict";
				var options = {};
				options.common = {
					onLoad: function () {
						$('#messages').text('Start typing password');
					}
				};
				options.ui = {
					showPopover: true,
					bootstrap2: true,
					showErrors: false
				};
				$('#password').pwstrength(options);
			});
			</script> 
			<div class="tab-pane" id="change-password">
				<form action="<?php echo $options["url"] ?>/inc/change_password.php" method="post">
					<input type="hidden" value="member_edit.php?id=<?php echo $id ?>&" name="redirect" id="redirect"/>
					<input type="hidden" class="span2" name="admin" id="admin" value="TRUE" />
					<input type="hidden" class="span2" name="email" id="email" value="<?php echo $email; ?>" />
					<input type="hidden" class="span2" name="salt" id="salt" value="<?php echo $salt; ?>" />
					<input type="hidden" class="span2" name="former_password" id="former_password" value="<?php echo $password; ?>" />
					<input type="hidden" class="normal text-right span2 Yekan" name="old_password" id="old_password" value="<?php echo $password; ?>" />
					<table class="table table-striped table-right">
			<tr>
				<td class="span2"><h5 class="normal">گذرواژه</h5></td>
							<td style="padding: 5px 0 0 0;"><input type="password" name="password" id="password" style="font: normal 11px tahoma; width:100px;" placeholder="" /><br /></td>
						</tr>
						<tr>
							<td><h5 class="normal">تأیید گذرواژه</h5></h5></td>
							<td style="padding: 5px 0 0 0;"><input type="password" name="confirm_password" id="confirm_password" style="font: normal 11px tahoma; width:100px;" placeholder="" /></td>
						</tr>
					</table>
					<button type="submit" name="submit" class="btn btn-info" onClick="return passformhash(this.form,this.form.old_password,this.form.password,this.form.confirm_password);"><span>تغییر</span> <i class="icon-edit icon-white"></i></button> 
				</form>
			</div>
			<div class="tab-pane span3 pull-right" id="profile-picture">
				<div class="alert text-right">
					<ul>
						<li>حجم تصویر باید کمتر از <?php echo $options['profile_image_max_file_size'] ?> کیلوبایت باشد.</li>
						<li>فرمت تصویر باید یکی از موارد زیر باشد:
						<ul>
							<li>jpg, jpeg, png, gif</li>
						</ul>	
					</ul>
				</div>
				<a class="fancybox" href="<?php if (if_file_exists('files/profile_images/'.$profile_image)){ echo 'files/profile_images/'.$profile_image; } else{ echo 'img/profile_image.png'; } ?>">
					<img class="img-circle span2" style="box-shadow: 0px 0px 5px #000;" src="<?php if (if_file_exists('files/profile_images/'.$profile_image)){ echo 'files/profile_images/'.$profile_image; } else{ echo 'img/profile_image.png'; } ?>" >
				</a>
				<form action="<?php echo $options["url"] ?>/inc/upload_file.php" method="post" enctype="multipart/form-data">
					<input type="hidden" value="member_edit.php?id=<?php echo $id ?>&" id="redirect" name="redirect"  />
					<input type="hidden" value="profile_images" name="directory" />
					<input type="hidden" value="TRUE" name="replace" />
					
					<input type="hidden" name="FILE_TYPES" value="image/jpeg image/png image/gif" />
					<input type="hidden" name="MAX_FILE_SIZE" value="<?php echo ($options['profile_image_max_file_size']*1024) ?>" />
					
					<input type="hidden" value="id" id="idd" name="idd"  />
					<input type="hidden" value="member" id="model" name="model"  />
					
					<input type="hidden" value="<?php echo $id ?>" id="value" name="value"  />
					<input type="file" name="file" id="file" class="" /><br />
					
					<button type="submit" name="submit" class="btn btn-info"><span>آپلود</span> <i class="icon-upload icon-white"></i></button>
				
				</form>
				<?php if (if_file_exists('files/profile_images/'.$profile_image)){?>
					<form action="<?php echo $options["url"] ?>/inc/delete_file.php" method="post">
						<input type="hidden" value="member_edit.php?id=<?php echo $id ?>&" id="redirect" name="redirect" />
						<input type="hidden" value="profile_images" name="directory" />
						<input type="hidden" value="<?php echo $profile_image ?>" id="file" name="file"  />
						
						<input type="hidden" value="username" id="idd" name="idd"  />
						<input type="hidden" value="member" id="model" name="model"  />
						<input type="hidden" value="<?php echo $username ?>" id="value" name="value"  />
						
						<button type="submit" name="submit" class="pull-left btn btn-danger"><span>حذف</span> <i class="icon-trash icon-white"></i></button>
					</form>
				<?php } ?>
				
			</div>
            <div class="tab-pane" id="wallet">
            	<table class="table table-striped table-right">
			<tr>
				<td class="span2"><h5 class="normal">موجودی</h5></td>
                        <td style="padding: 5px 0 0 0;"><input type="number" placeholder="" value="<?php echo $balance ?>" class="normal text-right span2" id="price" name="price" style="font: normal 11px tahoma; width:100px;" /> ریال</td>
                    </tr>
                </table>
                <button class='btn btn-info pull-left' onclick="charge_balance();" type="submit"><span>ویرایش</span> <i class="icon-edit icon-white"></i></button>
            </div>
		</div>
        
	</div>
	<div style="clear:both"></div>
</div>
<script>
function charge_balance(){
	$.get("member_edit.php?action=charge_balance&username=<?php echo $username; ?>&balance="+($("#price").val()), function(data, status){
		fancyAlert(data);
	});	
	$("#code_section").hide();
}
</script>
<?php require_once('footer.php'); ?>