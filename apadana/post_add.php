<?php
include('header.php'); 
?>
      <?php if (if_has_permission($role,"edit_posts")){} else{header("Location: login.php?redirect=".urlencode("http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']));} ?>
      <?php
if (!empty($err)) {
	switch ($err) {
	case "insert" : $err_msg = "خطا در نوشتن دیتابیس!"; break;
	case "fill" : $err_msg = "موارد الزامی را پر کنید!"; break;
	case "exists" : $err_msg = "مطلب‌ی با این نامک وجود دارد!"; break;
	
	default : $err_msg = "خطای غیر منتظره‌ای پیش آمده!"; break;
	}
	
}
?>
      <?php
if (!empty($suc)) {
	switch ($suc) {
	case "insert" : $suc_msg = "مطلب اضافه شد!"; break;
	
	default : $suc_msg = "عملیات با موفقیت انجام شد!"; break;
	}
	
}
?>
      <?php	
if (!empty($err_msg)) {
	echo '<div class="alert alert-error no-print"><a href="'.str_replace('err='.$err,"",implode('&',array_unique(explode('&',$_SERVER['REQUEST_URI'])))).'"><button type="button" class="close">&times;</button></a><p>'. $err_msg .'</p></div>';
}
?>
      <?php
if (!empty($suc_msg)) {
	echo '<div class="alert alert-success no-print"><a href="'.str_replace('suc='.$suc,"",implode('&',array_unique(explode('&',$_SERVER['REQUEST_URI'])))).'"><button type="button" class="close">&times;</button></a><p>'. $suc_msg .'</p></div>';
}
?>
      <?php if (empty($_SESSION['posts_redirect'])){$_SESSION['posts_redirect']="posts.php";} ?>
      <div class="container"> <a href="<?php echo $_SESSION['posts_redirect'] ?>">
        <button class='pull-left btn'><span>بازگشت</span> <i class="icon-chevron-left"></i></button>
        </a>
        <button class="btn disabled"><span id="subtitle">اضافه‌کردن مطلب</span></button>
        <br />
        <br />
        <div class="alert alert-info">
          <button type="button" class="close" data-dismiss="alert">&times;</button>
          <p>مواردی که با علامت ستاره‌ی قرمز مشخص شده‌ است، الزامی است.</p>
        </div>
        <div id="main" class="span4 pull-right">
          <form action="<?php echo $options["url"] ?>/inc/add_post.php" method="post">
            <input type="hidden" name="redirect" id="redirect" value="post_add.php?" />
            <table class="table table-striped table-right">
              <tr>
              <tr>
                	<td><h5 class="span1 pull-right normal">نویسنده <span class="red">*</span></h5></td>
                	<td style="padding: 5px 0 0 0;"><input type="text" style="font: normal 11px tahoma; width:150px;" value="<?php //echo $author; ?>" name="author" id="author" /></td>
				</tr>
                <tr>
                    <td><h5 class="normal">نام</h5></td>
                    <td style="padding: 5px 0 0 0;"><input type="text" style="font: normal 11px tahoma; width:150px;" value="<?php //echo $name ?>" name="name" id="name"/></td>
                </tr>
                <tr>
					<td><h5 class="normal">توضیحات</h5></td>
					<td style="padding-top: 5px;"><textarea style="font: normal 11px tahoma; width:200px;" name="description" id="description"><?php //echo $description; ?></textarea></td>
				</tr>
                <tr>
					<td><h5 class="normal">تصویر <span class="red">*</span></h5></td>
					<td style="padding: 5px 0 0 0;"><input type="text" style="font: normal 11px tahoma; width:100px;" value="<?php //echo $image ?>" name="image" id="image"/></td>
				</tr>
                <tr>
					<td><h5 class="normal">قیمت <span class="red">*</span></h5></td>
					<td style="padding: 5px 0 0 0;"><input type="number" maxlength="11" style="font: normal 11px tahoma; width:100px;" value="<?php //cho $price; ?>" name="price" id="price"/> ریال</td>
				</tr>
                <tr>
					<td><h5 class="normal">لینک دانلود</h5></td>
					<td style="padding: 5px 0 0 0;"><input type="text" style="font: normal 11px tahoma; width:200px;" value="<?php //echo $download_link; ?>" name="download_link" id="download_link"/></td>
				</tr>
                <tr>
					<td><h5 class="normal">تعداد دانلود</h5></td>
					<td style="padding: 5px 0 0 0;"><input type="number" maxlength="11" style="font: normal 11px tahoma; width:100px;" value="<?php //echo $download_count; ?>" name="download_count" id="download_count"/></td>
				</tr>
                <tr>
					<td><h5 class="normal">تاریخ</h5></td>
					<td style="padding: 5px 0 0 0;"><input type="text" style="font: normal 11px tahoma; width:100px;" value="<?php //echo $date ?>" name="date" id="date"/></td>
				</tr>
                <tr>
					<td><h5 class="normal">زمان</h5></td>
					<td style="padding: 5px 0 0 0;"><input type="text" style="font: normal 11px tahoma; width:100px;" value="<?php //echo $time ?>" name="time" id="time"/></td>
				</tr>
                <tr>
					<td><h5 class="normal">وضعیت <span class="red">*</span></h5></td>
					<td style="padding-top: 5px;">
						<select class="tahoma size-11" style="width: 100px;" name="status" id="status">
							<?php
							foreach ($post_statuses as $status1=>$status1_value){
								echo '<option ';
								//if ($status1==$status){ echo 'selected="selected" ';}
								echo 'value="'.$status1.'">'.$status1_value.'</option>';
							}
							?>
						</select>
					</td> 
				</tr>
            </table>
            <button class='btn btn-info pull-left' type="submit"><span>اضافه کردن</span> <i class="icon-plus icon-white"></i></button>
          </form>
        </div>
      </div>
      <?php include('footer.php'); ?>