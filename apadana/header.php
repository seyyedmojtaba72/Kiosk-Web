<?php
$file_name = basename($_SERVER['PHP_SELF']);
require_once 'inc/lib/emoji/Emoji.php';
require_once 'inc/lib/emoji/EmojiIndex.php';
$emoji = new Emoji(new EmojiIndex(), '//twemoji.maxcdn.com/16x16/%s.png');
require_once 'inc/db_connect.php';
require_once 'inc/functions.php';
ob_start();
sec_session_start();
require_once 'inc/information.php';

if (login_check() == true) {
    $logged = 'in';
} else {
    $logged = 'out';
}

if ($logged == 'in'){
	$id = $_SESSION['id'];
	$username = $_SESSION['username'];
	$email = $_SESSION['email'];
	$password = $_SESSION['password'];
	$salt = $_SESSION['salt'];
	$role = $_SESSION['role'];
	$status = $_SESSION['status'];
	$profile_image = $_SESSION['profile_image'];
	$first_name = $_SESSION['first_name'];
	$last_name = $_SESSION['last_name'];
	$mobile_number = $_SESSION['mobile_number'];
	$mobile_number_status = $_SESSION['mobile_number_status'];
	$home_number = $_SESSION['home_number'];
	$fax_number = $_SESSION['fax_number'];
	$office = $_SESSION['office'];
	$shaba = $_SESSION['shaba'];
	$address = $_SESSION['address'];
	$balance = $_SESSION['balance'];
	$about = $_SESSION['about'];
	$extras = $_SESSION['extras'];
} else{
	$role="";
}

require_once 'inc/statistics.php';
?>

<?php

$err = filter_input(INPUT_GET, 'err', $filter = FILTER_SANITIZE_STRING);
$suc = filter_input(INPUT_GET, 'suc', $filter = FILTER_SANITIZE_STRING);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html lang="fa" dir=ltr>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<title><?php echo $options['title'] ?></title>

	<link href="<?php echo $options['url'] ?>/style.css" rel="stylesheet" type="text/css" />
    
	<link href="<?php echo $options['url'] ?>/img/favicon.png" rel="shortcut icon" />
	<link href="<?php echo $options['url'] ?>/img/apple-touch-icon-57.png" rel="apple-touch-icon">
	<link href="<?php echo $options['url'] ?>/img/apple-touch-icon-72.png" rel="apple-touch-icon" sizes="72x72">
	<link href="<?php echo $options['url'] ?>/img/apple-touch-icon-144.png" rel="apple-touch-icon" sizes="114x114">

	<script src="<?php echo $options['url'] ?>/js/jquery-1.10.1.min.js"></script>
    <script src="<?php echo $options['url'] ?>/js/jquery.flip.js"></script>
	<script src="<?php echo $options['url'] ?>/js/functions.js"></script> 
	<script src="<?php echo $options['url'] ?>/js/forms.js"></script> 

	<!--[if lt IE 9]>
	  <script src="<?php echo $options['url'] ?>/js/html5.js"></script>
	<![endif]-->
	
	<script src="<?php echo $options['url'] ?>/lib/bootstrap-2.3.0/js/bootstrap.js"></script> 
	<script src="<?php echo $options['url'] ?>/lib/bootstrap-2.3.0/js/bootstrap-popover.js"></script>
	<script src="<?php echo $options['url'] ?>/lib/bootstrap-2.3.0/js/bootstrap-tooltip.js"></script>
	<script src="<?php echo $options['url'] ?>/lib/bootstrap-2.3.0/js/bootstrap-tab.js"></script>
	<script src="<?php echo $options['url'] ?>/lib/bootstrap-2.3.0/js/application.js"></script>
	
	<script>
	$(document).ready(function(){
		$("[rel=tooltip-top]").tooltip({ placement: 'top'});
		$("[rel=tooltip-bottom]").tooltip({ placement: 'bottom'});
		$("[rel=tooltip-left]").tooltip({ placement: 'left'});
		$("[rel=tooltip-right]").tooltip({ placement: 'right'});
	});
	$(document).ready(function(){
		$("[rel=popover-top]").popover({ placement: 'top'});
		$("[rel=popover-bottom]").popover({ placement: 'bottom'});
		$("[rel=popover-left]").popover({ placement: 'left'});
		$("[rel=popover-right]").popover({ placement: 'right'});
	});
	</script>
	<script type="text/javascript" src="<?php echo $options['url'] ?>/lib/fancybox-2.1.5/jquery.fancybox.js?v=2.1.5"></script>
	<script type="text/javascript" src="<?php echo $options['url'] ?>/lib/persianDatepicker-0.1.0/js/persianDatepicker.min.js"></script>
	<script src="<?php echo $options['url'] ?>/lib/chosen-v1.3.0/chosen.jquery.min.js"></script>
    <script src="<?php echo $options['url'] ?>/lib/summernote/dist/summernote.js"></script>
</head>
<body>
	<div class="wrapper">
		<div class="header no-print">
			<div class="navbar fixed-bottom" style="margin-bottom:0px;">
				<div class="navbar-inner">
					<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</a>
					<?php if (login_check() == true) { ?>
					<div class="top-identifier">
						<a onMouseOver="load_popup_image('#profile_image','<?php if (if_file_exists("files/profile_images/".$profile_image)){ echo "files/profile_images/".$profile_image; } else{ echo "img/profile_image.png"; } ?>','bottom');" onMouseOut="dismiss_popup_image('#profile_image');" href="<?php echo $options["url"] ?>/profile.php">
							<img id="profile_image" width=35 height=35 src="<?php if (if_file_exists("files/profile_images/".$profile_image)){ echo "files/profile_images/".$profile_image; } else{ echo "img/profile_image.png"; } ?>" />
						</a>
						<a href="<?php echo $options["url"] ?>/panel.php"><h5 class="name normal"><?php if ($first_name." ".$last_name!=" "){ echo $first_name." ".$last_name; } else { echo $username; } ?></h5></a>
					</div>
					<?php } else {?>
					<div class="top-identifier">
						<a onMouseOver="load_popup_image('#profile_image','img/profile_image.png','bottom');" onMouseOut="dismiss_popup_image('#profile_image');" href="<?php echo $options["url"] ?>/login.php"><img id="profile_image" width=35 height=35 src="img/profile_image.png"></a>
						<a href="<?php echo $options["url"] ?>/login.php"><h5 class="name normal">کاربر مهمان</h5>
					</a>
					</div>
					<?php } ?>
					<div class="top-statistics">
						<a href="<?php echo $options["url"] ?>/statistics.php"><h5 class="title normal"><?php echo $online_visitors->count_users(); ?> نفر آنلاین</h5></a>
					</div>
					<div class="pull-left" id="loading" style="padding: 5px 10px;">
						<?php load_spinner($options["spinner"]); ?>
					</div>
					<img src="<?php echo $options['url'] ?>/img/icon.png" class="pull-right" style="margin-top:5px; width:32px;height:32px;" />
					<a class="brand" href="<?php echo $options['url'] ?>"><?php echo $options['title'] ?></a>
					<div class="nav nav-collapse pull-right">
						<li class="<?php if ($file_name=="home.php") { echo 'active'; } ?>"><a href="<?php echo $options['url'] ?>/home.php">خانه</a></li>
						<?php if (login_check($mysqli) == true) { ?>
                        <li class="<?php if ($file_name=="panel.php") { echo 'active'; } ?>"><a href="<?php echo $options['url'] ?>/panel.php">محیط کاربری</a>
                        <li class="<?php if ($file_name=="profile.php") { echo 'active'; } ?>"><a href="<?php echo $options['url'] ?>/profile.php">نمایه</a></li>
                        <?php } ?>
                        <?php if (login_check($mysqli) != true) { ?>
                        <li class="<?php if ($file_name=="register.php") { echo 'active'; } ?>"><a href="<?php echo $options['url'] ?>/register.php">ثبت‌نام</a></li>
                        <?php } ?>
                        <?php if (login_check($mysqli) != true) { ?>
                        <li class="<?php if ($file_name=="login.php") { echo 'active'; } ?>"><a href="<?php echo $options['url'] ?>/login.php">ورود</a></li>
                        <?php } ?>
                        <?php if (login_check($mysqli) == true) { ?>
                        <li><a href="<?php echo $options['url'] ?>/logout.php">خروج</a></li>
                        <?php } ?>
					</div>
				</div>
			</div>
		</div>