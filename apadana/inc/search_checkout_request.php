<?php
require_once 'db_connect.php';
require_once 'functions.php';
ob_start();
sec_session_start();

$redirect = $_POST['redirect'];

$user = $_POST['user'];
$price = $_POST['price'];
$date = $_POST['date'];
$time = $_POST['time'];
$status = $_POST['status'];


$statement = 'SELECT * FROM checkout_requests WHERE';

if (!empty($user)){ $statement .= ' user = "'.$user.'" AND'; }
if (!empty($price)){ $statement .= ' price = "'.$price .'" AND'; }
if (!empty($date)){ $statement .= ' date LIKE "%'.$date .'%" AND'; }
if (!empty($time)){ $statement .= ' time LIKE "%'.$time .'%" AND'; }
if (!empty($status)){ $statement .= ' status LIKE "%'.$status .'%" AND'; }

if (substr($statement,strlen($statement)-3,strlen($statement))=="AND"){
	$statement = substr($statement,0,strlen($statement)-3);
}

if (substr($statement,strlen($statement)-5,strlen($statement))=="WHERE"){
	$statement = substr($statement,0,strlen($statement)-5);
}


$result = $mysqli->query($statement);

$num_rows = $result->num_rows;

if ($num_rows > 0){
	header('Location: ../'.$redirect.'&suc=search&search=true&user='.$user.'&'.'price='.$price.'&'.'date='.$date.'&'.'time='.$time.'&'.'status='.$status);
	exit;
}  else{
	header('Location: ../'.$redirect.'&err=no-match');
	exit;
}

?>