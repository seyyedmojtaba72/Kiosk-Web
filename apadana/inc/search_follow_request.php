<?php
require_once 'db_connect.php';
require_once 'functions.php';
ob_start();
sec_session_start();

$redirect = $_POST['redirect'];

$follower = $_POST['follower'];
$followed = $_POST['followed'];
$status = $_POST['status'];
$date = $_POST['date'];
$time = $_POST['time'];

$statement = 'SELECT * FROM follow_requests WHERE';

if (!empty($follower)){ $statement .= ' follower = "'.$follower.'" AND'; }
if (!empty($followed)){ $statement .= ' followed = "'.$followed.'" AND'; }
if (!empty($status)){ $statement .= ' status LIKE "%'.$status .'%" AND'; }
if (!empty($date)){ $statement .= ' date LIKE "%'.$date .'%" AND'; }
if (!empty($time)){ $statement .= ' time LIKE "%'.$time .'%" AND'; }

if (substr($statement,strlen($statement)-3,strlen($statement))=="AND"){
	$statement = substr($statement,0,strlen($statement)-3);
}

if (substr($statement,strlen($statement)-5,strlen($statement))=="WHERE"){
	$statement = substr($statement,0,strlen($statement)-5);
}



$result = $mysqli->query($statement);

$num_rows = $result->num_rows;

if ($num_rows > 0){
	header('Location: ../'.$redirect.'&suc=search&search=true&follower='.$follower.'&'.'followed='.$followed.'&'.'status='.$status.'&'.'date='.$date.'&'.'time='.$time);
	exit;
}  else{
	header('Location: ../'.$redirect.'&err=no-match');
	exit;
}

?>