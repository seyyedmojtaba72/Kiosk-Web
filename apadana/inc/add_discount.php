<?php
require_once 'db_connect.php';
require_once 'functions.php';
require_once 'information.php';

$redirect = $_POST['redirect'];
$code = $_POST['code'];
$title = $_POST['title'];
$percentage = $_POST['percentage'];
$number = $_POST['number'];
$used = $_POST['used'];
$min_price = $_POST['min_price'];
$start_date = $_POST['start_date'];
$end_date = $_POST['end_date'];
$description = $_POST['description'];
$status = $_POST['status'];

if (ifallisset($code, $title, $percentage, $number, $used, $min_price, $start_date, $end_date)){
	
	$statement = 'SELECT * FROM discounts WHERE code="'.$code.'"';

	$result = $mysqli->query($statement);
	
	$num_rows = $result->num_rows;
	
	if ($num_rows > 0){
		header('Location: ../'.$redirect.'&err=exists');
		exit;
	}	

	if ($stmt = $mysqli->prepare("INSERT INTO discounts (code, title, percentage, number, used, min_price, start_date, end_date, description, status) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")){
	
		$stmt->bind_param('ssssssssss', $code, $title, $percentage, $number, $used, $min_price, $start_date, $end_date, $description, $status);
				
		if (! $stmt->execute()) {
			header('Location: ../'.$redirect.'&err=insert');
			exit;
		}

		
		header('Location: ../'.$redirect.'&suc=insert');
		exit;
	}
	else{
		header('Location: ../'.$redirect.'&err=fill');
		exit;
	}
	

}
else{
	header('Location: ../'.$redirect.'&err=fill');
	exit;
}
?>