<?php
require_once 'db_connect.php';
require_once 'functions.php';
require_once 'information.php';

$redirect = $_POST['redirect'];
$id = $_POST['id'];
$code = $_POST['code'];
$title = $_POST['title'];
$percentage = $_POST['percentage'];
$number = $_POST['number'];
$used = $_POST['used'];
$min_price = $_POST['min_price'];
$start_date = $_POST['start_date'];
$end_date = $_POST['end_date'];
$description = $_POST['description'];
$status = $_POST['status'];


if (ifallisset($code, $title, $percentage, $number, $used, $min_price, $start_date, $end_date)){	


	$stmt = $mysqli->prepare('SELECT code FROM discounts WHERE code="'.$code.'"');
	$stmt->execute();
	$stmt->store_result();
	$num_rows = $stmt->num_rows;
	$stmt->bind_result($new_code);
	$stmt->fetch();
	$stmt->close();
	
	if (($num_rows > 0) && ($code!=$new_code)){
		header('Location: ../'.$redirect.'&err=exists');
		exit;
	}

	
	if ($stmt = $mysqli->prepare("UPDATE discounts SET code=?, title=?, percentage=?, number=?, used=?, min_price=?, start_date=?, end_date=?, description=?, status=? WHERE id=?")){
		
		$stmt->bind_param('sssssssssss', $code, $title, $percentage, $number, $used, $min_price, $start_date, $end_date, $description, $status, $id);
		
				
		if (! $stmt->execute()) {
			header('Location: ../'.$redirect.'&err=edit');
			exit;
		}

		header('Location: ../'.$redirect.'&suc=edit');
		exit;
	}
} else{
	header('Location: ../'.$redirect.'&err=fill');
	exit;
}

?>