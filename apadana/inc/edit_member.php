<?php
require_once 'db_connect.php';
require_once 'functions.php';

$redirect = $_REQUEST['redirect'];
$former_email = $_REQUEST['former_email'];
$username = $_REQUEST['username'];
$email = $_REQUEST['email'];
$role = $_REQUEST['role'];
$level = $_REQUEST['level'];
$status = $_REQUEST['status'];
$first_name = $_REQUEST['first_name'];
$last_name = $_REQUEST['last_name'];
$mobile_number = $_REQUEST['mobile_number'];
$mobile_number_status = $_REQUEST['mobile_number_status'];
$home_number = $_REQUEST['home_number'];
$fax_number = $_REQUEST['fax_number'];
$office = $_REQUEST['office'];
$shaba = $_REQUEST['shaba'];
$address = $_REQUEST['address'];
$presenter = $_REQUEST['presenter'];
$reward_percentage = $_REQUEST['reward_percentage'];
$about = $_REQUEST['about'];
$extras[] = $_POST['extras'];

$extras_text = "";

foreach($extras[0] as $extra){
	$extras_text .= $extra.";";
}

if (ifallisset($username, $email, $role, $status)){
	
	$username = strtolower($username);
	if (!preg_match('/^[a-z0-9_.-]{1,50}$/', $username) || !preg_match('/\w/', $username)){
		header('Location: ../'.$redirect.'&err=invalid-username');
		exit;
	}
	
	$stmt = $mysqli->prepare('SELECT email FROM members WHERE username="'.$username.'"');
	$stmt->execute();
	$stmt->bind_result($new_email);
	$stmt->store_result();
	$num_rows = $stmt->num_rows;
	$stmt->fetch();
	$stmt->close();
	
	if (($num_rows > 0) && ($former_email!=$new_email)){
		header('Location: '.$redirect.'&err=username-exists');
		exit;
	}
	
	
	if (!empty($mobile_number)){
		if (strlen($mobile_number)!=10){
			header('Location: ../'.$redirect.'&err=mobile_number-length');
			exit;
		}
		
		$stmt = $mysqli->prepare('SELECT id FROM members WHERE mobile_number="'.$mobile_number.'"');
		$stmt->execute();
		$stmt->bind_result($id2);
		$stmt->store_result();
		$num_rows = $stmt->num_rows;
		$stmt->fetch();
		$stmt->close();
	
		if (($num_rows > 0) &&  $id2!=$id){
			header('Location: ../'.$redirect.'&err=mobile_number-exists');
			exit;
		}
		
	}
	
	
	$stmt = $mysqli->prepare('SELECT email FROM members WHERE email="'.$email.'"');
	$stmt->execute();
	$stmt->bind_result($new_email);
	$stmt->store_result();
	$num_rows = $stmt->num_rows;
	$stmt->fetch();
	$stmt->close();
	
	
	
	if (($num_rows > 0) && ($former_email!=$new_email)){
		header('Location: '.$redirect.'&err=email-exists');
		exit;
	}
	
	
	
	if ($stmt = $mysqli->prepare("UPDATE members SET username=?, email=?, role=?, level=?, status=?, first_name=?, last_name=?, mobile_number=?, mobile_number_status=?, home_number=?, fax_number=?, office=?, shaba=?, address=?, presenter=?, reward_percentage=?, about=?, extras=?, timestamp=? WHERE email=?")){
	
		$stmt->bind_param('ssssssssssssssssssss', $username, $email, $role, $level, $status, $first_name, $last_name, $mobile_number, $mobile_number_status, $home_number, $fax_number, $office, $shaba, $address, $presenter, $reward_percentage, $about, $extras_text, time(), $former_email);
		
		
		$result = mysql_select('members', array('email'=>$email), array('status', 'mobile_number_status'));
		if ($result[0]["status"]!= $status && $status == "active"){
			// E-MAIL
					
			$to = $email;
			$subject = "خوش‌آمدید";
			
			$html_body = str_replace("%MOBILE_NUMBER%", $mobile_number, str_replace("%PASSWORD%", $hash, str_replace("%EMAIL%", $email, str_replace("%USERNAME%", $username, $options['welcome_email_text']))));
		
			
			send_email($to, $subject, $html_body, "", "");
	
		}
		
		$name = $first_name." ".$last_name;
		if ($name == " "){
			$name = $username;
		}
		
		if ($result[0]["mobile_number_status"]!= $status && $mobile_number_status == "1"){

			// SMS
				
			$to = $mobile_number;		
			$sms_body = str_replace("%NAME%", $name, str_replace("%USERNAME%", $username, $options['welcome_sms_text']));
			send_sms($to, $sms_body);
		}

		
				
		if (! $stmt->execute()) {
			header('Location: ../'.$redirect.'&err=edit');
			exit;
		}
		
		
		header('Location: ../'.$redirect.'&suc=edit');
		exit;
	}
} else{
	header('Location: ../'.$redirect.'&err=fill');
	exit;
}

?>