<?php
require_once 'db_connect.php';
require_once 'functions.php';
ob_start();
sec_session_start();

$redirect = $_POST['redirect'];

$customer = $_POST['customer'];
$post = $_POST['post'];
$date = $_POST['date'];
$time = $_POST['time'];

$statement = 'SELECT * FROM purchase_history WHERE';

if (!empty($customer)){ $statement .= ' customer = "'.$customer.'" AND'; }
if (!empty($post)){ $statement .= ' post = "'.$post.'" AND'; }
if (!empty($date)){ $statement .= ' date LIKE "%'.$date .'%" AND'; }
if (!empty($time)){ $statement .= ' time LIKE "%'.$time .'%" AND'; }

if (substr($statement,strlen($statement)-3,strlen($statement))=="AND"){
	$statement = substr($statement,0,strlen($statement)-3);
}

if (substr($statement,strlen($statement)-5,strlen($statement))=="WHERE"){
	$statement = substr($statement,0,strlen($statement)-5);
}


$result = $mysqli->query($statement);

$num_rows = $result->num_rows;

if ($num_rows > 0){
	header('Location: ../'.$redirect.'&suc=search&search=true&customer='.$customer.'&'.'post='.$post.'&'.'date='.$date.'&'.'time='.$time);
	exit;
}  else{
	header('Location: ../'.$redirect.'&err=no-match');
	exit;
}

?>