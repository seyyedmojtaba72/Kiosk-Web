<?php
include_once 'db_connect.php';
include_once 'functions.php';

$redirect = $_POST['redirect'];
$former_slug = $_REQUEST['former_slug'];
$slug = $_REQUEST['slug'];
$role_title = $_REQUEST['role_title'];
$permissions = $_REQUEST['permissions'];
	

if (ifallisset($slug, $role_title, $permissions)){
	
	$stmt = $mysqli->prepare('SELECT slug FROM members_roles WHERE slug="'.$slug.'"');
	$stmt->execute();
	$stmt->store_result();
	$num_rows = $stmt->num_rows;
	$stmt->bind_result($new_slug);
	$stmt->fetch();
	$stmt->close();
	
	if (($num_rows > 0) && ($former_slug!=$new_slug)){
		header('Location: '.$redirect.'&err=exists');
		exit;
	}
	
	
	
	if ($stmt = $mysqli->prepare("UPDATE members_roles SET slug=?, role_title=?, permissions=? WHERE slug=?")){
	
		$stmt->bind_param('ssss', $slug, $role_title, $permissions, $former_slug);
				
		if (! $stmt->execute()) {
			header('Location: ../'.$redirect.'&err=edit');
			exit;
		}
		
		header('Location: ../'.$redirect.'&suc=edit');
		exit;
	}
} else{
	header('Location: ../'.$redirect.'&err=fill');
	exit;
}

?>