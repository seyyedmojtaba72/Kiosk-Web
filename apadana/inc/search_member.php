<?php
require_once 'db_connect.php';
require_once 'functions.php';
ob_start();
sec_session_start();

$redirect = $_POST['redirect'];
$id = $_REQUEST['id'];
$username = $_REQUEST['username'];
$email = $_REQUEST['email'];
$first_name = $_REQUEST['first_name'];
$last_name = $_REQUEST['last_name'];
$mobile_number = $_REQUEST['mobile_number'];
$role = $_REQUEST['role'];
$status = $_REQUEST['status'];
$balance = $_REQUEST['balance'];

$statement = 'SELECT * FROM members WHERE';

if (!empty($id)){ $statement .= ' id ="'.$id .'" AND'; }
if (!empty($username)){ $statement .= ' username LIKE "%'.$username .'%" AND'; }
if (!empty($email)){ $statement .= ' email LIKE "%'.$email.'%" AND'; }
if (!empty($first_name)){ $statement .= ' first_name LIKE "%'.$first_name.'%" AND'; }
if (!empty($last_name)){ $statement .= ' last_name LIKE "%'.$last_name.'%" AND'; }
if (!empty($mobile_number)){ $statement .= ' mobile_number LIKE "%'.$mobile_number.'%" AND'; }
if (!empty($role)){ $statement .= ' role LIKE "%'.$role.'%" AND'; }
if (!empty($status)){ $statement .= ' status LIKE "%'.$status.'%" AND'; }
if (!empty($balance)){ $statement .= ' balance ="'.$balance.'" AND'; }


if (substr($statement,strlen($statement)-3,strlen($statement))=="AND"){
	$statement = substr($statement,0,strlen($statement)-3);
}

if (substr($statement,strlen($statement)-5,strlen($statement))=="WHERE"){
	$statement = substr($statement,0,strlen($statement)-5);
}


$result = $mysqli->query($statement);

$num_rows = $result->num_rows;

if ($num_rows > 0){
	header('Location: ../'.$redirect.'&suc=search&search=true&id='.$id.'&username='.$username.'&email='.$email.'&first_name='.$first_name.'&last_name='.$last_name.'&mobile_number='.$mobile_number.'&role='.$role.'&status='.$status.'&balance='.$balance);
	exit;
}  else{
	header('Location: ../'.$redirect.'&err=no-match');
	exit;
}



?>