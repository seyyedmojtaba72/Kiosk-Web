<?php
require_once 'db_connect.php';
require_once 'functions.php';

$username= filter_input(INPUT_GET, 'username', $filter = FILTER_SANITIZE_STRING);
$password= filter_input(INPUT_GET, 'password', $filter = FILTER_SANITIZE_STRING);

$result = mysql_select("members", array('username'=>$username), array('email', 'password', 'status', 'mobile_number'));
$email = $result[0]['email'];
$db_password = $result[0]['password'];
$status = $result[0]['status'];
$mobile_number = $result[0]['mobile_number'];


if ($status != "inactive"){		
	header('Location: ../login.php?suc=already-active');
	exit;
} else if ($password == $db_password){
	
	$status = "active";
	$insert_stmt = $mysqli->prepare("UPDATE members SET status=? WHERE username=?");
	$insert_stmt->bind_param('ss',$status ,$username);
	$insert_stmt->execute();	
	
	
	// E-MAIL
	
	$subject = "خوش‌آمدید";
	
	$html_body = str_replace("%MOBILE_NUMBER%", $mobile_number, str_replace("%EMAIL%", $email, str_replace("%USERNAME%", $username, $options['welcome_email_text'])));

	
	send_email($email, $subject, $html_body, "", "normal");
	
	header('Location: ../login.php?suc=active');
	exit;

} else {
	header('Location: ../login.php?err=invalid-activation');
	exit;
	
}
		

				
?>