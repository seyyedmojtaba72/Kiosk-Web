<?php
require_once 'db_connect.php';
require_once 'functions.php';
require_once 'information.php';

$redirect = $_POST['redirect'];
$id = $_POST['id'];
$receiver = $_POST['receiver'];
$subject = $_POST['subject'];
$email_body = htmlspecialchars($_POST['email_body']);
$date = $_POST['date'];
$time = $_POST['time'];
$timestamp = $_POST['timestamp'];

if (ifallisset($receiver, $subject, $email_body, $date, $time)){
	
	if ($stmt = $mysqli->prepare("UPDATE emails SET receiver=?, subject=?, email_body=?, date=?, time=?, timestamp=? WHERE id=?")){
		
		$stmt->bind_param('sssssss', $receiver, $subject, $email_body, $date, $time, $timestamp, $id);
		
				
		if (! $stmt->execute()) {
			header('Location: ../'.$redirect.'&err=edit');
			exit;
		}

		header('Location: ../'.$redirect.'&suc=edit');
		exit;
	}
} else{
	header('Location: ../'.$redirect.'&err=fill');
	exit;
}

?>