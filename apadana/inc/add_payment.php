<?php
require_once 'db_connect.php';
require_once 'functions.php';
require_once 'information.php';

$redirect = $_POST['redirect'];
$customer = $_POST['customer'];
$transaction_id = $_POST['transaction_id'];
$amount = $_POST['amount'];
$action = $_POST['action'];
$date = $_POST['date'];
$time = $_POST['time'];
$status = $_POST['status'];
$timestamp = $_POST['timestamp'];

if (ifallisset($customer, $transaction_id, $amount, $action, $date, $time, $status)){	

	if ($stmt = $mysqli->prepare("INSERT INTO payments (customer, transaction_id, amount, action, date, time, status, timestamp) VALUES (?, ?, ?, ?, ?, ?, ?, ?)")){
	
		$stmt->bind_param('ssssssss', $customer, $transaction_id, $amount, $action, $date, $time, $status, $timestamp);
				
		if (! $stmt->execute()) {
			header('Location: ../'.$redirect.'&err=insert');
			exit;
		}

		
		header('Location: ../'.$redirect.'&suc=insert');
		exit;
	}
	else{
		header('Location: ../'.$redirect.'&err=fill');
		exit;
	}
	

}
else{
	header('Location: ../'.$redirect.'&err=fill');
	exit;
}
?>