<?php
require_once 'db_connect.php';
require_once 'functions.php';
ob_start();
sec_session_start();

$redirect = $_POST['redirect'];

$author = $_REQUEST['author'];
$post = $_REQUEST['post'];
$comment = $_REQUEST['comment'];
$date = $_REQUEST['date'];
$time = $_REQUEST['time'];
$status = $_REQUEST['status'];

$statement = 'SELECT * FROM posts_comments WHERE';
				
if (!empty($author)){ $statement .= ' author = "'.$author .'" AND'; }
if (!empty($post)){ $statement .= ' post = "'.$post .'" AND'; }
if (!empty($comment)){ $statement .= ' comment LIKE "%'.$comment .'%" AND'; }
if (!empty($date)){ $statement .= ' date LIKE "%'.$date .'%" AND'; }
if (!empty($time)){ $statement .= ' time LIKE "%'.$time .'%" AND'; }
if (!empty($status)){ $statement .= ' status="'.$status .'" AND'; }

if (substr($statement,strlen($statement)-3,strlen($statement))=="AND"){
	$statement = substr($statement,0,strlen($statement)-3);
}

if (substr($statement,strlen($statement)-5,strlen($statement))=="WHERE"){
	$statement = substr($statement,0,strlen($statement)-5);
}

$result = $mysqli->query($statement);

$num_rows = $result->num_rows;

if ($num_rows > 0){
	header('Location: ../'.$redirect.'&suc=search&search=true&author='.$author.'&post='.$post.'&comment='.$comment.'&date='.$date.'&time='.$time.'&status='.$status);
	exit;
}  else{
	header('Location: ../'.$redirect.'&err=no-match');
	exit;
}

?>