<?php
require_once 'db_connect.php';
require_once 'functions.php';

$redirect = $_REQUEST['redirect'];
$id = $_REQUEST['id'];
$model = $_REQUEST['model'];
$password = $_REQUEST['password'];

if ($model=="student"){
	$idd="student_id";
	$table="students";
	}
elseif($model=="teacher"){
	$idd="personal_id";
	$table="teachers";
	}
elseif($model=="manager"){
	$idd="personal_id";
	$table="managers";
	}
	
if (ifallisset($password)){
	$statement = 'UPDATE '.$table.' SET password="'.$password.'" WHERE '.$idd.'="'.$id.'"';
	
	if ($stmt = $mysqli->prepare($statement)){;
				
		// Execute the prepared query.
		if (! $stmt->execute()) {
			header('Location: ../'.$redirect.'err=password-edit');
			exit;
		}
		
		header('Location: ../'.$redirect.'suc=password-edit');
		exit;
	}
} else{
	header('Location: ../'.$redirect.'err=fill');
	exit;
}

?>