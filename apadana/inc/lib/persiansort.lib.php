<?php

/*
 * Copyright 2004 Ehsan Akhgari <me@ehsanakhgari.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 An example usage for the sorting function:
 
	function db_query($query) // $query is the SQL query to execute
	{
		global $db;
		$db->setFetchMode( DB_FETCHMODE_ASSOC );
		$res =& $db->query( $query );
		$arr = array();
		if (!DB::isError( $res ))
		{
			$length = $res->numRows();
			for ($i = 0; $i < $length; ++ $i)
			{
				$arr[] =& $res->fetchRow();
			}
			$res->free();
		}
		if (count( $arr ))
		{
			mysql_persian_sort(  // the sorting function
				$arr,            // the array to be sorted
				'sort_key_field' // this should be set
				                 // to the name of the
				                 // field which is the
				                 // sort key
			);
		}
		return $arr;
	}
*/

function mysql_persian_sort($arr, $key)
{
	global $_db_sort_index;
	$_db_sort_index = $key;
	usort( $arr, '_db_sort_compare' );
	return $arr;
}

function _db_generate_sort_array()
{
	global $sort_array;
	
	$sort_order = //'آاأإؤئبپتثجچحخدذرزژسشصضطظعغفقکگلمنوهی۱۲۳۴۵۶۷۸۹۰';
		array( 'آ', 'ا', 'أ', 'إ', 'ؤ', 'ئ', 'ب', 'پ', 'ت', 'ث', 'ج', 'چ', 'ح', 'خ', 'د',
			'ذ', 'ر', 'ز', 'ژ', 'س', 'ش', 'ص', 'ض', 'ط', 'ظ', 'ع', 'غ', 'ف', 'ق', 'ک', 'گ',
			'ل', 'م', 'ن', 'و', 'ه', 'ی', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹', '۰' );
	$sort_array = array();
	$index = 1;
	foreach ($sort_order as $item)
	{
		$sort_array[ $item ] = $index ++;
	}
}

_db_generate_sort_array();

function _db_sort_compare($elem1, $elem2)
{
	global $_db_sort_index;
	
	return _db_sort_compare_internal( @$elem1[ $_db_sort_index ], @$elem2[ $_db_sort_index ] );
}

function _db_sort_compare_internal($elem1, $elem2)
{
	// if one of the items don't have the sort index we're looking for
	// assume NULL > everything
	if (is_null( $elem1 ) && is_null( $elem2 ))
	{
		return 0; // assume they are equal
	}
	if (is_null( $elem1 ))
	{
		return 1;
	}
	else if (is_null( $elem2 ))
	{
		return -1;
	}
	
	// check to see if both sort index values are numbers, in which case, perform a natsort
	if (strlen( $elem1 ) && strlen( $elem2 ) &&
		ctype_digit( $elem1 ) && ctype_digit( $elem2 ))
	{
		return strnatcmp( $elem1, $elem2 );
	}
	
	// try to compare Persian strings
	global $sort_array;
	$len1 = strlen( $elem1 );
	$len2 = strlen( $elem2 );
	$failed = false;
	for ($i = 0, $j = 0; $i < $len1 && $j < $len2;)
	{
		$c1 = _db_extract_utf8_char( $elem1, $i );
		$c2 = _db_extract_utf8_char( $elem2, $j );
		
		if (array_key_exists( $c1, $sort_array ) &&
			array_key_exists( $c2, $sort_array ))
		{
			if ($sort_array[ $c1 ] == $sort_array[ $c2 ])
			{
				continue;
			}
			else
			{
				return $sort_array[ $c1 ] - $sort_array[ $c2 ];
			}
		}
		else
		{
			$failed = true;
			break;
		}
	}
	
	if ($failed == false)
	{
		return 0; // equal strings
	}
	
	return strcmp( $elem1, $elem2 );
}

function _db_extract_utf8_char($str, &$i)
{
	$ascii_pos = ord( $str{ $i } );
	if ($ascii_pos < 192)
	{
		return $str{ $i ++ };
	}
	else if (($ascii_pos >= 240) && ($ascii_pos <= 255))
	{
		$i += 4;
		return substr( $str, $i - 4, $i );
	}
	else if (($ascii_pos >= 224) && ($ascii_pos <= 239))
	{
		$i += 3;
		return substr( $str, $i - 3, $i );
	}
	else
	{
		$i += 2;
		return substr( $str, $i - 2, $i );
	}
}

?>