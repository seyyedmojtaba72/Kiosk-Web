<?php
include_once 'db_connect.php';
$redirect = $_REQUEST['redirect'];
$slug = $_REQUEST['slug'];

if ($stmt = $mysqli->prepare('DELETE FROM members_roles WHERE slug=?')){

	$stmt->bind_param('s', $slug);
			
	if (! $stmt->execute()) {
		header('Location: ../'.$redirect.'&err=delete');
		exit;
	}
	
	header('Location: ../'.$redirect.'&suc=delete');
	exit;
}

?>