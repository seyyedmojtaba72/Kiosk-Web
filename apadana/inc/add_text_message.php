<?php
require_once 'db_connect.php';
require_once 'functions.php';
require_once 'information.php';

$redirect = $_POST['redirect'];
$text = $_POST['text'];
$date = $_POST['date'];
$time = $_POST['time'];
$timestamp = $_POST['timestamp'];

if (ifallisset($receiver, $text, $date, $time)){	

	if ($stmt = $mysqli->prepare("INSERT INTO text_messages (text, date, time, timestamp) VALUES (?, ?, ?, ?)")){
	
		$stmt->bind_param('ssss', $text, $date, $time, $timestamp);
				
		if (! $stmt->execute()) {
			header('Location: ../'.$redirect.'&err=insert');
			exit;
		}

		
		header('Location: ../'.$redirect.'&suc=insert');
		exit;
	}
	else{
		header('Location: ../'.$redirect.'&err=fill');
		exit;
	}
	

}
else{
	header('Location: ../'.$redirect.'&err=fill');
	exit;
}
?>