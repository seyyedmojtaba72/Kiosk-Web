<?php
require_once 'db_connect.php';
$redirect = $_REQUEST['redirect'];
$id = $_REQUEST['id'];

if ($stmt = $mysqli->prepare("DELETE FROM posts WHERE id=?")){

	$stmt->bind_param('s', $id);
			
	if (! $stmt->execute()) {
		
		header('Location: ../'.$redirect.'&err=delete');
		exit;
	}
	
	
	header('Location: ../'.$redirect.'&suc=delete');
	exit;
}

?>