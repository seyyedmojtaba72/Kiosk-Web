<?php 
require_once 'config.php';
require_once 'db_connect.php';
require_once 'information.php';
require_once 'functions.php';

$username = filter_input(INPUT_GET, 'username', $filter = FILTER_SANITIZE_STRING);
$password = filter_input(INPUT_GET, 'password', $filter = FILTER_SANITIZE_STRING);
$redirect = filter_input(INPUT_GET, 'redirect', $filter = FILTER_SANITIZE_STRING);

if (empty($redirect)){
	$redirect = "login.php?";
}

if (empty($username) | empty($password)){
	header('Location: ../index.php');
	exit;
}

if ($stmt = $mysqli->prepare("SELECT email, mobile_number FROM members WHERE username = ? LIMIT 1")) {
	$stmt->bind_param('s', $username);
	$stmt->execute(); 
	$stmt->store_result();

	$stmt->bind_result($email, $mobile_number);
	$stmt->fetch();
}


// E-MAIL
			
$to = $email;
$subject = "فعال‌سازی حساب";

$html_body = str_replace("%ACTIVATION_LINK_URL%", $options['url'].'/inc/activate.php?username='.$username.'&password='.$password, str_replace("%MOBILE_NUMBER%", $mobile_number, str_replace("%EMAIL%", $email, str_replace("%USERNAME%", $username, $options['registeration_email_text']))));

send_email($to, $subject, $html_body, "", "normal");

header('Location: ../'.$redirect.'&suc=check-email');
exit;
?>