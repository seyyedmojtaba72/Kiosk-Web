<?php
require_once __DIR__.'/db_connect.php';
require_once __DIR__.'/information.php';
require_once __DIR__.'/online_payment.php';

function sec_session_start() {
    $session_name = 'sec_session_id';   // Set a custom session name
    $secure = SECURE_AGENT;
    // This stops JavaScript being able to access the session id.
    $httponly = true;
    // Forces sessions to only use cookies.
    if (ini_set('session.use_only_cookies', 1) === FALSE) {
        header("Location: ../login.php?err=safe");
        exit();
    }
    // Gets current cookies params.
    $cookieParams = session_get_cookie_params();
    session_set_cookie_params($cookieParams["lifetime"],
        $cookieParams["path"], 
        $cookieParams["domain"], 
        $secure,
        $httponly);
    // Sets the session name to the one set above.
    session_name($session_name);
    session_start();
    session_regenerate_id();
}

function login($username, $password) {
	$username = strtolower($username);
	
	global $mysqli;
	$_SESSION = array();
    if ($stmt = $mysqli->prepare("SELECT id, email, password, salt, role, status, profile_image, first_name, last_name, mobile_number, mobile_number_status, home_number, fax_number, office, shaba, address, balance, about, extras FROM members WHERE username = ? LIMIT 1")) {
        $stmt->bind_param('s', $username); 
        $stmt->execute(); 
        $stmt->store_result();

        $stmt->bind_result($id, $email, $db_password, $salt, $role, $status, $profile_image, $first_name, $last_name, $mobile_number, $mobile_number_status, $home_number, $fax_number, $office, $shaba, $address, $balance, $about, $extras);
        $stmt->fetch();
 		
        $password = crypt($password,$salt);
		
        if ($stmt->num_rows == 1) {
 
            if (checkbrute($id) == true) {
                return false;
            } else {
				
                if ($db_password == $password) {
					
					if  ($status == "inactive"){
						return "inactive";
					}

                    $user_browser = $_SERVER['HTTP_USER_AGENT'];
                    $_SESSION['id'] = preg_replace("/[^0-9]+/", "", $id);
                    $username = preg_replace("/[^a-zA-Z0-9_\-]+/","",$username);
                    $_SESSION['username'] = $username;
					$_SESSION['email'] = $email;
					$_SESSION['password'] = $password;
					$_SESSION['salt'] = $salt;
					$_SESSION['role'] = $role;
					$_SESSION['status'] = $status;
					$_SESSION['profile_image'] = $profile_image;
					$_SESSION['first_name'] = $first_name;
					$_SESSION['last_name'] = $last_name;
					$_SESSION['mobile_number'] = $mobile_number;
					$_SESSION['mobile_number_status'] = $mobile_number_status;
					$_SESSION['home_number'] = $home_number;
					$_SESSION['fax_number'] = $fax_number;
					$_SESSION['office'] = $office;
					$_SESSION['shaba'] = $shaba;
					$_SESSION['address'] = $address;
					$_SESSION['balance'] = $balance;
					$_SESSION['about'] = $about;
					$_SESSION['extras'] = $extras;
					
                    $_SESSION['login_string'] = hash('sha512', $password . $user_browser);

					
					
                    return "active";
                } else {
					
                    $now = time();
                    $mysqli->query("INSERT INTO login_attempts(member_id, time)
                                    VALUES ('$id', '$now')");
                    return $password;
                }
				
            }
        } else {
            // No user exists.
            return false;
        }
    }
}


function checkbrute($member_id) {
	global $mysqli;
    // Get timestamp of current time 
    $now = time();
 
    // All login attempts are counted from the past 2 hours. 
    $valid_attempts = $now - (2 * 60 * 60);
 
    if ($stmt = $mysqli->prepare("SELECT time 
                             FROM login_attempts <code><pre>
                             WHERE member_id = ? 
                            AND time > '$valid_attempts'")) {
        $stmt->bind_param('i', $member_id);
 
        // Execute the prepared query. 
        $stmt->execute();
        $stmt->store_result();
 
        // If there have been more than 5 failed logins 
        if ($stmt->num_rows > 5) {
            return true;
        } else {
            return false;
        }
    }
}



function login_check() {
	global $mysqli, $_SESSION;
    // Check if all session variables are set
    if (isset($_SESSION['id'], $_SESSION['username'], $_SESSION['login_string'])) {

        $member_id = $_SESSION['id'];
        $login_string = $_SESSION['login_string'];
        $username = $_SESSION['username'];
 
        $user_browser = $_SERVER['HTTP_USER_AGENT'];
 
        if ($stmt = $mysqli->prepare("SELECT password FROM members WHERE id = ? LIMIT 1")) {
            $stmt->bind_param('i', $member_id);
            $stmt->execute();
            $stmt->store_result();
 
            if ($stmt->num_rows == 1) {
                $stmt->bind_result($password);
                $stmt->fetch();
                $login_check = hash('sha512', $password . $user_browser);
 
                if ($login_check == $login_string) {
                    // Logged In!!!! 
                    return true;
                } else {
                    // Not logged in 
                    return false;
                }
            } else {
                // Not logged in 
                return false;
            }
        } else {
            // Not logged in 
            return false;
        }
    } else {
        // Not logged in 
        return false;
    }
}




function esc_url($url) {
 
    if ('' == $url) {
        return $url;
    }
 
    $options["url"] = preg_replace('|[^a-z0-9-~+_.?#=!&;,/:%@$\|*\'()\\x80-\\xff]|i', '', $url);
 
    $strip = array('%0d', '%0a', '%0D', '%0A');
    $url = (string) $url;
 
    $count = 1;
    while ($count) {
        $url = str_replace($strip, '', $url, $count);
    }
 
    $url = str_replace(';//', '://', $url);
 
    $url = htmlentities($url);
 
    $url = str_replace('&amp;', '&#038;', $url);
    $url = str_replace("'", '&#039;', $url);
 
    if ($url !== '/') {
        // We're only interested in relative links from $_SERVER['PHP_SELF']
        return '';
    } else {
        return $url;
    }
}



function randomPassword($length) {
    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < $length; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}


function randomNumberPassword($length) {
    $alphabet = "0123456789";
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < $length; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}




function ifallisset(){
	
	for($i = 0 ; $i < func_num_args(); $i++) {
		$a = func_get_arg($i);
		if (!isset($a)){
			return false;
		} else {
			if (empty($a)){
				if (strlen($a)<=0){
					return false;
				}
			}
		}
	}
	return true;
}


function ifoneisset(){
	
	for($i = 0 ; $i < func_num_args(); $i++) {
		$a = func_get_arg($i);
		if (!empty($a)){
			return true;
		}
	}
	return false;
}

function if_has_permission($role, $permission){
	if ($role=="admin"){
		return true;
	}
	
	global $mysqli;
	$stmt = $mysqli->prepare("SELECT permissions FROM members_roles WHERE slug = ?");
	$stmt->bind_param('s', $role); 
	$stmt->execute(); 
	$stmt->store_result();
	$stmt->bind_result($permissions);
	$stmt->fetch();
	$stmt->close();
	
	if (stripos($permissions, $permission.";")!==false){
		return true;
	} else{
		return false;
	}
}

function if_file_exists($url){
	if (substr($url,strlen($url)-1,strlen($url))=="/"){
		return false;
	} else {
		return file_exists($url);
	}
}

function get_file_size($file, $type = "B", $round = 2){
	if ($file==""){
		return "0";
	}
	$ch = curl_init($file);
	curl_setopt($ch, CURLOPT_NOBODY, true);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HEADER, true);
	//curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
	$data = curl_exec($ch);
	curl_close($ch);
	if ($data === false) {
	  return "0";
	}
	
	$filesize = '0';
	$status = 'unknown';
	if (preg_match('/^HTTP\/1\.[01] (\d\d\d)/', $data, $matches)) {
	  $status = (int)$matches[1];
	}
	if (preg_match('/Content-Length: (\d+)/', $data, $matches)) {
	  $filesize = (int)$matches[1];
	}
	
	switch($type){
		case "B":
			$filesize =  $filesize;
			break;
		case "KB":
			$filesize =  $filesize * .0009765625; // bytes to KB
			break;
		case "MB":
			$filesize =  $filesize * (.0009765625) * .0009765625; // bytes to MB
			break;
		case "GB":
			$filesize =  $filesize * (.0009765625) * (.0009765625) * .0009765625; // bytes to GB
			break;
		default:
			$filesize =  $filesize;
			break;
	}
	
	if($filesize <= 0){
		return $filesize = '0';
	}
	
	else{
	   return round($filesize, $round);
	}
}

function get_file_last_modified($url){
    $ch = curl_init($url);

	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($ch, CURLOPT_HEADER, TRUE);
	curl_setopt($ch, CURLOPT_NOBODY, TRUE);
	curl_setopt($ch, CURLOPT_FILETIME, TRUE);
	
	$data = curl_exec($ch);
	$filetime = curl_getinfo($ch, CURLINFO_FILETIME);
	
	curl_close($ch);
	return $filetime;
}

function fix_address($address){
	$array = explode('&',$address);
	$new_address = $array[0];
	for ($i=1; $i<sizeof($array); $i++){
		$cell = $array[$i];
		if ($cell != ""){
			$new_address .= "&".$cell;
		}
	}
	return $new_address;
}


function load_spinner($spinner){
	global $options;
	$css = '<link href="'.$options['url'].'/css/main/spinners/'.$spinner.'.css" rel="stylesheet" type="text/css" />';
	$html = "";
	$js = "";
	
	switch($spinner){
		case 1:{
			$html = '
			<div class="spinner" style="margin: 0px; width: 30px; height: 30px;">
			  <div class="bounce1"></div>
			  <div class="bounce2"></div>
			  <div class="bounce3"></div>
			</div>';
			break;
		}
		case 2:{
			$html = '
			<div class="spinner" style="margin: 0px; width: 30px; height: 30px;">
			  <div class="double-bounce1"></div>
			  <div class="double-bounce2"></div>
			</div>';
			break;
		}
		case 3:{
			$html = '
			<div class="spinner" style="margin: 0px; width: 30px; height: 30px;">
			  <div class="rect2"></div>
			  <div class="rect3"></div>
			  <div class="rect4"></div>
			</div>';
			break;
		}
		case 4:{
			$html = '
			<div class="spinner" style="margin: 0px; width: 30px; height: 30px;">
			  <div class="cube1"></div>
			  <div class="cube2"></div>
			</div>';
			break;
		}case 5:{
			$html = '
			<div class="spinner" style="margin: 0px; width: 30px; height: 30px;">
			</div>';
			break;
		}
		case 6:{
			$html = '
			<div class="spinner" style="margin: 0px; width: 30px; height: 30px;">
			  <div class="dot1"></div>
			  <div class="dot2"></div>
			</div>';
			break;
		}case 7:{
			$html = '
			<div class="spinner" style="margin: 7px 0 0 0; height: 10px;">
			  <div class="bounce1"></div>
			  <div class="bounce2"></div>
			  <div class="bounce3"></div>
			</div>';
			break;
		}
		case 8:{
			$html = '
			<div class="spinner" style="margin: 3px 0 0 0; width: 25px; height: 25px;">
			  <div class="spinner-container container1">
				<div class="circle1"></div>
				<div class="circle2"></div>
				<div class="circle3"></div>
				<div class="circle4"></div>
			  </div>
			  <div class="spinner-container container2">
				<div class="circle1"></div>
				<div class="circle2"></div>
				<div class="circle3"></div>
				<div class="circle4"></div>
			  </div>
			  <div class="spinner-container container3">
				<div class="circle1"></div>
				<div class="circle2"></div>
				<div class="circle3"></div>
				<div class="circle4"></div>
			  </div>
			</div>';
			break;
		}
		case 9:{
			$html = '
			<div class="spinner" style="margin: 0px; width: 30px; height: 30px;">
			  <div class="dot four"></div>
			  <div class="dot three"></div>
			  <div class="dot two"></div>
			  <div class="dot one"></div>
			</div>';
			break;
		}
		case 10:{
			$html = '
			<div class="spinner" style="margin: 0px; width: 30px; height: 30px;">
				<img src="'.$options['url'].'/css/main/spinners/'.$spinner.'.svg" width="30" alt="">
			</div>';
			break;
		}
		case 11:{
			$html = '
			<div class="spinner" style="margin: 0px; width: 30px; height: 30px;">
				<img src="'.$options['url'].'/css/main/spinners/'.$spinner.'.svg" width="30" alt="">
			</div>';
			break;
		}
		case 12:{
			$html = '
			<div class="spinner" style="margin: 0px; width: 30px; height: 30px;">
				<img src="'.$options['url'].'/css/main/spinners/'.$spinner.'.svg" width="30" alt="">
			</div>';
			break;
		}
		case 13:{
			$html = '
			<div class="spinner" style="margin: 0px; width: 30px; height: 30px;">
			</div>';
			break;
		}
		case 14:{
			$html = '
			<div class="spinner" style="margin: 0px; width: 30px; height: 30px;">
				<div class="loading">
					<div class="l1">
					  <div></div>
					</div>
					<div class="l2">
					  <div></div>
					</div>
					<div class="l3">
					  <div></div>
					</div>
					<div class="l4">
					  <div></div>
					</div>
				</div>
			</div>';
			break;
		}
		case 15:{
			$html = '
			<div class="spinner" style="margin: 0px; width: 30px; height: 30px;">
				<img src="'.$options['url'].'/css/main/spinners/'.$spinner.'.svg" width="30" alt="">
			</div>';
			break;
		}
		case 16:{
			$html = '
			<div class="spinner" style="margin: 0px; width: 30px; height: 30px;">
				<img src="'.$options['url'].'/css/main/spinners/'.$spinner.'.svg" width="30" alt="">
			</div>';
			break;
		}
		case 17:{
			$html = '
			<div class="spinner" style="margin: 0px; width: 30px; height: 30px;">
				<img src="'.$options['url'].'/css/main/spinners/'.$spinner.'.svg" width="30" alt="">
			</div>';
			break;
		}
		case 18:{
			$html = '
			<div class="spinner" style="margin: 0px; width: 30px; height: 30px;">
				<img src="'.$options['url'].'/css/main/spinners/'.$spinner.'.svg" width="30" alt="">
			</div>';
			break;
		}
		case 19:{
			$html = '
			<div class="spinner" style="margin: 0px; width: 30px; height: 30px;">
				<img src="'.$options['url'].'/css/main/spinners/'.$spinner.'.svg" width="30" alt="">
			</div>';
			break;
		}
		case 20:{
			$html = '
			<div class="spinner" style="margin: 5px 0 -5px 0; width: 50px; height: 30px;">
				<img src="'.$options['url'].'/css/main/spinners/'.$spinner.'.svg" width="50" alt="">
			</div>';
			break;
		}
		case 21:{
			$html = '
			<div class="spinner" style="margin: 0px; width: 30px; height: 30px;">
				<img src="'.$options['url'].'/css/main/spinners/'.$spinner.'.svg" width="30" alt="">
			</div>';
			break;
		}
		case 22:{
			$html = '
			<div class="spinner" style="margin: 0px; width: 30px; height: 30px;">
				<img src="'.$options['url'].'/css/main/spinners/'.$spinner.'.svg" width="30" alt="">
			</div>';
			break;
		}
		case 23:{
			$html = '
			<div class="spinner" style="margin: 0px; width: 30px; height: 30px;">
				<img src="'.$options['url'].'/css/main/spinners/'.$spinner.'.svg" width="30" alt="">
			</div>';
			break;
		}
		case 24:{
			$html = '
			<div class="spinner" style="margin: 0px; width: 30px; height: 30px;">
				<img src="'.$options['url'].'/css/main/spinners/'.$spinner.'.svg" width="30" alt="">
			</div>';
			break;
		}
		case 25:{
			$html = '
			<div class="spinner" style="margin: 0px; width: 30px; height: 30px;">
				<img src="'.$options['url'].'/css/main/spinners/'.$spinner.'.svg" width="30" alt="">
			</div>';
			break;
		}
		case 26:{
			$html = '
			<div class="spinner" style="margin: 5px 0px -5px 0; width: 40px; height: 30px;">
				<img src="'.$options['url'].'/css/main/spinners/'.$spinner.'.svg" width="40" alt="">
			</div>';
			break;
		}
		case 27:{
			$css = '';
			$html = '
			<div class="spinner" style="margin: -3px; width: 35px; height: 35px;">
				<img src="'.$options['url'].'/css/main/spinners/'.$spinner.'.gif" alt="" />
			</div>';
			break;
		}
		case 28:{
			$css = '';
			$html = '
			<div class="spinner" style="margin: -3px; width: 35px; height: 35px;">
				<img src="'.$options['url'].'/css/main/spinners/'.$spinner.'.gif" alt="" />
			</div>';
			break;
		}
		case 27:{
			$css = '';
			$html = '
			<div class="spinner" style="margin: -3px; width: 35px; height: 35px;">
				<img src="'.$options['url'].'/css/main/spinners/'.$spinner.'.gif" alt="" />
			</div>';
			break;
		}
		case 28:{
			$css = '';
			$html = '
			<div class="spinner" style="margin: -3px; width: 35px; height: 35px;">
				<img src="'.$options['url'].'/css/main/spinners/'.$spinner.'.gif" alt="" />
			</div>';
			break;
		}
		case 29:{
			$css = '';
			$html = '
			<div class="spinner" style="margin: -3px; width: 35px; height: 35px;">
				<img src="'.$options['url'].'/css/main/spinners/'.$spinner.'.gif" alt="" />
			</div>';
			break;
		}
		case 30:{
			$css = '';
			$html = '
			<div class="spinner" style="margin: -5px; width: 40px; height: 40px;">
				<img src="'.$options['url'].'/css/main/spinners/'.$spinner.'.gif" alt="" />
			</div>';
			break;
		}
		case 31:{
			$css = '';
			$html = '
			<div class="spinner" style="margin: -5px; width: 68px; height: 40px;">
				<img src="'.$options['url'].'/css/main/spinners/'.$spinner.'.gif" alt="" />
			</div>';
			break;
		}
		case 32:{
			$css = '';
			$html = '
			<div class="spinner" style="margin: -5px; width: 30px; height: 30px;">
				<img src="'.$options['url'].'/css/main/spinners/'.$spinner.'.gif" alt="" />
			</div>';
			break;
		}
		case 33:{
			$css = '';
			$html = '
			<div class="spinner" style="margin: 0px; width: 30px; height: 30px;">
				<img src="'.$options['url'].'/css/main/spinners/'.$spinner.'.gif" alt="" />
			</div>';
			break;
		}
		case 34:{
			$css = '';
			$html = '
			<div class="spinner" style="margin: 0px; width: 30px; height: 30px;">
				<img src="'.$options['url'].'/css/main/spinners/'.$spinner.'.gif" alt="" />
			</div>';
			break;
		}
		case 35:{
			$css = '';
			$html = '
			<div class="spinner" style="margin: 0px; width: 30px; height: 30px;">
				<img src="'.$options['url'].'/css/main/spinners/'.$spinner.'.gif" alt="" />
			</div>';
			break;
		}
	}
	
	echo $css;
	echo $html;
	echo $js;
}


function mysql_insert($table, $parameters, $mode="normal"){
	global $mysqli;
	$insert_stmt = "INSERT INTO ".$table." (";
	foreach ($parameters as $parameter=>$parameter_value){
		$insert_stmt .= $parameter.", ";
	}
	$insert_stmt .= ") VALUES (";
	foreach ($parameters as $parameter=>$parameter_value){
		$insert_stmt .= "\"".htmlspecialchars($parameter_value)."\", ";
	}
	$insert_stmt.=")";
	$insert_stmt = str_ireplace(", )",")", $insert_stmt);
	
	if ($mode=="debug"){
		echo $insert_stmt;
	}
	
	$stmt = $mysqli->prepare($insert_stmt);
	$stmt->execute(); 
	$result = $mysqli->insert_id; 
	$stmt->close();
	
	return $result;
}

function mysql_update($table, $ids, $parameters, $mode = "normal"){
	global $mysqli;
	$update_stmt = "UPDATE ".$table." SET ";
	foreach ($parameters as $parameter=>$parameter_value){
		$update_stmt .= $parameter."=\"".htmlspecialchars($parameter_value)."\", ";
	}
	$update_stmt .= "WHERE ";
	foreach ($ids as $id=>$id_value){
		$update_stmt .= $id."=\"".$id_value."\" AND ";
	}
	$update_stmt.="END";

	$update_stmt = str_ireplace(", WHERE"," WHERE", $update_stmt);
	$update_stmt = str_ireplace(" AND END","", $update_stmt);
	
	if ($mode == "debug"){	
		echo $update_stmt;
	}
	
	$stmt = $mysqli->prepare($update_stmt);
	$result = $stmt->execute(); 
	$stmt->close();
	
	return $result;
}

function mysql_delete($table, $ids, $mode="normal"){
	global $mysqli;
	$delete_stmt = "DELETE FROM ".$table." WHERE ";
	foreach ($ids as $id=>$id_value){
		$delete_stmt .= $id."=\"".$id_value."\" AND ";
	}
	$delete_stmt.="END";

	$delete_stmt = str_ireplace(" AND END","", $delete_stmt);
	
	$stmt = $mysqli->prepare($delete_stmt);
	$result = $stmt->execute(); 
	$stmt->close();
	
	if ($mode=="debug"){
		echo $delete_stmt;
	}
	
	return $result;
}

function mysql_select($table, $ids, $parameters, $mode = "normal"){
	global $mysqli;
	$select_stmt = "SELECT ";
	
	foreach ($parameters as $parameter){
		$select_stmt .= $parameter.", ";
	}
	
	$select_stmt .= "FROM ".$table;
	
	if (sizeof($ids)>0){
		$select_stmt .=" WHERE ";
		foreach ($ids as $id=>$id_value){
			$select_stmt .= $id."=\"".$id_value."\" AND ";
		}
		$select_stmt.="END";
	}
	
	$select_stmt = str_ireplace(", FROM"," FROM", $select_stmt);
	$select_stmt = str_ireplace(" AND END","", $select_stmt);
	
	if ($mode == "debug"){
		echo $select_stmt;
	}

	$res = $mysqli->query($select_stmt);
	
	$result = array();
	
	while($row = $res->fetch_array()){
		$resu = array();

		foreach ($parameters as $parameter){
			$resu[$parameter] = htmlspecialchars_decode($row[$parameter]);
		}
		$result[] = $resu;
	}
	
	return $result;
}

function send_email($to, $subject, $html_body="", $alt_body="", $mode=""){
	
	require_once 'config.php';
	require_once 'lib/PHPMailer/PHPMailerAutoload.php';
	
	global $options;
	require_once dirname(__FILE__).'/class/persianDate.class.php';
	$persian_date = new persianDate();
	
	date_default_timezone_set("Iran");
	$date = $persian_date->date('Y/m/d');
	$time = date("H:i:s");
	
	$mail = new PHPMailer();
	
	$mail ->CharSet = "UTF-8";

	if($mode == "debug") {
		$mail->SMTPDebug = 3;                              	 // Enable verbose debug output
	}
	
	$mail->isSMTP();                                     	 // Set mailer to use SMTP
	$mail->Host = $options["email_host"]; 					 	 // Specify main and backup SMTP servers
	$mail->SMTPAuth = true;                             		  // Enable SMTP authentication
	$mail->Username = $options["email_user"];                	 // SMTP username
	$mail->Password = $options["email_pass"];                      		   // SMTP password
	//$mail->SMTPSecure = 'none';                      	   // Enable TLS encryption, `ssl` also accepted
	$mail->Port = $options["email_port"];                                    	 // TCP port to connect to
	
	$mail->From = $options["email"];
	$mail->FromName = $options["title"];
	$to_full = explode(":", $to);
	$to_email = $to_full[0];
	if (isset($to_full[1])){
		$to_name = $to_full[1];
	} else {
		$to_name = "";
	}
	
	$mail->addAddress($to_email, $to_name);     					// Add a recipient
	//$mail->addAddress('seyyedmojtaba.akramzade@gmail.com');        // Name is optional
	$mail->addReplyTo($options["email"], 'Information');
	//$mail->addCC('cc@example.com');
	//$mail->addBCC('bcc@example.com');

	//$mail->WordWrap = 50;                                 // Set word wrap to 50 characters
	//$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
	//$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
	$mail->isHTML(true);                                  // Set email format to HTML

	$mail->Subject = $subject;
	
	$body =  
	'
	<html>
	<head>
	<style>
	
		@font-face {
			font-family: "WYekan";
			src: url("%URL%/fonts/WYekan.woff") format("woff");
			font-weight: normal;
			font-style: normal;
		}
		
		body{
			font: normal 11px Tahoma;
			direction: rtl;
		}
		
		.container{
		}
		
		.content{
		}
		
		a{
			color: darkblue;
			text-decoration:none;
			transition: all .4s;
		}
			a:hover{
				color: blue;
				text-decoration: none;
			}
			
		h1, .h1, h2, .h2, h3, .h3, h4, .h4, h5, .h5, h6, .h6{
			font-family: "WYekan", B Yekan, Arial;
			font-weight: normal;
			line-height: 100%;
			padding:0;
			margin-top:0;
		}
		
		h1, .h1{
			font-size: 25px;
		}
		
		h2, .h2{
			font-size: 20px;
		}
		
		h3, .h3{
			font-size: 17px;
		}
		
		h4, .h4{
			font-size: 15px;
		}
		
		h5, .h5{
			font-size: 13px;
		}
		
		h6, .h6{
			font-size: 10px;
		}
		
		p{
			font: normal 11px Tahoma;
		}
		
		.Yekan{
			font-family: "WYekan", B Yekan, Arial;
		}
		
		.normal{
			font-weight: normal;
		}
		
		.footer{
			height: 30px;
		}
		
		.pull-left{
			float: left;
			
		}
		
		.pull-right{
			float: right;
		}
		
	</style>
	
	</head>
	<body dir="rtl" style="direction: rtl;">
		<div class="container">
			%BODY%
		</div>
	</body>
	</html>';
	
	$body = str_replace("%TITLE%", $options['title'], str_replace("%URL%", $options['url'], str_replace("%SUBJECT%", $subject, str_replace("%TEXT%", $html_body, str_replace("%BODY%", $options['email_template'], $body)))));
	
	$mail->Body = $body;
	
	$mail->AltBody = $alt_body;
	
	
	mysql_insert('emails', array('receiver'=>$to, 'subject'=>$subject, 'email_body'=>$html_body, 'timestamp'=>time(), 'date'=>$date, 'time'=>$time), $mode);
	
	if ($mode=="debug"){
		return $mail->Body;
	} else {
		
		if(!$mail->send()) {
			return 'Mailer Error: ' . $mail->ErrorInfo;
		} else {
			return 'Message has been sent';
		}
		
	}
}

function send_message($to, $subject, $message_body=""){
	
	global $options;
	require_once dirname(__FILE__).'/class/persianDate.class.php';
	$persian_date = new persianDate();
	
	date_default_timezone_set("Iran");
	$date = $persian_date->date('Y/m/d');
	$time = date("H:i:s");
	
	$body = str_replace("%TITLE%", $options['title'], str_replace("%URL%", $options['url'], str_replace("%TEXT%", $message_body, $options['message_template'])));
	
	
	mysql_insert('messages', array('receiver'=>$to, 'subject'=>$subject, 'text'=>$body, 'timestamp'=>time(), 'date'=>$date, 'time'=>$time));
}

function send_sms($to, $sms_body=""){
	
	global $options;
	require_once dirname(__FILE__).'/class/persianDate.class.php';
	$persian_date = new persianDate();
	
	date_default_timezone_set("Iran");
	$date = $persian_date->date('Y/m/d');
	$time = date("H:i:s");
	
	$body = str_replace("%TITLE%", $options['title'], str_replace("%URL%", $options['url'], str_replace("%TEXT%", $sms_body, $options['sms_template'])));
	
	ini_set("soap.wsdl_cache_enabled", "0");
	try {
		$client = new SoapClient("http://87.107.121.54/post/send.asmx?wsdl");
		$parameters['username'] = $options['sms_user'];
		$parameters['password'] = $options['sms_pass'];
		$parameters['from'] = $options['sms_number'];
		$parameters['to'] = array($to);
		$parameters['text'] = $body;
		$parameters['isflash'] = false;
		$parameters['udh'] = "";
		$parameters['recId'] = array(0);
		$parameters['status'] = 0x0;
		//echo $client->GetCredit(array("username"=>$options['sms_user'],"password"=>$options['sms_pass']))->GetCreditResult;
		$client->SendSms($parameters)->SendSmsResult;
	} catch (SoapFault $ex) {
		echo $ex->faultstring;
	}	
	
	mysql_insert('text_messages', array('receiver'=>$to, 'text'=>$body, 'timestamp'=>time(), 'date'=>$date, 'time'=>$time));
}


function getCategoryFullName($table, $slug){
	global $mysqli;
	
	$full_name = "";
	
	while (!empty($slug)){
		$result = mysql_select($table, array('slug'=>$slug), array('parent', 'category_title'), "");
		
		if (isset($result[0]["parent"])){
			$slug = $result[0]["parent"];
			$name = $result[0]["category_title"];
			
			$full_name = $name . " » ". $full_name;
		} else {
			$slug = "";
		}
	}
	
	$full_name = substr_replace($full_name, "", strlen($full_name)-4);

	
	return $full_name;
}

function normalDate($date, $splitter="/"){
	$date_array = explode($splitter, $date);

	if (intval($date_array[1])<10){
		$date_array[1] = "0".intval($date_array[1]);
	}
	
	if (intval($date_array[2])<10){
		$date_array[2] = "0".intval($date_array[2]);
	}
	
	return $date_array[0].$splitter.$date_array[1].$splitter.$date_array[2];
}

function sendPushNotification($ids, $flag, $title, $data, $mode="normal"){
	$apiKey = GCM_SERVER_API_KEY;
	if ($mode == "debug"){
		echo "GCM_SERVER_API_KEY: ".GCM_SERVER_API_KEY;
	}
	
	
	$ids = array_filter($ids);
	if (sizeof($ids)==0){
		return;
	}
	
	$registration_ids = array();
	foreach ($ids as $id){	
		$results = mysql_select("members", array("id"=>$id), array("gcm_token"));
		$gcm_token = $results[0]["gcm_token"];
		
		if ($mode == "debug"){
			echo "GCM Token: ".$gcm_token;
		}
		
		if (sizeof($results) == 0 || empty($gcm_token)){
			continue;
		}
		
		array_push($registration_ids, $gcm_token);

	}
	
	

	$data_array = array();
	$data_array['flag'] = $flag;
	$data_array['title'] = $title;
	$data_array['data'] = $data;
	$data_array['timestamp'] = time();

	$fields = array(
		'registration_ids' => $registration_ids,
		'data' => $data_array,
	);

    // Set POST variables
    $url = 'https://fcm.googleapis.com/fcm/send';

    $headers = array(
        'Authorization: key=' . $apiKey,
        'Content-Type: application/json'
    );
    // Open connection
    $ch = curl_init();

    // Set the url, number of POST vars, POST data
    curl_setopt($ch, CURLOPT_URL, $url);

    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    // Disabling SSL Certificate support temporarly
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

    $response = array();

    // Execute post
	$result = curl_exec($ch);
	if ($result === FALSE) {
		die('Curl failed: ' . curl_error($ch));
	}

	// Close connection
	curl_close($ch);

	return $result;
		
}

?>