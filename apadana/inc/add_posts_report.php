<?php
require_once 'db_connect.php';
require_once 'functions.php';


$author = $_REQUEST['author'];
$post = $_REQUEST['post'];
$report = $_REQUEST['report'];
$date = $_REQUEST['date'];
$time = $_REQUEST['time'];

if (ifallisset($author, $post, $report, $date, $time)){
		
	if ($stmt = $mysqli->prepare("INSERT INTO posts_reports (author, post, report, date, time) VALUES (?, ?, ?, ?, ?)")){;
	
		$stmt->bind_param('sssss', $author, $post, $report, $date, $time);
				
		if (! $stmt->execute()) {
			header('Location: ../posts_report_add.php?err=insert');
			exit;
		}
		
		header('Location: ../posts_report_add.php?suc=insert');
		exit;
	}
	else{
		header('Location: ../posts_report_add.php?err=fill');
		exit;
	}
	

}
else{
	header('Location: ../posts_report_add.php?err=fill');
	exit;
}
?>