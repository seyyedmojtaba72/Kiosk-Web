<?php
require_once 'db_connect.php';
require_once 'functions.php';
ob_start();
sec_session_start();

$redirect = $_POST['redirect'];

$ip = $_POST['ip'];
$location = $_POST['location'];
$date = $_POST['date'];
$time = $_POST['time'];
$os = $_POST['os'];
$browser = $_POST['browser'];


$statement = 'SELECT DISTINCT ip FROM statistics WHERE';

if (!empty($ip)){ $statement .= ' ip LIKE "%'.$ip .'%" AND'; }
if (!empty($location)){ $statement .= ' location LIKE "%'.$location .'%" AND'; }
if (!empty($date)){ $statement .= ' date LIKE "%'.$date .'%" AND'; }
if (!empty($time)){ $statement .= ' time LIKE "%'.$time .'%" AND'; }
if (!empty($os)){ $statement .= ' os LIKE "%'.$os .'%" AND'; }
if (!empty($browser)){ $statement .= ' browser LIKE "%'.$browser .'%" AND'; }

if (substr($statement,strlen($statement)-3,strlen($statement))=="AND"){
	$statement = substr($statement,0,strlen($statement)-3);
}

if (substr($statement,strlen($statement)-5,strlen($statement))=="WHERE"){
	$statement = substr($statement,0,strlen($statement)-5);
}


$result = $mysqli->query($statement);

$num_rows = $result->num_rows;

if ($num_rows > 0){
	header('Location: ../'.$redirect.'&suc=search&search=true&ip='.$ip.'&location='.$location.'&date='.$date.'&time='.$time.'&os='.$os.'&browser='.$browser);
	exit;
}  else{
	header('Location: ../'.$redirect.'&err=no-match');
	exit;
}

?>