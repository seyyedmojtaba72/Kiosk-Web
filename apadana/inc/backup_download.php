<?php
require_once 'db_connect.php';

//ENTER THE RELEVANT INFO BELOW
$host = DB_HOST;
$user = DB_USER;
$pass = DB_PASS;
$name = DB_NAME;
$tables = '*';

$mysqli->query("SET NAMES 'utf8'");

//get all of the tables
if($tables == '*'){
	$tables = array();
	$result = $mysqli->query('SHOW TABLES');
	while($row = mysql_fetch_row($result)){
		$tables[] = $row[0];
	}
}
else{
	$tables = is_array($tables) ? $tables : explode(',',$tables);
}
$return='';
//cycle through
foreach($tables as $table){
	$result = $mysqli->query('SELECT * FROM '.$table);
	$num_fields = mysql_num_fields($result);

	$return.= 'DROP TABLE '.$table.';';
	$row2 = mysql_fetch_row($mysqli->query('SHOW CREATE TABLE '.$table));
	$return.= "\n\n".$row2[1].";\n\n";

	for ($i = 0; $i < $num_fields; $i++) {
		while($row = mysql_fetch_row($result)){
			$return.= 'INSERT INTO '.$table.' VALUES(';
			for($j=0; $j<$num_fields; $j++) {
				$row[$j] = addslashes($row[$j]);
				$row[$j] = str_replace("\n","\\n",$row[$j]);
				if (isset($row[$j])) { $return.= '"'.$row[$j].'"' ; } else { $return.= '""'; }
				if ($j<($num_fields-1)) { $return.= ','; }
			}
			$return.= ");\n";
		}
	}
	$return.="\n\n\n";
}

//save file
$handle = fopen('backup.sql','w+');
fwrite($handle,$return);
fclose($handle);

header('Location: ../options.php?suc=download');
exit;

?>