<?php
require_once 'information.php';

//$demo_mode = false;
$upload_dir = '../files/uploads/';
$allowed_ext = array();

if(strtolower($_SERVER['REQUEST_METHOD']) != 'post'){
	exit_status('Error! Wrong HTTP method!');
}


if(array_key_exists('file',$_FILES) && $_FILES['file']['error'] == 0 ){
	
	$file = $_FILES['file'];
	
	/*
	if($demo_mode){
		
		exit_status('Uploads are ignored in demo mode.');
	}
	*/
	/*
	if(!in_array(get_extension($file['name']),$allowed_ext)){
		exit_status('فرمت فایل قابل قبول نیست!');
	}	
	*/
	
	// Move the uploaded file from the temporary 
	// directory to the uploads folder:
	
	$full_name = $file["name"];
	$name = substr($full_name,0,strpos($full_name,"."));
	$ext = end(explode(".", $full_name));
	
	$path = $upload_dir."/".$name.".".$ext;
	
	$i=0;
	while(file_exists($path)){
		$i++;
		$path = $upload_dir."/".$name.$i.".".$ext;
	}
	
	
	if(move_uploaded_file($file['tmp_name'], $path)){
		exit_status(str_replace('..',$options["url"],$path));
	}
	
}

exit_status('آپلود شکست خورد!');


// Helper functions

function exit_status($str){
	echo json_encode(array('status'=>$str));
	exit;
}

function get_extension($file_name){
	$ext = explode('.', $file_name);
	$ext = array_pop($ext);
	return strtolower($ext);
}
?>