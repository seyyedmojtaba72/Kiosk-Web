<?php
require_once 'config.php';
require_once 'db_connect.php';
require_once 'information.php';
require_once 'functions.php';

ob_start();
sec_session_start();

$redirect = $_POST['redirect'];
$session = $_REQUEST['session'];
$former_email = $_POST['former_email'];
$username = $_POST['username'];
$email = $_POST['email'];
$first_name = $_POST['first_name'];
$last_name = $_POST['last_name'];
$former_mobile_number = $_POST['former_mobile_number'];
$mobile_number = $_POST['mobile_number'];
$home_number = $_POST['home_number'];
$fax_number = $_POST['fax_number'];
$office = $_POST['office'];
$shaba = $_POST['shaba'];
$address = $_POST['address'];
$about = $_POST['about'];
if (isset($_POST['extras'])){
	$extras[] = $_POST['extras'];
}

if (!empty($email)) {
    // Sanitize and validate the data passed in
	
	$username = strtolower($username);
	if (!preg_match('/^[a-z0-9_.-]{1,50}$/', $username) || !preg_match('/\w/', $username)){
		header('Location: '.$redirect.'&err=invalid-username');
		exit;
	}
	
	if (strlen($username) < 6 && $username!="admin"){
		header('Location: '.$redirect.'&err=min-length');
		return;
	}
	
	if (strlen($username) > 50){
		header('Location: '.$redirect.'&err=max-username-length');
		exit;
	}
	
	if (strlen($email) > 50){
		header('Location: '.$redirect.'&err=max-email-length');
		exit;
	}
	
	
	$stmt = $mysqli->prepare('SELECT email FROM members WHERE username="'.$username.'"');
	$stmt->execute();
	$stmt->bind_result($new_email);
	$stmt->store_result();
	$num_rows = $stmt->num_rows;
	$stmt->fetch();
	$stmt->close();
	
	if (($num_rows > 0) && ($former_email!=$new_email)){
		header('Location: '.$redirect.'&err=username-exists');
		exit;
	}

    $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        header('Location: ../'.$redirect.'&err=invalid-email');
		exit;
    }
	
	
	if (!empty($mobile_number)){
		if (strlen($mobile_number)!=10){
			header('Location: ../'.$redirect.'&err=mobile_number-length');
			exit;
		}
		

		$stmt = $mysqli->prepare('SELECT id FROM members WHERE mobile_number="'.$mobile_number.'"');
		$stmt->execute();
		$stmt->store_result();
		$stmt->bind_result($id2);
		$num_rows = $stmt->num_rows;
		$stmt->fetch();
		$stmt->close();
	
		if ($num_rows > 0 && $id2 != $id ){
			header('Location: ../'.$redirect.'&err=mobile_number-exists');
			exit;
		}
		
		if ($former_mobile_number != $mobile_number){
		
			$insert_stmt = $mysqli->prepare("UPDATE members SET mobile_number_status=0 WHERE email=\"".$email."\"");
			$insert_stmt->execute();
			$stmt->fetch();
			$stmt->close();
		}
	}
 
    $prep_stmt = "SELECT id FROM members WHERE email = ? LIMIT 1";
    $stmt = $mysqli->prepare($prep_stmt);
 
    if ($stmt) {
        $stmt->bind_param('s', $email);
        $stmt->execute();
        $stmt->store_result();
 		
        if (($stmt->num_rows > 0) & ($former_email!=$email)) {
            header('Location: ../'.$redirect.'&err=email-exists');
			exit;
        }
    } else {
        	header('Location: ../'.$redirect.'&err=database');
			exit;
    }
	
	
	$results = mysql_select("members", array("email"=>$former_email), array("extras"));
	$extras_array = explode(";", $results[0]['extras']);
	
	if (in_array("trusted", $extras_array)){
		$extras_array = array();
		array_push($extras_array, "trusted");
	} else {
		$extras_array = array();
	}
	
	
	if (isset($extras[0])){	
		foreach($extras[0] as $extra){
			array_push($extras_array, $extra);
		}
	}
	
	
	$extras_array = array_filter(array_unique($extras_array));
	

	
	$new_extras = "";
	foreach($extras_array as $extra){
		$new_extras .= $extra.";";
	}
	
 
    if (empty($err_msg)) {

        if ($insert_stmt = $mysqli->prepare("UPDATE members SET username=?, email=?, first_name=?, last_name=?, mobile_number=?, home_number=?, fax_number=?, office=?, shaba=?, address=?, about=?, extras=?, timestamp=? WHERE email=?")) {
            $insert_stmt->bind_param('ssssssssssssss', $username, $email, $first_name, $last_name, $mobile_number, $home_number, $fax_number, $office, $shaba, $address, $about, $new_extras, time(), $former_email);
            if (! $insert_stmt->execute()) {
                header('Location: ../'.$redirect.'&err=insert');
		exit;
            }
        }
		
		$_SESSION['username'] = $username;
		$_SESSION['email'] = $email;
		$_SESSION['first_name'] = $first_name;
		$_SESSION['last_name'] = $last_name;
		$_SESSION['mobile_number'] = $mobile_number;
		$_SESSION['home_number'] = $home_number;
		$_SESSION['fax_number'] = $fax_number;
		$_SESSION['office'] = $office;
		$_SESSION['shaba'] = $shaba;
		$_SESSION['address'] = $address;
		$_SESSION['about'] = $about;
		$_SESSION['extras'] = $new_extras;
		
		header('Location: ../'.$redirect.'&suc=update');
		exit;
    }
	
} else{
	header('Location: ../'.$redirect.'&err=fill');
	exit;
}
	
?>