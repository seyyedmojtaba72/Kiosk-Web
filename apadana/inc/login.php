<?php
require_once 'db_connect.php';
require_once 'functions.php';
 
sec_session_start();

$redirect = $_REQUEST['redirect'];
$username = strtolower($_REQUEST['username']);
$password = $_REQUEST['password'];

if (isset($username, $password)) {
   
	$status = login($username, $password);

    if ($status == "active") {
        header('Location: '.$redirect);
		exit;
    }
	
	else if ($status == "user-pass") {
        header('Location: ../login.php?err=user-pass&redirect='.$redirect);
		exit;
    }
	
	else if ($status == "inactive") {
		if ($stmt = $mysqli->prepare("SELECT password FROM members WHERE username = ? LIMIT 1")) {
        $stmt->bind_param('s', $username);
        $stmt->execute(); 
        $stmt->store_result();
 
        $stmt->bind_result($password);
        $stmt->fetch();
		}
        header('Location: ../login.php?err=inactive&username='.$username.'&password='.$password.'&redirect='.$redirect);
		exit;
    }
	
	
	else {
		header('Location: ../login.php?err=user-pass&redirect='.$redirect);
		exit;
	}
	
}

?>