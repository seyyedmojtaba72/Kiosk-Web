<?php
require_once 'db_connect.php';

$redirect = $_POST['redirect'];
$directory = $_POST['directory'];
if (!isset($directory)){ $directory = "uploads"; }
$full_name = $_POST['file'];
$name = substr($full_name,0,strpos($full_name,"."));
$ext = end(explode(".", $full_name));

if ($directory=="profile_images"){
	$idd = $_POST['idd'];
	$model = $_POST['model'];
	$value = $_POST['value'];
	
	$name = $value;
}

$path = "../files/".$directory."/".$name.".".$ext;

if(file_exists($path)){
	unlink($path);
}


if ($directory=="profile_images"){
	$stmt = $mysqli->prepare("UPDATE ".$model."s SET profile_image='' WHERE ".$idd."=?");
	$stmt->bind_param("s",$value);
	$stmt->execute();
	$stmt->close();
	header('Location: ../'.$redirect.'&suc=delete');
	exit;
}

header('Location: ../'.$redirect.'&suc=delete');
exit;
	
?>