<?php
require_once 'db_connect.php';
require_once 'functions.php';
ob_start();
sec_session_start();

$redirect = $_POST['redirect'];

$receiver = $_POST['receiver'];
$subject = $_POST['subject'];
$date = $_POST['date'];
$time = $_POST['time'];

$statement = 'SELECT * FROM emails WHERE';

if (!empty($receiver)){ $statement .= ' receiver LIKE "%'.$receiver .'%" AND'; }
if (!empty($subject)){ $statement .= ' subject LIKE "%'.$subject.'%" AND'; }
if (!empty($date)){ $statement .= ' date LIKE "%'.$date .'%" AND'; }
if (!empty($time)){ $statement .= ' time = "'.$time .'" AND'; }


if (substr($statement,strlen($statement)-3,strlen($statement))=="AND"){
	$statement = substr($statement,0,strlen($statement)-3);
}

if (substr($statement,strlen($statement)-5,strlen($statement))=="WHERE"){
	$statement = substr($statement,0,strlen($statement)-5);
}

$result = $mysqli->query($statement);

$num_rows = $result->num_rows;

if ($num_rows > 0){
	header('Location: ../'.$redirect.'&suc=search&search=true&receiver='.$receiver.'&'.'subject='.$subject.'&'.'date='.$date.'&'.'time='.$time);
	exit;
}  else{
	header('Location: ../'.$redirect.'&err=no-match');
	exit;
}

?>