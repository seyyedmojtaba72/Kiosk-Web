<?php
require_once dirname(__FILE__).'/db_connect.php';
require_once dirname(__FILE__).'/functions.php';
require_once dirname(__FILE__).'/information.php';

require_once dirname(__FILE__).'/class/onlineUsers.class.php';
require_once dirname(__FILE__).'/class/persianDate.class.php';
require_once dirname(__FILE__).'/class/userAgent.class.php';

require_once dirname(__FILE__).'/geoip/geoipcity.inc';

$online_visitors = new usersOnline;
$persian_date = new persianDate();

$gi = geoip_open(__DIR__."/geoip/GeoLiteCity.dat", GEOIP_STANDARD);
$ip = $online_visitors->ipCheck();

if (empty($ip)){
	$ip = getenv("REMOTE_ADDR");
}

if (strlen($ip)<5 || substr($ip,0,3) =="192" || substr($ip,0,3) =="127" || substr($ip,0,3) =="172"){
	$ip = "109.125.130.38";
} else {
	$record = geoip_record_by_addr($gi, $ip);
	$location = $record->city;
}

geoip_close($gi);

if (empty($location)){
	$location = "Unknown";
}

date_default_timezone_set("Iran");
$date = $persian_date->date('Y/m/d');
$time = date("H:i:s");

$user_agent = new userAgent();
$os = $user_agent->getOS();
$browser = $user_agent->getBrowser();

if (!isset($role)){ $role = ""; }
if ($role != "admin"){
	global $mysqli;
	if ($stmt = $mysqli->prepare("INSERT INTO statistics (ip, location, date, time, os, browser) VALUES (?,?,?,?,?,?)")) {
		$stmt->bind_param('ssssss', $ip, $location, $date, $time, $os, $browser); 
		$stmt->execute(); 
		$stmt->close();
	}
}

?>