<?php
require_once __DIR__.'/config.php';
ob_start();
$filename = $_FILES["file"]["tmp_name"];

$mysql_host = DB_HOST;
$mysql_username = DB_USER;
$mysql_password = DB_PASS;
$mysql_database = DB_NAME;

if (!empty($filename)){
	
	set_time_limit(0);                   // ignore php timeout
	//ignore_user_abort(true);             // keep on going even if user pulls the plug*
	while(ob_get_level())ob_end_clean(); // remove output buffers
	ob_implicit_flush(true); 	          // output stuff directly
	
	
	$mysqli->query("SET NAMES 'utf8'");
	// Temporary variable, used to store current query
	$templine = '';
	// Read in entire file
	$lines = file($filename);
	// Loop through each line
	foreach ($lines as $line){
	// Skip it if it's a comment
		if (substr($line, 0, 2) == '--' || $line == '')
			continue;
	
		// Add this line to the current segment
		$templine .= $line;
		// If it has a semicolon at the end, it's the end of the query
		if (substr(trim($line), -1, 1) == ';'){
			// Perform the query
			$mysqli->query($templine) or print('Error performing query \'<strong>' . $templine . '\': ' . mysql_error() . '<br /><br />');
			// Reset temp variable to empty
			$templine = '';
		}
	}
	header('Location: ../options.php?suc=upload');
	exit;
} else{
	header('Location: ../options.php?err=fill');
	exit;
}
?>