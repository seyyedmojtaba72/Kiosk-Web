<?php
require_once 'db_connect.php';
require_once 'functions.php';
ob_start();
sec_session_start();

$redirect = $_POST['redirect'];

$user = $_REQUEST['user'];
$post = $_REQUEST['post'];
$date = $_REQUEST['date'];
$time = $_REQUEST['time'];
$status = $_REQUEST['status'];

$statement = 'SELECT * FROM posts_likes WHERE';
				
if (!empty($user)){ $statement .= ' user = "'.$user .'" AND'; }
if (!empty($post)){ $statement .= ' post = "'.$post .'" AND'; }
if (!empty($date)){ $statement .= ' date LIKE "%'.$date .'%" AND'; }
if (!empty($time)){ $statement .= ' time LIKE "%'.$time .'%" AND'; }

if (substr($statement,strlen($statement)-3,strlen($statement))=="AND"){
	$statement = substr($statement,0,strlen($statement)-3);
}

if (substr($statement,strlen($statement)-5,strlen($statement))=="WHERE"){
	$statement = substr($statement,0,strlen($statement)-5);
}

$result = $mysqli->query($statement);

$num_rows = $result->num_rows;

if ($num_rows > 0){
	header('Location: ../'.$redirect.'&suc=search&search=true&user='.$user.'&post='.$post.'&date='.$date.'&time='.$time);
	exit;
}  else{
	header('Location: ../'.$redirect.'&err=no-match');
	exit;
}

?>