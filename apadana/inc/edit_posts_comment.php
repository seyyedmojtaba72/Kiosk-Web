<?php
require_once 'db_connect.php';
require_once 'functions.php';

$redirect = $_REQUEST['redirect'];
$id = $_REQUEST['id'];

$author = $_REQUEST['author'];
$post = $_REQUEST['post'];
$comment = $_REQUEST['comment'];
$date = $_REQUEST['date'];
$time = $_REQUEST['time'];
$status = $_REQUEST['status'];

if (ifallisset($author, $post, $comment, $date, $time)){
	
	if ($stmt = $mysqli->prepare("UPDATE posts_comments SET author=?, post=?, comment=?, date=?, time=?, status=?, timestamp=? WHERE id=?")){;
	
		$stmt->bind_param('ssssssss', $author, $post, $comment, $date, $time, $status, time(), $id);
				
		if (! $stmt->execute()) {
			header('Location: ../'.$redirect.'&err=edit');
			exit;
		}
		
		header('Location: ../'.$redirect.'&suc=edit');
		exit;
	}
} else{
	header('Location: ../'.$redirect.'&err=fill');
	exit;
}

?>