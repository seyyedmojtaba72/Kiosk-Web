<?php
require_once 'db_connect.php';
require_once 'functions.php';
require_once 'information.php';

$redirect = $_POST['redirect'];
$id = $_POST['id'];
$receiver = $_POST['receiver'];
$text = $_POST['text'];
$date = $_POST['date'];
$time = $_POST['time'];
$timestamp = $_POST['timestamp'];

if (ifallisset($receiver, $text, $date, $time)){
	
	if ($stmt = $mysqli->prepare("UPDATE text_messages SET receiver=?, text=?, date=?, time=?, timestamp=? WHERE id=?")){
		
		$stmt->bind_param('ssssss', $receiver, $text, $date, $time, $timestamp, $id);
		
				
		if (! $stmt->execute()) {
			header('Location: ../'.$redirect.'&err=edit');
			exit;
		}

		header('Location: ../'.$redirect.'&suc=edit');
		exit;
	}
} else{
	header('Location: ../'.$redirect.'&err=fill');
	exit;
}

?>