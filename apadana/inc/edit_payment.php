<?php
require_once 'db_connect.php';
require_once 'functions.php';
require_once 'information.php';

$redirect = $_POST['redirect'];
$id = $_POST['id'];
$customer = $_POST['customer'];
$transaction_id = $_POST['transaction_id'];
$amount = $_POST['amount'];
$action = $_POST['action'];
$date = $_POST['date'];
$time = $_POST['time'];
$status = $_POST['status'];
$timestamp = $_POST['timestamp'];


if (ifallisset($customer, $transaction_id, $amount, $action, $date, $time, $status)){ 
	
	if ($stmt = $mysqli->prepare("UPDATE payments SET customer=?, transaction_id=?, amount=?, action=?, date=?, time=?, status=?, timestamp=? WHERE id=?")){
		
		$stmt->bind_param('sssssssss', $customer, $transaction_id, $amount, $action, $date, $time, $status, $timestamp, $id);
		
				
		if (! $stmt->execute()) {
			header('Location: ../'.$redirect.'&err=edit');
			exit;
		}

		header('Location: ../'.$redirect.'&suc=edit');
		exit;
	}
} else{
	header('Location: ../'.$redirect.'&err=fill');
	exit;
}

?>