<?php
require_once 'db_connect.php';
require_once 'functions.php';
ob_start();
sec_session_start();

$redirect = $_POST['redirect'];

$author = $_POST['author'];
$name = $_POST['name'];
$price = $_POST['price'];
$download_count = $_POST['download_count'];
$date = $_POST['date'];
$time = $_POST['time'];
$status = $_POST['status'];


$statement = 'SELECT * FROM posts WHERE';

if (!empty($author)){ $statement .= ' author = "'.$author.'" AND'; }
if (!empty($name)){ $statement .= ' name LIKE "%'.$name.'%" AND'; }
if (!empty($price)){ $statement .= ' price = "'.$price .'" AND'; }
if (!empty($download_count)){ $statement .= ' download_count = "'.$download_count .'" AND'; }
if (!empty($date)){ $statement .= ' date LIKE "%'.$date .'%" AND'; }
if (!empty($time)){ $statement .= ' time LIKE "%'.$time .'%" AND'; }
if (!empty($status)){ $statement .= ' status LIKE "%'.$status .'%" AND'; }

if (substr($statement,strlen($statement)-3,strlen($statement))=="AND"){
	$statement = substr($statement,0,strlen($statement)-3);
}

if (substr($statement,strlen($statement)-5,strlen($statement))=="WHERE"){
	$statement = substr($statement,0,strlen($statement)-5);
}


$result = $mysqli->query($statement);

$num_rows = $result->num_rows;

if ($num_rows > 0){
	header('Location: ../'.$redirect.'&suc=search&search=true&author='.$author.'&'.'name='.$name.'&'.'price='.$price.'&'.'download_count='.$download_count.'&'.'date='.$date.'&'.'time='.$time.'&status='.$status);
	exit;
}  else{
	header('Location: ../'.$redirect.'&err=no-match');
	exit;
}

?>