<?php
require_once 'db_connect.php';
require_once 'functions.php';


$author = $_REQUEST['author'];
$post = $_REQUEST['post'];
$comment = $_REQUEST['comment'];
$date = $_REQUEST['date'];
$time = $_REQUEST['time'];
$status = $_REQUEST['status'];


if (ifallisset($author, $post, $comment, $date, $time)){
		
	if ($stmt = $mysqli->prepare("INSERT INTO posts_comments (author, post, comment, date, time, status, timestamp) VALUES (?, ?, ?, ?, ?, ?, ?)")){;
	
		$stmt->bind_param('sssssss', $author, $post, $comment, $date, $time, $status, time());
				
		if (! $stmt->execute()) {
			header('Location: ../posts_comment_add.php?err=insert');
			exit;
		}
		
		header('Location: ../posts_comment_add.php?suc=insert');
		exit;
	}
	else{
		header('Location: ../posts_comment_add.php?err=fill');
		exit;
	}
	

}
else{
	header('Location: ../posts_comment_add.php?err=fill');
	exit;
}
?>