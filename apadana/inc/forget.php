<?php

require_once 'db_connect.php';
require_once 'functions.php';
require_once 'information.php';

//$redirect = $_POST['redirect'];
$email = $_POST['email'];

if (!isset($redirect)){
	$redirect = "forget.php?";
}

if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
	header('Location: ../forget.php?err=invalid-email');
	exit;
}

$prep_stmt = "SELECT id FROM members WHERE email = ? LIMIT 1";
$stmt = $mysqli->prepare($prep_stmt);

if ($stmt) {

	$stmt->bind_param('s', $email);
	$stmt->execute();
	$stmt->store_result();

	if ($stmt->num_rows < 1) {
		header('Location: ../forget.php?err=email');
		exit;
	}

}


$stmt = $mysqli->prepare("SELECT username, password, mobile_number FROM members WHERE email = ? LIMIT 1");
$stmt->bind_param('s', $email); 
$stmt->execute();
$stmt->store_result();
$stmt->bind_result($username, $password, $mobile_number);
$stmt->fetch();



// E-MAIL
			
$to = $email;
$subject = "بازنشانی گذرواژه";

$html_body = str_replace("%RESET_LINK_URL%", $options["url"]."/inc/reset.php?username=".$username."&password=".$password, str_replace("%MOBILE_NUMBER%", $mobile_number, str_replace("%EMAIL%", $email, str_replace("%USERNAME%", $username, $options['forget_email_text']))));
echo $html_body;exit;
send_email($to, $subject, $html_body, "", "normal");

header('Location: ../'.$redirect.'&suc=check-email');
exit;

?>