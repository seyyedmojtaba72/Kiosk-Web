<?php
require_once 'db_connect.php';
require_once 'functions.php';

$redirect = $_REQUEST['redirect'];
$id = $_REQUEST['id'];

$author = $_REQUEST['author'];
$post = $_REQUEST['post'];
$report = $_REQUEST['report'];
$date = $_REQUEST['date'];
$time = $_REQUEST['time'];


if (ifallisset($author, $post, $report, $date, $time)){
	
	if ($stmt = $mysqli->prepare("UPDATE posts_reports SET author=?, post=?, report=?, date=?, time=? WHERE id=?")){;
	
		$stmt->bind_param('ssssss', $author, $post, $report, $date, $time, $id);
				
		if (! $stmt->execute()) {
			header('Location: ../'.$redirect.'&err=edit');
			exit;
		}
		
		header('Location: ../'.$redirect.'&suc=edit');
		exit;
	}
} else{
	header('Location: ../'.$redirect.'&err=fill');
	exit;
}

?>