<?php
require_once 'db_connect.php';
require_once 'functions.php';

$username= filter_input(INPUT_GET, 'username', $filter = FILTER_SANITIZE_STRING);
$password= filter_input(INPUT_GET, 'password', $filter = FILTER_SANITIZE_STRING);
$hash= filter_input(INPUT_GET, 'hashed', $filter = FILTER_SANITIZE_STRING);

if (empty($username) || empty($password)){ header('Location: ../login.php?err=invalid-reset'); exit; }

if ($stmt = $mysqli->prepare("SELECT password, salt FROM members WHERE username = ? LIMIT 1")) {

	$stmt->bind_param('s', $username); 
	$stmt->execute(); 
	$stmt->store_result();

	$stmt->bind_result($db_password , $salt);
	$stmt->fetch();


	if ($password == $db_password){
		
		$password = randomPassword(10);
		
		$cost = 10;	
		$random_salt = strtr(base64_encode(mcrypt_create_iv(16, MCRYPT_DEV_URANDOM)), '+', '.');
		$random_salt = sprintf("$2a$%02d$", $cost) . $random_salt;
		$hash = crypt($password, $random_salt);
		
		$stmt = $mysqli->prepare("UPDATE members SET password=?, salt=? WHERE username=?");
		$stmt->bind_param('sss',$hash ,$random_salt, $username);
		
		$stmt->execute();	
	
	
		header('Location: ../login.php?suc=reset&password='.$password);
		exit;

			
	} else {
		header('Location: ../login.php?err=invalid-reset');
		exit;
		
	}

}

		
?>