<?php
require_once 'db_connect.php';
require_once 'functions.php';
require_once 'information.php';

$redirect = $_REQUEST['redirect'];
$id = $_POST['id'];
$user = $_POST['user'];
$price = $_POST['price'];
$date = $_POST['date'];
$time = $_POST['time'];
$status = $_POST['status'];

if (ifallisset($user, $price, $status)){	
		
	if ($stmt = $mysqli->prepare("UPDATE checkout_requests SET user=?, price=?, date=?, time=?, status=? WHERE id=?")){
		
		$result = mysql_select('checkout_requests', array('id'=>$id), array('status'));
		$former_status = $result[0]['status'];
	
		$stmt->bind_param('ssssss', $user, $price, $date, $time, $status, $id);
				
		if (! $stmt->execute()) {
			header('Location: ../'.$redirect.'&err=edit');
			exit;
		}

		
		$result = mysql_select('members', array('id'=>$user), array('email', 'mobile_number', 'mobile_number_status'));
			
		$email = $result[0]['email'];
		$mobile_number = $result[0]['mobile_number'];
		$mobile_number_status = $result[0]['mobile_number_status'];
		
		
		
		if ($status=="done" && $former_status != $status){
			if ($mobile_number_status == "1"){
				// SMS
				
				$sms_body = $options['checkout_done_sms_text'];
				send_sms($mobile_number, $sms_body);
			}
			
			
			// EMAIL
			
			$email_body = $options['checkout_done_email_text'];
			send_email($email, "تکمیل تصفیه حساب", $email_body);
		}
			
		
		header('Location: ../'.$redirect.'&suc=edit');
		exit;
	}
} else{
	header('Location: ../'.$redirect.'&err=fill');
	exit;
}

?>