<?php
require_once 'db_connect.php';
require_once 'functions.php';

$redirect = $_REQUEST['redirect'];
$id = $_POST['id'];
$author = $_POST['author'];
$name = $_POST['name'];
$description = $_POST['description'];
$image = $_POST['image'];
$price = $_POST['price'];
$download_link = $_POST['download_link'];
$download_count = $_POST['download_count'];
$date = $_POST['date'];
$time = $_POST['time'];
$status = $_POST['status'];
$timestamp = time();

if (ifallisset($author, $image, $price, $status)){	
		
	if ($stmt = $mysqli->prepare("UPDATE posts SET author=?, name=?, description=?, image=?, price=?, download_link=?, download_count=?, date=?, time=?, status=? WHERE id=?")){
		
	
		$stmt->bind_param('sssssssssss', $author, $name, $description, $image, $price, $download_link, $download_count, $date, $time, $status, $id);
				
		if (! $stmt->execute()) {
			header('Location: ../'.$redirect.'&err=edit');
			exit;
		}
		
		header('Location: ../'.$redirect.'&suc=edit');
		exit;
	}
} else{
	header('Location: ../'.$redirect.'&err=fill');
	exit;
}

?>