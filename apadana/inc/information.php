<?php
require_once('db_connect.php');

$title_divider_character = "|";

$member_roles = array(
	"user" => "کاربر",
	"admin" => "مدیر",
);

$member_permissions = array(
	"edit_posts" => "می‌تواند مطالب را ویرایش کند",
	"edit_checkout_requests" => "می‌تواند درخواست‌های تصفیه را ویرایش کند",
	"edit_members_statistics" => "می‌تواند آمار اعضا را ویرایش کند",
	"edit_users_reports" => "می‌تواند گزارشات کاربران را ویرایش کند",
	"edit_payments" => "می‌تواند پرداخت‌ها را ویرایش کند",
	"edit_discounts" => "می‌تواند تخفیف‌ها را ویرایش کند",
	"edit_emails" => "می‌تواند ایمیل‌ها را ویرایش کند",
	"edit_messages" => "می‌تواند پیام‌ها را ویرایش کند",
	"edit_text_messages" => "می‌تواند پیامک‌ها را ویرایش کند",
	"manage_files" => "می‌تواند فایل‌ها را مدیریت کند",
	"upload_file" => "می‌تواند فایل آپلود کند",
	"view_statistic" => "می‌تواند آمار را مشاهده کند",
	"edit_options" => "می‌تواند تنظیمات را ویرایش کند",
);

$member_levels = array(
	"normal" => "معمولی",
	"vip" => "VIP",
);

$member_statuses = array(
	"inactive" => "غیرفعال",
	"active" => "فعال",
);

$member_extras = array(
	"trusted" => "مورد اطمینان",
	"private" => "حساب خصوصی",
);

$mobile_number_statuses = array(
	"0" => "تایید نشده",
	"1" => "تایید شده",
);

$post_statuses = array(
	"published" => "متتشر شده",
	"deleted" => "حذف شده",
);

$comment_statuses = array(
	"published" => "منتشر شده",
	"spam" => "جفنگ",
	"deleted" => "حذف شده",
);

$checkout_request_statuses = array(
	"not-checked" => "بررسی نشده",
	"processing" => "در حال بررسی",
	"done" => "انجام شده",
);

$follow_request_statuses = array(
	"waiting" => "در حال انتظار",
	"accepted" => "قبول شده",
	"blocked" => "بلاک شده",
);

$user_report_subjects = array(
	"user_fake" => "حساب جعلی",
	"user_content" => "محتوای نامناسب",
	"user_abuse" => "سوء رفتار",
	"bug_android" => "باگ نرم افزاری اندروید",
	"feedback" => "بازخورد",
);

$user_report_statuses = array(
	"not-checked" => "بررسی نشده",
	"processing" => "در حال بررسی",
	"done" => "انجام شده",
);

$payment_statuses = array(
	"success" => "موفق",
	"fail" => "ناموفق",
);

$discount_statuses = array(
	"active" => "فعال",
	"inactive" => "غیرفعال",
);

$payment_actions = array(
	"pay_order" => "پرداخت سفارش",
	"charge_balance" => "شارژ موجودی",
);

// --------------------

$options = array();

$stmt = $mysqli->prepare("select count(1) FROM options");

$stmt->execute();
$stmt->bind_result($rows);
$stmt->fetch();
$stmt->close();

for ($i=0;$i<$rows;$i++){
	$stmt = $mysqli->prepare("SELECT variable, value FROM options LIMIT 1 OFFSET ?");
	$stmt->bind_param('s', $i); 
	$stmt->execute(); 
	$stmt->store_result();

	$stmt->bind_result($variable, $value);
	$stmt->fetch();
	$stmt->close();
	
	$options[$variable]=$value;
}

// --------------------

$parameters = array(

	"SYSTEM_HOME_URL" => $options["SYSTEM_HOME_URL"],
	"API_HOME_URL" => $options["API_HOME_URL"],
	"BACKUP_API_HOME_URL" => $options["BACKUP_API_HOME_URL"],
	"PROFILE_IMAGES_URL" => $options["PROFILE_IMAGES_URL"],
	"GROUP_IMAGES_URL" => $options["GROUP_IMAGES_URL"],
	"USERS_UPLOADS_URL" => $options["USERS_UPLOADS_URL"],
	"FOOTER_DESCRIPTION" => $options["FOOTER_DESCRIPTION"],
	"POSTS_BUFFER_ITEMS" => $options["POSTS_BUFFER_ITEMS"],
	"COMMENTS_BUFFER_ITEMS" => $options["COMMENTS_BUFFER_ITEMS"],
	"USERS_BUFFER_ITEMS" => $options["USERS_BUFFER_ITEMS"],
	"HISTORY_BUFFER_ITEMS" => $options["HISTORY_BUFFER_ITEMS"],
	"CHATS_MESSAGES_BUFFER_ITEMS" => $options["CHATS_MESSAGES_BUFFER_ITEMS"],
	"HISTORY_REFRESH_INTERVAL" => $options["HISTORY_REFRESH_INTERVAL"],
	"MAX_PROFILE_IMAGE_SIZE" => $options["MAX_PROFILE_IMAGE_SIZE"],
	"MAX_UPLOAD_FILE_SIZE" => $options["MAX_UPLOAD_FILE_SIZE"],
	
);

$in_app_skus = array(
	"CHARGE_10000" => 6000, 
	"CHARGE_20000" => 12000, 
	"CHARGE_50000" => 30000, 
	"CHARGE_100000" => 60000, 
	"CHARGE_200000" => 120000, 
	"CHARGE_500000" => 300000, 
	"CHARGE_1000000" => 600000, 
	
);

?>