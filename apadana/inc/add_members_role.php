<?php
include_once 'db_connect.php';
include_once 'functions.php';

$redirect = $_REQUEST['redirect'];
$slug = $_REQUEST['slug'];
$role_title = $_REQUEST['role_title'];
$permissions = $_REQUEST['permissions'];

if (ifallisset($slug, $role_title, $permissions)){
	
	$statement = 'SELECT * FROM roles WHERE slug="'.$slug.'"';

	$result = $mysqli->query($statement);
	
	$num_rows = $result->num_rows;
	
	if ($num_rows > 0){
		header('Location: ../err=exists');
		exit;
	} else {
		
		if ($stmt = $mysqli->prepare("INSERT INTO members_roles (slug, role_title, permissions) VALUES (?, ?, ?)")){
		
			$stmt->bind_param('sss', $slug, $role_title, $permissions);
					
			if (! $stmt->execute()) {
				header('Location: ../'.$redirect.'&err=insert');
				exit;
			}
			
			header('Location: ../'.$redirect.'&suc=insert');
			exit;
		} 
		else{
			header('Location: ../'.$redirect.'&err=fill');
			exit;
		}

	}
	
}
else{
	header('Location: ../err=fill');
	exit;
}
?>