<?php
require_once 'db_connect.php';
require_once 'functions.php';
ob_start();
sec_session_start();

$redirect = $_POST['redirect'];

$code = $_POST['code'];
$title = $_POST['title'];
$percentage = $_POST['percentage'];
$number = $_POST['number'];
$start_date = $_POST['start_date'];
$end_date = $_POST['end_date'];
$status = $_POST['status'];

$statement = 'SELECT * FROM discounts WHERE';

if (!empty($code)){ $statement .= ' code = "'.$code .'" AND'; }
if (!empty($title)){ $statement .= ' title LIKE "%'.$title.'%" AND'; }
if (!empty($percentage)){ $statement .= ' percentage = "'.$percentage .'" AND'; }
if (!empty($number)){ $statement .= ' number = "'.$number .'" AND'; }
if (!empty($start_date)){ $statement .= ' start_date LIKE "%'.$start_date .'%" AND'; }
if (!empty($end_date)){ $statement .= ' end_date LIKE "%'.$end_date .'%" AND'; }
if (!empty($status)){ $statement .= ' status = "'.$status .'" AND'; }


if (substr($statement,strlen($statement)-3,strlen($statement))=="AND"){
	$statement = substr($statement,0,strlen($statement)-3);
}

if (substr($statement,strlen($statement)-5,strlen($statement))=="WHERE"){
	$statement = substr($statement,0,strlen($statement)-5);
}

$result = $mysqli->query($statement);

$num_rows = $result->num_rows;

if ($num_rows > 0){
	header('Location: ../'.$redirect.'&suc=search&search=true&code='.$code.'&'.'title='.$title.'&'.'percentage='.$percentage.'&'.'number='.$number.'&'.'start_date='.$start_date.'&'.'end_date='.$end_date.'&'.'status='.$status);
	exit;
}  else{
	header('Location: ../'.$redirect.'&err=no-match');
	exit;
}

?>