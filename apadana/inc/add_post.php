<?php
require_once 'db_connect.php';
require_once 'functions.php';

$redirect = $_POST['redirect'];
$author = $_POST['author'];
$name = $_POST['name'];
$description = $_POST['description'];
$image = $_POST['image'];
$price = $_POST['price'];
$download_link = $_POST['download_link'];
$download_count = $_POST['download_count'];
$date = $_POST['date'];
$time = $_POST['time'];
$status = $_POST['status'];
$timestamp = time();

if (ifallisset($author, $image, $price, $status)){		

	if ($stmt = $mysqli->prepare("INSERT INTO posts (author, name, description, image, price, download_link, download_count, date, time, status, timestamp) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")){
	
		$stmt->bind_param('sssssssssss', $author, $name, $description, $image, $price, $download_link, $download_count, $date, $time, $status, $timestamp);
				
		if (! $stmt->execute()) {
			header('Location: ../'.$redirect.'&err=insert');
			exit;
		}
		
		header('Location: ../'.$redirect.'&suc=insert');
		exit;
	}
	else{
		header('Location: ../'.$redirect.'&err=fill');
		exit;
	}
	

}
else{
	header('Location: ../'.$redirect.'&err=fill');
	exit;
}
?>