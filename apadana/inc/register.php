<?php
require_once 'db_connect.php';
require_once 'information.php';
require_once 'lib/recaptcha.lib.php';
require_once 'functions.php';

		

$privatekey = "6LdFOPASAAAAAADDtpH6UpRlwAQJVn5XjXj_VpNV";
$resp = recaptcha_check_answer ($privatekey,
							 $_SERVER["REMOTE_ADDR"],
							 $_POST["recaptcha_challenge_field"],
							 $_POST["recaptcha_response_field"]);

$error_msg = "";
$default_role = DEFAULT_ROLE;
$username = $_POST['username'];

$email = $_POST['email'];
$mobile_number = $_POST['mobile_number'];
$presenter = $_POST['presenter'];
$reward_percentage = $options['reward_percentage'];

if (!$resp->is_valid) {
	// What happens when the CAPTCHA was entered incorrectly
	header('Location: ../register.php?err=incorrect-key');
	exit;
}

if (!isset($_POST['rules'])) {
	header('Location: ../register.php?err=check-rules');
	exit;
}


if (isset($_POST['username'], $_POST['email'], $_POST['password'])) {
    $username = strtolower(filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING));
    $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
    $password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);
    
    
	if (!preg_match('/^[a-z0-9_.-]{1,50}$/', $username) || !preg_match('/\w/', $username)){
		header('Location: ../register.php?err=invalid-username');
		exit;
	}

	
	if (strlen($username) < 6 || strlen($password) < 6){
		header('Location: ../register.php?err=min-length');
		exit;
	}
	
	if (strlen($username) > 50){
		header('Location: ../register.php?err=max-username-length');
		exit;
	}

    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
		header('Location: ../register.php?err=invalid-email');
		exit;
    }
	
	if (strlen($email) > 50){
		header('Location: ../register.php?err=max-email-length');
		exit;
	}
	
	if (!empty($mobile_number)){
		if (strlen($mobile_number)!=10){
			header('Location: ../register.php?err=mobile_number-length');
			exit;
		}
		
	}
 
    
	
$prep_stmt = "SELECT id FROM members WHERE username = ? LIMIT 1";
    $stmt = $mysqli->prepare($prep_stmt);
 	
    if ($stmt) {
        $stmt->bind_param('s', $username);
        $stmt->execute();
        $stmt->store_result();
 		
        if ($stmt->num_rows == 1) {			
			header('Location: ../register.php?err=username-exists');
			exit;
        }
	} else {
		header('Location: ../register.php?err=database');
		exit;
    }
	 
    $prep_stmt = "SELECT id FROM members WHERE email = ? LIMIT 1";
    $stmt = $mysqli->prepare($prep_stmt);
 
    if ($stmt) {
        $stmt->bind_param('s', $email);
        $stmt->execute();
        $stmt->store_result();
 
        if ($stmt->num_rows == 1) {
			header('Location: ../register.php?err=email-exists');
			exit;
        }
    } else {
		header('Location: ../register.php?err=database');
		exit;
    }
    
    if (!empty($mobile_number)){
	
	    $prep_stmt = "SELECT id FROM members WHERE mobile_number = ? LIMIT 1";
	    $stmt = $mysqli->prepare($prep_stmt);
	 
	    if ($stmt) {
		$stmt->bind_param('s', $mobile_number);
		$stmt->execute();
		$stmt->store_result();
	 
		if ($stmt->num_rows == 1) {
				header('Location: ../register.php?err=mobile_number-exists');
				exit;
		}
			
		} else {
			header('Location: ../register.php?err=database');
			exit;
	    }
    
    }
 
    if (empty($error_msg)) {
		$cost = PASSWORD_COST;	
		$random_salt = strtr(base64_encode(mcrypt_create_iv(16, MCRYPT_DEV_URANDOM)), '+', '.');
		$random_salt = sprintf("$2a$%02d$", $cost) . $random_salt;
		$hash = crypt($password, $random_salt);
 		$verification = 'active';
		
        if ($insert_stmt = $mysqli->prepare('INSERT INTO members (username, email, mobile_number, mobile_number_status, password, salt, role, level, status, presenter, reward_percentage, timestamp) VALUES ("'.$username.'", "'.$email.'", "'.$mobile_number.'", "1", "'.$hash.'", "'.$random_salt.'", "'.$default_role.'", "normal", "'.$verification.'", "'.$presenter.'", "'.$reward_percentage.'", "'.time().'")')) {
			
            if (! $insert_stmt->execute()) {
                header('Location: ../register.php?err=insert');
				exit;
            }
			
			
			// E-MAIL
			
			/*$subject = "فعال‌سازی حساب";
			
			$html_body = str_replace("%ACTIVATION_LINK_URL%", $options["url"].'/inc/activate.php?username='.$username.'&password='.$hash, str_replace("%MOBILE_NUMBER%", $mobile_number, str_replace("%EMAIL%", $email, str_replace("%USERNAME%", $username, $options['registeration_email_text']))));
		
			
			send_email($email, $subject, $html_body);*/
			
			$subject = "خوش‌آمدید";
	
			$html_body = str_replace("%MOBILE_NUMBER%", $mobile_number, str_replace("%EMAIL%", $email, str_replace("%USERNAME%", $username, $options['welcome_email_text'])));
		
			send_email($email, $subject, $html_body, "", "normal");


			header('Location: ../login.php?suc=register');
			exit;
        } 
    }
} else {
	header('Location: ../register.php?err=fill');
	exit;
}

?>