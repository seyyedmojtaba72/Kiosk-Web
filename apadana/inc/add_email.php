<?php
require_once 'db_connect.php';
require_once 'functions.php';
require_once 'information.php';

$redirect = $_POST['redirect'];
$receiver = $_POST['receiver'];
$subject = $_POST['subject'];
$email_body = htmlspecialchars($_POST['email_body']);
$date = $_POST['date'];
$time = $_POST['time'];
$timestamp = $_POST['timestamp'];

if (ifallisset($receiver, $subject, $email_body, $date, $time)){	

	if ($stmt = $mysqli->prepare("INSERT INTO emails (receiver, subject, email_body, date, time, timestamp) VALUES (?, ?, ?, ?, ?, ?)")){
	
		$stmt->bind_param('ssssss', $receiver, $subject, $email_body, $date, $time, $timestamp);
				
		if (! $stmt->execute()) {
			header('Location: ../'.$redirect.'&err=insert');
			exit;
		}

		
		header('Location: ../'.$redirect.'&suc=insert');
		exit;
	}
	else{
		header('Location: ../'.$redirect.'&err=fill');
		exit;
	}
	

}
else{
	header('Location: ../'.$redirect.'&err=fill');
	exit;
}
?>