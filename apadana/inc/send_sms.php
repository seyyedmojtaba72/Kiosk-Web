<?php
require_once 'db_connect.php';
require_once 'functions.php';
require_once 'information.php';

$redirect = $_POST['redirect'];
$receiver = array_unique($_POST['receiver']);
$text = $_POST['text'];

if (ifallisset($receiver, $text)){
	
	for($i=0; $i<sizeof($receiver); $i++){	
		$receiver1 = $receiver[$i];		
		send_sms($receiver1, $text);
	}
	
	header('Location: ../'.$redirect.'&suc=send');
} else {
	header('Location: ../'.$redirect.'&err=fill');
	exit;
}
?>