<?php
include_once 'db_connect.php';
include_once 'config.php';
include_once 'functions.php';

$redirect = $_POST['redirect'];
if(!isset($redirect)){
	$redirect = "profile.php?";
}

$admin = $_POST['admin'];
if(!isset($admin)){
	$admin = "FALSE";
}
$password = $_POST['password'];
$confirm_password = $_POST['confirm_password'];
$email = $_POST['email'];
$salt = $_POST['salt'];
$former_password = $_POST['former_password'];
$old_password = $_POST['old_password'];



if (strlen($password) < 6){
	header('Location: '.$redirect.'&err=min-length');
	exit;
}


$old_password = crypt($old_password,$salt);
if ($admin != "TRUE"){
	if ($old_password!=$former_password){
		header('Location: '.$redirect.'&err=con-oldpsw');
		exit;
	}
}

$cost = PASSWORD_COST;	
$random_salt = strtr(base64_encode(mcrypt_create_iv(16, MCRYPT_DEV_URANDOM)), '+', '.');
$random_salt = sprintf("$2a$%02d$", $cost) . $random_salt;
$hash = crypt($password, $random_salt);

$insert_stmt = $mysqli->prepare("UPDATE members SET password=?, salt=? WHERE email=?");
$insert_stmt->bind_param('sss',$hash ,$random_salt, $email);
$insert_stmt->execute();	

if ($admin == "TRUE"){
	header('Location: ../'.$redirect.'&suc=password');
	exit;
} else {
	header('Location: ../login.php?suc=password');
	exit;
}

?>