<?php
require_once 'db_connect.php';
require_once 'information.php';
require_once 'functions.php';

$redirect = $_REQUEST['redirect'];
$username = $_REQUEST['username'];
$email = $_REQUEST['email'];
$password = $_REQUEST['password'];
$confirm_password = $_REQUEST['confirm_password'];
$role = $_REQUEST['role'];
$level = $_REQUEST['level'];
$status = $_REQUEST['status'];
$first_name = $_REQUEST['first_name'];
$last_name = $_REQUEST['last_name'];
$mobile_number = $_REQUEST['mobile_number'];
$mobile_number_status = $_REQUEST['mobile_number_status'];
$home_number = $_REQUEST['home_number'];
$fax_number = $_REQUEST['fax_number'];
$office = $_REQUEST['office'];
$shaba = $_REQUEST['shaba'];
$address = $_REQUEST['address'];
$presenter = $_REQUEST['presenter'];
$balance = $_REQUEST['balance'];
$reward_percentage = $_REQUEST['reward_percentage'];
$about = $_REQUEST['about'];
$extras[] = $_POST['extras'];

$extras_text = "";

foreach($extras[0] as $extra){
	$extras_text .= $extra.";";
}

if (ifallisset($username, $email, $password, $confirm_password)){
	
	
	$username = strtolower(filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING));
	if (!preg_match('/^[a-z0-9_.-]{1,50}$/', $username) || !preg_match('/\w/', $username)){
		header('Location: ../'.$redirect.'&err=invalid-username');
		exit;
	}
	    $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
	    $email = filter_var($email, FILTER_VALIDATE_EMAIL);
	    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			header('Location: ../'.$redirect.'&err=invalid-email');
			exit;
	    }
	
	if (!empty($mobile_number)){
		if (strlen($mobile_number)!=10){
			header('Location: ../'.$redirect.'&err=mobile_number-length');
			exit;
		}	
		
		$prep_stmt = "SELECT id FROM members WHERE mobile_number = ? LIMIT 1";
		$stmt = $mysqli->prepare($prep_stmt);
	 
		if ($stmt) {
			$stmt->bind_param('s', $mobile_number);
			$stmt->execute();
			$stmt->store_result();
	 
			if ($stmt->num_rows >= 1) {
				header('Location: ../'.$redirect.'&err=mobile_number-exists');
				exit;
			}
		} else {
			header('Location: ../'.$redirect.'&err=database');
			exit;
		}
	}
	
	$password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);
	
	$prep_stmt = "SELECT id FROM members WHERE username = ? LIMIT 1";
    $stmt = $mysqli->prepare($prep_stmt);
 	
    if ($stmt) {
        $stmt->bind_param('s', $username);
        $stmt->execute();
        $stmt->store_result();
 		
        if ($stmt->num_rows == 1) {			
			header('Location: ../'.$redirect.'&err=username-exists');
			exit;
        }
	} else {
		header('Location: ../'.$redirect.'&err=database');
		exit;
    }
	 
    $prep_stmt = "SELECT id FROM members WHERE email = ? LIMIT 1";
    $stmt = $mysqli->prepare($prep_stmt);
 
    if ($stmt) {
        $stmt->bind_param('s', $email);
        $stmt->execute();
        $stmt->store_result();
 
        if ($stmt->num_rows == 1) {
			header('Location: ../'.$redirect.'&err=email-exists');
			exit;
        }
    } else {
		header('Location: ../'.$redirect.'&err=database');
		exit;
    }
	

	$cost = 10;	
	$random_salt = strtr(base64_encode(mcrypt_create_iv(16, MCRYPT_DEV_URANDOM)), '+', '.');
	$random_salt = sprintf("$2a$%02d$", $cost) . $random_salt;
	$hash = crypt($password, $random_salt);
	
	$insert_stmt = $mysqli->prepare("INSERT INTO members (username, email, password, salt, role, level, status, first_name, last_name, mobile_number, mobile_number_status, home_number, fax_number, office, shaba, address, presenter, balance, reward_percentage, about, extras, timestamp) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
	$insert_stmt->bind_param('ssssssssssssssssssssss', $username, $email, $hash, $random_salt, $role, $level, $status, $first_name, $last_name, $mobile_number, $mobile_number_status, $home_number, $fax_number, $office, $shaba, $address, $presenter, $balance, $reward_percentage, $about, $extras_text, time());
	
	if (! $insert_stmt->execute()) {
		header('Location: ../'.$redirect.'&err=insert');
		exit;
	}
	
	if ($status == "active"){
		// E-MAIL
				
		$to = $email;
		$subject = "خوش‌آمدید";
		
		$html_body = str_replace("%MOBILE_NUMBER%", $mobile_number, str_replace("%PASSWORD%", $hash, str_replace("%EMAIL%", $email, str_replace("%USERNAME%", $username, $options['welcome_email_text']))));
	
		
		send_email($to, $subject, $html_body, "", "");

	}
	
	if ($mobile_number_status == "1"){
		// SMS
			
		$to = $mobile_number;		
		$sms_body = str_replace("%NAME%", $name, str_replace("%USERNAME%", $username, $options['welcome_sms_text']));
		
		send_sms($to, $sms_body);
	}
	
	header('Location: ../'.$redirect.'&suc=insert');
	exit;

} else {
	header('Location: ../'.$redirect.'&err=fill');
	exit;
}
?>