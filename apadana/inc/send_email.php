<?php
require_once 'db_connect.php';
require_once 'functions.php';
require_once 'information.php';

$redirect = $_POST['redirect'];
$receiver = array_unique($_POST['receiver']);
$subject = $_POST['subject'];
$email_body = ($_POST['email_body']);


if (ifallisset($receiver, $subject, $email_body)){
	for($i=0; $i<sizeof($receiver); $i++){	
		$receiver1 = $receiver[$i];	
		send_email($receiver1, $subject, $email_body);
	}
	
	header('Location: ../'.$redirect.'&suc=send');
} else {
	header('Location: ../'.$redirect.'&err=fill');
	exit;
}
?>