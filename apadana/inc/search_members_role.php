<?php
include_once 'db_connect.php';
include_once 'functions.php';
ob_start();
sec_session_start();

$redirect = $_POST['redirect'];
$slug = $_REQUEST['slug'];
$role_title = $_REQUEST['role_title'];
$permissions = $_REQUEST['permissions'];

$statement = 'SELECT * FROM members_roles WHERE';

if (!empty($slug)){ $statement .= ' slug LIKE "%'.$slug .'%" AND'; }
if (!empty($role_title)){ $statement .= ' role_title LIKE "%'.$role_title.'%" AND'; }
if (!empty($permissions)){ $statement .= ' permissions LIKE "%'.$permissions.'%" AND'; }


if (substr($statement,strlen($statement)-3,strlen($statement))=="AND"){
	$statement = substr($statement,0,strlen($statement)-3);
}

if (substr($statement,strlen($statement)-5,strlen($statement))=="WHERE"){
	$statement = substr($statement,0,strlen($statement)-5);
}


$result = $mysqli->query($statement);

$num_rows = $result->num_rows;

if ($num_rows > 0){
	header('Location: ../'.$redirect.'&suc=search&search=true&slug='.$slug.'&role_title='.$role_title.'&permissions='.$permissions);
	exit;
}  else{
	header('Location: ../'.$redirect.'&err=no-match');
	exit;
}



?>