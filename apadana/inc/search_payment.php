<?php
require_once 'db_connect.php';
require_once 'functions.php';
ob_start();
sec_session_start();

$redirect = $_POST['redirect'];

$customer = $_POST['customer'];
$transaction_id = $_POST['transaction_id'];
$action = $_POST['action'];
$amount = $_POST['amount'];
$date = $_POST['date'];
$time = $_POST['time'];
$status = $_POST['status'];

$statement = 'SELECT * FROM payments WHERE';

if (!empty($customer)){ $statement .= ' customer = "'.$customer .'" AND'; }
if (!empty($transaction_id)){ $statement .= ' transaction_id = "'.$transaction_id.'" AND'; }
if (!empty($amount)){ $statement .= ' amount = "'.$amount .'" AND'; }
if (!empty($action)){ $statement .= ' action = "'.$action .'" AND'; }
if (!empty($date)){ $statement .= ' date LIKE "%'.$date .'%" AND'; }
if (!empty($time)){ $statement .= ' time = "'.$time .'" AND'; }
if (!empty($status)){ $statement .= ' status = "'.$status .'" AND'; }


if (substr($statement,strlen($statement)-3,strlen($statement))=="AND"){
	$statement = substr($statement,0,strlen($statement)-3);
}

if (substr($statement,strlen($statement)-5,strlen($statement))=="WHERE"){
	$statement = substr($statement,0,strlen($statement)-5);
}


$result = $mysqli->query($statement);

$num_rows = $result->num_rows;

if ($num_rows > 0){
	header('Location: ../'.$redirect.'&suc=search&search=true&customer='.$customer.'&'.'transaction_id='.$transaction_id.'&'.'amount='.$amount.'action='.$action.'&'.'date='.$date.'&'.'time='.$time.'&'.'status='.$status);
	exit;
}  else{
	header('Location: ../'.$redirect.'&err=no-match');
	exit;
}

?>