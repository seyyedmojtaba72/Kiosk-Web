<?php 
include('header.php'); 
?>

<?php if (if_has_permission($role,"edit_text_messages")){} else{header("Location: login.php?redirect=".urlencode("http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']));} ?>

<?php
if (!empty($err)) {
	switch ($err) {
	case "insert" : $err_msg = "خطا در نوشتن دیتابیس!"; break;
	case "fill" : $err_msg = "موارد الزامی را پر کنید!"; break;
	case "exists" : $err_msg = "پیامک وجود دارد!"; break;
	
	default : $err_msg = "خطای غیر منتظره‌ای پیش آمده!"; break;
	}
	
}
?> 
<?php
if (!empty($suc)) {
	switch ($suc) {
	case "insert" : $suc_msg = "پیامک اضافه شد!"; break;
	
	default : $suc_msg = "عملیات با موفقیت انجام شد!"; break;
	}
	
}
?> 
<?php
if (!empty($err_msg)) {
	echo '<div class="alert alert-error no-print"><a href="'.str_replace('err='.$err,"",implode('&',array_unique(explode('&',$_SERVER['REQUEST_URI'])))).'"><button type="button" class="close">&times;</button></a><p>'. $err_msg .'</p></div>';
}
?>
<?php
if (!empty($suc_msg)) {
	echo '<div class="alert alert-success no-print"><a href="'.str_replace('suc='.$suc,"",implode('&',array_unique(explode('&',$_SERVER['REQUEST_URI'])))).'"><button type="button" class="close">&times;</button></a><p>'. $suc_msg .'</p></div>';
}
?>

<?php
if (empty($_SESSION['text_messages_redirect'])){$_SESSION['text_messages_redirect']="text_messages.php";}
?>

<div class="container">
	<a href="<?php echo $_SESSION['text_messages_redirect'] ?>"><button class='pull-left btn'><span>بازگشت</span> <i class="icon-chevron-left"></i></button></a>
	<button class="btn disabled"><span id="subtitle">اضافه‌کردن پیامک</span></button><br /><br />
	<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert">&times;</button>
	<p>مواردی که با علامت ستاره‌ی قرمز مشخص شده‌ است، الزامی است.</p></div>
	<div id="main" class="span8 pull-right">
		<form action="<?php echo $options["url"] ?>/inc/add_text_message.php" method="post">
        	<input type="hidden" name="redirect" id="redirect" value="text_message_add.php?" />
        	<input type="hidden" value="<?php echo time(); ?>" name="timestamp" id="timestamp" />
			<table class="table table-striped table-right">
			<tr>
				<td class="span2"><h5 class="normal">دریافت‌کننده <span class="red">*</span></h5></td>
					<td style="padding: 5px 0 0 0;">
                    	<select class="chosen-select chosen-rtl" name="receiver" id="receiver" data-placeholder="کاربری را انتخاب کنید.">
                        	<option value="" selected="selected"></option>
							<?php
							
							$result = $mysqli->query("SELECT * FROM members");
							$rows = $result->num_rows;
							
							for ($i=0;$i<$rows;$i++){
								$stmt = $mysqli->prepare("SELECT role, mobile_number, first_name, last_name FROM members LIMIT 1 OFFSET ?");
								$stmt->bind_param('s', $i);
								$stmt->execute();
								$stmt->store_result();
						 
								$stmt->bind_result($role, $mobile_number, $receiver_first_name, $receiver_last_name);
								$stmt->fetch();
								$stmt->close();
								
							
								$display_name = $receiver_first_name." ".$receiver_last_name;
								if ($display_name == " "){
									$display_name = $mobile_number;
								}
								echo '<option value="'.$mobile_number.'" ';
								//if ($receiver == $username){echo 'selected="selected"';}
								echo '>';
								echo $display_name.'</option>';
									
							
							}
							?>
						</select>
                        <script>
						$("#receiver").chosen({
							width: "165px !important",
							allow_single_deselect: true,
							no_results_text : "کاربری یافت نشد.",
						});
						</script>
                        &ensp;<a target="_blank" href="member_add.php"><button class="btn btn-success" type="button" style="margin:-10px 0 0 0; padding:0 2px;"><i class="icon-plus"></i></button></a>
                    </td>
				</tr>
                <tr>
					<td><h5 class="normal">متن <span class="red">*</span></h5></td>
                    <td style="padding: 5px 0 0 0;"><textarea maxlength="" style="font: normal 11px tahoma; width:200px;" value="" name="text" id="text"></textarea></td>
				</tr>
                <tr>
					<td><h5 class="normal">تاریخ <span class="red">*</span></h5></td>
					<td style="padding: 5px 0 0 0;">
                    	<input type="text" style="font: normal 11px tahoma; width:100px;" value="<?php echo $date ?>" name="date" id="date" maxlength="20" />
                    </td>
				</tr>
                <tr>
					<td><h5 class="normal">زمان <span class="red">*</span></h5></td>
					<td style="padding: 5px 0 0 0;">
                    	<input type="text" style="font: normal 11px tahoma; width:100px;" value="<?php echo $time ?>" name="time" id="time" maxlength="20" />
                    </td>
				</tr>
			</table>
			<button class='btn btn-info pull-left' type="submit"><span>اضافه کردن</span> <i class="icon-plus icon-white"></i></button>
		</form>
	</div>
</div>

<?php include('footer.php'); ?>