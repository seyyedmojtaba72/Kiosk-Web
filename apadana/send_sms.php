<?php 
include('header.php'); 
?>

<?php if (if_has_permission($role,"edit_text_messages")){} else{header("Location: login.php?redirect=".urlencode("http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']));} ?>

<?php
if (!empty($err)) {
	switch ($err) {
	case "insert" : $err_msg = "خطا در نوشتن دیتابیس!"; break;
	case "fill" : $err_msg = "موارد الزامی را پر کنید!"; break;
	case "exists" : $err_msg = "پیامک وجود دارد!"; break;
	
	default : $err_msg = "خطای غیر منتظره‌ای پیش آمده!"; break;
	}
	
}
?> 
<?php
if (!empty($suc)) {
	switch ($suc) {
	case "send" : $suc_msg = "پیامک ارسال شد!"; break;
	
	default : $suc_msg = "عملیات با موفقیت انجام شد!"; break;
	}
	
}
?> 
<?php
if (!empty($err_msg)) {
	echo '<div class="alert alert-error no-print"><a href="'.str_replace('err='.$err,"",implode('&',array_unique(explode('&',$_SERVER['REQUEST_URI'])))).'"><button type="button" class="close">&times;</button></a><p>'. $err_msg .'</p></div>';
}
?>
<?php
if (!empty($suc_msg)) {
	echo '<div class="alert alert-success no-print"><a href="'.str_replace('suc='.$suc,"",implode('&',array_unique(explode('&',$_SERVER['REQUEST_URI'])))).'"><button type="button" class="close">&times;</button></a><p>'. $suc_msg .'</p></div>';
}
?>

<?php
if (empty($_SESSION['text_messages_redirect'])){$_SESSION['text_messages_redirect']="text_messages.php";}
?>

<div class="container">
	<a href="<?php echo $_SESSION['text_messages_redirect'] ?>"><button class='pull-left btn'><span>بازگشت</span> <i class="icon-chevron-left"></i></button></a>
	<button class="btn disabled"><span id="subtitle">ارسال پیامک</span></button><br /><br />
	<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert">&times;</button>
	<p>مواردی که با علامت ستاره‌ی قرمز مشخص شده‌ است، الزامی است.</p></div>
	<div id="main" class="span8 pull-right">
		<form action="<?php echo $options["url"] ?>/inc/send_sms.php" method="post">
        	<input type="hidden" name="redirect" id="redirect" value="send_sms.php?" />
			<table class="table table-striped table-right">
			<tr>
				<td class="span2"><h5 class="normal">دریافت‌کننده(ها) <span class="red">*</span></h5></td>
					<td style="padding: 5px 0 0 0;">
                    	<select class="chosen-select chosen-rtl" name="receiver[]" id="receiver" data-placeholder="کاربری را انتخاب کنید." multiple="multiple">
							<?php
							foreach ($member_roles as $role1=>$role1_value){
								
								echo '<optgroup label="'.$role1_value.'">';
							
								$result = $mysqli->query('SELECT * FROM members WHERE role = "'.$role1.'"');
								$rows = $result->num_rows;
								
								for ($i=0;$i<$rows;$i++){
									$stmt = $mysqli->prepare('SELECT role, username, mobile_number, mobile_number_status, first_name, last_name FROM members WHERE role = "'.$role1.'" LIMIT 1 OFFSET ?');
									$stmt->bind_param('s', $i);
									$stmt->execute();
									$stmt->store_result();
							 
									$stmt->bind_result($role, $username, $mobile_number, $mobile_number_status, $receiver_first_name, $receiver_last_name);
									$stmt->fetch();
									$stmt->close();
									
								
									$display_name = $receiver_first_name." ".$receiver_last_name;
									if ($display_name == " "){
										$display_name = $username;
									}
									if ($mobile_number_status == "0"){
										$display_name .= " (تأیید نشده)";
									}
									
									echo '<option value="'.$mobile_number.'" ';
									//if ($receiver == $username){echo 'selected="selected"';}
									echo '>';
									echo $display_name.'</option>';

								}
								
								echo '</optgroup>';
							}
							
							$option_groups = mysql_select('members_roles', array(), array('slug', 'role_title'));
							for ($j=0; $j<sizeof($option_groups); $j++){
								$role1 = $option_groups[$j];
								
								echo '<optgroup label="'.$role1['role_title'].'">';
							
								$result = $mysqli->query('SELECT * FROM members WHERE role = "'.$role1['slug'].'"');
								$rows = $result->num_rows;
								
								for ($i=0;$i<$rows;$i++){
									$stmt = $mysqli->prepare('SELECT role, username, mobile_number, mobile_number_status, first_name, last_name FROM members WHERE role = "'.$role1.'" LIMIT 1 OFFSET ?');
									$stmt->bind_param('s', $i);
									$stmt->execute();
									$stmt->store_result();
							 
									$stmt->bind_result($role, $username, $mobile_number, $mobile_number_status, $receiver_first_name, $receiver_last_name);
									$stmt->fetch();
									$stmt->close();
									
								
									$display_name = $receiver_first_name." ".$receiver_last_name;
									if ($display_name == " "){
										$display_name = $username;
									}
									if ($mobile_number_status == "0"){
										$display_name .= " (تأیید نشده)";
									}
									
									echo '<option value="'.$mobile_number.'" ';
									//if ($receiver == $username){echo 'selected="selected"';}
									echo '>';
									echo $display_name.'</option>';

								}
								
								echo '</optgroup>';
							}
							?>
						</select>
                        <script>
						$("#receiver").chosen({
							width: "165px !important",
							allow_single_deselect: true,
							no_results_text : "کاربری یافت نشد.",
						});
						</script>
                        &ensp;
                        <button class="btn btn-success" id="add_new" type="button" style="margin:-10px 0 0 0; padding:0 2px;"><i class="icon-plus"></i></button>
                        <button class="btn btn-primary" id="select_all" type="button" style="margin:-10px 0 0 0; padding:0 2px;"><i class="icon-ok"></i></button>
                        <button class="btn btn-danger" id="select_none" type="button" style="margin:-10px 0 0 0; padding:0 2px;"><i class="icon-remove"></i></button><div id="adddd"></div>
                        <script>
						$('#add_new').click(function(){
							fancyPopup('<div style="width: 150px;"><br><input type="number" id="new" placeholder="شماره موبایل" maxlength="10" style="font: normal 11px tahoma; width:100px;" /><h5 class="normal pull-left">98+</h5><br><br><button class="btn btn-primary pull-left" id="add" type="button" onclick=\'add_new($("#new").val());\'><span>افزودن</span><i class="icon-plus icon-white"></i></button></div>' );
						});
						
						function add_new(number){
							if (number.length != 10){
								fancyAlert("طول شماره باید 10 باشد!");
								return;
							}
							$("#receiver").append($('<option value="'+number+'" selected="selected">'+number+'</option>')); 
							$("#receiver").trigger("chosen:updated");
							fancyAlert("شماره موبایل افزوده شد.");
						}
						
						$('#select_all').click(function(){
							$('#receiver option').prop('selected', true);
							$('#receiver').trigger('chosen:updated');
						});
						$('#select_none').click(function(){
							$('#receiver option').prop('selected', false);
							$('#receiver').trigger('chosen:updated');
						});
						
						</script>
                    </td>
				</tr>
                <tr>
					<td><h5 class="normal">متن <span class="red">*</span></h5></td>
                    <td style="padding: 5px 0 0 0;"><textarea maxlength="" style="font: normal 11px tahoma; width:200px;" value="" name="text" id="text"></textarea></td>
				</tr>
			</table>
			<button class='btn btn-info pull-left' type="submit"><span>ارسال</span> <i class="icon-comment icon-white"></i></button>
		</form>
	</div>
</div>

<?php include('footer.php'); ?>