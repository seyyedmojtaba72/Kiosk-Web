<?php 
include('header.php'); 
?>

<?php if ($role != "admin"){header("Location: login.php?redirect=".urlencode("http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']));} ?>

<?php
if (!empty($err)) {
	switch ($err) {
	case "not-exists" : $err_msg = "نقش عضو وجود ندارد! ممکن است حذف شده باشد."; break;
	case "delete" : $err_msg = "خطا در حذف!"; break;
	case "no-match" : $err_msg = "جستجو نتیجه‌ای نداشت! <a style='margin-right:50px;' href='members_roles.php'><button class='btn Yekan normal'>نمایش همه</button></a>"; break;
	
	default : $err_msg = "خطای غیر منتظره‌ای پیش آمده!"; break;
	}
	
}
?> 
<?php
if (!empty($suc)) {
	switch ($suc) {
	case "delete" : $suc_msg = "نقش عضو حذف شد!"; break;
	case "search" : $suc_msg = "جستجو با موفقیت انجام شد! <a style='margin-right:50px;' href='members_roles.php'><button class='btn Yekan normal'>نمایش همه</button></a>"; break;
	
	default : $suc_msg = "عملیات با موفقیت انجام شد!"; break;
	}
	
}
?> 
<?php
if (!empty($err_msg)) {
	echo '<div class="alert alert-error no-print"><a href="'.str_replace('err='.$err,"",implode('&',array_unique(explode('&',$_SERVER['REQUEST_URI'])))).'"><button type="button" class="close">&times;</button></a><p>'. $err_msg .'</p></div>';
}
?>
<?php
if (!empty($suc_msg)) {
	echo '<div class="alert alert-success no-print"><a href="'.str_replace('suc='.$suc,"",implode('&',array_unique(explode('&',$_SERVER['REQUEST_URI'])))).'"><button type="button" class="close">&times;</button></a><p>'. $suc_msg .'</p></div>';
}
?>
<?php

$_SESSION['members_roles_redirect'] = 
str_replace(basename($_SERVER['PHP_SELF']),basename($_SERVER['PHP_SELF'])."?",str_replace("?","",implode('&',array_unique(explode('&', $_SERVER['REQUEST_URI'])))));

// ------

$amount = $options['list_rows_per_page'];
$search = filter_input(INPUT_GET, 'search', $filter = FILTER_SANITIZE_STRING);
$order_by = filter_input(INPUT_GET, 'order_by', $filter = FILTER_SANITIZE_STRING);
$mode = filter_input(INPUT_GET, 'mode', $filter = FILTER_SANITIZE_STRING);
$page = filter_input(INPUT_GET, 'page', $filter = FILTER_SANITIZE_STRING);

if (empty($search)){$search = "false";}
if (empty($order_by)){$order_by = 'row';}
if (empty($mode)){$mode = 'DESC';}
if (empty($page)){$page = 1;}

// ------

$slug = filter_input(INPUT_GET, 'slug', $filter = FILTER_SANITIZE_STRING);
$role_title = filter_input(INPUT_GET, 'role_title', $filter = FILTER_SANITIZE_STRING);
$permissions = filter_input(INPUT_GET, 'permissions', $filter = FILTER_SANITIZE_STRING);

function get_link($order_by,$mode,$page){
	global $options, $amount, $search;
	global $slug, $role_title, $permissions;

	$link="members_roles.php?";
	if ($search=="true"){
		
		$link.='suc=search&search=true&slug='.$slug.'&role_title='.$role_title.'&permissions='.$permissions.'&';		
	} 
	
	$link.="order_by=".$order_by."&mode=".$mode;
	if ($page!=0){$link.="&page=".$page;}
	
	return $link;
}
?>
	
	
<div class="container">
	<div class="pull-left no-print">
		<button class='btn btn-success' onClick="print();"><span>چاپ</span> <i class="icon-print icon-white"></i></button>
		<a href="members_role_add.php"><button class='btn btn-primary'><span>اضافه‌کردن</span> <i class="icon-plus icon-white"></i></button></a>
		<a href="panel.php"><button class='btn'><span>بازگشت</span> <i class="icon-chevron-left"></i></button></a>
	</div>
    <button class="btn disabled"><span id="subtitle">نقش‌های اعضا</span></button><br />
	<div class="pull-right">
		<?php
        if ($search=="true"){
    
            $statement = 'SELECT * FROM members_roles WHERE';
            
            if (!empty($slug)){ $statement .= ' slug LIKE "%'.$slug .'%" AND'; }
            if (!empty($role_title)){ $statement .= ' role_title LIKE "%'.$role_title.'%" AND'; }
            if (!empty($permissions)){ $statement .= ' permissions LIKE "%'.$permissions.'%" AND'; }
            
            if (substr($statement,strlen($statement)-3,strlen($statement))=="AND"){
                $statement = substr($statement,0,strlen($statement)-3);
            }
            
            if (substr($statement,strlen($statement)-5,strlen($statement))=="WHERE"){
                $statement = substr($statement,0,strlen($statement)-5);
            }
                        
            
            $result = $mysqli->query($statement);
        
            $mutch = $result->num_rows;
        } else {
            $statement = 'SELECT * FROM members_roles';
            
            $result = $mysqli->query($statement);
        
            $mutch = $result->num_rows;
        }
        $pages = floor(($mutch-1)/$amount)+1;
        ?>
        <div style="margin: 10px 0;">
            <?php if ($mutch>0){ ?>
                <a class="no-print" href="<?php echo get_link($order_by,$mode,$page-1); ?>"><button class="btn btn-small btn-inverse <?php if ($page==1) { echo 'disabled'; } ?>" <?php if ($page==1) { echo 'disabled="disabled"'; } ?>><i class="icon-chevron-right icon-white"></i></button></a>
                &emsp;
                <span class="h5">
                    <?php echo $mutch ?> مورد یافت شد
                    &emsp;///&emsp;
                    نمایش موارد <?php echo (($page-1)*$amount+1).' تا '.min($page*$amount,$mutch) ?>
                    &emsp;///&emsp;
                    صفحه‌ی
                    &ensp;
                    <select class="tahoma size-11" style="margin-top: 10px; width: 50px; height: 25px;" name="pagg" id="pagg" onChange='go_to_page("<?php echo get_link($order_by,$mode,0); ?>","pagg");'>
                        <?php
                        for ($i=0;$i<$pages;$i++){
                            echo '<option ';
                            if ($page==$i+1){ echo 'selected="selected" '; }
                            echo 'value='.($i+1).'>'.($i+1).'</option>';
                        }
                        ?>
                    </select>
                    &ensp;
                    <?php echo ' از '.$pages ?>
                </span>
                &emsp;
                <a class="no-print" href="<?php echo get_link($order_by,$mode,$page+1); ?>"><button class="btn btn-small btn-inverse <?php if ($page==$pages) { echo 'disabled'; } ?>" <?php if ($page==$pages) { echo 'disabled="disabled"'; } ?>><i class="icon-chevron-left icon-white"></i></button></a>
          	<?php } ?>
    	</div>
    </div>
    <div id="batch" class="no-print pull-left" style="margin: 15px 0; display: none;">
        <button class="btn btn-danger" onClick="batchDelete();"><span>حذف</span> <i class="icon-trash icon-white"></i></button>
        <button class="btn btn-info" onClick="batchEdit();"><span>ويرايش</span> <i class="icon-edit icon-white"></i></button>
    </div>
	<div class="clearfix"></div>
	<form action="<?php echo $options["url"] ?>/inc/search_members_role.php" method="post">
		<input type="hidden" value="members_roles.php?order_by=<?php echo $order_by ?>&mode=<?php echo $mode ?>&" name="redirect" id="redirect" />
		<table class="table table-striped table-hover text-center" style="margin-bottom:-20px;">
			<td style="width: 50px;"><h5><input id="batch_check" type="checkbox" style="margin: 5px 0 0px 5px;" onChange="batchCheck();" /></h5></td>
			<td style="width: 170px;"><h5><input type="text" maxlength="10" style="font: normal 11px tahoma; width:150px;" 
			value="<?php echo $slug; ?>" name="slug" id="slug"/></h5></td>
			<td style="width: 170px;"><h5><input type="text" style="font: normal 11px tahoma; width:150px;" 
			value="<?php echo $role_title; ?>" name="role_title" id="role_title"/></h5></td>
			<td style="width: 320px;"><h5><input type="text" style="font: normal 11px tahoma; width:300px;" 
			value="<?php echo $permissions; ?>" name="permissions" id="permissions"/></h5></td>
			<td class="no-print" style="width: 50px;"><h4 class='normal'>
				<button class='btn btn-info' type="submit"><i class="icon-search"></i></button>
			</h4></td>
		</table>
	</form>
	<table class="table table-striped table-hover text-center">
		<tr>
			<td style="width: 50px;"><h5 class="normal ">ردیف <br />
			<a href="<?php echo get_link("row",'ASC',$page); ?>" class="<?php if ($order_by=="row" && $mode=="ASC"){ echo "red_outline "; } ?>icon-chevron-up no-print"></a><a href="<?php echo get_link("row",'DESC',$page); ?>" class="<?php if ($order_by=="row" && $mode=="DESC"){ echo "red_outline "; } ?>icon-chevron-down no-print"></a></h5></td>
			<td style="width: 170px;"><h5 class="normal">نامک <br />
            <a href="<?php echo get_link('slug','ASC',$page); ?>" class="<?php if ($order_by=="slug" && $mode=="ASC"){ echo "red_outline "; } ?>icon-chevron-up no-print"></a><a href="<?php echo get_link('slug','DESC',$page); ?>" class="<?php if ($order_by=="slug" && $mode=="DESC"){ echo "red_outline "; } ?>icon-chevron-down no-print"></a></h5></td>
			<td style="width: 170px;"><h5 class="normal">عنوان <br />
            <a href="<?php echo get_link('role_title','ASC',$page); ?>" class="<?php if ($order_by=="role_title" && $mode=="ASC"){ echo "red_outline "; } ?>icon-chevron-up no-print"></a><a href="<?php echo get_link('role_title','DESC',$page); ?>" class="<?php if ($order_by=="role_title" && $mode=="DESC"){ echo "red_outline "; } ?>icon-chevron-down no-print"></a></h5></td>
			<td style="width: 320px;"><h5 class="normal">مجوزها <br />
            <a href="<?php echo get_link('permissions','ASC',$page); ?>" class="<?php if ($order_by=="permissions" && $mode=="ASC"){ echo "red_outline "; } ?>icon-chevron-up no-print"></a><a href="<?php echo get_link('permissions','DESC',$page); ?>" class="<?php if ($order_by=="permissions" && $mode=="DESC"){ echo "red_outline "; } ?>icon-chevron-down no-print"></a></h5></td>
			<td class="no-print" style="width: 50px;"><h5 class="normal">عملیات</h5></td>
		</tr>
		
		<?php
		
		if ($order_by == "row"){ $order_by = "id"; }
		
		if ($search=="true"){
			
			$statement .= ' ORDER BY '.$order_by.' '.$mode;
				
			$result = $mysqli->query($statement.' LIMIT '.(($page-1)*$amount).' , '.$amount);
		
			$rows = $result->num_rows;
			
			for ($i=0;$i<$rows;$i++){
			$statement2 = str_replace("SELECT *","SELECT id, slug, role_title, permissions",$statement);
			$statement2 .= ' LIMIT '.((($page-1)*$amount+$i).' , 1');

			$stmt = $mysqli->prepare($statement2);
			$stmt->execute();
			$stmt->store_result();
	 
			$stmt->bind_result($id, $slug, $role_title, $permissions);
			$stmt->fetch();
			$stmt->close();
			?>
			<tr>
				<?php echo '
				<td><input name="checkbox[]" type="checkbox" style="margin: -1px 0 -1px 5px;" value="'.$id.'" onChange="loadBatch();" />'.(($page-1)*$amount+$i+1).'</td>
				<td>'.$slug.'</td>
				<td>'.$role_title.'</td>
				<td style="max-width: 300px;">';
				$permissions2 = explode(";",$permissions);
				for($i2=0;$i2<sizeof($permissions2);$i2++){
					
					$permissions3 = $permissions2[$i2];
				
					foreach($member_permissions as $permission_slug => $permission_title){
						if ($permissions3==$permission_slug){ 
							echo "&emsp;".$permission_title."&emsp;";
						}
					}			
				}
				echo '</td>
				<td class="no-print">
				<a href="members_role_delete.php?id='.$id.'"><button class="btn btn-danger" style=" padding:0 2px;"><i class="icon-trash"></i></button></a>
				<a href="members_role_edit.php?id='.$id.'"><button class="btn btn-info" style=" padding:0 2px;"><i class="icon-edit"></i></button></a>
				</td>
				';
				?>
			</tr>
			<?php
			}
	
	 
		} else{

		$statement .= ' ORDER BY '.$order_by.' '.$mode;
		$statement .= ' LIMIT '.(($page-1)*$amount).' , '.$amount;
		
		$result = $mysqli->query($statement);
	
		$rows = $result->num_rows;

		for ($i=0;$i<$rows;$i++){
			$statement2 = 'SELECT id, slug, role_title, permissions FROM members_roles ';
			$statement2 .= ' ORDER BY '.$order_by.' '.$mode;
			$statement2 .= ' LIMIT '.((($page-1)*$amount+$i).' , 1');
		
			$stmt = $mysqli->prepare($statement2);
			$stmt->execute();
			$stmt->store_result();
	 
			$stmt->bind_result($id, $slug, $role_title, $permissions);
			$stmt->fetch();
			$stmt->close();
			
			?>
			
			<tr>
				<?php echo '
				<td><input name="checkbox[]" type="checkbox" style="margin: -1px 0 -1px 5px;" value="'.$id.'" onChange="loadBatch();" />'.(($page-1)*$amount+$i+1).'</td>
				<td>'.$slug.'</td>
				<td>'.$role_title.'</td>
				<td style="max-width: 300px;">';
				$permissions2 = explode(";",$permissions);
				for($i2=0;$i2<sizeof($permissions2);$i2++){
					
					$permissions3 = $permissions2[$i2];
				
					foreach($member_permissions as $permission_slug => $permission_title){
						if ($permissions3==$permission_slug){ 
							echo "&emsp;".$permission_title."&emsp;";
						}
					}			
				}
				echo '</td>
				<td class="no-print">
				<a href="members_role_delete.php?id='.$id.'"><button class="btn btn-danger" style=" padding:0 2px;"><i class="icon-trash"></i></button></a>
				<a href="members_role_edit.php?id='.$id.'"><button class="btn btn-info" style=" padding:0 2px;"><i class="icon-edit"></i></button></a>
				</td>
				';
				?>
			</tr>
			<?php
			}
		}
		?>
	</table>
	<?php if ($mutch == 0){
		echo '<div class="alert alert-warning no-print"><p>موردی یافت نشد!</i></p></div>';
	} ?>
</div>
<script>
function loadBatch(){
	var values = new Array();
	$.each($("input[name='checkbox[]']:checked"), function() {
		values.push($(this).val());
	});
	
	if (values.length > 0){
		$("#batch").fadeIn();
	} else {
		$("#batch").fadeOut();
	}
}

function batchEdit(){
	$.each($("input[name='checkbox[]']:checked"), function() {
		window.open('members_role_edit.php?id='+$(this).val(), '_blank');
	});

}

function batchDelete(){
	$.each($("input[name='checkbox[]']:checked"), function() {
			window.open('members_role_delete.php?id='+$(this).val(), '_blank');
	});
}

function batchCheck(){
	if ($("#batch_check").is(":checked")){
		$.each($("input[name='checkbox[]']"), function() {
			$(this).prop("checked", true);
		});
	} else{
		$.each($("input[name='checkbox[]']"), function() {
			$(this).prop("checked", false);
		});
	}
	
	loadBatch();
}
</script>
<?php include('footer.php'); ?>