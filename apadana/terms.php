<?php 

header('Location: http://blog.kioskapp.ir/terms');
exit;

include('header.php'); 

if (!empty($err)) {
	switch ($err) {
	
	default : $err_msg = "خطای غیر منتظره‌ای پیش آمده!"; break;
	}
	
}
?>
<?php
if (!empty($suc)) {
	switch ($suc) {
			
	default : $suc_msg = "عملیات با موفقیت انجام شد!"; break;
	}
	
}
?>
<?php
if (!empty($err_msg)) {
	echo '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">&times;</button><p>'. $err_msg .'</p></div>';
}
?>
<?php
if (!empty($suc_msg)) {
	echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button><p>'. $suc_msg .'</p></div>';
}
?>
<div class="container">
	<div class="pull-left">
		<a href="<?php echo $options["url"]; ?>/home.php"><button class='btn'><span>بازگشت</span> <i class="icon-chevron-left"></i></button></a>
	</div>
	<button class="btn disabled"><span id="subtitle">شرایط استفاده از نرم افزار</span></button><br /><br />
	<div id="main-content" style="margin-right: 20px">
		<h5 style="line-height: 20px;" class="normal"><?php echo nl2br($options["terms_text"]); ?></h5>
	</div>
	<div class="content_footer"></div>
</div>
<?php include('footer.php'); ?>