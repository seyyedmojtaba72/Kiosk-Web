<?php 

include('header.php'); 
?>

<?php if (if_has_permission($role,"edit_discounts")){} else{header("Location: login.php?redirect=".urlencode("http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']));} ?>

<?php
if (!empty($err)) {
	switch ($err) {
	case "edit" : $err_msg = "خطا در نوشتن دیتابیس!"; break;
	case "fill" : $err_msg = "موارد الزامی را پر کنید!"; break;
	case "exists" : $err_msg = "تخفیفی با این کد وجود دارد!"; break;
	
	default : $err_msg = "خطای غیر منتظره‌ای پیش آمده!"; break;
	}
	
}
?> 
<?php
if (!empty($suc)) {
	switch ($suc) {
	case "edit" : $suc_msg = "تخفیف ویرایش شد!"; break;
	
	default : $suc_msg = "عملیات با موفقیت انجام شد!"; break;
	}
	
}
?> 
<?php
if (!empty($err_msg)) {
	echo '<div class="alert alert-error no-print"><a href="'.str_replace('err='.$err,"",implode('&',array_unique(explode('&',$_SERVER['REQUEST_URI'])))).'"><button type="button" class="close">&times;</button></a><p>'. $err_msg .'</p></div>';
}
?>
<?php
if (!empty($suc_msg)) {
	echo '<div class="alert alert-success no-print"><a href="'.str_replace('suc='.$suc,"",implode('&',array_unique(explode('&',$_SERVER['REQUEST_URI'])))).'"><button type="button" class="close">&times;</button></a><p>'. $suc_msg .'</p></div>';
}
?>
<?php

if (!isset($_SESSION['discounts_redirect'])){$_SESSION['discounts_redirect'] = "discounts.php";}

// ------

$id = filter_input(INPUT_GET, 'id', $filter = FILTER_SANITIZE_STRING);

// ------

$stmt = 'SELECT code, title, percentage, number, used, min_price, start_date, end_date, description, status FROM discounts WHERE id="'.$id.'"';

if ($mysqli->query($stmt)->num_rows<1){ header('Location: discounts.php?err=not-exists'); exit; }

$stmt = $mysqli->prepare($stmt);
$stmt->execute();
$stmt->store_result();

$stmt->bind_result($code, $title, $percentage, $number, $used, $min_price, $start_date, $end_date, $description, $status);
$stmt->fetch();
$stmt->close();

?>

<div class="container">
	<div class="pull-left">
		<a href="discount_delete.php?<?php echo 'id='.$id ?>"><button class='btn btn-danger'><span>حذف</span> <i class="icon-trash icon-white"></i></button></a>
        <a href="<?php echo $_SESSION['discounts_redirect'] ?>"><button class='btn'><span>بازگشت</span> <i class="icon-chevron-left"></i></button></a>
	</div>
	<button class="btn disabled"><span id="subtitle">ویرایش تخفیف</span></button>
    <br /><br />
	<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert">&times;</button>
	<p>مواردی که با علامت ستاره‌ی قرمز مشخص شده‌ است، الزامی است.</p></div>
	<div id="main" class="span8 pull-right">
		<form action="<?php echo $options["url"] ?>/inc/edit_discount.php" method="post">
			<input type="hidden" value="discount_edit.php?id=<?php echo $id ?>&" name="redirect" id="redirect"/>
			<input type="hidden" value="<?php echo $id ?>" name="id" id="id"/>
			<table class="table table-striped table-right">
			<tr>
				<td class="span2"><h5 class="normal">کد <span class="red">*</span></h5></td>
					<td style="padding: 5px 0 0 0;"><input type="text" maxlength="30" style="font: normal 11px tahoma; width:150px;" value="<?php echo $code; ?>" name="code" id="code" readonly="readonly" /></td>
				</tr>
                <tr>
					<td><h5 class="normal">عنوان <span class="red">*</span></h5></td>
					<td style="padding: 5px 0 0 0;"><input type="text" maxlength="300" style="font: normal 11px tahoma; width:150px;" value="<?php echo $title; ?>" name="title" id="title"/></td>
				</tr>
                <tr>
					<td><h5 class="normal">درصد <span class="red">*</span></h5></td>
					<td style="padding: 5px 0 0 0;"><input type="number" maxlength="3" style="font: normal 11px tahoma; width:100px;" value="<?php echo $percentage; ?>" name="percentage" id="percentage"/></td>
				</tr>
                <tr>
					<td><h5 class="normal">تعداد <span class="red">*</span></h5></td>
					<td style="padding: 5px 0 0 0;"><input type="number" maxlength="11" style="font: normal 11px tahoma; width:100px;" value="<?php echo $number; ?>" name="number" id="number"/></td>
				</tr>
                <tr>
					<td><h5 class="normal">استفاده‌شده <span class="red">*</span></h5></td>
					<td style="padding: 5px 0 0 0;"><input type="number" maxlength="11" style="font: normal 11px tahoma; width:100px;" value="<?php echo $used; ?>" name="used" id="used"/></td>
				</tr>
                <tr>
					<td><h5 class="normal">حداقل قیمت <span class="red">*</span></h5></td>
					<td style="padding: 5px 0 0 0;"><input type="number" maxlength="11" style="font: normal 11px tahoma; width:100px;" value="<?php echo $min_price; ?>" name="min_price" id="min_price"/> ریال</td>
				</tr>
                <tr>
					<td><h5 class="normal">تاریخ شروع <span class="red">*</span></h5></td>
					<td style="padding: 5px 0 0 0;"><h1 id="asd"></h1>	
                    	<input type="text" style="font: normal 11px tahoma; width:100px;" value="<?php echo $start_date ?>" name="start_date" id="start_date" maxlength="20" />
                        <script>
						$("#start_date").persianDatepicker();
						</script>
                    </td>
				</tr>
                <tr>
					<td><h5 class="normal">تاریخ پایان <span class="red">*</span></h5></td>
					<td style="padding: 5px 0 0 0;">
                    	<input type="text" style="font: normal 11px tahoma; width:100px;" value="<?php echo $end_date ?>" name="end_date" id="end_date" maxlength="20" />
                        <script>
						$("#end_date").persianDatepicker();
						</script>
                    </td>
				</tr>
                <tr>
					<td><h5 class="normal">توضیحات</h5></td>
					<td style="padding: 5px 0 0 0;"><textarea style="font: normal 11px tahoma;" name="description" id="description" maxlength="3000" ><?php echo $description; ?></textarea></td>
				</tr>
                <tr>
					<td><h5 class="normal">وضعیت</h5></td>
					<td style="padding: 5px 0 0 0;">
                    	<select class="tahoma size-11" style="width: 115px;" name="status" id="status">
							<?php
                            foreach ($discount_statuses as $status1=>$status_text){
                                echo '<option ';
                                if ($status1==$status){ echo 'selected="selected" ';}
                                echo 'value="'.$status1.'">'.$status_text.'</option>';
                            }
                            ?>
                    	</select>
                	</td>
                </tr>
			</table>
            <button class='btn btn-info pull-left' type="submit"><span>ویرایش</span> <i class="icon-edit icon-white"></i></button>
		</form>
	</div>
</div>
<?php include('footer.php'); ?>