<?php 
require_once('header.php'); 

if (!empty($err)) {
	switch ($err) {
	
	case "wrong-email" : $err_msg = "کاربری با این ایمیل وجود ندارد!"; break;
	case "invalid-email" : $err_msg = "ایمیل وارد شده معتبر نیست!"; break;
	case "email" : $err_msg = "کاربری با این ایمیل وجود ندارد!"; break;
	
	default : $err_msg = "خطای غیر منتظره‌ای پیش آمده!"; break;
	}
	
}
?> 
<?php
if (!empty($suc)) {
	switch ($suc) {
	
	case "check-email" : $suc_msg = "درخواست بازنشانی گذرواژه فرستاده شد. لطفاً ایمیل خود و پوشه‌ی Spam یا Bulk را بررسی کنید !"; break;
	
	default : $suc_msg = "عملیات با موفقیت انجام شد!"; break;
	
	}
	
}
?> 
<?php
if (!empty($err_msg)) {
	echo '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">&times;</button><p>'. $err_msg .'</p></div>';
}
?>
<?php
if (!empty($suc_msg)) {
	echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button><p>'. $suc_msg .'</p></div>';
}
?>
<div class="container" style="width: 350px;">
	   
	<div class="text-center">
		<button class="btn disabled"><span id="subtitle">بازنشانی گذرواژه</span></button><br /><br />
		<div class="alert alert-info text-right"><button type="button" class="close" data-dismiss="alert">&times;</button><p>
		ایمیل را وارد کنید. لینک بازنشانی گذرواژه برایتان ارسال خواهد شد.
		</p></div>
		<div class="text-left" style="width:260px;">
			 <form action="<?php echo $options["url"] ?>/inc/forget.php" method="post" name="forget_form">
				<input type="email" placeholder="ایمیل" class="span2" id="email" name="email" style="font: normal 12px 'WYekan',B Yekan;" /><br />
				  
				<button class="btn btn-primary normal" type="submit" onClick="return forgetformhash(this.form,this.form.email);"><span>ارسال</span> <i class="icon-envelope icon-white"></i></button>
			   
			</form>
		</div>
	</div>
</div>

<?php require_once('footer.php'); ?>