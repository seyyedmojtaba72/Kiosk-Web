<?php 

include('header.php'); 
?>

<?php if (if_has_permission($role,"edit_text_messages")){} else{header("Location: login.php?redirect=".urlencode("http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']));} ?>

<?php
if (!empty($err)) {
	switch ($err) {
	case "edit" : $err_msg = "خطا در نوشتن دیتابیس!"; break;
	case "fill" : $err_msg = "موارد الزامی را پر کنید!"; break;
	case "exists" : $err_msg = "پیامک وجود دارد!"; break;
	
	default : $err_msg = "خطای غیر منتظره‌ای پیش آمده!"; break;
	}
	
}
?> 
<?php
if (!empty($suc)) {
	switch ($suc) {
	case "edit" : $suc_msg = "پیامک ویرایش شد!"; break;
	
	default : $suc_msg = "عملیات با موفقیت انجام شد!"; break;
	}
	
}
?> 
<?php
if (!empty($err_msg)) {
	echo '<div class="alert alert-error no-print"><a href="'.str_replace('err='.$err,"",implode('&',array_unique(explode('&',$_SERVER['REQUEST_URI'])))).'"><button type="button" class="close">&times;</button></a><p>'. $err_msg .'</p></div>';
}
?>
<?php
if (!empty($suc_msg)) {
	echo '<div class="alert alert-success no-print"><a href="'.str_replace('suc='.$suc,"",implode('&',array_unique(explode('&',$_SERVER['REQUEST_URI'])))).'"><button type="button" class="close">&times;</button></a><p>'. $suc_msg .'</p></div>';
}
?>
<?php

if (!isset($_SESSION['text_messages_redirect'])){$_SESSION['text_messages_redirect'] = "text_messages.php";}

// ------

$id = filter_input(INPUT_GET, 'id', $filter = FILTER_SANITIZE_STRING);

// ------

$stmt = 'SELECT receiver, text, date, time FROM text_messages WHERE id="'.$id.'"';

if ($mysqli->query($stmt)->num_rows<1){ header('Location: text_messages.php?err=not-exists'); exit; }

$stmt = $mysqli->prepare($stmt);
$stmt->execute();
$stmt->store_result();

$stmt->bind_result($receiver, $text, $date, $time);
$stmt->fetch();
$stmt->close();

$result = mysql_select("members", array('mobile_number'=>$receiver), array('id'));

?>

<div class="container">
	<div class="pull-left">
		<a href="text_message_delete.php?<?php echo 'id='.$id ?>"><button class='btn btn-danger'><span>حذف</span> <i class="icon-trash icon-white"></i></button></a>
        <a href="<?php echo $_SESSION['text_messages_redirect'] ?>"><button class='btn'><span>بازگشت</span> <i class="icon-chevron-left"></i></button></a>
	</div>
	<button class="btn disabled"><span id="subtitle">ویرایش پیامک</span></button>
    <br /><br />
	<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert">&times;</button>
	<p>مواردی که با علامت ستاره‌ی قرمز مشخص شده‌ است، الزامی است.</p></div>
	<div id="main" class="span8 pull-right">
		<form action="<?php echo $options["url"] ?>/inc/edit_text_message.php" method="post">
			<input type="hidden" value="text_message_edit.php?id=<?php echo $id ?>&" name="redirect" id="redirect"/>
			<input type="hidden" value="<?php echo $id ?>" name="id" id="id"/>
            <input type="hidden" value="<?php echo time(); ?>" name="timestamp" id="timestamp" />
			<table class="table table-striped table-right">
			<tr>
				<td class="span2"><h5 class="normal">دریافت‌کننده <span class="red">*</span></h5></td>
					<td style="padding: 5px 0 0 0;">
                    	<select class="chosen-select chosen-rtl" name="receiver" id="receiver" data-placeholder="کاربری را انتخاب کنید.">
							<option value="" selected="selected"></option>
                        	<?php if (sizeof($result)<=0){ ?><option value="<?php echo $receiver; ?>" selected="selected"><?php echo $receiver; ?></option><?php } ?>
							<?php
							
							$result = $mysqli->query("SELECT * FROM members");
							$rows = $result->num_rows;
							
							for ($i=0;$i<$rows;$i++){
								$stmt = $mysqli->prepare("SELECT role, mobile_number, first_name, last_name FROM members LIMIT 1 OFFSET ?");
								$stmt->bind_param('s', $i);
								$stmt->execute();
								$stmt->store_result();
						 
								$stmt->bind_result($role, $mobile_number, $receiver_first_name, $receiver_last_name);
								$stmt->fetch();
								$stmt->close();
								
								
								$display_name = $receiver_first_name." ".$receiver_last_name;
								if ($display_name == " "){
									$display_name = $mobile_number;
								}
								echo '<option value="'.$mobile_number.'" ';
								if ($receiver == $mobile_number){echo 'selected="selected"';}
								echo '>';
								echo $display_name.'</option>';
								
						
							}
							?>
						</select>
                        <script>
						$("#receiver").chosen({
							width: "165px !important",
							allow_single_deselect: true,
							no_results_text : "کاربری یافت نشد.",
						});
						</script>
                        &ensp;<a target="_blank" href="member_add.php"><button class="btn btn-success" type="button" style="margin:-10px 0 0 0; padding:0 2px;"><i class="icon-plus"></i></button></a>
                    </td>
				</tr>
                <tr>
					<td><h5 class="normal">متن <span class="red">*</span></h5></td>
					<td style="padding: 5px 0;"><textarea maxlength="" style="font: normal 11px tahoma; width:200px;" value="" name="text" id="text"><?php echo $text; ?></textarea></td>
				</tr>
                <tr>
					<td><h5 class="normal">تاریخ <span class="red">*</span></h5></td>
					<td style="padding: 5px 0 0 0;">
                    	<input type="text" style="font: normal 11px tahoma; width:100px;" value="<?php echo $date ?>" name="date" id="date" maxlength="20" />
                    </td>
				</tr>
                <tr>
					<td><h5 class="normal">زمان <span class="red">*</span></h5></td>
					<td style="padding: 5px 0 0 0;">
                    	<input type="text" style="font: normal 11px tahoma; width:100px;" value="<?php echo $time ?>" name="time" id="time" maxlength="20" />
                    </td>
				</tr>
			</table>
            <button class='btn btn-info pull-left' type="submit"><span>ویرایش</span> <i class="icon-edit icon-white"></i></button>
		</form>
	</div>
</div>
<?php include('footer.php'); ?>