<?php 
require_once('header.php'); 
?>

<?php if ($role != "admin"){header("Location: login.php?redirect=".urlencode("http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']));} ?>

<?php
if (!empty($err)) {
	switch ($err) {
	
	default : $err_msg = "خطای غیر منتظره‌ای پیش آمده!"; break;
	}
	
}
?> 
<?php
if (!empty($suc)) {
	switch ($suc) {
	
	default : $suc_msg = "عملیات با موفقیت انجام شد!"; break;
	}
	
}
?> 
<?php
if (!empty($err_msg)) {
	echo '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">&times;</button><p>'. $err_msg .'</p></div>';
}
?>
<?php
if (!empty($suc_msg)) {
	echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button><p>'. $suc_msg .'</p></div>';
}
?>
	
<?php

if (!isset($_SESSION['members_redirect'])){$_SESSION['members_redirect'] = "members.php";}

// ------

$id = filter_input(INPUT_GET, 'id', $filter = FILTER_SANITIZE_STRING);

// ------

$stmt = 'SELECT username, email, role, level, status, profile_image, first_name, last_name, mobile_number, mobile_number_status, home_number, fax_number, office, address, presenter, balance, reward_percentage FROM members WHERE id="'.$id.'"';

if ($mysqli->query($stmt)->num_rows<1){ header('Location: members.php?err=not-exists'); exit; }

$stmt = $mysqli->prepare($stmt);
$stmt->execute();
$stmt->store_result();

$stmt->bind_result($username, $email, $role, $level, $status, $profile_image, $first_name, $last_name, $mobile_number, $mobile_number_status, $home_number, $fax_number, $office, $address, $presenter, $balance, $reward_percentage);
$stmt->fetch();
$stmt->close();

/* PRESENTER */

$stmt = $mysqli->prepare("SELECT username, first_name, last_name FROM members WHERE id='".$presenter."'");
$stmt->execute();
$stmt->store_result();
$stmt->bind_result($presenter_username, $presenter_first_name, $presenter_last_name);
$stmt->fetch();
$stmt->close();

$presenter_display_name = $presenter_first_name." ".$presenter_last_name;
if ($presenter_display_name == " "){
	$presenter_display_name = $presenter_username;
}

$presenter_link = '<a href="member_edit.php?id='.$presenter.'">'.$presenter_display_name.'</a>';

?>
<div class="container">
	<div class="pull-left">
    	<a href="member_edit.php?<?php echo 'id='.$id ?>"><button class='btn btn-primary'><span>ویرایش</span> <i class="icon-edit icon-white"></i></button></a>
		<a href="<?php echo $_SESSION['members_redirect'] ?>"><button class='btn'><span>بازگشت</span> <i class="icon-chevron-left"></i></button></a>
	</div>
	<button class="btn disabled"><span id="subtitle">حذف عضو</span></button><br /><br />
	<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>
		<p>آیا شما مطمئنید؟
		<form action="<?php echo $options["url"] ?>/inc/delete_member.php" method="post">
			<input type="hidden" value="members.php?" name="redirect" id="redirect"/>
			<input type="hidden" value="<?php echo $id ?>" name="id" id="id"/>
			<button style="margin-right:50px;" type="submit" class="btn btn-danger Yekan normal">بله</button>
			<a href="<?php echo $_SESSION['members_redirect'] ?>" class="btn Yekan normal">خیر</a>
		</form>
		</p>
	</div>
	<div class="pull-right span2" id="sidebar">
		<div class="pull-right" id="thumb">
			<a class="fancybox" href="<?php if (if_file_exists('files/profile_images/'.$profile_image)){ echo 'files/profile_images/'.$profile_image; } else{ echo 'img/profile_image.png'; } ?>">
				<img class="img-circle span2" style="box-shadow: 0px 0px 5px #000;" src="<?php if (if_file_exists('files/profile_images/'.$profile_image)){ echo 'files/profile_images/'.$profile_image; } else{ echo 'img/profile_image.png'; } ?>" ></a>
		</div>
	</div>
	<div id="main" class="span4 pull-right">
		<table class="table table-striped table-right">
			<tr>
				<td class="span2"><h5 class="normal">نام کاربری</h5></td>
				<td style="padding: 5px 0 0 0;"><?php echo $username ?></td>
			</tr>
			<tr>
				<td><h5 class="normal">ایمیل</h5></td>
				<td style="padding: 5px 0 0 0;"><?php echo $email ?></td>
			</tr>
			<tr>
				<td><h5 class="normal">نقش</h5></td>
				<td style="padding: 5px 0 0 0;">
					<?php
					if (isset($member_roles[$role])){
						echo $member_roles[$role];
					} else {
						$stmt = $mysqli->prepare('SELECT role_title FROM members_roles WHERE slug="'.$role.'"');
						$stmt->execute();	
						$stmt->bind_result($role_title);
						$stmt->fetch();
						$stmt->close();
						
						echo $role_title;
					}
					?>
				</td>
			</tr>
            <tr>
				<td><h5 class="normal">سطح</h5></td>
				<td style="padding: 5px 0 0 0;"><?php echo $member_levels[$level]; ?></td>
			</tr>
            <tr>
				<td><h5 class="normal">وضعیت</h5></td>
				<td style="padding: 5px 0 0 0;"><?php echo $member_statuses[$status] ?></td>
			</tr>
			<tr>
				<td><h5 class="normal">نام</h5></td>
				<td style="padding: 5px 0 0 0;"><?php echo $first_name ?></td>
			</tr>
			<tr>
				<td><h5 class="normal">نام خانوادگی</h5></td>
				<td style="padding: 5px 0 0 0;"><?php echo $last_name ?></td>
			</tr>
            <tr>
				<td><h5 class="normal">شماره موبایل</h5></td>
				<td style="padding: 5px 0 0 0;">98<?php echo $mobile_number ?>+</td>
			</tr>
            <tr>
				<td><h5 class="normal">وضعیت شماره موبایل</h5></td>
				<td style="padding: 5px 0 0 0;"><?php echo $mobile_number_statuses[$mobile_number_status] ?></td>
			</tr>
			<tr>
				<td><h5 class="normal">شماره منزل</h5></td>
				<td style="padding: 5px 0 0 0;">98<?php echo $home_number ?>+</td>
			</tr>
            <tr>
				<td><h5 class="normal">شماره فکس</h5></td>
				<td style="padding: 5px 0 0 0;">98<?php echo $fax_number ?>+</td>
			</tr>
            <tr>
				<td><h5 class="normal">شرکت</h5></td>
				<td style="padding: 5px 0 0 0;"><?php echo $office ?></td>
			</tr>
            <tr>
				<td><h5 class="normal">آدرس</h5></td>
				<td style="padding: 5px 0 0 0;"><?php echo $address ?></td>
			</tr>
            <tr>
				<td><h5 class="normal">معرف</h5></td>
				<td style="padding: 5px 0 0 0;"><?php echo $presenter_link ?></td>
			</tr>
            <tr>
				<td><h5 class="normal">موجودی</h5></td>
				<td style="padding: 5px 0 0 0;"><?php echo $balance ?> ریال</td>
			</tr>
            <tr>
				<td><h5 class="normal">درصد جایزه</h5></td>
				<td style="padding: 5px 0 0 0;"><?php echo $reward_percentage ?>%</td>
			</tr>
		</table>
	</div>
	<div style="clear:both"></div>
</div>
<?php require_once('footer.php'); ?>