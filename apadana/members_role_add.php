<?php 
include('header.php'); 
?>

<?php if ($role != "admin"){header("Location: login.php?redirect=".urlencode("http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']));} ?>

<?php
if (!empty($err)) {
	switch ($err) {
	case "insert" : $err_msg = "خطا در نوشتن دیتابیس!"; break;
	case "fill" : $err_msg = "موارد الزامی را پر کنید!"; break;
	case "exists" : $err_msg = "نقش عضو وجود دارد!"; break;
	
	default : $err_msg = "خطای غیر منتظره‌ای پیش آمده!"; break;
	}
	
}
?> 
<?php
if (!empty($suc)) {
	switch ($suc) {
	case "insert" : $suc_msg = "نقش عضو اضافه شد!"; break;
	
	default : $suc_msg = "عملیات با موفقیت انجام شد!"; break;
	}
	
}
?> 
<?php
if (!empty($err_msg)) {
	echo '<div class="alert alert-error no-print"><a href="'.str_replace('err='.$err,"",implode('&',array_unique(explode('&',$_SERVER['REQUEST_URI'])))).'"><button type="button" class="close">&times;</button></a><p>'. $err_msg .'</p></div>';
}
?>
<?php
if (!empty($suc_msg)) {
	echo '<div class="alert alert-success no-print"><a href="'.str_replace('suc='.$suc,"",implode('&',array_unique(explode('&',$_SERVER['REQUEST_URI'])))).'"><button type="button" class="close">&times;</button></a><p>'. $suc_msg .'</p></div>';
}
?>
<?php if (empty($_SESSION['members_roles_redirect'])){$_SESSION['members_roles_redirect']="members_roles.php";} ?>
<div class="container">
	<a href="<?php echo $_SESSION['members_roles_redirect'] ?>"><button class='pull-left btn'><span>بازگشت</span> <i class="icon-chevron-left"></i></button></a>
	<button class="btn disabled"><span id="subtitle">اضافه‌کردن نقش عضو</span></button><br /><br />
	<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert">&times;</button>
	<p>مواردی که با علامت ستاره‌ی قرمز مشخص شده‌ است، الزامی است.</p></div>
	<h5 class="normal">مجوز‌های موجود:</h5>
	<div style="background: #ddd; margin-bottom: 10px; padding: 5px;">
		<?php
		foreach($member_permissions as $permission_slug => $permission_title){ ?>
		<button class="btn tahoma size-11" style="margin: 0 0 5px 10px;" onclick='a = "<?php echo $permission_slug ?>"; b= "<?php echo $permission_title ?>"; add_permission(a,b);'><?php echo $permission_title ?></button>
		<?php } ?>
	</div>
	<div id="right" class="span6 pull-right">
		<form action="<?php echo $options["url"] ?>/inc/add_members_role.php" method="post">
        	<input type="hidden" name="redirect" id="redirect" value="members_role_add.php?" />
			<input type="hidden" maxlength="1000" style="font: normal 11px tahoma; width:110px;" name="permissions" id="permissions" />
			<table class="table table-striped table-right">
			<tr>
				<td class="span2"><h5 class="normal">نامک <span class="red">*</span></h5></td>
					<td style="padding: 5px 0 0 0;"><input type="text" maxlength="100" style="font: normal 11px tahoma; width:100px;" value="" name="slug" id="slug"/></td>
				</tr>
				<tr>
					<td><h5 class="normal">عنوان <span class="red">*</span></h5></td>
					<td style="padding: 5px 0 0 0;"><input type="text" maxlength="200" style="font: normal 11px tahoma; width:150px;" value="" name="role_title" id="role_title"/></td>
				</tr>
				<tr>
					<td><h5 class="normal">مجوزها <span class="red">*</span></h5></td>
					<td style="padding: 5px 0; width: 400px;">
						<div class="normal tahoma size-11" id="permissions_span"></div>
					</td>
				</tr>
			</table>
			<button class='btn btn-info pull-left' type="submit"><span>اضافه کردن</span> <i class="icon-plus icon-white"></i></button>
		</form>
	</div>

</div>
<script type="text/javascript">

function add_permission(slug, title){
	
	var permissions = $("#permissions").val();
	var permission_span = '<button id="p_'+slug+'" class="btn tahoma size-11" style="margin: 0 0 5px 10px;">'+title+'</button>';

	if ((permissions).indexOf(slug) < 0){
			$("#permissions").val(permissions+slug+";");
			$("#permissions_span").append(permission_span).button().click();
			$("#p_"+slug).click(function(){delete_permission(slug,title)});
	};
}

</script>
<script type="text/javascript">

function delete_permission(slug, title){
	
	$("#permissions").val($("#permissions").val().replace(slug+";",""));
	$("#p_"+slug).remove();
	
}

</script>
<?php include('footer.php'); ?>