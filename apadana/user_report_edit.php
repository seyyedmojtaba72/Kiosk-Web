<?php 
include('header.php'); 
?>

<?php if (if_has_permission($role,"edit_user_reports")){} else{header("Location: login.php?redirect=".urlencode("http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']));} ?>

<?php
if (!empty($err)) {
	switch ($err) {
	case "edit" : $err_msg = "خطا در نوشتن دیتابیس!"; break;
	case "fill" : $err_msg = "موارد الزامی را پر کنید!"; break;
	case "exists" : $err_msg = "گزارشی با این نامک وجود دارد!"; break;
	
	default : $err_msg = "خطای غیر منتظره‌ای پیش آمده!"; break;
	}
	
}
?> 
<?php
if (!empty($suc)) {
	switch ($suc) {
	case "edit" : $suc_msg = "گزارش ویرایش شد!"; break;
	
	default : $suc_msg = "عملیات با موفقیت انجام شد!"; break;
	}
	
}
?> 
<?php
if (!empty($err_msg)) {
	echo '<div class="alert alert-error no-print"><a href="'.str_replace('err='.$err,"",implode('&',array_unique(explode('&',$_SERVER['REQUEST_URI'])))).'"><button type="button" class="close">&times;</button></a><p>'. $err_msg .'</p></div>';
}
?>
<?php
if (!empty($suc_msg)) {
	echo '<div class="alert alert-success no-print"><a href="'.str_replace('suc='.$suc,"",implode('&',array_unique(explode('&',$_SERVER['REQUEST_URI'])))).'"><button type="button" class="close">&times;</button></a><p>'. $suc_msg .'</p></div>';
}
?>
<?php

if (!isset($_SESSION['user_reports_redirect'])){$_SESSION['user_reports_redirect'] = "user_reports.php";}

// ------

$id = filter_input(INPUT_GET, 'id', $filter = FILTER_SANITIZE_STRING);

// ------

$stmt = 'SELECT reporter, reported, subject, report, results, date, time, status FROM user_reports WHERE id="'.$id.'"';

if ($mysqli->query($stmt)->num_rows<1){ header('Location: user_reports.php?err=not-exists'); exit; }

$stmt = $mysqli->prepare($stmt);
$stmt->execute();
$stmt->store_result();

$stmt->bind_result($reporter, $reported, $subject, $report, $results, $date, $time, $status);
$stmt->fetch();
$stmt->close();

?>
<div class="container">
	<div class="pull-left">
		<a href="<?php echo $_SESSION['user_reports_redirect'] ?>"><button class='btn'><span>بازگشت</span> <i class="icon-chevron-left"></i></button></a>
	</div>
	<button class="btn disabled"><span id="subtitle">ویرایش گزارش</span></button><br /><br />
	<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert">&times;</button>
	<p>مواردی که با علامت ستاره‌ی قرمز مشخص شده‌ است، الزامی است.</p></div>
	<div id="main" class="span6 pull-right">
		<form action="<?php echo $options["url"] ?>/inc/edit_user_report.php" method="post">
			<input type="hidden" value="user_report_edit.php?id=<?php echo $id ?>&" name="redirect" id="redirect"/>
			<input type="hidden" value="<?php echo $id ?>" name="id" id="id"/>
			<table class="table table-striped table-right">
            	<tr>
                	<td><h5 class="span1 pull-right normal">گزارش کننده <span class="red">*</span></h5></td>
                	<td style="padding: 5px 0 0 0;"><input type="text" style="font: normal 11px tahoma; width:150px;" value="<?php echo $reporter; ?>" name="reporter" id="reporter" /></td>
				</tr>
                <tr>
					<td><h5 class="normal">گزارش شونده</h5></td>
					<td style="padding: 5px 0 0 0;"><input type="text" style="font: normal 11px tahoma; width:150px;" value="<?php echo $reported; ?>" name="reported" id="reported" /></td>
				</tr>
                <tr>
                	<td><h5 class="span1 pull-right normal">موضوع <span class="red">*</span></h5></td>
                	<td style="padding-top: 5px;">
						<select class="tahoma size-11" style="width: 200px;" name="subject" id="subject">
							<?php
							foreach ($user_report_subjects as $subject1=>$subject1_value){
								echo '<option ';
								if ($subject1==$subject){ echo 'selected="selected" ';}
								echo 'value="'.$subject1.'">'.$subject1_value.'</option>';
							}
							?>
						</select>
					</td> 
				</tr>
                <tr>
                	<td><h5 class="span1 pull-right normal">گزارش <span class="red">*</span></h5></td>
                	<td style="padding: 5px 0 0 0;"><textarea class="normal text-right span2" id="report" name="report" style="font: normal 11px tahoma; width:250px;"><?php echo $report ?></textarea></td>
				</tr>
                <tr>
                	<td><h5 class="span1 pull-right normal">نتایج بررسی <span class="red">*</span></h5></td>
                	<td style="padding: 5px 0 0 0;"><textarea class="normal text-right span2" id="results" name="results" style="font: normal 11px tahoma; width:250px;"><?php echo $results ?></textarea></td>
				</tr>
                <tr>
					<td><h5 class="normal">تاریخ <span class="red">*</span></h5></td>
					<td style="padding: 5px 0 0 0;"><input type="text" style="font: normal 11px tahoma; width:100px;" value="<?php echo $date ?>" name="date" id="date"/></td>
				</tr>
                <tr>
					<td><h5 class="normal">زمان <span class="red">*</span></h5></td>
					<td style="padding: 5px 0 0 0;"><input type="text" style="font: normal 11px tahoma; width:100px;" value="<?php echo $time ?>" name="time" id="time"/></td>
				</tr>
                <tr>
					<td><h5 class="normal">وضعیت <span class="red">*</span></h5></td>
					<td style="padding-top: 5px;">
						<select class="tahoma size-11" style="width: 100px;" name="status" id="status">
							<?php
							foreach ($user_report_statuses as $status1=>$status1_value){
								echo '<option ';
								if ($status1==$status){ echo 'selected="selected" ';}
								echo 'value="'.$status1.'">'.$status1_value.'</option>';
							}
							?>
						</select>
					</td> 
				</tr>
			</table>
			<button class='btn btn-info pull-left' type="submit"><span>ویرایش</span> <i class="icon-edit icon-white"></i></button>
		</form>
	</div>
</div>
<?php include('footer.php'); ?>