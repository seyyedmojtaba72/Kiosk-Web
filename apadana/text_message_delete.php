<?php 
include('header.php'); 
?>

<?php if (if_has_permission($role,"edit_text_messages")){} else{header("Location: login.php?redirect=".urlencode("http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']));} ?>

<?php
if (!empty($err)) {
	switch ($err) {
	
	default : $err_msg = "خطای غیر منتظره‌ای پیش آمده!"; break;
	}
	
}
?> 
<?php
if (!empty($suc)) {
	switch ($suc) {
	
	default : $suc_msg = "عملیات با موفقیت انجام شد!"; break;
	}
	
}
?> 
<?php
if (!empty($err_msg)) {
	echo '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">&times;</button><p>'. $err_msg .'</p></div>';
}
?>
<?php
if (!empty($suc_msg)) {
	echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button><p>'. $suc_msg .'</p></div>';
}
?>
	
<?php

if (!isset($_SESSION['text_messages_redirect'])){$_SESSION['text_messages_redirect'] = "text_messages.php";}

// ------

$id = filter_input(INPUT_GET, 'id', $filter = FILTER_SANITIZE_STRING);

// ------

$stmt = 'SELECT receiver, text, date, time FROM text_messages WHERE id="'.$id.'"';

if ($mysqli->query($stmt)->num_rows<1){ header('Location: text_messages.php?err=not-exists'); exit; }

$stmt = $mysqli->prepare($stmt);
$stmt->execute();
$stmt->store_result();

$stmt->bind_result($receiver, $text, $date, $time);
$stmt->fetch();
$stmt->close();


/* RECEIVER */

$stmt = $mysqli->prepare("SELECT id, mobile_number, first_name, last_name FROM members WHERE mobile_number='".$receiver."'");
$stmt->execute();
$stmt->store_result();
$stmt->bind_result($receiver_id, $receiver_mobile_number, $receiver_first_name, $receiver_last_name);
$stmt->fetch();
$nums = $stmt->num_rows;
$stmt->close();

$receiver_display_name = $receiver_first_name." ".$receiver_last_name;
if ($receiver_display_name == " "){
	$receiver_display_name = $receiver_mobile_number;
}

$receiver_link = '<a href="member_edit.php?id='.$receiver_id.'">'.$receiver_display_name.'</a>';

if ($nums<=0){
	$receiver_link = $receiver;
}

?>

<div class="container">
	<div class="pull-left">
    	<a href="text_message_edit.php?<?php echo 'id='.$id ?>"><button class='btn btn-primary'><span>ویرایش</span> <i class="icon-edit icon-white"></i></button></a>
		<a href="<?php echo $_SESSION['text_messages_redirect'] ?>"><button class='btn'><span>بازگشت</span> <i class="icon-chevron-left"></i></button></a>
	</div>
	<button class="btn disabled"><span id="subtitle">حذف پیامک</span></button><br /><br />
	<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>
		<p>آیا شما مطمئنید؟
		<form action="<?php echo $options["url"] ?>/inc/delete_text_message.php" method="post">
			<input type="hidden" value="text_messages.php?" name="redirect" id="redirect"/>
			<input type="hidden" value="<?php echo $id ?>" name="id" id="id"/>
			<button style="margin-right:50px;" type="submit" class="btn btn-danger Yekan normal">بله</button>
			<a href="<?php echo $_SESSION['text_messages_redirect'] ?>" type="button" class="btn Yekan normal">خیر</a>
		</form>
		</p>
	</div>
	<div id="main" class="span7 pull-right">
		<table class="table table-striped table-right">
			<tr>
				<td class="span2"><h5 class="normal">دریافت‌کننده </h5></td>
				<td style="padding: 5px 0 0 0;"><?php echo $receiver_link; ?></td>
			</tr>
            <tr>
				<td><h5 class="normal">متن </h5></td>
				<td style="padding: 5px 0 0 0;"><?php echo nl2br($text); ?></td>
			</tr>
            <tr>
				<td><h5 class="normal">تاریخ </h5></td>
				<td style="padding: 5px 0 0 0;"><?php echo $date; ?></td>
			</tr>
			<tr>
				<td><h5 class="normal">زمان </h5></td>
				<td style="padding: 5px 0 0 0;"><?php echo $time; ?></td>
			</tr>
		</table>
	</div>
</div>
<?php include('footer.php'); ?>