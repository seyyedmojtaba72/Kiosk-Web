<?php

$action = filter_input(INPUT_GET, 'action', $filter = FILTER_SANITIZE_STRING);

if ($action == "get_spinner"){
	require_once('inc/functions.php');
	
	$spinner = filter_input(INPUT_GET, 'spinner', $filter = FILTER_SANITIZE_STRING);
	
	load_spinner($spinner);
	return;
}

require_once('header.php'); 
?>
<?php if (if_has_permission($role,"edit_options")){} else{header("Location: login.php?redirect=".urlencode("http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']));} ?>
<?php
if (!empty($err)) {
	switch ($err) {
	case "edit" : $err_msg = "خطا در نوشتن دیتابیس!"; break;
	case "fill" : $err_msg = "موارد الزامی را پر کنید!"; break;
	
	default : $err_msg = "خطای غیر منتظره‌ای پیش آمده!"; break;
	}
	
}
?> 
<?php
if (!empty($suc)) {
	switch ($suc) {
	case "edit" : $suc_msg = "تنظیمات ویرایش شد!"; break;
	case "run" : $suc_msg = "دستور اجرا شد!"; break;
	case "download" : $suc_msg = "پشتیبان تهیه شد!"; header('Location: inc/backup.sql'); break;
	case "upload" : $suc_msg = "پشتیبان بازگردانده شد!"; break;

	default : $suc_msg = "عملیات با موفقیت انجام شد!"; break;
	}
	
}
?> 
<div id="alert-error" class="alert alert-error no-print <?php if (empty($err_msg)){ echo "hidden"; } ?>"> <a href="<?php echo str_replace('err='.$err,"", fix_address($_SERVER['REQUEST_URI'])); ?>">
  <button type="button" class="close">&times;</button>
  </a>
  <p id="error-message">
    <?php if (!empty($err_msg)){ echo $err_msg; } ?>
  </p>
</div>
<div id="alert-success" class="alert alert-success no-print <?php if (empty($suc_msg)){ echo "hidden"; } ?>"> <a href="<?php echo str_replace('suc='.$suc,"", fix_address($_SERVER['REQUEST_URI'])); ?>">
  <button type="button" class="close">&times;</button>
  </a>
  <p id="success-message">
    <?php if (!empty($suc_msg)){ echo $suc_msg; } ?>
  </p>
</div>
<?php function print_options($table, $section){
	global $mysqli;
	$stmt = $mysqli->prepare('select count(1) FROM '.$table.' WHERE display="auto" AND section="'.$section.'"');
	$stmt->execute();
	$stmt->bind_result($rows);
	$stmt->fetch();
	$stmt->close();
	
	echo '
	<table class="table table-striped table-hover text-center">
		<tr>
			<td style="width: 30%;"><h5 class="normal">عنوان</h5></td>
			<td style="width: 60%;"><h5 class="normal">مقدار <span class="red">*</span></h5></td>
			<td style="width: 5%;"></td>
			<td style="width: 5%;"><h5 class="normal">عملیات</h5></td>
		</tr>';

	for ($i=0;$i<$rows;$i++){
		$stmt = $mysqli->prepare("SELECT variable, variable_title, help, type, pre_values, value FROM ".$table." WHERE display='auto' AND section='".$section."' LIMIT 1 OFFSET ?");
		$stmt->bind_param('s', $i);
		$stmt->execute();
		$stmt->store_result();
 
		$stmt->bind_result($variable, $variable_title, $help, $type, $pre_values, $value);
		$stmt->fetch();
		$stmt->close();
		
		$options[$variable] = $value;


		echo '
		<tr>
			<form method="post" id="#'.$variable.'">	
				<td><h5 class="normal">'.$variable_title.'</h5>'.nl2br($help).'</td>
				<input type="hidden" value="options.php?" name="redirect" />
				<input type="hidden" value="'.$table.'" name="table" />
				<input type="hidden" value="'.$type.'" name="type" />
				<input type="hidden" value="'.$variable.'" name="variable" />
				<td><h5>';
					if ($type=="text" | $type=="number" | $type=="email" | $type=="password"){
						echo '
						<input id="input_'.$section.'_'.$variable.'" type="'.$type.'"  style="font: normal 11px tahoma; width:160px;" value="'.$value.'" name="value"/>';
					} elseif ($type=="select"){
						$values = explode(",",$pre_values);
						echo '
						<select id="input_'.$section.'_'.$variable.'" class="tahoma size-11" style="width: 175px; height: 25px;" name="value">
							';
							for ($i2=0;$i2<sizeof($values);$i2++){
								echo '<option ';
								if ($options[$variable]==$values[$i2]){ echo 'selected="selected" '; }
								echo 'value='.$values[$i2].'>'.$values[$i2].'</option>';
							}
							echo '
						</select>';
					} elseif ($type=="textarea"){
						echo '
						<textarea id="input_'.$section.'_'.$variable.'"  style="font: normal 11px tahoma; width:300px;" name="value">'.$value.'</textarea>';
					}  elseif ($type=="html"){
						echo '</h5>
						<div class="text-right" style="direction: ltr; margin: auto; width: 500px; background: white;">
							<textarea id="input_'.$section.'_'.$variable.'"  style="font: normal 11px tahoma; width: 100%; height: 200px; margin:0;" name="value">'.$value.'</textarea>
							<script type="text/javascript">
							  $("#input_'.$section.'_'.$variable.'").summernote({
								height: 200,
								direction: "rtl"
							  });
							</script>
						</div><h5>
						';
					}
					echo '
					</h5></td>
					<td style="width: 50px;"><h1>
						<span id="spinner_'.$section.'_'.$variable.'"></span>
					</h1></td>
					<td><h5><button onclick="edit_option(this.form, \'spinner_'.$section.'_'.$variable.'\');" class="btn btn-info" style="margin-top:5px;padding:0 2px;" type="button"><i class="icon-edit"></i></button></h5></td>
			</form>
		</tr>';
	}
	
	echo '</table>';	
} ?>
<div class="container" style="min-height: 450px;">
	<div class="pull-left">
		<a href="panel.php"><button class='btn'><span>بازگشت</span> <i class="icon-chevron-left"></i></button></a>
	</div>
	<div id="main-content">
    	<button class="btn disabled"><span id="subtitle">تنظیمات</span></button><br /><br />
		<div class="text-left">
			<ul class="nav nav-tabs">
				<li class="pull-right active"><a href="#system" data-toggle="tab">سامانه</a></li>
                <li class="pull-right"><a href="#application" data-toggle="tab">اپلیکیشن</a></li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active" id="system">
					<ul class="nav nav-pills">
						<li class="pull-right active"><a href="#general" data-toggle="tab">عمومی</a></li>
                        <li class="pull-right"><a href="#pages" data-toggle="tab">صفحات</a></li>
                        <li class="pull-right"><a href="#email" data-toggle="tab">ایمیل</a></li>
                        <li class="pull-right"><a href="#sms" data-toggle="tab">پیامک</a></li>
                        <li class="pull-right"><a href="#online-payment" data-toggle="tab">پرداخت آنلاین</a></li>
						<li class="pull-right"><a href="#database" data-toggle="tab">پایگاه داده</a></li>
                        <li class="pull-right"><a href="#custom" data-toggle="tab">سفارشی</a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="general">
							<?php print_options("options", "general"); ?>
						</div>
                        <div class="tab-pane" id="pages">
							<?php print_options("options", "pages"); ?>
						</div>
                        <div class="tab-pane" id="email">
							<?php print_options("options", "email"); ?>
						</div>
                        <div class="tab-pane" id="sms">
							<?php print_options("options", "sms"); ?>
						</div>
                        <div class="tab-pane" id="online-payment">
							<?php print_options("options", "online-payment"); ?>
						</div>
						<div class="tab-pane" id="database">
							<table class="table table-striped table-hover text-center">
								<tr>
									<td><h5 class="normal">تهیه‌ی پشتیبان از پایگاه داده:</h5></td>
									<td>
										<form action="<?php echo $options["url"] ?>/inc/backup_upload.php" method="post" enctype="multipart/form-data" style="margin-top: 5px;">
											<input type="file" style="font: normal 11px Tahoma;" name="file" id="file" />
											<button class="btn btn-primary" type="submit"><span>ارسال</span> <i class="icon-upload icon-white"></i></button>
										</form>
										
									</td>
									<td>
										<form action="<?php echo $options["url"] ?>/inc/backup_download.php" method="post" style="margin-top: 5px;">
											<button class="btn btn-success" type="submit"><span>دریافت</span> <i class="icon-download icon-white"></i></button>
										</form>
									</td>
								</tr>
								<tr>
									<form action="<?php echo $options["url"] ?>/inc/run_command.php" method="post">
										<td><h5 class="normal">اجرای دستور SQL:</h5></td>
										<td><h5><input type="text" name="command" style="width: 400px; direction: ltr;" /></h5></td>
										<td><h5><button class="btn btn-info" type="submit"><i class="icon-play-circle"></i></button></h5></td>
									</form>
								</tr>
							</table>
						</div>
                        <div class="tab-pane" id="custom">
							<?php print_options("options", "custom"); ?>
						</div>
					</div>
				</div>
                <div class="tab-pane" id="application">
					<ul class="nav nav-pills">
                    	<li class="pull-right active"><a href="#app_general" data-toggle="tab">عمومی</a></li>
						<li class="pull-right"><a href="#versioning" data-toggle="tab">نسخه‌بندی</a></li>
					</ul>
					<div class="tab-content">
                    	<div class="tab-pane active" id="app_general">
							<?php print_options("options", "app_general"); ?>
						</div>
						<div class="tab-pane" id="versioning">
							<?php print_options("options", "versioning"); ?>
						</div>
					</div>
				</div>
			</div>
		</div>	
	</div>
</div>
<script>
function edit_option(form, loader){
	show_small_loading(loader, '<?php echo $options["url"]; ?>');
	if (window.XMLHttpRequest) {
		// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	} else {
		// code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange = function() {
	
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			if (xmlhttp.responseText == "success"){
				$("#alert-success").css("display","block").css("visibility","visible");
				document.getElementById("success-message").innerHTML = "تنظیمات ویرایش شد.";
			
			} else {
				$("#alert-error").css("display","block").css("visibility","visible");
				document.getElementById("error-message").innerHTML = "تنظیمات ویرایش نشد.";
			}
			
			setTimeout(function(){hide_small_loading(loader, '<?php echo $options["url"]; ?>')}, 250);
		}
	}
	
	xmlhttp.open("POST","inc/edit_option.php");
	xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	if (form.type.value == "html"){
		$(form.value).destroy();
	}
	xmlhttp.send("table=" + form.table.value + "&variable=" + form.variable.value + "&value=" + form.value.value);
	if (form.type.value == "html"){
		$(form.value).summernote({height: 200,direction: "rtl",focus: true});
	}
	
}

$('#input_general_spinner').change(function(){
	$.get("options.php?action=get_spinner&spinner="+this.value, function(data,status){
		$('#spinner_general_spinner').html(data);
	});
});
</script>
<?php require_once('footer.php'); ?>