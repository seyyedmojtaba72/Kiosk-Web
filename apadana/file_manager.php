<?php 		
require_once('header.php');
?>

<?php if (if_has_permission($role,"manage_files")){} else{header("Location: login.php?redirect=".urlencode("http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']));} ?>

<?php
if (!empty($err)) {
	switch ($err) {
	
	default : $err_msg = "خطای غیر منتظره‌ای پیش آمده!"; break;
	}
	
}
?> 
<?php
if (!empty($suc)) {
	switch ($suc) {
	
	default : $suc_msg = "عملیات با موفقیت انجام شد!"; break;
	}
	
}
?> 
<?php
if (!empty($err_msg)) {
	echo '<div class="alert alert-error no-print"><a href="'.str_replace('err='.$err,"",implode('&',array_unique(explode('&',$_SERVER['REQUEST_URI'])))).'"><button type="button" class="close">&times;</button></a><p>'. $err_msg .'</p></div>';
}
?>
<?php
if (!empty($suc_msg)) {
	echo '<div class="alert alert-success no-print"><a href="'.str_replace('suc='.$suc,"",implode('&',array_unique(explode('&',$_SERVER['REQUEST_URI'])))).'"><button type="button" class="close">&times;</button></a><p>'. $suc_msg .'</p></div>';
}
?>


<div class="container">
	<div class="pull-left">
		<a href="panel.php"><button class='btn'><span>بازگشت</span> <i class="icon-chevron-left"></i></button></a>
	</div>
	<div id="main-content">
		<button class="btn disabled"><span id="subtitle">مدیریت فایل</span></button><br /><br />
		
		<div id="elfinder"><iframe src="<?php echo $options["url"] ?>/lib/elfinder-2.0-rc1/elfinder.php" width="1000" height="510" scrolling="yes"></iframe></div>
		
	</div>		
</div>
<?php require_once('footer.php'); ?>