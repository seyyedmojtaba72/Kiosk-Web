<?php 

$step = filter_input(INPUT_GET, 'step', $filter = FILTER_SANITIZE_STRING);

if ($step == "0"){
	require_once 'inc/db_connect.php';
	require_once 'inc/functions.php';
	ob_start();
	sec_session_start();
	require_once 'inc/information.php';
	require_once('inc/statistics.php');
	
	$action = filter_input(INPUT_GET, 'action', $filter = FILTER_SANITIZE_STRING);
	
	if ($action == "perform_discount"){
		$code = filter_input(INPUT_GET, 'code', $filter = FILTER_SANITIZE_STRING);
		$amount = filter_input(INPUT_GET, 'amount', $filter = FILTER_SANITIZE_STRING);
		$id = filter_input(INPUT_GET, 'id', $filter = FILTER_SANITIZE_STRING);
		
		$results = array();
		
		$result = mysql_select('discounts', array('code'=>$code), array('percentage', 'number', 'used', 'min_price', 'start_date', 'end_date', 'product', 'status'));
		
		if (sizeof($result)<=0){
			$results['status'] = 'fail';
			$results['result'] = 'کد وارد شده صحیح نیست!';
			
			echo json_encode($results);
			return;
		} 
		
		$start_date = str_replace("/", "", normalDate($result[0]["start_date"], "/"));
		$end_date = str_replace("/", "", normalDate($result[0]["end_date"], "/"));
		$date = str_replace("/", "", normalDate($date, "/"));
		
		if($start_date <= $date && $date <= $end_date){}
		else{
			$results['status'] = 'fail';
			$results['result'] = 'تاریخ کد وارد شده منقضی شده است!';
			
			echo json_encode($results);
			return;
		}
		
		
		$number = $result[0]['number'];
		$used = $result[0]['used'];
		$rest = $number - $used;
		
		if ($rest <= 0){
			$results['status'] = 'fail';
			$results['result'] = 'تعداد استفاده از کد وارد شده منقضی شده است!';
			
			echo json_encode($results);
			return;
		}
		
		$min_price = $result[0]['min_price'];
		
		if ($amount < $min_price){
			$results['status'] = 'fail';
			$results['result'] = 'حداقل قیمت سفارش باید '.$min_price.' ریال باشد!';
			
			echo json_encode($results);
			return;
		}
		
		$product = $result[0]['product'];
		
		if (!empty($product) && !empty($id)){
			$order_result = mysql_select('orders', array('id'=>$id), array('product'));
			$order_product = $order_result[0]["product"];
			
			if ($order_product != $product){
				$results['status'] = 'fail';
				$results['result'] = 'کد تخفیف وارد شده مربوط به این محصول نیست!';
				
				echo json_encode($results);
				return;
			}
			
		}
		
		$status = $result[0]['status'];
		
		if ($status == "inactive"){
			$results['status'] = 'fail';
			$results['result'] = 'کد وارد شده صحیح نیست!';
			
			echo json_encode($results);
			return;
		}
				
		$results['status'] = 'success';
		$results['result'] = 'تخفیف اعمال شد.';
		$results['discount'] = $result[0]['percentage'];
		
		mysql_update('discounts', array('code'=>$code), array('used'=>($used+1)));
	
		echo json_encode($results);
		return;
	}
	
}

if ($step == "1"){
	require_once 'inc/db_connect.php';
	require_once 'inc/functions.php';
	ob_start();
	sec_session_start();
	require_once 'inc/information.php';
	require_once('inc/statistics.php');
	
	
	$action = filter_input(INPUT_GET, 'action', $filter = FILTER_SANITIZE_STRING);
	$customer = filter_input(INPUT_GET, 'customer', $filter = FILTER_SANITIZE_STRING);
	$amount = filter_input(INPUT_GET, 'amount', $filter = FILTER_SANITIZE_STRING);
	$redirect = filter_input(INPUT_GET, 'redirect', $filter = FILTER_SANITIZE_STRING);
	$mode = filter_input(INPUT_GET, 'mode', $filter = FILTER_SANITIZE_STRING);
	$id = filter_input(INPUT_GET, 'id', $filter = FILTER_SANITIZE_STRING);
	
	$results = array();
	
	$api = $options['online_payment_api_key'];
	$redirect = urlencode($options['url']."/online_payment.php?step=2&action=".$action."&customer=".$customer."&amount=".$amount."&redirect=".$redirect."&mode=".$mode."&id=".$id);
	
	if ($options['online_payment_gateway'] == "Payline"){
		
		/*if ($mode == "test"){
			$results['status'] = 'success';
			$results['result'] = $options['url']."/online_payment.php?step=2&action=".$action."&customer=".$customer."&amount=".$amount."&redirect=".$redirect."&mode=".$mode."&id=".$id;
			echo json_encode($results);
			return;
		}*/
			
	
		$url = 'http://46.209.244.198/payment/gateway-send';
		
		$result = pay_send($url,$api,$amount,$redirect);
		
		if(($result > 0 && is_numeric($result))){
			$results['status'] = 'success';
			$results['result'] = "http://46.209.244.198/payment/gateway-$result";
		} else {
			
			$results['status'] = 'fail';
		
			switch($result){
			case '-1':
			$result_text = 'api ارسالی با نوع api تعریف شده در payline سازگار نیست.';
			break;
			case '-2':
			$result_text = 'مبلغ کمتر از 1000 ریال است.';
			break;
			case '-3':
			$result_text = 'مقدار redirect رشته null است.';
			break;
			case '-4':
			$result_text = 'درگاهی با اطلاعات ارسالی شما یافت نشده و یا در حالت انتظار می باشد.';
			break;
			}
			
			$results['result'] = $result_text;
			
		}
	} else if ($options['online_payment_gateway'] == "ZarinPal"){
		$description = '';  // Required
		if ($action == "charge_balance"){
			$description = 'شارژ حساب';
		}
		
		$email = ''; // Optional
		$mobile =''; // Optional
	
	
		// URL also Can be https://ir.zarinpal.com/pg/services/WebGate/wsdl
		$client = new SoapClient('https://de.zarinpal.com/pg/services/WebGate/wsdl', array('encoding' => 'UTF-8')); 
	
		$result = $client->PaymentRequest(
			array(
					'MerchantID' 	=> $api,
					'Amount' 	=> $amount/10,
					'Description' 	=> $description,
					'Email' 	=> $email,
					'Mobile' 	=> $mobile,
					'CallbackURL' 	=> urldecode($redirect)
				)
		);
	
		//Redirect to URL You can do it also by creating a form
		if($result->Status == 100){
			$results['status'] = 'success';
			$results['result'] = 'https://www.zarinpal.com/pg/StartPay/'.$result->Authority;
		} else {
			$results['status'] = 'fail';
			$results['result'] = $result->Status;
		}
			
	}
	
	echo json_encode($results);
	return;
	
}

require_once('header.php'); 

if ($logged != "in" ){header("Location: login.php?redirect=".urlencode("http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']));} 


?>

<?php
if (!empty($err)) {
	switch ($err) {
	
	default : $err_msg = "خطاي غير منتظره‌اي پيش آمده!"; break;
	}
	
}
?> 
<?php
if (!empty($suc)) {
	switch ($suc) {

	default : $suc_msg = "عمليات با موفقيت انجام شد!"; break;
	}
	
}
?> 
<?php
if (!empty($err_msg)) {
	echo '<div class="alert alert-error no-print"><a href="'.str_replace('err='.$err,"",implode('&',array_unique(explode('&',$_SERVER['REQUEST_URI'])))).'"><button type="button" class="close">&times;</button></a><p>'. $err_msg .'</p></div>';
}
?>
<?php
if (!empty($suc_msg)) {
	echo '<div class="alert alert-success no-print"><a href="'.str_replace('suc='.$suc,"",implode('&',array_unique(explode('&',$_SERVER['REQUEST_URI'])))).'"><button type="button" class="close">&times;</button></a><p>'. $suc_msg .'</p></div>';
}
?>
<?php

$action = filter_input(INPUT_GET, 'action', $filter = FILTER_SANITIZE_STRING);
$customer = filter_input(INPUT_GET, 'customer', $filter = FILTER_SANITIZE_STRING);
$amount = filter_input(INPUT_GET, 'amount', $filter = FILTER_SANITIZE_STRING);
$redirect = filter_input(INPUT_GET, 'redirect', $filter = FILTER_SANITIZE_STRING);

if (empty($redirect)){
	$redirect = "profile.php";
}

$mode = filter_input(INPUT_GET, 'mode', $filter = FILTER_SANITIZE_STRING);
$id = filter_input(INPUT_GET, 'id', $filter = FILTER_SANITIZE_STRING);

if (empty($amount)){
	header('Location: panel.php');
	exit;
}

if ($step == "2"){
	$api = $options['online_payment_api_key'];
	
	if ($options['online_payment_gateway'] == "Payline"){
		
		$url = 'http://46.209.244.198/payment/gateway-result-second';
		$trans_id = $_POST['trans_id'];
		$id_get = $_POST['id_get'];
		$result = pay_get($url,$api,$trans_id,$id_get);
		
		switch($result){
		case '-1' :
			$result_text = 'api ارسالی با نوع api تعریف شده در payline سازگار نیست.';
			break;
		case '-2' :
			$result_text = 'trans_id ارسال شده معتبر نمی باشد.';
			break;
		case '-3' :
			$result_text = 'id_get ارسالی معتبر نمی باشد.';
			break;
		case '-4' :
			$result_text = 'چنین تراکنشی در سیستم وجود ندارد و یا موفقیت آمیز نبوده است.';
			break;
		case '1' :
		
			if ($action=="charge_balance"){
				$result = mysql_select('members', array('id'=>$customer), array('balance'));
				$balance = $result[0]["balance"];
				$balance += $amount;
				mysql_update('members', array('id'=>$customer), array('balance'=>$balance));
				
				if ($_SESSION['id'] == $customer){
					$_SESSION['balance'] = $balance;
				}
			} 
			
			mysql_insert('payments', array('customer'=>$customer, 'transaction_id'=>$trans_id, 'amount'=>$amount, 'action'=>$action, 'timestamp'=>time(), 'date'=>$date, 'time'=>$time, 'status'=>"success"));
			
			header('Location: '.urldecode($redirect).'&suc=pay&transaction_id='.$trans_id);
			exit;
			break;
		}
		
		if($result<0 && is_numeric($result)){
			mysql_insert('payments', array('customer'=>$customer, 'transaction_id'=>$trans_id, 'amount'=>$amount, 'action'=>$action, 'timestamp'=>time(), 'date'=>$date, 'time'=>$time, 'status'=>"fail"));
		}
	
	} else if ($options['online_payment_gateway'] == "ZarinPal"){
		$Authority = $_GET['Authority'];
		
		if($_GET['Status'] == 'OK'){
			// URL also Can be https://ir.zarinpal.com/pg/services/WebGate/wsdl
			$client = new SoapClient('https://de.zarinpal.com/pg/services/WebGate/wsdl', array('encoding' => 'UTF-8')); 
			
			$result = $client->PaymentVerification(
				array(
						'MerchantID'	 => $api,
						'Authority' 	 => $Authority,
						'Amount'	 => $amount/10
					)
			);
			
			if($result->Status == 100){
				if ($action=="charge_balance"){
					$result = mysql_select('members', array('id'=>$customer), array('balance'));
					$balance = $result[0]["balance"];
					$balance += $amount;
					mysql_update('members', array('id'=>$customer), array('balance'=>$balance));
				
					if ($_SESSION['id'] == $customer){
						$_SESSION['balance'] = $balance;
					}
				} 
			
				mysql_insert('payments', array('customer'=>$customer, 'transaction_id'=>$result->RefID, 'amount'=>$amount, 'action'=>$action, 'timestamp'=>time(), 'date'=>$date, 'time'=>$time, 'status'=>"success"));
				
				header('Location: '.urldecode($redirect).'&suc=pay&transaction_id='.$result->RefID);
				exit;
			} else {
				$result_text = 'پرداخت ناموفق بود. وضعیت: '.$result->Status;
				mysql_insert('payments', array('customer'=>$customer, 'transaction_id'=>$result->RefID, 'amount'=>$amount, 'action'=>$action, 'timestamp'=>time(), 'date'=>$date, 'time'=>$time, 'status'=>"fail"));
			}
	
		} else {
			$result_text = 'پرداخت منصرف شد.';
		}
	}
	
	echo '<script>fancyAlert("'.$result_text.'");</script>';
}

?>
<div class="container">
	<div class="pull-left">
		<a href="<?php echo urldecode($redirect); ?>"><button class='btn'><span>بازگشت</span> <i class="icon-chevron-left"></i></button></a>
	</div>
	<button class="btn disabled"><span id="subtitle">پرداخت آنلاين</span></button><br /><br />
    <input type="hidden" id="amount" value="<?php echo $amount; ?>" />
	<div id="right" class="span4 pull-right text-right">
        <table class="table table-striped table-right">
			<tr>
				<td class="span2"><h5 class="normal">مبلغ </h5></td>
                <td style="padding: 5px 0 0 0;"><?php echo $amount; ?> ریال</td>
            </tr>
            <tr>
                <td><h5 class="normal">قابل پرداخت </h5></td>
                <td style="padding: 5px 0 0 0;"><span id="to_pay"><?php echo $amount; ?></span> ریال</td>
            </tr>
        </table>
        <span id="spinner_pay"></span>
        <div class="pull-left">
        	<input type="text" id="discount_code" maxlength="30" placeholder="کد تخفیف" style="margin: 0px; font: normal 11px tahoma; width:100px;"/>
        	<button class='btn btn-success' onclick="perform_discount();" type="submit"><span>اعمال</span> <i class="icon-gift icon-white"></i></button>
        	<button class='btn btn-info' onclick="pay();" type="submit"><span>پرداخت</span> <i class="icon-ok icon-white"></i></button>
        </div>
	</div>
	<div style="clear:both"></div>
</div>
<script>
var discount_used=0;
function pay(){
	show_small_loading('spinner_pay', '<?php echo $options["url"]; ?>');
	$.get("online_payment.php?step=1&action=<?php echo $action; ?>&customer=<?php echo $_SESSION['id']; ?>&amount="+$("#amount").val()+"&redirect=<?php echo $redirect; ?>&mode=<?php echo $mode; ?>&id=<?php echo $id; ?>", function(data, status){
		var results = $.parseJSON(data);
		if (results['status'] == "success"){
			window.location.href = results['result'];
		} else {
			fancyAlert(results['result']);
		}
		hide_small_loading('spinner_pay');
	});
}

function perform_discount(){
	if (discount_used == 1){
		fancyAlert("شما قبلاً از تخفیف استفاده کرده اید.");
		return;
	}
	show_small_loading('spinner_pay', '<?php echo $options["url"]; ?>');
	$.get("online_payment.php?step=0&action=perform_discount&code="+$("#discount_code").val()+"&amount="+$("#amount").val()+"&id=<?php echo $id; ?>", function(data, status){
		var results = $.parseJSON(data);
		if (results['status'] == "success"){
			discount_used = 1;
			$("#amount").val(Math.floor($("#amount").val()-$("#amount").val()*(results["discount"])/100));
			$("#to_pay").html(Math.floor($("#amount").val()-$("#amount").val()*(results["discount"])/100));
		} 
		fancyAlert(results['result']);
		hide_small_loading('spinner_pay');
	});
}		
</script>
<?php require_once('footer.php'); ?>