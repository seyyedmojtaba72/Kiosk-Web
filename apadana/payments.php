<?php include('header.php'); ?>

<?php if ($role == "admin" | if_has_permission($role,"edit_payments")){} else{header("Location: login.php?redirect=".urlencode("http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']));} ?>

<?php
if (!empty($err)) {
	switch ($err) {
	case "not-exists" : $err_msg = "پرداخت وجود ندارد! ممکن است حذف شده باشد."; break;
	case "delete" : $err_msg = "خطا در حذف!"; break;
	case "no-match" : $err_msg = "جستجو نتیجه‌ای نداشت! <a style='margin-right:50px;' href='payments.php'><button class='btn Yekan normal'>نمایش همه</button></a>"; break;
	
	default : $err_msg = "خطای غیر منتظره‌ای پیش آمده!"; break;
	}
	
}
?> 
<?php
if (!empty($suc)) {
	switch ($suc) {
	case "delete" : $suc_msg = "پرداخت حذف شد!"; break;
	case "search" : $suc_msg = "جستجو با موفقیت انجام شد! <a style='margin-right:50px;' href='payments.php'><button class='btn Yekan normal'>نمایش همه</button></a>"; break;
	
	default : $suc_msg = "عملیات با موفقیت انجام شد!"; break;
	}
	
}
?> 
<?php
if (!empty($err_msg)) {
	echo '<div class="alert alert-error no-print"><a href="'.str_replace('err='.$err,"",implode('&',array_unique(explode('&',$_SERVER['REQUEST_URI'])))).'"><button type="button" class="close">&times;</button></a><p>'. $err_msg .'</p></div>';
}
?>
<?php
if (!empty($suc_msg)) {
	echo '<div class="alert alert-success no-print"><a href="'.str_replace('suc='.$suc,"",implode('&',array_unique(explode('&',$_SERVER['REQUEST_URI'])))).'"><button type="button" class="close">&times;</button></a><p>'. $suc_msg .'</p></div>';
}
?>
<?php

$_SESSION['payments_redirect'] = 
str_replace(basename($_SERVER['PHP_SELF']),basename($_SERVER['PHP_SELF'])."?",str_replace("?","",implode('&',array_unique(explode('&', $_SERVER['REQUEST_URI'])))));

// ------

$list_amount = $options['list_rows_per_page'];
$search = filter_input(INPUT_GET, 'search', $filter = FILTER_SANITIZE_STRING);
$order_by = filter_input(INPUT_GET, 'order_by', $filter = FILTER_SANITIZE_STRING);
$mode = filter_input(INPUT_GET, 'mode', $filter = FILTER_SANITIZE_STRING);
$page = filter_input(INPUT_GET, 'page', $filter = FILTER_SANITIZE_STRING);

if (empty($search)){$search = "false";}
if (empty($order_by)){$order_by = 'row';}
if (empty($mode)){$mode = 'DESC';}
if (empty($page)){$page = 1;}

// ------

$customer = filter_input(INPUT_GET, 'customer', $filter = FILTER_SANITIZE_STRING);
$transaction_id = filter_input(INPUT_GET, 'transaction_id', $filter = FILTER_SANITIZE_STRING);
$amount = filter_input(INPUT_GET, 'amount', $filter = FILTER_SANITIZE_STRING);
$action = filter_input(INPUT_GET, 'action', $filter = FILTER_SANITIZE_STRING);
$date = filter_input(INPUT_GET, 'date', $filter = FILTER_SANITIZE_STRING);
$time = filter_input(INPUT_GET, 'time', $filter = FILTER_SANITIZE_STRING);
$status = filter_input(INPUT_GET, 'status', $filter = FILTER_SANITIZE_STRING);

function get_link($order_by,$mode,$page){
	global $options, $list_amount, $search;
	global $customer, $transaction_id, $amount, $action, $date, $time, $status;

	$link="payments.php?";
	if ($search=="true"){
		
		$link.='suc=search&search=true&customer='.$customer.'&transaction_id='.$transaction_id.'&amount='.$amount.'&action='.$action.'&date='.$date.'&time='.$time.'&status='.$status.'&';		
	} 
	
	$link.="order_by=".$order_by."&mode=".$mode;
	if ($page!=0){$link.="&page=".$page;}
	
	return $link;
}
?>
	
	
<div class="container">
	<div class="pull-left no-print">
		<button class='btn btn-success' onclick="print();"><span>چاپ</span> <i class="icon-print icon-white"></i></button>
		<a href="payment_add.php"><button class='btn btn-primary'><span>اضافه‌کردن</span> <i class="icon-plus icon-white"></i></button></a>
		<a href="panel.php"><button class='btn'><span>بازگشت</span> <i class="icon-chevron-left"></i></button></a>
	</div>
    <button class="btn disabled"><span id="subtitle">پرداخت‌ها</span></button>
    <a href="discounts.php"><button class='btn btn-primary'><span>تخفیف‌ها</span> <i class="icon-gift icon-white"></i></button></a><br />
	<div class="pull-right">
		<?php
		if ($search=="true"){
	
			$statement = 'SELECT * FROM payments WHERE';
			
			if (!empty($customer)){ $statement .= ' customer = "'.$customer .'" AND'; }
			if (!empty($transaction_id)){ $statement .= ' transaction_id = "'.$transaction_id.'" AND'; }
			if (!empty($amount)){ $statement .= ' amount = "'.$amount .'" AND'; }
			if (!empty($action)){ $statement .= ' action = "'.$action .'" AND'; }
			if (!empty($date)){ $statement .= ' date LIKE "%'.$date .'%" AND'; }
			if (!empty($time)){ $statement .= ' time = "'.$time .'" AND'; }
			if (!empty($status)){ $statement .= ' status = "'.$status .'" AND'; }
			
			
			if (substr($statement,strlen($statement)-3,strlen($statement))=="AND"){
				$statement = substr($statement,0,strlen($statement)-3);
			}
			
			if (substr($statement,strlen($statement)-5,strlen($statement))=="WHERE"){
				$statement = substr($statement,0,strlen($statement)-5);
			}
						
			
			$result = $mysqli->query($statement);
		
			$mutch = $result->num_rows;
		} else {
			$statement = 'SELECT * FROM payments';
			
			$result = $mysqli->query($statement);
		
			$mutch = $result->num_rows;
		}
		$pages = floor(($mutch-1)/$list_amount)+1;
		?>
		<div style="margin: 10px 0;">
            <?php if ($mutch>0){ ?>
                <a class="no-print" href="<?php echo get_link($order_by,$mode,$page-1); ?>"><button class="btn btn-small btn-inverse <?php if ($page==1) { echo 'disabled'; } ?>" <?php if ($page==1) { echo 'disabled="disabled"'; } ?>><i class="icon-chevron-right icon-white"></i></button></a>
                &emsp;
                <span class="h5">
                    <?php echo $mutch ?> مورد یافت شد
                    &emsp;///&emsp;
                    نمایش موارد <?php echo (($page-1)*$list_amount+1).' تا '.min($page*$list_amount,$mutch) ?>
                    &emsp;///&emsp;
                    صفحه‌ی
                    &ensp;
                    <select class="tahoma size-11" style="margin-top: 10px; width: 50px; height: 25px;" name="pagg" id="pagg" onChange='go_to_page("<?php echo get_link($order_by,$mode,0); ?>","pagg");'>
                        <?php
                        for ($i=0;$i<$pages;$i++){
                            echo '<option ';
                            if ($page==$i+1){ echo 'selected="selected" '; }
                            echo 'value='.($i+1).'>'.($i+1).'</option>';
                        }
                        ?>
                    </select>
                    &ensp;
                    <?php echo ' از '.$pages ?>
                </span>
                &emsp;
                <a class="no-print" href="<?php echo get_link($order_by,$mode,$page+1); ?>"><button class="btn btn-small btn-inverse <?php if ($page==$pages) { echo 'disabled'; } ?>" <?php if ($page==$pages) { echo 'disabled="disabled"'; } ?>><i class="icon-chevron-left icon-white"></i></button></a>
          	<?php } ?>
    	</div>
	</div>
    <div id="batch" class="no-print pull-left" style="margin: 15px 0; display: none;">
		<button class="btn btn-danger" onClick="batchDelete();"><span>حذف</span> <i class="icon-trash icon-white"></i></button>
		<button class="btn btn-info" onClick="batchEdit();"><span>ويرايش</span> <i class="icon-edit icon-white"></i></button>
	</div>
	<div class="clearfix"></div>
	<form action="<?php echo $options["url"] ?>/inc/search_payment.php" method="post">
		<input type="hidden" value="payments.php?order_by=<?php echo $order_by ?>&mode=<?php echo $mode ?>&" name="redirect" id="redirect" />
		<table class="table table-striped table-hover text-center" style="margin-bottom:-20px;">
			<td style="width: 50px;"><h5><input id="batch_check" type="checkbox" style="margin: 5px 0 0px 5px;" onChange="batchCheck();" /></h5></td>
            <td style="width: 170px;"><h5><input type="number" style="font: normal 11px tahoma; width:130px;" 
			value="<?php echo $customer; ?>" name="customer" id="customer" maxlength="11" /></h5></td>
			<td style="width: 150px;"><h5><input type="number" style="font: normal 11px tahoma; width:130px;" 
			value="<?php echo $transaction_id; ?>" name="transaction_id" id="transaction_id" maxlength="11" /></h5></td>
            <td style="width: 150px;"><h5><input type="number" style="font: normal 11px tahoma; width:130px;" 
			value="<?php echo $amount; ?>" name="amount" id="amount" maxlength="20" /></h5></td>
            <td style="width: 120px;"><h5>
            	<select class="tahoma size-11" style="width: 100px;" name="action" id="action">
                	<option value="" selected="selected"></option>
					<?php
                    foreach ($payment_actions as $action1=>$action_text){
                        echo '<option ';
                        if ($action1==$action){ echo 'selected="selected" ';}
                        echo 'value="'.$action1.'">'.$action_text.'</option>';
                    }
                    ?>
                </select>
            </h5></td>
            <td style="width: 100px;"><h5>
            	<input type="text" style="font: normal 11px tahoma; width:80px;" value="<?php echo $date ?>" name="date" id="date" maxlength="20" />
            </h5></td>
            <td style="width: 100px;"><h5>
            	<input type="text" style="font: normal 11px tahoma; width:80px;" value="<?php echo $time ?>" name="time" id="time" maxlength="20" />
            </h5></td>
            <td style="width: 120px;"><h5>
            	<select class="tahoma size-11" style="width: 100px;" name="status" id="status">
                	<option value="" selected="selected"></option>
					<?php
                    foreach ($payment_statuses as $status1=>$status_text){
                        echo '<option ';
                        if ($status1==$status){ echo 'selected="selected" ';}
                        echo 'value="'.$status1.'">'.$status_text.'</option>';
                    }
                    ?>
                </select>
            </h5></td>
			<td class="no-print" style="width: 80px;"><h4 class='normal'>
				<button class='btn btn-info' type="submit"><i class="icon-search"></i></button>
			</h4></td>
		</table>
	</form>
	<table class="table table-striped table-hover text-center">
		<tr>
			<td style="width: 50px;"><h5 class="normal ">ردیف <br />
			<a href="<?php echo get_link('row','ASC',$page); ?>" class="<?php if ($order_by=="row" && $mode=="ASC"){ echo "red_outline "; } ?>icon-chevron-up no-print"></a><a href="<?php echo get_link('row','DESC',$page); ?>" class="<?php if ($order_by=="row" && $mode=="DESC"){ echo "red_outline "; } ?>icon-chevron-down no-print"></a></h5></td>
			<td style="width: 170px;"><h5 class="normal">مشتری <br />
			<a href="<?php echo get_link('customer','ASC',$page); ?>" class="<?php if ($order_by=="customer" && $mode=="ASC"){ echo "red_outline "; } ?>icon-chevron-up no-print"></a><a href="<?php echo get_link('customer','DESC',$page); ?>" class="<?php if ($order_by=="customer" && $mode=="DESC"){ echo "red_outline "; } ?>icon-chevron-down no-print"></a></h5></td>
			<td style="width: 150px;"><h5 class="normal">شماره تراکنش <br />
			<a href="<?php echo get_link('transaction_id','ASC',$page); ?>" class="<?php if ($order_by=="transaction_id" && $mode=="ASC"){ echo "red_outline "; } ?>icon-chevron-up no-print"></a><a href="<?php echo get_link('transaction_id','DESC',$page); ?>" class="<?php if ($order_by=="transaction_id" && $mode=="DESC"){ echo "red_outline "; } ?>icon-chevron-down no-print"></a></h5></td>
            <td style="width: 150px;"><h5 class="normal">مبلغ <br />
			<a href="<?php echo get_link('amount','ASC',$page); ?>" class="<?php if ($order_by=="amount" && $mode=="ASC"){ echo "red_outline "; } ?>icon-chevron-up no-print"></a><a href="<?php echo get_link('amount','DESC',$page); ?>" class="<?php if ($order_by=="amount" && $mode=="DESC"){ echo "red_outline "; } ?>icon-chevron-down no-print"></a></h5></td>
            <td style="width: 120px;"><h5 class="normal">عملیات <br />
			<a href="<?php echo get_link('action','ASC',$page); ?>" class="<?php if ($order_by=="action" && $mode=="ASC"){ echo "red_outline "; } ?>icon-chevron-up no-print"></a><a href="<?php echo get_link('action','DESC',$page); ?>" class="<?php if ($order_by=="action" && $mode=="DESC"){ echo "red_outline "; } ?>icon-chevron-down no-print"></a></h5></td>
            <td style="width: 100px;"><h5 class="normal">تاریخ <br />
			<a href="<?php echo get_link('date','ASC',$page); ?>" class="<?php if ($order_by=="date" && $mode=="ASC"){ echo "red_outline "; } ?>icon-chevron-up no-print"></a><a href="<?php echo get_link('date','DESC',$page); ?>" class="<?php if ($order_by=="date" && $mode=="DESC"){ echo "red_outline "; } ?>icon-chevron-down no-print"></a></h5></td>
            <td style="width: 100px;"><h5 class="normal">زمان <br />
			<a href="<?php echo get_link('time','ASC',$page); ?>" class="<?php if ($order_by=="time" && $mode=="ASC"){ echo "red_outline "; } ?>icon-chevron-up no-print"></a><a href="<?php echo get_link('time','DESC',$page); ?>" class="<?php if ($order_by=="time" && $mode=="DESC"){ echo "red_outline "; } ?>icon-chevron-down no-print"></a></h5></td>
            <td style="width: 120px;"><h5 class="normal">وضعیت <br />
			<a href="<?php echo get_link('status','ASC',$page); ?>" class="<?php if ($order_by=="status" && $mode=="ASC"){ echo "red_outline "; } ?>icon-chevron-up no-print"></a><a href="<?php echo get_link('status','DESC',$page); ?>" class="<?php if ($order_by=="status" && $mode=="DESC"){ echo "red_outline "; } ?>icon-chevron-down no-print"></a></h5></td>
			<td class="no-print" style="width: 80px;"><h5 class="normal">عملیات</h5></td>
		</tr>
		
		<?php
		
		if ($order_by == "row"){ $order_by = "id"; }
		
		if ($search=="true"){
			
			$statement .= ' ORDER BY '.$order_by.' '.$mode;
				
			$result = $mysqli->query($statement.' LIMIT '.(($page-1)*$list_amount).' , '.$list_amount);
		
			$rows = $result->num_rows;
			
			for ($i=0;$i<$rows;$i++){
			$statement2 = str_replace("SELECT *","SELECT id, customer, transaction_id, amount, action, date, time, status",$statement);
			$statement2 .= ' LIMIT '.((($page-1)*$list_amount+$i).' , 1');

			$stmt = $mysqli->prepare($statement2);
			$stmt->execute();
			$stmt->store_result();
	 
			$stmt->bind_result($id, $customer, $transaction_id, $amount, $action, $date, $time, $status);
			$stmt->fetch();
			$stmt->close();
			
			/* CUSTOMER */
			
			$stmt = $mysqli->prepare("SELECT id, username, first_name, last_name FROM members WHERE id='".$customer."'");
			$stmt->execute();
			$stmt->store_result();
			$stmt->bind_result($customer_id, $customer_username, $customer_first_name, $customer_last_name);
			$stmt->fetch();
			$stmt->close();
			
			$customer_display_name = $customer_first_name." ".$customer_last_name;
			if ($customer_display_name == " "){
				$customer_display_name = $customer_username;
			}
			
			$customer_link = '<a href="member_edit.php?id='.$customer_id.'">'.$customer_display_name.'</a>';
			
			?>
			
			<tr class="<?php if($status=="success"){ echo "success"; } else if ($status == "fail"){ echo "error"; } ?>">
				<?php echo '
				<td><input name="checkbox[]" type="checkbox" style="margin: -1px 0 -1px 5px;" value="'.$id.'" onChange="loadBatch();" />'.(($page-1)*$list_amount+$i+1).'</td>
				<td>'.$customer_link.'</td>
				<td>'.$transaction_id.'</td>
				<td>'.$amount.' ریال</td>
				<td>'.$payment_actions[$action].'</td>
				<td>'.$date.'</td>
				<td>'.$time.'</td>
				<td>'.$payment_statuses[$status].'</td>
				<td class="no-print">
				<a href="payment_delete.php?id='.$id.'"><button class="btn btn-danger" style=" padding:0 2px;"><i class="icon-trash"></i></button></a>
				<a href="payment_edit.php?id='.$id.'"><button class="btn btn-info" style=" padding:0 2px;"><i class="icon-edit"></i></button></a>
				</td>
				';
				?>
			</tr>
			<?php
			}
	
	 
		} else{

		$statement .= ' ORDER BY '.$order_by.' '.$mode;
		$statement .= ' LIMIT '.(($page-1)*$list_amount).' , '.$list_amount;
		
		$result = $mysqli->query($statement);
	
		$rows = $result->num_rows;

		for ($i=0;$i<$rows;$i++){
			$statement2 = 'SELECT id, customer, transaction_id, amount, action, date, time, status FROM payments';
			$statement2 .= ' ORDER BY '.$order_by.' '.$mode;
			$statement2 .= ' LIMIT '.((($page-1)*$list_amount+$i).' , 1');
		
			$stmt = $mysqli->prepare($statement2);
			$stmt->execute();
			$stmt->store_result();
	 
			$stmt->bind_result($id, $customer, $transaction_id, $amount, $action, $date, $time, $status);
			$stmt->fetch();
			$stmt->close();
			
			/* CUSTOMER */
			
			$stmt = $mysqli->prepare("SELECT id, username, first_name, last_name FROM members WHERE id='".$customer."'");
			$stmt->execute();
			$stmt->store_result();
			$stmt->bind_result($customer_id, $customer_username, $customer_first_name, $customer_last_name);
			$stmt->fetch();
			$stmt->close();
			
			$customer_display_name = $customer_first_name." ".$customer_last_name;
			if ($customer_display_name == " "){
				$customer_display_name = $customer_username;
			}
			
			$customer_link = '<a href="member_edit.php?id='.$customer_id.'">'.$customer_display_name.'</a>';
			
			?>
			
			<tr class="<?php if($status=="success"){ echo "success"; } else if ($status == "fail"){ echo "error"; } ?>">
				<?php echo '
				<td><input name="checkbox[]" type="checkbox" style="margin: -1px 0 -1px 5px;" value="'.$id.'" onChange="loadBatch();" />'.(($page-1)*$list_amount+$i+1).'</td>
				<td>'.$customer_link.'</td>
				<td>'.$transaction_id.'</td>
				<td>'.$amount.' ریال</td>
				<td>'.$payment_actions[$action].'</td>
				<td>'.$date.'</td>
				<td>'.$time.'</td>
				<td>'.$payment_statuses[$status].'</td>
				<td class="no-print">
				<a href="payment_delete.php?id='.$id.'"><button class="btn btn-danger" style=" padding:0 2px;"><i class="icon-trash"></i></button></a>
				<a href="payment_edit.php?id='.$id.'"><button class="btn btn-info" style=" padding:0 2px;"><i class="icon-edit"></i></button></a>
				</td>
				';
				?>
			</tr>
			<?php
			}
		}
		?>
	</table>
	<?php if ($mutch == 0){
		echo '<div class="alert alert-warning no-print"><p>موردی یافت نشد!</i></p></div>';
	} ?>
</div>
<script>
function loadBatch(){
	var values = new Array();
	$.each($("input[name='checkbox[]']:checked"), function() {
		values.push($(this).val());
	});
	
	if (values.length > 0){
		$("#batch").fadeIn();
	} else {
		$("#batch").fadeOut();
	}
}

function batchEdit(){
	$.each($("input[name='checkbox[]']:checked"), function() {
		window.open('payment_edit.php?id='+$(this).val(), '_blank');
	});

}

function batchDelete(){
	$.each($("input[name='checkbox[]']:checked"), function() {
			window.open('payment_delete.php?id='+$(this).val(), '_blank');
	});
}

function batchCheck(){
	if ($("#batch_check").is(":checked")){
		$.each($("input[name='checkbox[]']"), function() {
			$(this).prop("checked", true);
		});
	} else{
		$.each($("input[name='checkbox[]']"), function() {
			$(this).prop("checked", false);
		});
	}
	
	loadBatch();
}
</script>
<?php include('footer.php'); ?>