<?php 
include('header.php'); 
?>

<?php if (if_has_permission($role,"edit_payments")){} else{header("Location: login.php?redirect=".urlencode("http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']));} ?>

<?php
if (!empty($err)) {
	switch ($err) {
	case "insert" : $err_msg = "خطا در نوشتن دیتابیس!"; break;
	case "fill" : $err_msg = "موارد الزامی را پر کنید!"; break;
	case "exists" : $err_msg = "پرداخت وجود دارد!"; break;
	
	default : $err_msg = "خطای غیر منتظره‌ای پیش آمده!"; break;
	}
	
}
?> 
<?php
if (!empty($suc)) {
	switch ($suc) {
	case "insert" : $suc_msg = "پرداخت اضافه شد!"; break;
	
	default : $suc_msg = "عملیات با موفقیت انجام شد!"; break;
	}
	
}
?> 
<?php
if (!empty($err_msg)) {
	echo '<div class="alert alert-error no-print"><a href="'.str_replace('err='.$err,"",implode('&',array_unique(explode('&',$_SERVER['REQUEST_URI'])))).'"><button type="button" class="close">&times;</button></a><p>'. $err_msg .'</p></div>';
}
?>
<?php
if (!empty($suc_msg)) {
	echo '<div class="alert alert-success no-print"><a href="'.str_replace('suc='.$suc,"",implode('&',array_unique(explode('&',$_SERVER['REQUEST_URI'])))).'"><button type="button" class="close">&times;</button></a><p>'. $suc_msg .'</p></div>';
}
?>

<?php
if (empty($_SESSION['payments_redirect'])){$_SESSION['payments_redirect']="payments.php";}
?>

<div class="container">
	<a href="<?php echo $_SESSION['payments_redirect'] ?>"><button class='pull-left btn'><span>بازگشت</span> <i class="icon-chevron-left"></i></button></a>
	<button class="btn disabled"><span id="subtitle">اضافه‌کردن پرداخت</span></button><br /><br />
	<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert">&times;</button>
	<p>مواردی که با علامت ستاره‌ی قرمز مشخص شده‌ است، الزامی است.</p></div>
	<div id="main" class="span8 pull-right">
		<form action="<?php echo $options["url"] ?>/inc/add_payment.php" method="post">
        	<input type="hidden" name="redirect" id="redirect" value="payment_add.php?" />
        	<input type="hidden" value="<?php echo time(); ?>" name="timestamp" id="timestamp" />
			<table class="table table-striped table-right">
			<tr>
				<td class="span2"><h5 class="normal">مشتری <span class="red">*</span></h5></td>
					<td style="padding: 5px 0 0 0;"><input type="number" maxlength="11" style="font: normal 11px tahoma; width:150px;" value="<?php //echo $customer; ?>" name="customer" id="customer"/></td>
				</tr>
				<tr>
					<td><h5 class="normal">شماره تراکنش <span class="red">*</span></h5></td>
					<td style="padding: 5px 0 0 0;"><input type="number" maxlength="11" style="font: normal 11px tahoma; width:150px;" value="" name="transaction_id" id="transaction_id"/></td>
				</tr>
                <tr>
					<td><h5 class="normal">مبلغ <span class="red">*</span></h5></td>
					<td style="padding: 5px 0 0 0;"><input type="number" style="font: normal 11px tahoma; width:150px;" value="" name="amount" id="amount"/> ریال</td>
				</tr>
                <tr>
					<td><h5 class="normal">عملیات <span class="red">*</span></h5></td>
					<td style="padding: 5px 0 0 0;">
                        <select class="tahoma size-11" style="width: 100px;" name="action" id="action">
                        	<option selected="selected" ></option>
                            <?php
                            foreach ($payment_actions as $action1=>$action_text){
                                echo '<option ';
                                //if ($action1==$action){ echo 'selected="selected" ';}
                                echo 'value="'.$action1.'">'.$action_text.'</option>';
                            }
                            ?>
                        </select>
                	</td>
				</tr>
                <tr>
					<td><h5 class="normal">تاریخ <span class="red">*</span></h5></td>
					<td style="padding: 5px 0 0 0;">
                    	<input type="text" style="font: normal 11px tahoma; width:100px;" value="<?php echo $date ?>" name="date" id="date" maxlength="20" />
                    </td>
				</tr>
                <tr>
					<td><h5 class="normal">زمان <span class="red">*</span></h5></td>
					<td style="padding: 5px 0 0 0;">
                    	<input type="text" style="font: normal 11px tahoma; width:100px;" value="<?php echo $time ?>" name="time" id="time" maxlength="20" />
                    </td>
				</tr>
                <tr>
					<td><h5 class="normal">وضعیت</h5></td>
					<td style="padding: 5px 0 0 0;">
                    	<select class="tahoma size-11" style="width: 115px;" name="status" id="status">
							<?php
                            foreach ($payment_statuses as $status=>$status_text){
                                echo '<option ';
                                //if ($status1==$status){ echo 'selected="selected" ';}
                                echo 'value="'.$status.'">'.$status_text.'</option>';
                            }
                            ?>
                    	</select>
                	</td>
				</tr>
			</table>
			<button class='btn btn-info pull-left' type="submit"><span>اضافه کردن</span> <i class="icon-plus icon-white"></i></button>
		</form>
	</div>
</div>
<?php include('footer.php'); ?>