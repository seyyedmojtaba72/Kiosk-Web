<?php 
include('header.php'); 
?>

<?php if (if_has_permission($role,"edit_payments")){} else{header("Location: login.php?redirect=".urlencode("http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']));} ?>

<?php
if (!empty($err)) {
	switch ($err) {
	
	default : $err_msg = "خطای غیر منتظره‌ای پیش آمده!"; break;
	}
	
}
?> 
<?php
if (!empty($suc)) {
	switch ($suc) {
	
	default : $suc_msg = "عملیات با موفقیت انجام شد!"; break;
	}
	
}
?> 
<?php
if (!empty($err_msg)) {
	echo '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">&times;</button><p>'. $err_msg .'</p></div>';
}
?>
<?php
if (!empty($suc_msg)) {
	echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button><p>'. $suc_msg .'</p></div>';
}
?>
	
<?php

if (!isset($_SESSION['payments_redirect'])){$_SESSION['payments_redirect'] = "payments.php";}

// ------

$id = filter_input(INPUT_GET, 'id', $filter = FILTER_SANITIZE_STRING);

// ------

$stmt = 'SELECT customer, transaction_id, amount, action, date, time, status FROM payments WHERE id="'.$id.'"';

if ($mysqli->query($stmt)->num_rows<1){ header('Location: payments.php?err=not-exists'); exit; }

$stmt = $mysqli->prepare($stmt);
$stmt->execute();
$stmt->store_result();

$stmt->bind_result($customer, $transaction_id, $amount, $action, $date, $time, $status);
$stmt->fetch();
$stmt->close();


/* CUSTOMER */

$stmt = $mysqli->prepare("SELECT id, username, first_name, last_name FROM members WHERE id='".$customer."'");
$stmt->execute();
$stmt->store_result();
$stmt->bind_result($customer_id, $customer_username, $customer_first_name, $customer_last_name);
$stmt->fetch();
$stmt->close();

$customer_display_name = $customer_first_name." ".$customer_last_name;
if ($customer_display_name == " "){
	$customer_display_name = $customer_username;
}

$customer_link = '<a href="member_edit.php?id='.$customer_id.'">'.$customer_display_name.'</a>';

?>

<div class="container">
	<div class="pull-left">
    	<a href="payment_edit.php?<?php echo 'id='.$id ?>"><button class='btn btn-primary'><span>ویرایش</span> <i class="icon-edit icon-white"></i></button></a>
		<a href="<?php echo $_SESSION['payments_redirect'] ?>"><button class='btn'><span>بازگشت</span> <i class="icon-chevron-left"></i></button></a>
	</div>
	<button class="btn disabled"><span id="subtitle">حذف پرداخت</span></button><br /><br />
	<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>
		<p>آیا شما مطمئنید؟
		<form action="<?php echo $options["url"] ?>/inc/delete_payment.php" method="post">
			<input type="hidden" value="payments.php?" name="redirect" id="redirect"/>
			<input type="hidden" value="<?php echo $id ?>" name="id" id="id"/>
			<button style="margin-right:50px;" type="submit" class="btn btn-danger Yekan normal">بله</button>
			<a href="<?php echo $_SESSION['payments_redirect'] ?>" type="button" class="btn Yekan normal">خیر</a>
		</form>
		</p>
	</div>
	<div id="main" class="span7 pull-right">
		<table class="table table-striped table-right">
			<tr>
				<td class="span2"><h5 class="normal">مشتری </h5></td>
				<td style="padding: 5px 0 0 0;"><?php echo $customer_link; ?></td>
			</tr>
			<tr>
				<td><h5 class="normal">شماره تراکنش </h5></td>
				<td style="padding: 5px 0 0 0;"><?php echo $transaction_id; ?></td>
			</tr>
            <tr>
				<td><h5 class="normal">مبلغ </h5></td>
				<td style="padding: 5px 0 0 0;"><?php echo $amount; ?> ریال</td>
			</tr>
            <tr>
				<td><h5 class="normal">عملیات </h5></td>
				<td style="padding: 5px 0 0 0;"><?php echo $payment_actions[$action]; ?></td>
			</tr>
            <tr>
				<td><h5 class="normal">تاریخ </h5></td>
				<td style="padding: 5px 0 0 0;"><?php echo $date; ?></td>
			</tr>
			<tr>
				<td><h5 class="normal">زمان </h5></td>
				<td style="padding: 5px 0 0 0;"><?php echo $time; ?></td>
			</tr>
            <tr>
				<td><h5 class="normal">وضعیت </h5></td>
				<td style="padding: 5px 0 0 0;"><?php echo $payment_statuses[$status]; ?></td>
			</tr>
		</table>
	</div>
</div>
<?php include('footer.php'); ?>