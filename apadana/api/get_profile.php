<?php
require_once dirname(__FILE__).'/inc/dbhandler.php';

$author = $_GET['author'];
$user = $_GET['user'];
$method = $_GET['method'];

$response = array();

try {
	$mDBHandler = new DBHandler();
	$records = $mDBHandler->getProfile($author, $user, $method);
	
	for($i=0; $i<count($records); $i++) {
		$record = $records[$i];		
		
		
		$posts[] = array(
	
		'id'=> $record['id'], 
		'username'=> $record['username'],
		'profile_image'=> $record['profile_image'],
		'first_name'=> $record['first_name'],
		'last_name'=> $record['last_name'],
		'about'=> $record['about'],
		'extras'=> $record['extras'],
		'timestamp'=> $record['timestamp'],
		'posts'=> $record['posts'],
		'followers'=> $record['followers'],
		'followeds'=> $record['followeds'],
		'follow'=> $record['follow'],
		'request'=> $record['request'],

		

		);
	
	$response['profile'] = $posts;
		
	}

	
	
} catch (Exception $e) {
	$response['profile'] = "-1";
}

echo json_encode($response);

?>