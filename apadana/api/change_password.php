<?php
require_once dirname(__FILE__).'/inc/dbhandler.php';

$username = $_GET['username'];
$former_password = $_GET['former_password'];
$password = $_GET['password'];

$response = array();

try {
	$mDBHandler = new DBHandler();
	$records = $mDBHandler->changePassword($username, $former_password, $password);
	
	echo $records;
	
} catch (Exception $e) {
	echo '-1'; // When there is no record in database
	//die("There was a problem: " . $e->getMessage());
}
?>