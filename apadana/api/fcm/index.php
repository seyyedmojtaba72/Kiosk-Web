<?php

//error_reporting(-1);
//ini_set('display_errors', 'On');

error_reporting(-1);
ini_set('display_errors', 1);


require_once '../inc/dbhandler.php';
require '../../inc/lib/Slim/Slim.php';
require_once '../../inc/functions.php';

\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();


/* * *
 * fetching all chats
 */
$app->post('/chats/get', function() use ($app) {
	$response = array();
	$mDBHandler = new DbHandler();

	verifyRequiredParams(array('user'));

	$user = $app->request->post('user');
	$timestamp = "0";
	if (($app->request->post('timestamp')) != null){
		$timestamp = $app->request->post('timestamp');
	}
    
	
	$response["error"] = false;
	try {
		$records = $mDBHandler->getChats($user, 0, 0, $timestamp);
		
		for($i=0; $i<count($records); $i++) {
			$record = $records[$i];
			$posts[] = array(
				'id'=> $record['id'], 	
				'creator'=> $record['creator'],
				'type'=> $record['type'],
				'participants'=> $record['participants'],
				'name'=> $record['name'],
				'image'=> $record['image'],
				'about'=> $record['about'],
				'timestamp'=> $record['timestamp'],
				'last_timestamp'=> $record['last_timestamp'],
				'new'=> $record['new'],
				'last_message'=> $record['last_message'],
				'last_message_id'=> $record['last_message_id'],
				'last_message_sender'=> $record['last_message_sender'],
				'last_message_timestamp'=> $record['last_message_timestamp'],
				);
			
			$response['chats'] = $posts;
		}
	
		
	} catch (Exception $e) {
		$response['chats'] = '-1'; // When there is no record in database
		//die("There was a problem: " . $e->getMessage());
	}
	

    echoRespnse(200, $response);
});

/**
 * Messaging in a chat room
 * Will send push notification using Topic Messaging
 *  */
$app->post('/chat/send', function() use ($app) {
	global $app;
	$mDBHandler = new DbHandler();

	verifyRequiredParams(array('chat', 'sender', 'action'));

	$chat = $app->request->post('chat');
	$sender = $app->request->post('sender');
	$action = $app->request->post('action');
	$message = '';
	if ($app->request->post('message') != null){
		$message = $app->request->post('message');
	}
	$extras = '';
	if ($app->request->post('extras') != null){
		$extras = $app->request->post('extras');
	}
	$timestamp = time();

	$records = $mDBHandler->sendChatsMessage($chat, $sender, $action, $message, $extras, $timestamp);
	
	$response['id'] = $records;
	
	$new_extras = array();
	if (strstr($extras, 'reply')){
		$new_extras['reply_id'] = str_replace("reply:", "", $extras);
		$results = mysql_select("chats_messages", array("id"=>$new_extras['reply_id']), array("sender", "action", "message"));
		
		$results_sender = mysql_select("members", array("id"=>$results[0]["sender"]), array("username", "first_name", "last_name"));
		$sender_name = $results_sender[0]['first_name']." ".$results_sender[0]['last_name'];
		if ($sender_name==" "){
			$sender_name = $results_sender[0]['username'];
		}
		
		
		$new_extras['reply_sender'] = $sender_name;
		$new_extras['reply_action'] = $results[0]['action'];
		$message = "";
		if (sizeof(explode(' ', $results[0]['message']))>10){
			$message  = implode(' ', array_slice(explode(' ', $results[0]['message']), 0, 10))."...";
		} else {
			$message = $results[0]['message'];
		}
		
		$new_extras['reply_message'] = $message;
		
		$response['extras'] = json_encode($new_extras);
	} else if (strstr($extras, 'forward')){
		$new_extras['forward_id'] = str_replace("forward:", "", $extras);
		$results = mysql_select("chats_messages", array("id"=>$new_extras['forward_id']), array("sender", "action", "message"));
		
		$results_sender = mysql_select("members", array("id"=>$results[0]["sender"]), array("username", "first_name", "last_name"));
		$sender_name = $results_sender[0]['first_name']." ".$results_sender[0]['last_name'];
		if ($sender_name==" "){
			$sender_name = $results_sender[0]['username'];
		}
		
		
		$new_extras['forward_sender'] = $sender_name;
		$new_extras['forward_action'] = $results[0]['action'];
		$message = "";
		if (sizeof(explode(' ', $results[0]['message']))>10){
			$message  = implode(' ', array_slice(explode(' ', $results[0]['message']), 0, 10))."...";
		} else {
			$message = $results[0]['message'];
		}
		
		$new_extras['forward_message'] = $message;
		
		$response['extras'] = json_encode($new_extras);
	}  else {
		$response['extras'] = $extras;
	}
	
	
	$response['error'] = false;

	echoRespnse(200, $response);
});



/**
 * Fetching single chat including all the chat messages
 *  */
$app->post('/chat/get', function() use ($app) {
    global $app;
    $mDBHandler = new DbHandler();
	
	verifyRequiredParams(array('chat', 'start', 'amount'));

	$chat = $app->request->post('chat');
	$start = $app->request->post('start');
	$amount = $app->request->post('amount');
	
	
	try {
		$records = $mDBHandler->getChatsMessages($chat, $start, $amount);
		
		for($i=0; $i<count($records); $i++) {
			$record = $records[$i];
			$posts[] = array(
				'id'=> $record['id'], 	
				'chat'=> $record['chat'],
				'sender'=> $record['sender'],
				'sender_name'=> $record['sender_name'],
				'sender_profile_image'=> $record['sender_profile_image'],
				'action'=> $record['action'],
				'message'=> $record['message'],
				'extras'=> $record['extras'],
				'timestamp'=> $record['timestamp'],
				);
			
			$response['chats_messages'] = $posts;
		}
	
		
	} catch (Exception $e) {
		$response['chats_messages'] = '-1'; // When there is no record in database
		//die("There was a problem: " . $e->getMessage());
	}


    $response["error"] = false;

    echoRespnse(200, $response);
});

/**
 * Verifying required params posted or not
 */
function verifyRequiredParams($required_fields) {
    $error = false;
    $error_fields = "";
    $request_params = array();
    $request_params = $_REQUEST;
    // Handling PUT request params
    if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
        $app = \Slim\Slim::getInstance();
        parse_str($app->request()->getBody(), $request_params);
    }
    foreach ($required_fields as $field) {
        if (!isset($request_params[$field]) || strlen(trim($request_params[$field])) <= 0) {
            $error = true;
            $error_fields .= $field . ', ';
        }
    }

    if ($error) {
        // Required field(s) are missing or empty
        // echo error json and stop the app
        $response = array();
        $app = \Slim\Slim::getInstance();
        $response["error"] = true;
        $response["message"] = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
        echoRespnse(400, $response);
        $app->stop();
    }
}

/**
 * Validating email address
 */
function validateEmail($email) {
    $app = \Slim\Slim::getInstance();
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $response["error"] = true;
        $response["message"] = 'Email address is not valid';
        echoRespnse(400, $response);
        $app->stop();
    }
}

function IsNullOrEmptyString($str) {
    return (!isset($str) || trim($str) === '');
}

/**
 * Echoing json response to client
 * @param String $status_code Http response code
 * @param Int $response Json response
 */
function echoRespnse($status_code, $response) {
    $app = \Slim\Slim::getInstance();
    // Http response code
    $app->status($status_code);

    // setting response content type to json
    $app->contentType('application/json');

    echo json_encode($response);
}

$app->run();
?>