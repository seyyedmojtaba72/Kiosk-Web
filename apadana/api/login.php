<?php

require_once dirname(__FILE__).'/inc/dbhandler.php';

$username = $_GET['username'];
$password = $_GET['password'];
$gcm_token = "";
if (isset($_GET['gcm_token'])){
	$gcm_token = $_GET['gcm_token'];
}

$response = array();

try {
	$mDBHandler = new DBHandler();
	$records = $mDBHandler->Login($username, $password, $gcm_token);
	
	if ($records == "inactive" | $records == "incorrect"){
		echo $records;
	} else {
		for($i=0; $i<count($records); $i++) {
			$record = $records[$i];		
			
			
			$posts[] = array(
		
			'id'=> $record['id'], 
			'username'=> $record['username'],
			'password'=> $record['password'],
			'email'=> $record['email'],
			'role'=> $record['role'],
			'profile_image'=> $record['profile_image'],
			'first_name'=> $record['first_name'],
			'last_name'=> $record['last_name'],
			'mobile_number'=> $record['mobile_number'],
			'status'=> $record['status'],
			'level'=> $record['level'],
			'balance'=> $record['balance'],
			'shaba'=> $record['shaba'],
			'about'=> $record['about'],
			'extras'=> $record['extras'],
			'timestamp'=> $record['timestamp'],
			'posts'=> $record['posts'],
			'followers'=> $record['followers'],
			'followeds'=> $record['followeds'],
			

			);
		
		$response['login'] = $posts;
			
		}

		echo json_encode($response);
	}
	
	
} catch (Exception $e) {
	echo '-1'; // When there is no record in database
	//die("There was a problem: " . $e->getMessage());
}
?>