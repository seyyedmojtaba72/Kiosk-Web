
<?php
require_once dirname(__FILE__).'/inc/dbhandler.php';

$post = $_GET['post'];
$user = $_GET['user'];


$mDBHandler = new DBHandler();

try {
	$records = $mDBHandler->getPost($post, $user);
	
	for($i=0; $i<count($records); $i++) {
		$record = $records[$i];
		$posts[] = array(
			'id'=> $record['id'], 	
			'author'=> $record['author'],
			'author_name'=> $record['author_name'],
			'author_profile_image'=> $record['author_profile_image'],
			'author_timestamp'=> $record['author_timestamp'],
			'name'=> $record['name'],
			'description'=> $record['description'],
			'image'=> $record['image'],
			'price'=> $record['price'],
			'download_count'=> $record['download_count'],
			'timestamp'=> $record['timestamp'],
			'likes'=> $record['likes'],
			'user_like'=> $record['user_like'],
			'comments'=> $record['comments'],
			'downloadable'=> $record['downloadable'],
			);
		
		$response['post'] = $posts;
	}

	
} catch (Exception $e) {
	$response['post'] = '-1'; // When there is no record in database
	//die("There was a problem: " . $e->getMessage());
}


echo json_encode($response);
	


	

?>