<?php
require_once dirname(__FILE__).'/inc/dbhandler.php';
$mDBHandler = new DBHandler();

$username = $_GET['username'];
$user = $_GET['user'];
$start = $_GET['start'];
$amount = $_GET['amount'];

$response = array();

try {
	$mDBHandler = new DBHandler();
	$records = $mDBHandler->searchUsers($username, $user, $start, $amount);
	
	for($i=0; $i<count($records); $i++) {
		$record = $records[$i];
		$posts[] = array(			
	
		'id'=> $record['id'], 
		'username'=> $record['username'],
		'profile_image'=> $record['profile_image'],
		'name'=> $record['name'],
		'follow'=> $record['follow'],
		'timestamp'=> $record['timestamp'],

		);
	
	$response['users'] = $posts;
}	
	
} catch (Exception $e) {
	$response['users'] = '-1'; // When there is no record in database
	//die("There was a problem: " . $e->getMessage());
}

echo json_encode($response);
			
?>