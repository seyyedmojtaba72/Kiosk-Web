<?php
require_once dirname(__FILE__).'/inc/dbhandler.php';

$response = array();

$user = $_GET['user'];
$start = $_GET['start'];
$amount = $_GET['amount'];

$mDBHandler = new DBHandler();


try {
	$records = $mDBHandler->getChats($user, $start, $amount);
	
	for($i=0; $i<count($records); $i++) {
		$record = $records[$i];
		$posts[] = array(
			'id'=> $record['id'],
			'creator'=> $record['creator'],
			'type'=> $record['type'],
			'participants'=> $record['participants'],
			'name'=> $record['name'],
			'image'=> $record['image'],
			'about'=> $record['about'],
			'timestamp'=> $record['timestamp'],
			'last_timestamp'=> $record['last_timestamp'],
			'new'=> $record['new'],
			'last_message'=> $record['last_message'],
			'last_message_id'=> $record['last_message_id'],
			'last_message_sender'=> $record['last_message_sender'],
			'last_message_timestamp'=> $record['last_message_timestamp'],
			);
		
		$response['chats'] = $posts;
	}

	
} catch (Exception $e) {
	$response['chats'] = '-1'; // When there is no record in database
	//die("There was a problem: " . $e->getMessage());
}


echo json_encode($response);



	

?>