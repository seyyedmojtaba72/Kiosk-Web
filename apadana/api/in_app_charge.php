<?php
require_once dirname(__FILE__).'/inc/dbhandler.php';

$user = $_GET['user'];
$sku = urldecode($_GET['sku']);
try {
	$mDBHandler = new DBHandler();
	$records = $mDBHandler->inAppCharge($user, $sku);
	
	echo $records;	
	
} catch (Exception $e) {
	echo '-1'; // When there is no record in database
	//die("There was a problem: " . $e->getMessage());
}
?>