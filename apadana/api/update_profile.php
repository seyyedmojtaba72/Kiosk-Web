<?php
require_once dirname(__FILE__).'/inc/dbhandler.php';

$username = $_GET['username'];
$first_name = urldecode($_GET['first_name']);
$last_name = urldecode($_GET['last_name']);
$mobile_number = $_GET['mobile_number'];
$shaba = $_GET['shaba'];
$former_email = $_GET['former_email'];
$email = $_GET['email'];
$profile_image = $_GET['profile_image'];
$about = urldecode($_GET['about']);
$extras = urldecode($_GET['extras']);

try {
	$mDBHandler = new DBHandler();
	$records = $mDBHandler->updateProfile($username, $first_name, $last_name, $mobile_number, $shaba, $former_email, $email, $profile_image, $about, $extras);
	
	echo $records;	
	
} catch (Exception $e) {
	echo '-1'; // When there is no record in database
	//die("There was a problem: " . $e->getMessage());
}
?>