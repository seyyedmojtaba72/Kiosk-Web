<?php
require_once dirname(__FILE__).'/inc/dbhandler.php';

$username = $_GET['username'];
$email = filter_input(INPUT_GET, 'email', $filter = FILTER_SANITIZE_STRING);
$password = $_GET['password'];

try {
	$mDBHandler = new DBHandler();
	$records = $mDBHandler->Register($username, $email, $password);
	
	echo $records;	
	
} catch (Exception $e) {
	echo '-1'; // When there is no record in database
	//die("There was a problem: " . $e->getMessage());
}
?>