<?php
require_once dirname(__FILE__).'/inc/dbhandler.php';

$response = array();

$chat = $_GET['chat'];
$start = $_GET['start'];
$amount = $_GET['amount'];

$mDBHandler = new DBHandler();


try {
	$records = $mDBHandler->getChatsMessages($chat, $start, $amount);
	
	for($i=0; $i<count($records); $i++) {
		$record = $records[$i];
		$posts[] = array(
			'id'=> $record['id'], 	
			'chat'=> $record['chat'],
			'sender'=> $record['sender'],
			'sender_name'=> $record['sender_name'],
			'sender_profile_image'=> $record['sender_profile_image'],
			'action'=> $record['action'],
			'message'=> $record['message'],
			'extras'=> $record['extras'],
			'timestamp'=> $record['timestamp'],
			);
		
		$response['chats_messages'] = $posts;
	}

	
} catch (Exception $e) {
	$response['chats_messages'] = '-1'; // When there is no record in database
	//die("There was a problem: " . $e->getMessage());
}


echo json_encode($response);



	

?>