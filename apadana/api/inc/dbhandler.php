<?php
require_once dirname(__FILE__).'/../../inc/config.php';
require_once dirname(__FILE__).'/../../inc/functions.php';
require_once dirname(__FILE__).'/../../inc/information.php';

/**
 * Description of class
 *
 */
class DBHandler {

    private $DB_HOST = DB_HOST;
    private $DB_USER = DB_USER;
    private $DB_PASS = DB_PASS;
    private $DB_NAME = DB_NAME;
    public $mysqli;
	
    
    public function __construct() {
		
        // CONNECT TO THE DATABASE
        $this->mysqli = new mysqli($this->DB_HOST, $this->DB_USER, $this->DB_PASS, $this->DB_NAME);
		$this->mysqli->set_charset("utf8");


        if (mysqli_connect_errno()) {
            throw new Exception("Unable to connect to the database. Error number: " . $this->mysqli->connect_errno);
		}
    }
    
    public function __destruct() {
        //We are done with the database. Close the connection handle.
        $this->mysqli->close();
    }
    
    /**
     * This method gets an object of Vote and insert it into database.
     * @param Vote $vote
     * @throws Exception
     */
	 
	
	public function setLastSeen($user){
		$timestamp = time();
		if ($user != ""){
			mysql_update("members", array("id"=>$user), array("last_seen"=>$timestamp));
		}
	}
	
	
	public function getOnline($user, $device_id, $version, $platform){
		require_once(dirname(__FILE__)."/../../inc/statistics.php");
		if (empty($platform)){
			$platform = "android";
		}
		
		$timestamp = time();
		
		$this->setLastSeen($user);

		$statement = 'SELECT id FROM users WHERE device_id="'.$device_id.'"';

		$result = $this->mysqli->query($statement);
		$rows = $result->num_rows;
			
		if ($rows > 0){

			$statement = 'UPDATE users SET last_time="'.$timestamp.'", version="'.$version.'" WHERE device_id="'.$device_id.'"';
			
			$stmt = $this->mysqli->prepare($statement);
			$stmt->execute();
			$stmt->fetch();
			$stmt->close();
			
			return "success";
			
		} else {
			
			$statement = 'INSERT INTO users (device_id, version, platform, last_time) VALUES("'.$device_id.'", "'.$version.'", "'.$platform.'", "'.$timestamp.'")';
			
			$stmt = $this->mysqli->prepare($statement);
			$stmt->execute();
			$stmt->fetch();
			$stmt->close();
			
			return "success";	
		}
		
		

	 }
	 
	 public function Login($username, $password, $gcm_token){
		 
		 
		$username = strtolower($username);
		
		$records = array();
		
		$stmt = $this->mysqli->prepare('SELECT id, email, password, salt, role, profile_image, first_name, last_name, mobile_number, status, level, balance, shaba, about, extras, timestamp FROM members WHERE username="'.$username.'" LIMIT 1');
		
		$stmt->execute(); 
		$stmt->store_result();

		$stmt->bind_result($id, $email, $db_password, $salt, $role, $profile_image, $first_name, $last_name, $mobile_number, $status, $level, $balance, $shaba, $about, $extras, $timestamp);
		$stmt->fetch();
			
			
		$hash = crypt($password,$salt);
		if ($stmt->num_rows == 1) {
			
			if ($db_password == $hash) {
				
				if ($status == "inactive"){
					return "inactive";
				}
				
				
				$posts = ($this->mysqli->query('SELECT id FROM posts WHERE author="'.$id.'" AND status="published"'))->num_rows;
				$followers = ($this->mysqli->query('SELECT id FROM follow_requests WHERE followed="'.$id.'" AND status="accepted"'))->num_rows;
				$followeds = ($this->mysqli->query('SELECT id FROM follow_requests WHERE follower="'.$id.'" AND status="accepted"'))->num_rows;
				
				
				$array = array();
				$array['id'] = $id;
				$array['username'] = $username;
				$array['password'] = $password;
				$array['email'] = $email;
				$array['role'] = $role;
				$array['profile_image'] = $profile_image;
				$array['first_name'] = ($first_name);
				$array['last_name'] = ($last_name);
				$array['mobile_number'] = $mobile_number;
				$array['status'] = $status;
				$array['level'] = $level;
				$array['balance'] = $balance;
				$array['shaba'] = $shaba;
				$array['about'] = ($about);
				$array['extras'] = ($extras);
				$array['timestamp'] = $timestamp;
				$array['posts'] = $posts;
				$array['followers'] = $followers;
				$array['followeds'] = $followeds;
				
				
				if (!empty($gcm_token)){
					$results = mysql_select("members", array("gcm_token"=>$gcm_token), array("id"));
					foreach($results as $result){
						mysql_update("members", array("id"=>$result["id"]), array("gcm_token"=>""));
					}
					
					mysql_update("members", array("username"=>$username), array("gcm_token"=>$gcm_token));
				}
				
				
				array_push($records, $array);
			} else {
				return "incorrect";
			}
			 
		} else {
		    // No user exists.
		    return "incorrect";
		}
		
		return $records;
	}
	
	
	public function Register($username, $email, $password){
		require_once dirname(__FILE__).'/../../inc/functions.php';
		global $options;
		$default_role = DEFAULT_ROLE;
		$timestamp = time();
		
		$username = strtolower($username);
		if (!preg_match('/^[a-z0-9_.-]{1,50}$/', $username) || !preg_match('/\w/', $username)){
			return "invalid-username";
		}
		
		$prep_stmt = "SELECT id FROM members WHERE username = ? LIMIT 1";
		$stmt = $this->mysqli->prepare($prep_stmt);
 	
	
		$stmt->bind_param('s', $username);
		$stmt->execute();
		$stmt->store_result();
		
		if ($stmt->num_rows == 1) {			
			return "username-exists";
		}
		
		if (strlen($username) > 50){
			return "max-username-length";
		}
		
	 	if (empty($email)){
			return "enter-email";
		}
		
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			return "invalid-email";
		}
		
		if (strlen($email) > 50){
			return "max-email-length";
		}
		
		if (!empty($mobile_number)){
			if (strlen($mobile_number)!=10){
				return "mobile_number-length";
			}
		
			$stmt = $this->mysqli->prepare('SELECT id FROM members WHERE mobile_number="'.$mobile_number.'"');
			$stmt->execute();
			$stmt->store_result();
			$num_rows = $stmt->num_rows;
			$stmt->fetch();
			$stmt->close();
			
			if (($num_rows > 0)){
				return "mobile_number-exists";
			}
		
		}
		
			
		$prep_stmt = "SELECT id FROM members WHERE email = ? LIMIT 1";
		$stmt = $this->mysqli->prepare($prep_stmt);
 

		$stmt->bind_param('s', $email);
		$stmt->execute();
		$stmt->store_result();
 
		if ($stmt->num_rows == 1) {			
			return "email-exists";
		}
		
		if (strlen($username) < 6 || strlen($password) < 6){
			return "min-length";
		}
    
		$cost = PASSWORD_COST;	
		$random_salt = strtr(base64_encode(mcrypt_create_iv(16, MCRYPT_DEV_URANDOM)), '+', '.');
		$random_salt = sprintf("$2a$%02d$", $cost) . $random_salt;
		$hash = crypt($password, $random_salt);
 		$verification = 'active';
		$level = 'normal';
		
		$statement = 'INSERT INTO members (username, email, mobile_number, password, salt, role, status, level, timestamp) VALUES ("'.$username.'", "'.$email.'", "", "'.$hash.'", "'.$random_salt.'", "'.$default_role.'", "'.$verification.'", "'.$level.'", "'.$timestamp.'")';
		
		$stmt = $this->mysqli->prepare($statement);
		$stmt->execute();
		$stmt->fetch();
		$stmt->close();
		
		// E-MAIL
		
		/*$subject = "فعال‌سازی حساب";
		
		$html_body = '
			<h5 class="normal">ثبت‌نام شما با موفقیت انجام شد. برای فعال‌سازی حساب خود روی لینک زیر کلیک کنید.</h5>
			<h5 class="normal">اگر شما این درخواست را نداده بودید؛ لطفاً این ایمیل را پاک کنید.</h5><br>
			<h5 class="normal">نام کاربری: '.$username.'</h5>
			<a class="Yekan" href="'.$options["url"].'/inc/activate.php?username='.$username.'&password='.$hash.'"><h5 class="normal">فعال‌سازی حساب</h5></a>
		';
			
		send_email($email, $subject, $html_body, "");*/
		
		$subject = "خوش‌آمدید";
	
		$html_body = str_replace("%MOBILE_NUMBER%", "", str_replace("%EMAIL%", $email, str_replace("%USERNAME%", $username, $options['welcome_email_text'])));
	
		send_email($email, $subject, $html_body, "", "normal");
		
			
		return "login";
    }
	
	
	public function updateProfile($username, $first_name, $last_name, $mobile_number, $shaba, $former_email, $email, $profile_image, $about, $extras){
		
		$timestamp = time();
		
		$username = strtolower($username);
		if (!preg_match('/^[a-z0-9_.-]{1,50}$/', $username) || !preg_match('/\w/', $username)){
			return "invalid-username";
		}
		
		$stmt = $this->mysqli->prepare('SELECT username FROM members WHERE email="'.$former_email.'"');
		$stmt->execute();
		$stmt->bind_result($new_username);
		$stmt->store_result();
		$num_rows = $stmt->num_rows;
		$stmt->fetch();
		$stmt->close();
		
		if (($num_rows > 0) && ($username!=$new_username)){
			return "username-exists";
		}
		

		if (empty($email)){
			return "enter-email";
		}
		
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			return "invalid-email";
		}
		

		if (strlen($username) < 6 && $username != "admin"){
			return "min-length";
		}
		
		if (strlen($username) > 50){
			return "max-username-length";
		}
		
		if (strlen($email) > 50){
			return "max-email-length";
		}
		

		$stmt = $this->mysqli->prepare('SELECT email FROM members WHERE email="'.$email.'"');
		$stmt->execute();
		$stmt->bind_result($new_email);
		$stmt->store_result();
		$num_rows = $stmt->num_rows;
		$stmt->fetch();
		$stmt->close();
		
		if (($num_rows > 0) && ($former_email!=$new_email)){
			return "email-exists";
		}
		

		if (!empty($mobile_number)){
			if (strlen($mobile_number)!=10){
				return "mobile_number-length";
			}
		
			$stmt = $this->mysqli->prepare('SELECT email FROM members WHERE mobile_number="'.$mobile_number.'"');
			$stmt->execute();
			$stmt->bind_result($new_email);
			$stmt->store_result();
			$num_rows = $stmt->num_rows;
			$stmt->fetch();
			$stmt->close();
			
			if (($num_rows > 0) && ($former_email!=$new_email)){
				return "mobile_number-exists";
			}
		
		}
		
		$results = mysql_select("members", array("email"=>$former_email), array("extras"));
		$extras_array = explode(";", $results[0]['extras']);
		
		if (in_array("trusted", $extras_array)){
			$extras_array = array();
			array_push($extras_array, "trusted");
		} else {
			$extras_array = array();
		}
		
		
		$former_extras = $results[0]['extras'];
	
		
		$new_extras_array = explode(";", $extras);
		
		foreach($new_extras_array as $extra){
			array_push($extras_array, $extra);
		}
		
		$extras_array = array_filter(array_unique($extras_array));
		
		$new_extras = "";
		foreach($extras_array as $extra){
			$new_extras .= $extra.";";
		}

		
		$insert_stmt = $this->mysqli->prepare("UPDATE members SET username=?, first_name=?, last_name=?, mobile_number=?, shaba=?, email=?, profile_image=?, about=?, extras=?, timestamp=? WHERE email=?");
		$insert_stmt->bind_param('sssssssssss', $username, $first_name, $last_name, $mobile_number, $shaba, $email, $profile_image, $about, $new_extras, $timestamp, $former_email);
		if ($insert_stmt->execute()) {
        	return "success";
		} else{
			return "fail";
		}
	}
	
	
	public function changePassword($username, $former_password, $password){
		if (strlen($password) < 6){
			return "min-length";
		}
		
		$stmt = $this->mysqli->prepare('SELECT password, salt FROM members WHERE username="'.$username.'" LIMIT 1');
		
        $stmt->execute(); 
        $stmt->store_result();

        $stmt->bind_result($db_password, $salt);
        $stmt->fetch();
		
		if ($stmt->num_rows == 1) {
		
        	$hash = crypt($former_password,$salt);
        
			
			if ($db_password != $hash) {
				return "incorrect";
			}
			
			$cost = PASSWORD_COST;	
			$random_salt = strtr(base64_encode(mcrypt_create_iv(16, MCRYPT_DEV_URANDOM)), '+', '.');
			$random_salt = sprintf("$2a$%02d$", $cost) . $random_salt;
			$hash = crypt($password, $random_salt);

			mysql_update("members", array("username"=>$username), array("password"=>$hash, "salt"=>$random_salt));
			
			
			return "success";
		}	

		return "fail";
	}
	

	
	public function getVersioningOptions() {
        $records = array();
        $query = "SELECT * FROM options WHERE section='versioning'";
        
        $result = $this->mysqli->query($query);
       
        if($result->num_rows <= 0)
            throw new Exception("There is no record in database!");
        
        while($row = $result->fetch_assoc()) {
			$array = array();
			$array['variable'] = $row['variable'];
			$array['value'] = $row['value'];
            
            array_push($records, $array);
		}

        $result->free();
        
        return $records;
    }
	
	public function getUpdate(){
		
        $query = 'SELECT * FROM options_versioning WHERE variable = "android_download_count"';
        
        $result = $this->mysqli->query($query);
		$row = $result->fetch_assoc();
		
		$stmt = $this->mysqli->prepare('UPDATE options_versioning SET value = "'. ($row["value"]+1).'" WHERE variable = "android_download_count"');
		$stmt->execute(); 
		$stmt->close();
		
		$result->free();
		
		return "success";
	}
	

	// CUSTOM API
	
	public function getPosts($author, $start, $amount, $user) {
	
		$this->setLastSeen($user);
		
        $records = array();

        $query = 'SELECT * FROM posts WHERE status="published"';
		if (!empty($author)){ 
		
			if ($author != $user){
		
				// CHECK IF BLOCKED
				
				$follow = "0";
				$results = mysql_select("follow_requests", array("follower"=>$user, "followed"=>$author), array('status'));
				if (sizeof($results)>0){
					$follow = $results[0]['status'];
				}
				
				if ($follow == "blocked"){
					throw new Exception("There is no record in database!");
				}
			
			
				// CHECK IF PRIVATE
				$results = mysql_select("members", array("id"=>$author), array("extras"));
				$extras = $results[0]['extras'];
				
				if (in_array("private", explode(";", $extras))){
				
					// CHECK IF NOT FOLLOWED
			
					$results = mysql_select("follow_requests", array('follower'=>$user, 'followed'=>$author, 'status'=>'accepted'), array('id'));
					if (sizeof($results)==0){
						throw new Exception("There is no record in database!");
					}
				}
			}
		
		
			$query .= ' AND (author="'.$author.'"';
		} else {
			$followeds = mysql_select("follow_requests", array('follower'=>$user, 'status'=>'accepted'), array('followed'));
			
			$query .= ' AND (author="'.$user.'" OR';
			
			foreach($followeds as $followed){
				$query .= ' author="'.$followed['followed'].'" OR';
			}
		}

		if (substr($query,strlen($query)-5,strlen($query))=="WHERE"){
			$query = substr($query,0,strlen($query)-5);
		}
		
		if (substr($query,strlen($query)-2,strlen($query))=="OR"){
			$query = substr($query,0,strlen($query)-2);
		}
		
		$query .= ') ORDER BY id DESC LIMIT '.$start.', '.$amount;

		$result = $this->mysqli->query($query);
	       
		if($result->num_rows <= 0)
		    throw new Exception("There is no record in database!");
		
		while($row = $result->fetch_assoc()) {
		
			if ($row['author'] != $user){
				// CHECK IF BLOCKED
			
				$follow = "0";
				$results = mysql_select("follow_requests", array("follower"=>$user, "followed"=>$row['author']), array('status'));
				if (sizeof($results)>0){
					$follow = $results[0]['status'];
				}
				
				if ($follow == "blocked"){
					continue;
				}
			
				// CHECK IF PRIVATE
				$results = mysql_select("members", array("id"=>$row['author']), array("extras"));
				$extras = $results[0]['extras'];

				
				if (in_array("private", explode(";", $extras))){
				
					// CHECK IF NOT FOLLOWED
			
					$results = mysql_select("follow_requests", array('follower'=>$user, 'followed'=>$row['author'], 'status'=>'accepted'), array('id'));
					if (sizeof($results)==0){
						continue;
					}
				}
			}
			
			
			$array = array();
			$array['id'] = $row['id'];
			$array['author'] = $row['author'];
			$stmt = $this->mysqli->prepare('SELECT username, first_name, last_name, profile_image, timestamp FROM members WHERE id="'.$row['author'].'"');
			$stmt->execute();
			$stmt->store_result();
			
			
			/* AUTHOR */
	 
			$stmt->bind_result($author_username, $author_first_name, $author_last_name, $author_profile_image, $author_timestamp);
			$stmt->fetch();
			$stmt->close();
			
			$author_name = $author_first_name." ".$author_last_name;
			if ($author_name==" "){
				$author_name = $author_username;
			}
			
			$array['author_name'] = $author_name;
			$array['author_profile_image'] = $author_profile_image;
			$array['author_timestamp'] = $author_timestamp;
			
			
			$array['name'] = ($row['name']);
			$array['description'] = ($row['description']);
			$array['image'] = $row['image'];
			$array['price'] = $row['price'];
			$array['download_count'] = $row['download_count'];
			$array['timestamp'] = $row['timestamp'];
			
			
			/* LIKES */
				
			$likes = ($this->mysqli->query('SELECT id FROM posts_likes WHERE post="'.$row['id'].'"'))->num_rows;
			
			/* USER LIKE */
			
			$user_like = 0;
			
			if (!empty($user)){	
				$user_like = ($this->mysqli->query('SELECT id FROM posts_likes WHERE post="'.$row['id'].'" AND user = "'.$user.'"'))->num_rows;
			}
			
			/* COMMENTS */
			
			$rows =($this->mysqli->query('SELECT id FROM posts_comments WHERE post="'.$row['id'].'" AND status="published"'))->num_rows;
			
			$comments = $rows."%end_row%";
				
			for ($i=0; $i<min(3, $rows); $i++){
				$statement = 'SELECT author, comment FROM posts_comments WHERE post="'.$row['id'].'" AND status="published" ORDER BY id DESC LIMIT '.$i.', 1';
				
				$stmt = $this->mysqli->prepare($statement);
				$stmt->execute();
				$stmt->store_result();
		 
				$stmt->bind_result($author, $comment);
				$stmt->fetch();
				$stmt->close();
				
				$stmt = $this->mysqli->prepare('SELECT username, first_name, last_name FROM members WHERE id="'.$author.'"');
				$stmt->execute();
				$stmt->store_result();
		 
				$stmt->bind_result($author_username, $author_first_name, $author_last_name);
				$stmt->fetch();
				$stmt->close();
				
				$author_name = $author_first_name." ".$author_last_name;
				if ($author_name==" "){
					$author_name = $author_username;
				}
				
				$comments .= $author."%end_record%".($author_name)."%end_record%".($comment)."%end_row%";
			}
			
			
			
			$array['likes'] = $likes;
			$array['user_like'] = $user_like;
			$array['comments'] = $comments;
			
			if (empty($row['download_link'])){
				$array['downloadable'] = "0";
			} else {
				$array['downloadable'] = "1";
			}
				
			array_push($records, $array);

		}

        $result->free();
		
        return $records;
    }
	
	public function editPost($id, $name, $description){
		require_once dirname(__FILE__).'/../../inc/functions.php';
		
		$results = mysql_update("posts", array("id"=>$id), array("name"=>$name, "description"=>$description));		
		
		return "success";	
	}
	
	public function getPost($post, $user){
		$this->setLastSeen($user);
		
		$records = array();

		$query = 'SELECT * FROM posts WHERE id="'.$post.'" AND status="published"';

		$result = $this->mysqli->query($query);
	       
		if($result->num_rows <= 0)
		    throw new Exception("There is no record in database!");
				
		
		while($row = $result->fetch_assoc()) {
	
			if ($row['author'] != $user){
				
				// CHECK IF BLOCKED
			
				$follow = "0";
				$results = mysql_select("follow_requests", array("follower"=>$user, "followed"=>$row['author']), array('status'));
				if (sizeof($results)>0){
					$follow = $results[0]['status'];
				}
				
				if ($follow == "blocked"){
					throw new Exception("There is no record in database!");
				}
			
				// CHECK IF PRIVATE
				$results = mysql_select("members", array("id"=>$row['author']), array("extras"));
				$extras = $results[0]['extras'];
				
				if (in_array("private", explode(";", $extras))){
				
					// CHECK IF NOT FOLLOWED
			
					$results = mysql_select("follow_requests", array('follower'=>$user, 'followed'=>$row['author'], 'status'=>'accepted'), array('id'));
					if (sizeof($results)==0){
						throw new Exception("There is no record in database!");
					}
				}
			}
			
			
		
			$array = array();
			$array['id'] = $row['id'];
			$array['author'] = $row['author'];
			$stmt = $this->mysqli->prepare('SELECT username, first_name, last_name, profile_image, timestamp FROM members WHERE id="'.$row['author'].'"');
			$stmt->execute();
			$stmt->store_result();
			
			
			/* AUTHOR */
	 
			$stmt->bind_result($author_username, $author_first_name, $author_last_name, $author_profile_image, $author_timestamp);
			$stmt->fetch();
			$stmt->close();
			
			$author_name = $author_first_name." ".$author_last_name;
			if ($author_name==" "){
				$author_name = $author_username;
			}
			
			$array['author_name'] = ($author_name);
			$array['author_profile_image'] = $author_profile_image;
			$array['author_timestamp'] = $author_timestamp;
			
			
			$array['name'] = ($row['name']);
			$array['description'] = ($row['description']);
			$array['image'] = $row['image'];
			$array['price'] = $row['price'];
			$array['download_count'] = $row['download_count'];
			$array['timestamp'] = $row['timestamp'];
			
			
			/* LIKES */
				
			$likes = ($this->mysqli->query('SELECT id FROM posts_likes WHERE post="'.$row['id'].'"'))->num_rows;
			
			/* USER LIKE */
			
			$user_like = 0;
			
			if (!empty($user)){	
				$user_like = ($this->mysqli->query('SELECT id FROM posts_likes WHERE post="'.$row['id'].'" AND user = "'.$user.'"'))->num_rows;
			}
			
			/* COMMENTS */
			
			$rows = ($this->mysqli->query('SELECT id FROM posts_comments WHERE post="'.$row['id'].'" AND status="published" '))->num_rows;
			
			$comments = $rows."%end_row%";
				
			for ($i=0; $i<min(3, $rows); $i++){
				$statement = 'SELECT author, comment FROM posts_comments WHERE post="'.$row['id'].'" AND status="published" ORDER BY id DESC LIMIT '.$i.', 1';
				
				$stmt = $this->mysqli->prepare($statement);
				$stmt->execute();
				$stmt->store_result();
		 
				$stmt->bind_result($author, $comment);
				$stmt->fetch();
				$stmt->close();
				
				
				$stmt = $this->mysqli->prepare('SELECT username, first_name, last_name FROM members WHERE id="'.$author.'"');
				$stmt->execute();
				$stmt->store_result();
		 
				$stmt->bind_result($author_username, $author_first_name, $author_last_name);
				$stmt->fetch();
				$stmt->close();
				
				$author_name = $author_first_name." ".$author_last_name;
				if ($author_name==" "){
					$author_name = $author_username;
				}
				
				$comments .= $author."%end_record%".($author_name)."%end_record%".($comment)."%end_row%";
			}
			
			
			
			$array['likes'] = $likes;
			$array['user_like'] = $user_like;
			$array['comments'] = $comments;
			
			if (empty($row['download_link'])){
				$array['downloadable'] = "0";
			} else {
				$array['downloadable'] = "1";
			}
				
			array_push($records, $array);

		}

        $result->free();
		
        return $records;
	}
	
	public function setMyPost($author, $name, $description, $image, $price, $download_link){
		$this->setLastSeen($author);
	
	
		require_once dirname(__FILE__).'/../../inc/functions.php';
		require_once dirname(__FILE__).'/../../inc/class/persianDate.class.php';
		$persian_date = new persianDate();
		
		if (!empty($download_link)){
			
			if (filter_var($download_link, FILTER_VALIDATE_URL) === FALSE) {
				return "invalid-link";
			}
			
			if (get_file_size($download_link)==0) {
				return "invalid-link";
			}
			
		}
		
		if (empty($image)){
			return "fail";
		}
		
		$date = $persian_date->date('Y/m/d');
		$time = date("H:i:s");
				

		$statement = 'INSERT INTO posts (author, name, description, image, price, download_link, date, time, status, timestamp) VALUES("'.$author.'","'.$name.'","'.$description.'", "'.$image.'", "'.$price.'","'.$download_link.'", "'.$date.'","'.$time.'","published", "'.time().'")';
		
		$stmt = $this->mysqli->prepare($statement);
		$stmt->execute();
		$stmt->fetch();
		$post = $this->mysqli->insert_id;
		$stmt->close();
		
		
		// SEARCH FOR USER TAGS

		preg_match_all('/(?<!\w)@\w+/', $description, $matches);
		 
		$results = mysql_select("members", array("id"=>$author), array("username", "first_name", "last_name"));
		$author_name = $results[0]['first_name']." ".$results[0]['last_name'];
		if ($author_name==" "){
			$author_name = $results[0]['username'];
		}
	
		 foreach($matches[0] as $match){
			 

			$results = mysql_select("members", array("username"=>$match), array("id"));
			if (sizeof($results>0)){
				$match_id = $results[0]["id"];
				
				
				if ($author != $match_id){
					$data = array();
					$data['id'] = $post;
					$data['author'] = $author_name;
					$data['action'] = "tag_post";
					$data['message'] = "";
				
					sendPushNotification(array($match_id), "101", "", $data);
				}
			}
			 
		 }
		
		
		return "success";
		
	}

	
	public function getPostComments($id, $user, $start, $amount){
		$this->setLastSeen($user);
		
		if (empty($start)){ $start = 0; }
		if (empty($amount)){ $amount = 5; }
		
		$records = array();
        $query = 'SELECT * FROM posts_comments WHERE post="'.$id.'" AND status="published" ORDER BY id DESC LIMIT '.$start.', '.$amount;

        $result = $this->mysqli->query($query);
       
        if($result->num_rows <= 0)
            throw new Exception("There is no record in database!");
        
        while($row = $result->fetch_assoc()) {
			
			$stmt = $this->mysqli->prepare('SELECT username, first_name, last_name, profile_image, timestamp FROM members WHERE id="'.$row['author'].'"');
			$stmt->execute();
			$stmt->store_result();
	 
			$stmt->bind_result($member_username, $member_first_name, $member_last_name, $member_profile_image, $member_timestamp);
			$stmt->fetch();
			$stmt->close();
			
			
			$follow = "0";
			$results = mysql_select("follow_requests", array("follower"=>$row['author'], "followed"=>$user), array('status'));
			if (sizeof($results)>0){
				$follow = $results[0]['status'];
			}
			
			if ($follow == "blocked"){
				continue;
			}
			
			$name = $member_first_name." ".$member_last_name;
			if ($name==" "){
				$name = $member_username;
			}
			
						
			$array = array();
			$array['id'] = $row['id'];
			$array['author'] = $row['author'];
			$array['author_name'] = ($name);
			$array['author_profile_image'] = $member_profile_image;
			$array['author_timestamp'] = $member_timestamp;
			$array['comment'] = ($row['comment']);
			$array['timestamp'] = $row['timestamp'];			
			
			array_push($records, $array);
		
		}
		
        $result->free();
		
		return $records;
        
	}

	public function setPostMyComment($id, $author, $comment){
		$this->setLastSeen($author);
		
		require_once dirname(__FILE__).'/../../inc/class/persianDate.class.php';
	
		$persian_date = new persianDate();

		
		$date = $persian_date->date('Y/m/d');
		$time = date("H:i:s");
				
		$statement = 'INSERT INTO posts_comments (author, post, comment, date, time, status, timestamp) VALUES("'.$author.'","'.$id.'","'.$comment.'", "'.$date.'","'.$time.'","published", "'.time().'")';
		
		$stmt = $this->mysqli->prepare($statement);
		$stmt->execute();
		$stmt->fetch();
		$stmt->close();
		
		// PUSH NOTIFICATION
		
		$results = mysql_select("posts", array("id"=>$id), array("author"));
		$post_author = $results[0]["author"];
		$results = mysql_select("members", array("id"=>$author), array("username", "first_name", "last_name"));
		$author_name = $results[0]['first_name']." ".$results[0]['last_name'];
		if ($author_name==" "){
			$author_name = $results[0]['username'];
		}
		
		
		if ($author != $post_author){
			$data = array();
			$data['id'] = $id;
			$data['author'] = $author_name;
			$data['action'] = "comment";
			$data['message'] = "";
		
			sendPushNotification(array($post_author), "101", "", $data);
		}
		
		
		// SEARCH FOR USER TAGS

		preg_match_all('/(?<!\w)@\w+/', $comment, $matches);
		
	
		foreach($matches[0] as $match){
			$match = str_replace("@", "", $match);

			$results = mysql_select("members", array("username"=>$match), array("id"));
			if (sizeof($results)>0){
				$match_id = $results[0]["id"];
				
				if ($author != $match_id){
					$data = array();
					$data['id'] = $id;
					$data['author'] = $author_name;
					$data['action'] = "tag_comment";
					$data['message'] = "";
				
					sendPushNotification(array($match_id), "101", "", $data);
				}
			}
			 
		}
			
			
		return "success";	
	}
	
	
	public function setPostMyReport($id, $author, $report){
		$this->setLastSeen($author);
		
		require_once dirname(__FILE__).'/../../inc/class/persianDate.class.php';
		$persian_date = new persianDate();
		
		$date = $persian_date->date('Y/m/d');
		$time = date("H:i:s");
				

		$statement = 'INSERT INTO posts_reports (author, post, report, date, time) VALUES("'.$author.'","'.$id.'","'.$report.'", "'.$date.'","'.$time.'")';
		
		$stmt = $this->mysqli->prepare($statement);
		$stmt->execute();
		$stmt->fetch();
		$stmt->close();
		
		return "success";
	}
	
	public function likePost($user, $post){
		$this->setLastSeen($user);
		
		require_once dirname(__FILE__).'/../../inc/class/persianDate.class.php';
		$persian_date = new persianDate();
		
		$date = $persian_date->date('Y/m/d');
		$time = date("H:i:s");
		
		$results = mysql_select("posts_likes", array('user'=>$user, 'post'=>$post), array('id'));
		
		if (sizeof($results) > 0){
			mysql_delete("posts_likes", array('user'=>$user, 'post'=>$post));
		} else {
			mysql_insert("posts_likes", array('user'=>$user, 'post'=>$post, 'date'=>$date, 'time'=>$time, 'timestamp'=>time()));
			
			// PUSH NOTIFICATION
		
			$results = mysql_select("posts", array("id"=>$post), array("author"));
			$post_author = $results[0]["author"];
			$results = mysql_select("members", array("id"=>$user), array("username", "first_name", "last_name"));
			$user_name = $results[0]['first_name']." ".$results[0]['last_name'];
			if ($user_name==" "){
				$user_name = $results[0]['username'];
			}
			
			if ($user != $post_author){
				$data = array();
				$data['id'] = $post;
				$data['author'] = $user_name;
				$data['action'] = "like";
				$data['message'] = "";
			
				sendPushNotification(array($post_author), "101", "", $data);
			}
			
		}
		
		return "success";	
	}
	
	public function purchasePost($customer, $post){
		$this->setLastSeen($customer);
		
		// IF POST EXISTS
		
		$results = mysql_select("posts", array('id'=>$post), array('author', 'price', 'download_link', 'download_count'));

		if (sizeof($results) == 0){
			return "invalid-post";
		}
		
		$author = $results[0]['author'];
		$price = $results[0]['price'];
		$download_link = $results[0]['download_link'];
		$download_count = $results[0]['download_count'];
		
		if ($author == $customer){
			return "success;".$download_link;
		}
	
		
		// IF IS PURCHASABLE

		
		if ($price > 0){
			
			// IF ALREADY PURCHASED
			
			$results = mysql_select("purchase_history", array('customer'=>$customer, 'post'=>$post), array('id'));
		
			if (sizeof($results) > 0){
				mysql_update("posts", array('id'=>$post), array('download_count'=>($download_count+1)));
				return "success;".$download_link;
			} else {
				
				// PURCHASE
				
				$results = mysql_select("members", array('id'=>$customer), array('balance'));
				if (sizeof($results)>0){
					$balance = $results[0]['balance'];
					
					if ($balance<$price){
						return "not-enough-balance";
					}
					
					mysql_update("members", array('id'=>$customer), array('balance'=>($balance-$price)));
					mysql_update("posts", array('id'=>$post), array('download_count'=>($download_count+1)));
					
					
					// AUTHOR NEW BALANCE
					
					$results = mysql_select("members", array('id'=>$author), array('balance'));
					if (sizeof($results)>0){
						$author_balance = $results[0]['balance'];
						
						$new_balance = $author_balance + $price * 85/100;
						mysql_update("members", array('id'=>$author), array('balance'=>$new_balance));
						
						require_once dirname(__FILE__).'/../../inc/class/persianDate.class.php';
						$persian_date = new persianDate();
						
						$date = $persian_date->date('Y/m/d');
						$time = date("H:i:s");
					
						$results = mysql_insert("purchase_history", array('customer'=>$customer, 'post'=>$post, 'date'=>$date, 'time'=>$time));
						return "success;".$download_link;
					}
				}
			
			}
		} else {
			mysql_update("posts", array('id'=>$post), array('download_count'=>($download_count+1)));
			return "success;".$download_link;
		}
		
		return "fail";
	}
	
	public function getProfile($author, $user, $method){
		$this->setLastSeen($user);
		
		$records = array();
		
		$query = 'SELECT id, username, profile_image, first_name, last_name, about, extras, timestamp FROM members';
		
		if ($method == "id"){
			$query .= ' WHERE id = "'.$author.'" LIMIT 1';
		} else if ($method == "username"){
			$query .= ' WHERE username = "'.$author.'" LIMIT 1';
		}
		
		
		$stmt = $this->mysqli->prepare($query);
		
		$result = $this->mysqli->query($query);
       
		if($result->num_rows <= 0)
			throw new Exception("There is no record in database!");

		
        $stmt->execute(); 
        $stmt->store_result();

        $stmt->bind_result($id, $username, $profile_image, $first_name, $last_name, $about, $extras, $timestamp);
        $stmt->fetch();
		
		
				
		$posts = ($this->mysqli->query('SELECT id FROM posts WHERE author="'.$id.'" AND status="published"'))->num_rows;
		$followers = ($this->mysqli->query('SELECT id FROM follow_requests WHERE followed="'.$id.'" AND status="accepted"'))->num_rows;
		$followeds = ($this->mysqli->query('SELECT id FROM follow_requests WHERE follower="'.$id.'" AND status="accepted"'))->num_rows;
		$follow = "0";
		$results = mysql_select("follow_requests", array("follower"=>$user, "followed"=>$id), array('status'));
		if (sizeof($results)>0){
			$follow = $results[0]['status'];
		}
		
		if ($follow == "blocked"){
			throw new Exception("There is no record in database!");
		}
		
		$request = "0";
		$results = mysql_select("follow_requests", array("follower"=>$id, "followed"=>$user), array('status'));
		if (sizeof($results)>0){
			$request = $results[0]['status'];
		}
		

		
		$array = array();
		$array['id'] = $id;
		$array['username'] = $username;
		$array['profile_image'] = $profile_image;
		$array['first_name'] = ($first_name);
		$array['last_name'] = ($last_name);
		$array['about'] = ($about);
		$array['extras'] = ($extras);
		$array['timestamp'] = $timestamp;
		$array['posts'] = $posts;
		$array['followers'] = $followers;
		$array['followeds'] = $followeds;
		$array['follow'] = $follow;
		$array['request'] = $request;
		
			
		array_push($records, $array);

		
		return $records;
	}
	
	
	public function followUser($follower, $followed){
		$this->setLastSeen($follower);
		
		require_once dirname(__FILE__).'/../../inc/class/persianDate.class.php';
		$persian_date = new persianDate();
		
		$date = $persian_date->date('Y/m/d');
		$time = date("H:i:s");
		
		// IF IS PRIVATE
			
		$results = mysql_select("members", array("id"=>$followed), array("extras"));
		$extras = $results[0]['extras'];
		
		if (in_array("private", explode(";", $extras))){
			
			// CHECK IF FOLLOWED
	
			$results = mysql_select("follow_requests", array('follower'=>$follower, 'followed'=>$followed, 'status'=>'accepted'), array('id'));
	
			if (sizeof($results) > 0){
				mysql_delete("follow_requests", array('follower'=>$follower, 'followed'=>$followed));
			} else {
	
				// CHECK IF REQUESTED
		
				$results = mysql_select("follow_requests", array('follower'=>$follower, 'followed'=>$followed, 'status'=>'waiting'), array('id'));
		
				if (sizeof($results) > 0){
					mysql_delete("follow_requests", array('follower'=>$follower, 'followed'=>$followed));
					return "success";
				} else {
					mysql_insert("follow_requests", array("follower"=>$follower, "followed"=>$followed, "status"=>"waiting", "date"=>$date, "time"=>$time, "timestamp"=>time()));
					
					
					// PUSH NOTIFICATION
		
					$results = mysql_select("members", array("id"=>$follower), array("username", "first_name", "last_name"));
					$follower_name = $results[0]['first_name']." ".$results[0]['last_name'];
					if ($follower_name==" "){
						$follower_name = $results[0]['username'];
					}
					
					$data = array();
					$data['id'] = $follower;
					$data['author'] = $follower_name;
					$data['action'] = "follow_request";
					$data['message'] = "";
				
					sendPushNotification(array($followed), "101", "", $data);					
					
					return "requested";
				}
			}
			
		} else {
	
			// CHECK IF FOLLOWED
	
			$results = mysql_select("follow_requests", array('follower'=>$follower, 'followed'=>$followed, 'status'=>'accepted'), array('id'));
	
			if (sizeof($results) > 0){
				mysql_delete("follow_requests", array('follower'=>$follower, 'followed'=>$followed));
			} else {
				mysql_insert("follow_requests", array('follower'=>$follower, 'followed'=>$followed, "status"=>"accepted", 'date'=>$date, 'time'=>$time, 'timestamp'=>time()));
				
				// PUSH NOTIFICATION
				
				$results = mysql_select("members", array("id"=>$follower), array("username", "first_name", "last_name"));
				$follower_name = $results[0]['first_name']." ".$results[0]['last_name'];
				if ($follower_name==" "){
					$follower_name = $results[0]['username'];
				}
				
				$data = array();
				$data['id'] = $follower;
				$data['author'] = $follower_name;
				$data['action'] = "follow";
				$data['message'] = "";
			
				sendPushNotification(array($followed), "101", "", $data);
				
			}
			
			return "success";
			
			
		}
		
		return "fail";
	}
	
	public function deleteMyPost($post, $author){
		$this->setLastSeen($author);
		
		$results = mysql_select("posts", array('id'=>$post), array('author'));
		
		if (sizeof($results) > 0){
			if ($results[0]['author'] == $author){
				mysql_update("posts", array('id'=>$post), array('status'=>'deleted'));
				return "success";
			} else {
				return "fail";
			}
		} else {
			return "fail";
		}
		
		return "fail";	
	}
	
	public function getUsers($method, $argument, $user, $start, $amount){
		$this->setLastSeen($user);
		
		$records = array();
		
		if ($method == "participate"){
			$results = mysql_select("chats", array("id"=>$argument), array("creator", "participants"));
			$creator = $results[0]["creator"];
			$participants = $results[0]["participants"];
			$participants_array = array_filter(array_unique(explode("!new_user!", $participants.$creator)));
			
			foreach($participants_array as $participant){
				$results = mysql_select("members", array("id"=>$participant), array("id", "username", "profile_image", "first_name", "last_name", "last_seen", "timestamp"));
				if (sizeof($results)>0){
					$author = $results[0];
					
					$author_name = $author['first_name']." ".$author['last_name'];
					if ($author_name==" "){
						$author_name = $author['username'];
					}
					$author_timestamp = $author['timestamp'];
					
					
					$follow = "0";
					$results = mysql_select("follow_requests", array("follower"=>$user, "followed"=>$author['id']), array('status'));
					if (sizeof($results)>0){
						$follow = $results[0]['status'];
					}
					
					if ($follow == "blocked") {
						continue;
					} else {
					
						$array = array();
						$array['id'] = $author['id'];
						$array['username'] = $author['username'];
						$array['name'] = $author_name;
						$array['profile_image'] = $author['profile_image'];
						$array['follow'] = $follow;
						$array['timestamp'] = $author_timestamp;
						
						$extras = array();
						if ($array['id'] == $creator){
							$extras['role'] = "admin";
						} else {
							$extras['role'] = "participant";
						}
						
						$extras['last_seen'] = $author['last_seen'];
						$array['last_seen'] = $author['last_seen'];
						
						$array['extras'] = json_encode($extras);
							
						array_push($records, $array);
					}
				}
			}
			
			// SORT
			
			for ($j=0; $j<sizeof($records)-1; $j++){
				for ($i=0; $i<sizeof($records)-1; $i++){
					if ($records[$i+1]['last_seen'] > $records[$i]['last_seen']){
						$temp = $records[$i];
						$records[$i] = $records[$i+1];
						$records[$i+1] = $temp;
					} 
				}
			}
			
			return $records;
		}
		
		
		
		if ($method == "liked"){	
			$query = 'SELECT user FROM posts_likes WHERE post = "'.$argument.'" ORDER BY id DESC LIMIT '.$start.', '.$amount;
		} else if ($method == "follows"){
			$query = 'SELECT follower FROM follow_requests WHERE followed = "'.$argument.'" AND status="accepted" ORDER BY id DESC LIMIT '.$start.', '.$amount;		
		} else if ($method == "followed"){
			$query = 'SELECT followed FROM follow_requests WHERE follower = "'.$argument.'" AND status="accepted" ORDER BY id DESC LIMIT '.$start.', '.$amount;			
		}
		
		$result = $this->mysqli->query($query);
       
		if($result->num_rows <= 0)
			throw new Exception("There is no record in database!");
			
		
		while($row = $result->fetch_assoc()) {
			
			if ($method == "liked"){
				$record_user = $row["user"];
			} else if ($method == "follows"){
				$record_user = $row["follower"];
			} else if ($method == "followed"){
				$record_user = $row["followed"];
			}
			
	
			$results = mysql_select("members", array("id"=>$record_user), array("id", "username", "profile_image", "first_name", "last_name", "timestamp"));
			if (sizeof($results)>0){
				$author = $results[0];
				
				$author_name = $author['first_name']." ".$author['last_name'];
				if ($author_name==" "){
					$author_name = $author['username'];
				}
				
				
				$follow = "0";
				$results = mysql_select("follow_requests", array("follower"=>$user, "followed"=>$author['id']), array('status'));
				if (sizeof($results)>0){
					$follow = $results[0]['status'];
				}
				
				if ($follow == "blocked"){
					continue;
				} else{
				
					$array = array();
					$array['id'] = $author['id'];
					$array['username'] = $author['username'];
					$array['name'] = $author_name;
					$array['profile_image'] = $author['profile_image'];
					$array['follow'] = $follow;
					$array['timestamp'] = $author['timestamp'];
					
					$array['extras'] = '';
						
						
					array_push($records, $array);
				}
			}
		}

		
		return $records;
	}


	public function requestCheckOut($user){
		$this->setLastSeen($user);
		
		
		global $options;
		require_once dirname(__FILE__).'/../../inc/class/persianDate.class.php';
		$persian_date = new persianDate();
		
		$date = $persian_date->date('Y/m/d');
		$time = date("H:i:s");
		
		$results = mysql_select("members", array('id'=>$user), array('balance', 'mobile_number', 'shaba'));
		if (sizeof($results)>0){
			$balance = $results[0]['balance'];
			$mobile_number = $results[0]['mobile_number'];
			$shaba = $results[0]['shaba'];
			
			if ($balance >= $options["min_checkout_balance"]){
				if (empty($mobile_number)){
					return "mobile_number";
				}
				if (empty($shaba)){
					return "shaba";
				}
				
				mysql_insert("checkout_requests", array('user'=>$user, 'price'=>min($balance, $options["max_checkout_price"]), 'date'=>$date, 'time'=>$time, 'status'=>'not-checked'));
				mysql_update("members", array('id'=>$user), array('balance'=>($balance - min($balance, $options["max_checkout_price"]))));
				
				return "success";
			} else {
				return "not-enough-balance;".$options["min_checkout_balance"];
			}
		}
		
		
		return "fail";	
	}
	
	public function searchPosts($tag, $start, $amount, $user) {
		$this->setLastSeen($user);
		
		$records = array();

		$query = 'SELECT * FROM posts WHERE status="published" AND name LIKE "%'.$tag.'%" OR description LIKE "%'.$tag.'%" AND status="published" ORDER BY id DESC LIMIT '.$start.', '.$amount;

		$result = $this->mysqli->query($query);
	       
		if($result->num_rows <= 0)
		    throw new Exception("There is no record in database!");
		
		while($row = $result->fetch_assoc()) {
				
			$follow = "0";
			if ($row['author'] != $user){
				// CHECK IF BLOCKED
			
				$follow = "0";
				$results = mysql_select("follow_requests", array("follower"=>$user, "followed"=>$row['author']), array('status'));
				if (sizeof($results)>0){
					$follow = $results[0]['status'];
				}
				
				if ($follow == "blocked"){
					continue;
				}
			
				// CHECK IF PRIVATE
				$results = mysql_select("members", array("id"=>$row['author']), array("extras"));
				$extras = $results[0]['extras'];

				
				if (in_array("private", explode(";", $extras))){
				
					// CHECK IF NOT FOLLOWED
			
					$results = mysql_select("follow_requests", array('follower'=>$user, 'followed'=>$row['author'], 'status'=>'accepted'), array('id'));
					if (sizeof($results)==0){
						continue;
					}
				}
			}
			
			$array = array();
			$array['id'] = $row['id'];
			$array['author'] = $row['author'];
			$stmt = $this->mysqli->prepare('SELECT username, first_name, last_name, profile_image, timestamp FROM members WHERE id="'.$row['author'].'"');
			$stmt->execute();
			$stmt->store_result();
			
			
			
			
			/* AUTHOR */
	 
			$stmt->bind_result($author_username, $author_first_name, $author_last_name, $author_profile_image, $author_timestamp);
			$stmt->fetch();
			$stmt->close();
			
			$author_name = $author_first_name." ".$author_last_name;
			if ($author_name==" "){
				$author_name = $author_username;
			}
			
			$array['author_name'] = ($author_name);
			$array['author_profile_image'] = $author_profile_image;
			$array['author_timestamp'] = $author_timestamp;
			
			
			$array['name'] = ($row['name']);
			$array['description'] = ($row['description']);
			$array['image'] = $row['image'];
			$array['price'] = $row['price'];
			$array['download_count'] = $row['download_count'];
			$array['timestamp'] = $row['timestamp'];
			
			
			
			/* LIKES */
				
			$likes = ($this->mysqli->query('SELECT id FROM posts_likes WHERE post="'.$row['id'].'"'))->num_rows;
			
			/* USER LIKE */
			
			$user_like = 0;
			
			if (!empty($user)){	
				$user_like = ($this->mysqli->query('SELECT id FROM posts_likes WHERE post="'.$row['id'].'" AND user = "'.$user.'"'))->num_rows;
			}
			
			/* COMMENTS */
			
			$rows = ($this->mysqli->query('SELECT id FROM posts_comments WHERE post="'.$row['id'].'" AND status="published"'))->num_rows;
			
			$comments = $rows."%end_row%";
				
			for ($i=0; $i<min(3, $rows); $i++){
				$statement = 'SELECT author, comment FROM posts_comments WHERE post="'.$row['id'].'" AND status="published" ORDER BY id DESC LIMIT '.$i.', 1';
				
				$stmt = $this->mysqli->prepare($statement);
				$stmt->execute();
				$stmt->store_result();
		 
				$stmt->bind_result($author, $comment);
				$stmt->fetch();
				$stmt->close();
				
				
				$stmt = $this->mysqli->prepare('SELECT username, first_name, last_name FROM members WHERE id="'.$author.'"');
				$stmt->execute();
				$stmt->store_result();
		 
				$stmt->bind_result($author_username, $author_first_name, $author_last_name);
				$stmt->fetch();
				$stmt->close();
				
				$author_name = $author_first_name." ".$author_last_name;
				if ($author_name==" "){
					$author_name = $author_username;
				}
				
				$comments .= $author."%end_record%".($author_name)."%end_record%".($comment)."%end_row%";
			}
			
			
			
			$array['likes'] = $likes;
			$array['user_like'] = $user_like;
			$array['comments'] = $comments;
			
			if (empty($row['download_link'])){
				$array['downloadable'] = "0";
			} else {
				$array['downloadable'] = "1";
			}
				
			array_push($records, $array);
		}

		$result->free();
			
		return $records;
	}
		
	public function searchUsers($username, $user, $start, $amount){
		$this->setLastSeen($user);
		
		$records = array();
		
		$query = 'SELECT id FROM members WHERE username LIKE "%'.$username.'%" OR first_name LIKE "%'.$username.'%" OR last_name LIKE "%'.$username.'%" ORDER BY id DESC LIMIT '.$start.', '.$amount;
		
		$result = $this->mysqli->query($query);
       
		if($result->num_rows <= 0)
			throw new Exception("There is no record in database!");
			
		
		while($row = $result->fetch_assoc()) {
			
			$record_user = $row["id"];
			
	
			$results = mysql_select("members", array("id"=>$record_user), array("id", "username", "profile_image", "first_name", "last_name", "profile_image", "timestamp"));
			if (sizeof($results)>0){
				$author = $results[0];
				
				$author_name = $author['first_name']." ".$author['last_name'];
				if ($author_name==" "){
					$author_name = $author['username'];
				}
				$author_timestamp = $author['timestamp'];
				
				
				$follow = "0";
				$results = mysql_select("follow_requests", array("follower"=>$user, "followed"=>$author['id']), array('status'));
				if (sizeof($results)>0){
					$follow = $results[0]['status'];
				}
				
				$followers = ($this->mysqli->query('SELECT id FROM follow_requests WHERE followed="'.$author['id'].'" AND status="accepted"'))->num_rows;;
				
				
				
				if ($follow == "blocked"){
					continue;
				} else{
					$array = array();
					$array['id'] = $author['id'];
					$array['username'] = $author['username'];
					$array['name'] = $author_name;
					$array['profile_image'] = $author['profile_image'];
					$array['follow'] = $follow;
					$array['followers'] = $followers;
					$array['timestamp'] = $author_timestamp;
						
						
					array_push($records, $array);
				}
			}
		}
		
		
		// SORT
		
		for ($j=0; $j<sizeof($records)-1; $j++){
			for ($i=0; $i<sizeof($records)-1; $i++){
				if ($records[$i+1]['followers'] > $records[$i]['followers']){
					$temp = $records[$i];
					$records[$i] = $records[$i+1];
					$records[$i+1] = $temp;
				} 
			}
		}

		
		return $records;
	}
	
	public function getHistory($user, $amount, $times){
		$this->setLastSeen($user);
		
		$times = substr($times, 0, 10);
				
		$records = array();
		
		$results = mysql_select("members", array('id'=>$user), array('username'));
		if (sizeof($results)==0){
			throw new Exception("There is no record in database!");
			return;
		}
		$username = $results[0]['username'];
		
		
		// FOLLOW REQUESTED
		
		$query = 'SELECT follower, timestamp FROM follow_requests WHERE followed = "'.$user.'" AND status="waiting" AND timestamp > '.$times.' ORDER BY id DESC LIMIT 0, '.$amount;
		
		$result = $this->mysqli->query($query);
			
		
		while($row = $result->fetch_assoc()) {
			
			$author = $row['follower'];
			$timestamp = $row['timestamp'];
			
			$results = mysql_select("members", array("id"=>$author), array("id", "username", "profile_image", "first_name", "last_name", "timestamp"));
			if (sizeof($results)>0){
				$author = $results[0];
				
				$author_name = $author['first_name']." ".$author['last_name'];
				if ($author_name==" "){
					$author_name = $author['username'];
				}
				$author_timestamp = $author['timestamp'];
				
				$array = array();
				$array['id'] = $author['id'];
				$array['author'] = $author['id'];
				$array['author_name'] = ($author_name);
				$array['author_profile_image'] = $author['profile_image'];
				$array['action'] = "follow_request";
				$array['timestamp'] = $timestamp;
				$array['author_timestamp'] = $author_timestamp;
				//$array['sort'] = time()-$timestamp;
				
				array_push($records, $array);
			}
		}
		
		
		// FOLLOW REQUEST ACCEPTED
		
		$query = 'SELECT followed, timestamp FROM follow_requests WHERE follower = "'.$user.'" AND status="accepted" AND timestamp > '.$times.' ORDER BY id DESC LIMIT 0, '.$amount;
		
		$result = $this->mysqli->query($query);
			
		
		while($row = $result->fetch_assoc()) {
			
			$author = $row['followed'];
			$timestamp = $row['timestamp'];
			
			$results = mysql_select("members", array("id"=>$author), array("id", "username", "profile_image", "first_name", "last_name", "extras", "timestamp"));
			if (sizeof($results)>0){
				$author = $results[0];
				
				$author_name = $author['first_name']." ".$author['last_name'];
				if ($author_name==" "){
					$author_name = $author['username'];
				}
				$author_timestamp = $author['timestamp'];
				
				if (!in_array("private", explode(";", $author['extras']))){
					continue;
				} else {
				
					$array = array();
					$array['id'] = $author['id'];
					$array['author'] = $author['id'];
					$array['author_name'] = ($author_name);
					$array['author_profile_image'] = $author['profile_image'];
					$array['action'] = "follow_accept";
					$array['timestamp'] = $timestamp;
					$array['author_timestamp'] = $author_timestamp;
					//$array['sort'] = time()-$timestamp;
					
					array_push($records, $array);
				}
			}
		}
		
		
		// FOLLOWED
		
		$query = 'SELECT follower, timestamp FROM follow_requests WHERE followed = "'.$user.'" AND status="accepted" AND timestamp > '.$times.' ORDER BY id DESC LIMIT 0, '.$amount;
		
		$result = $this->mysqli->query($query);
			
		
		while($row = $result->fetch_assoc()) {
			
			$author = $row['follower'];
			$timestamp = $row['timestamp'];
			
			$results = mysql_select("members", array("id"=>$author), array("id", "username", "profile_image", "first_name", "last_name", "timestamp"));
			if (sizeof($results)>0){
				$author = $results[0];
				
				$author_name = $author['first_name']." ".$author['last_name'];
				if ($author_name==" "){
					$author_name = $author['username'];
				}
				$author_timestamp = $author['timestamp'];
				
				$array = array();
				$array['id'] = $author['id'];
				$array['author'] = $author['id'];
				$array['author_name'] = ($author_name);
				$array['author_profile_image'] = $author['profile_image'];
				$array['action'] = "follow";
				$array['timestamp'] = $timestamp;
				$array['author_timestamp'] = $author_timestamp;
				//$array['sort'] = time()-$timestamp;
				
				array_push($records, $array);
			}
		}
		
		
		// POSTS LIKES & COMMENTS

		
		$query2 = 'SELECT id FROM posts WHERE author = "'.$user.'" AND status="published" ORDER BY id DESC';
		
		$result2 = $this->mysqli->query($query2);
		
		while($row2 = $result2->fetch_assoc()) {
			
			// POSTS LIKES

			
			$post = $row2['id'];
		
			$query = 'SELECT user, timestamp FROM posts_likes WHERE post = "'.$post.'" AND timestamp > '.$times.' AND user!="'.$user.'" ORDER BY id DESC LIMIT 0, '.$amount;
			
			$result = $this->mysqli->query($query);
				
			
			while($row = $result->fetch_assoc()) {
				
				$author = $row['user'];
				$timestamp = $row['timestamp'];
				
				$results = mysql_select("members", array("id"=>$author), array("id", "username", "profile_image", "first_name", "last_name", "timestamp"));
				if (sizeof($results)>0){
					$author = $results[0];
					
					$author_name = $author['first_name']." ".$author['last_name'];
					if ($author_name==" "){
						$author_name = $author['username'];
					}
					$author_timestamp = $author['timestamp'];
					
					$array = array();
					$array['id'] = $post;
					$array['author'] = $author['id'];
					$array['author_name'] = ($author_name);
					$array['author_profile_image'] = $author['profile_image'];
					$array['action'] = "like";
					$array['timestamp'] = $timestamp;
					$array['author_timestamp'] = $author_timestamp;
					//$array['sort'] = time()-$timestamp;
					
					array_push($records, $array);
				}
			}
			
		
			// POSTS COMMENTS
		
			$query = 'SELECT author, timestamp FROM posts_comments WHERE post = "'.$post.'" AND timestamp > '.$times.' AND author!="'.$user.'" AND status="published" ORDER BY id DESC LIMIT 0, '.$amount;
			
			$result = $this->mysqli->query($query);
				
			
			while($row = $result->fetch_assoc()) {
				
				$author = $row['author'];
				$timestamp = $row['timestamp'];
				
				$results = mysql_select("members", array("id"=>$author), array("id", "username", "profile_image", "first_name", "last_name", "timestamp"));
				if (sizeof($results)>0){
					$author = $results[0];
					
					$author_name = $author['first_name']." ".$author['last_name'];
					if ($author_name==" "){
						$author_name = $author['username'];
					}
					$author_timestamp = $author['timestamp'];
					
					$array = array();
					$array['id'] = $post;
					$array['author'] = $author['id'];
					$array['author_name'] = ($author_name);
					$array['author_profile_image'] = $author['profile_image'];
					$array['action'] = "comment";
					$array['timestamp'] = $timestamp;
					$array['author_timestamp'] = $author_timestamp;
					//$array['sort'] = time()-$timestamp;
					
					array_push($records, $array);
				}
			}
			
			
		}
		
		
		
		// TAG IN POST
		
		
		$query = 'SELECT id, author, timestamp FROM posts WHERE description LIKE "%@'.$username.'%" AND timestamp > '.$times.' AND status="published" AND author!="'.$user.'" ORDER BY id DESC LIMIT 0, '.$amount;
			
		$result = $this->mysqli->query($query);
			
		
		while($row = $result->fetch_assoc()) {
			
			$post = $row['id'];
			$author = $row['author'];
			$timestamp = $row['timestamp'];
			
			$results = mysql_select("members", array("id"=>$author), array("id", "username", "profile_image", "first_name", "last_name", "timestamp"));
			if (sizeof($results)>0){
				$author = $results[0];
				
				$author_name = $author['first_name']." ".$author['last_name'];
				if ($author_name==" "){
					$author_name = $author['username'];
				}
				$author_timestamp = $author['timestamp'];
				
				$array = array();
				$array['id'] = $post;
				$array['author'] = $author['id'];
				$array['author_name'] = ($author_name);
				$array['author_profile_image'] = $author['profile_image'];
				$array['action'] = "tag_post";
				$array['timestamp'] = $timestamp;
				$array['author_timestamp'] = $author_timestamp;
				//$array['sort'] = time()-$timestamp;
				
				array_push($records, $array);
			}
		}
		
		
		
		// TAG IN COMMENT
		
		$query = 'SELECT author, post, timestamp FROM posts_comments WHERE comment LIKE "%@'.$username.'%" AND timestamp > '.$times.' AND author!="'.$user.'" AND status="published" ORDER BY id DESC LIMIT 0, '.$amount;
			
		$result = $this->mysqli->query($query);
			
		
		while($row = $result->fetch_assoc()) {
			
			$post = $row['post'];
			$author = $row['author'];
			$timestamp = $row['timestamp'];
			
			$results = mysql_select("members", array("id"=>$author), array("id", "username", "profile_image", "first_name", "last_name", "timestamp"));
			if (sizeof($results)>0){
				$author = $results[0];
				
				$author_name = $author['first_name']." ".$author['last_name'];
				if ($author_name==" "){
					$author_name = $author['username'];
				}
				$author_timestamp = $author['timestamp'];
				
				$array = array();
				$array['id'] = $post;
				$array['author'] = $author['id'];
				$array['author_name'] = ($author_name);
				$array['author_profile_image'] = $author['profile_image'];
				$array['action'] = "tag_comment";
				$array['timestamp'] = $timestamp;
				$array['author_timestamp'] = $author_timestamp;
				//$array['sort'] = time()-$timestamp;
				
				array_push($records, $array);
			}
		}
		
			
		// SORT
		
		for ($j=0; $j<sizeof($records)-1; $j++){
			for ($i=0; $i<sizeof($records)-1; $i++){
				if ($records[$i+1]['timestamp'] > $records[$i]['timestamp']){
					$temp = $records[$i];
					$records[$i] = $records[$i+1];
					$records[$i+1] = $temp;
				} 
			}
		}
		
		
		return $records;
	}
	
	public function respondRequest($user, $follower, $action){
		$this->setLastSeen($user);
		
		require_once dirname(__FILE__).'/../../inc/class/persianDate.class.php';
		$persian_date = new persianDate();
		
		$date = $persian_date->date('Y/m/d');
		$time = date("H:i:s");
		
		$results = mysql_select("follow_requests", array("follower"=>$follower, "followed"=>$user), array("id"));
		if (sizeof($results)>0){
			if ($action == "accept"){
				mysql_update("follow_requests", array("follower"=>$follower, "followed"=>$user), array("status"=>"accepted", "date"=>$date, "time"=>$time, "timestamp"=>time()));
				
				// PUSH NOTIFICATION
		
				$results = mysql_select("members", array("id"=>$user), array("username", "first_name", "last_name"));
				$author_name = $results[0]['first_name']." ".$results[0]['last_name'];
				if ($author_name==" "){
					$author_name = $results[0]['username'];
				}
				
				$data = array();
				$data['id'] = $user;
				$data['author'] = $author_name;
				$data['action'] = "follow_accept";
				$data['message'] = "";
			
				sendPushNotification(array($follower), "101", "", $data);

			} else {
				mysql_delete("follow_requests", array('follower'=>$follower, 'followed'=>$user));
			}
			return "success";
		}
		
		return "fail";
	}
	
	public function blockUser($user, $id){
		$this->setLastSeen($id);
		
		require_once dirname(__FILE__).'/../../inc/class/persianDate.class.php';
		$persian_date = new persianDate();
		
		$date = $persian_date->date('Y/m/d');
		$time = date("H:i:s");
		
	
		// CHECK IF FOLLOWED

		$results = mysql_select("follow_requests", array('follower'=>$user, 'followed'=>$id, "status"=>"blocked"), array('id'));

		if (sizeof($results) > 0){
			mysql_delete("follow_requests", array('follower'=>$user, 'followed'=>$id, "status"=>"blocked"));
		} else {
			mysql_insert("follow_requests", array('follower'=>$user, 'followed'=>$id, "status"=>"blocked", "date"=>$date, "time"=>$time, "timestamp"=>time()));
		}
	
		return "success";
			
			
	}
	
	
	public function deleteComment($user, $comment){
		$this->setLastSeen($user);
	
		// CHECK IF EXITST

		$results = mysql_select("posts_comments", array('id'=>$comment), array('author', 'post'));

		if (sizeof($results) > 0){
			$author = $results[0]['author'];
			$post = $results[0]['post'];
			
			$post_author = "";
			$results = mysql_select("posts", array('id'=>$post), array('author'));
			if (sizeof($results)>0){
				$post_author = $results[0]['author'];
			}
			
			if ($user == $author || $user == $post_author){
				$results = mysql_update("posts_comments", array('id'=>$comment), array('status'=>"deleted"));
				return "success";
			}
		
		}
	
		return "fail";
			
			
	}
	
	public function Report($reporter, $reported, $subject, $report){
		$this->setLastSeen($reporter);
	
		require_once dirname(__FILE__).'/../../inc/class/persianDate.class.php';
		$persian_date = new persianDate();

		$date = $persian_date->date('Y/m/d');
		$time = date("H:i:s");
	
		mysql_insert("user_reports", array("reporter"=>$reporter, "reported"=>$reported, "subject"=>$subject, "report"=>$report, "date"=>$date, "time"=>$time, "status"=>"not-checked"));
	
		return "success";
			
	}
	
	public function inAppCharge($user, $sku){
		$this->setLastSeen($user);
		
		require_once dirname(__FILE__).'/../../inc/information.php';
		global $in_app_skus;

		if (!isset($in_app_skus[$sku])){
			return "fail";
		}
	
		// CHECK IF EXITST
		
		$results = mysql_select("members", array("id"=>$user), array("balance"));
		

		if (sizeof($results) > 0){
			$balance = $results[0]["balance"];
			$balance += $in_app_skus[$sku];

			$results = mysql_update("members", array("id"=>$user), array("balance"=>$balance));
			
			return "success";
		
		}
	
		return "fail";
			
			
	}
	
	public function startChat($creator, $participants, $name, $image){
		$this->setLastSeen($creator);
		
		$timestamp = time();
	
		// CHECK IF USERS EXISTS
		$new_participants_array = array();
		$participants_array = array_filter(array_unique(explode("!new_user!", $participants)));
		foreach($participants_array as $participant){
			if (!empty($participant)){
				$results = mysql_select("members", array("username"=>$participant), array("id"));
				if (sizeof($results)==0){
					return "no-user";
				} else {
					array_push($new_participants_array, $results[0]["id"]);
				}
			}
		}

		$new_participants = "!new_user!";
		foreach($new_participants_array as $new_participant){
			if (!empty($new_participant)){
				$new_participants .= $new_participant."!new_user!";	
			}
		}
		
		// SINGLE CHAT
		if (sizeof($new_participants_array)==1){
			// CHECK IF EXITST
	
			$results = mysql_select("chats", array("creator"=>$creator, "participants"=>$new_participants), array("id"));
	
			if (sizeof($results) > 0){
				return "success;".$results[0]["id"];
			} else {
				
				$results = mysql_select("chats", array("creator"=>$new_participants_array[0], "participants"=>"!new_user!".$creator."!new_user!"), array("id"));
				
				if (sizeof($results) > 0){
					return "success;".$results[0]["id"];
				} 
				
			}
			
			// IF NOT EXISTS
			
			$results = mysql_insert("chats", array("creator"=>$creator, "participants"=>$new_participants, "type"=>"single", "timestamp"=>$timestamp));
			return "success;".$results;
			
		} else {
		
			$participants_array = array_filter(array_unique(explode("!new_user!", $new_participants)));
				
			// GROUP CHAT
			$chat_id = mysql_insert("chats", array("creator"=>$creator, "participants"=>$new_participants, "type"=>"group", "name"=>$name, "timestamp"=>$timestamp));
			
			
			$id = mysql_insert("chats_messages", array("chat"=>$chat_id, "sender"=>$creator, "action"=>"create_group", "message"=>"", "timestamp"=>$timestamp));

			$results = mysql_select("members", array("id"=>$creator), array("username", "first_name", "last_name", "profile_image"));
			$sender_name = $results[0]['first_name']." ".$results[0]['last_name'];
			if ($sender_name==" "){
				$sender_name = $results[0]['username'];
			}
			$sender_profile_image = $results[0]['profile_image'];
		
			$data = array(
				'id'=>$id,
				'chat'=>$chat_id,
				'sender'=>$creator,
				'sender_name'=>$sender_name,
				'sender_profile_image'=>$sender_profile_image,
				'action'=>"create_group",
				'message'=>"",
				'extras'=>"",
				'timestamp'=>$timestamp
			);
			
			sendPushNotification($participants_array, "102", "", $data);
			
			foreach($participants_array as $participant){
				$results = mysql_select("members", array("id"=>$participant), array("username", "first_name", "last_name"));
				$participant_name = $results[0]['first_name']." ".$results[0]['last_name'];
				if ($participant_name==" "){
					$participant_name = $results[0]['username'];
				}
			
			
				$id = mysql_insert("chats_messages", array("chat"=>$chat_id, "sender"=>$creator, "action"=>"add", "message"=>$participant, "timestamp"=>$timestamp));
			
			
				$data = array(
					'id'=>$id,
					'chat'=>$chat_id,
					'sender'=>$creator,
					'sender_name'=>$sender_name,
					'sender_profile_image'=>$sender_profile_image,
					'action'=>"add",
					'message'=>$participant_name,
					'extras'=>"",
					'timestamp'=>$timestamp
				);
				
				sendPushNotification($participants_array, "102", "", $data);
			}
			
			
			return "success;".$chat_id;
		}
		
		return "fail";
			
			
	}
	
	public function getChats($user, $start, $amount) {
		$this->setLastSeen($user);
		
        $records = array();

        $query = 'SELECT * FROM chats WHERE creator="'.$user.'" OR participants LIKE "%!new_user!'.$user.'!new_user!%" ORDER BY id DESC';
		if ($amount > 0){
			$query.=' LIMIT '.$start.', '.$amount;
		}

        $result = $this->mysqli->query($query);
       
        if($result->num_rows <= 0)
            throw new Exception("There is no record in database!");
        
        while($row = $result->fetch_assoc()) {
	
			// IGNORING EMPTY CHATS
			$query2 = 'SELECT id FROM chats_messages WHERE chat = "'.$row['id'].'"';
			$result2 = $this->mysqli->query($query2);
			if($result2->num_rows == 0){
				continue;
			}
				
			$array = array();
			$array['id'] = $row['id'];		
			$array['creator'] = $row['creator'];
			$array['type'] = $row['type'];
			$array['participants'] = $row['participants'];
			
			// SINGLE CHAT
			if ($row['type'] == "single"){
				$author = explode("!new_user!", $row['participants'])[1];
				if ($user != $row['creator']){
					$author = $row['creator'];
				}
				
				$results = mysql_select("members", array("id"=>$author), array("username", "first_name", "last_name", "profile_image"));
				$author_name = $results[0]['first_name']." ".$results[0]['last_name'];
				if ($author_name==" "){
					$author_name = $results[0]['username'];
				}
				
				$array['name'] = $author_name;
				$array['image'] = $results[0]['profile_image'];
				
			} else if ($row['type'] == "group"){ // GROUP CHAT
				$array['name'] = $row['name'];
				$array['image'] = $row['image'];
			}
			
			$array['about'] = $row['about'];
			$array['timestamp'] = $row['timestamp'];
			
			
			
			// LAST TIMESTAMP
			$last_seens_array = array();
			$participants_array = array_filter(array_unique(explode("!new_user!", $row['participants'].'!new_user!'.$row['creator'])));
			if (($key = array_search($user, $participants_array)) !== false) {
				unset($participants_array[$key]);
			}
			foreach($participants_array as $participant){
				if (!empty($participant)){
					$results = mysql_select("members", array("id"=>$participant), array("last_seen"));
					if (sizeof($results)>0){
						array_push($last_seens_array, $results[0]["last_seen"]);
					}
				}
			}
			
			$array['last_timestamp'] = max($last_seens_array);

			
			$results = mysql_select("members", array("id"=>$user), array("last_seen"));
			$last_seen = $results[0]["last_seen"];
			
			$query2 = 'SELECT id FROM chats_messages WHERE chat = "'.$row['id'].'" AND sender != "'.$user.'" AND timestamp > '.$last_seen;
			$result2 = $this->mysqli->query($query2);
			if($result2->num_rows > 0){
				$array['new'] = $result2->num_rows;
			} else {
				$array['new'] = "0";
			}
			
			$query3 = 'SELECT id, sender, message, timestamp FROM chats_messages WHERE chat = "'.$row['id'].'" AND (action = "message" OR action = "mime-image") ORDER BY id DESC LIMIT 0, 1';
			$result3 = $this->mysqli->query($query3);
			   
			if($result3->num_rows > 0){
				while($row = $result3->fetch_assoc()) {
					$array['last_message'] = $row['message'];
					$array['last_message_id'] = $row['id'];
					$array['last_message_sender'] = $row['sender'];
					$array['last_message_timestamp'] = $row['timestamp'];
				}
			} else {
				$array['last_message'] = '';
				$array['last_message_id'] = '0';
				$array['last_message_sender'] = '';
				$array['last_message_timestamp'] = '';
			}
			
				
			array_push($records, $array);

		}

        $result->free();
		
        return $records;
    }
    
    
    
    public function getChatsMessages($chat, $start, $amount) {
		
        $records = array();

        $query = 'SELECT * FROM chats_messages WHERE chat="'.$chat.'" ORDER BY id DESC';
		if ($amount > 0){
			$query.=' LIMIT '.$start.', '.$amount;
		}
		

        $result = $this->mysqli->query($query);
       
        if($result->num_rows <= 0)
            throw new Exception("There is no record in database!");
        
        while($row = $result->fetch_assoc()) {
			
			$array = array();
			$array['id'] = $row['id'];		
			$array['chat'] = $row['chat'];
			$array['sender'] = $row['sender'];

				
			$results = mysql_select("members", array("id"=>$row['sender']), array("username", "first_name", "last_name", "profile_image"));
			$sender_name = $results[0]['first_name']." ".$results[0]['last_name'];
			if ($sender_name==" "){
				$sender_name = $results[0]['username'];
			}
			
			$array['sender_name'] = $sender_name;
			$array['sender_profile_image'] = $results[0]['profile_image'];
			$array['action'] = $row['action'];
			
			if ($row['action'] == "add" || $row['action'] == "remove"){
				$results = mysql_select("members", array("id"=>$row['message']), array("username", "first_name", "last_name"));
				$user_name = $results[0]['first_name']." ".$results[0]['last_name'];
				if ($user_name==" "){
					$user_name = $results[0]['username'];
				}
			
				$array['message'] = $user_name;
			} else {
				$array['message'] = $row['message'];
			}
			
			$extras = array();
			if (strstr($row['extras'], 'reply')){
				$extras['reply_id'] = str_replace("reply:", "", $row['extras']);
				$results = mysql_select("chats_messages", array("id"=>$extras['reply_id']), array("sender", "action", "message"));
				
				$results_sender = mysql_select("members", array("id"=>$results[0]["sender"]), array("username", "first_name", "last_name"));
				$sender_name = $results_sender[0]['first_name']." ".$results_sender[0]['last_name'];
				if ($sender_name==" "){
					$sender_name = $results_sender[0]['username'];
				}
				
				
				$extras['reply_sender'] = $sender_name;
				$extras['reply_action'] = $results[0]['action'];
				$message = "";
				if (sizeof(explode(' ', $results[0]['message']))>10){
					$message  = implode(' ', array_slice(explode(' ', $results[0]['message']), 0, 10))."...";
				} else {
					$message = $results[0]['message'];
				}
				
				$extras['reply_message'] = $message;
				
				$array['extras'] = json_encode($extras);
			} else if (strstr($row['extras'], 'forward')){
				$extras['forward_id'] = str_replace("forward:", "", $row['extras']);
				$results = mysql_select("chats_messages", array("id"=>$extras['forward_id']), array("sender", "action", "message"));
				
				$results_sender = mysql_select("members", array("id"=>$results[0]["sender"]), array("username", "first_name", "last_name"));
				$sender_name = $results_sender[0]['first_name']." ".$results_sender[0]['last_name'];
				if ($sender_name==" "){
					$sender_name = $results_sender[0]['username'];
				}
				
				
				$extras['forward_sender'] = $sender_name;
				$extras['forward_action'] = $results[0]['action'];
				$message = "";
				if (sizeof(explode(' ', $results[0]['message']))>10){
					$message  = implode(' ', array_slice(explode(' ', $results[0]['message']), 0, 10))."...";
				} else {
					$message = $results[0]['message'];
				}
				
				$extras['forward_message'] = $message;
				
				$array['extras'] = json_encode($extras);
			} else {
				$array['extras'] = $row['extras'];
			}
			$array['timestamp'] = $row['timestamp'];
				
			array_push($records, $array);

		}

        $result->free();
		
        return $records;
    }
	
    public function sendChatsMessage($chat, $sender, $action, $message, $extras, $timestamp){
		$this->setLastSeen($sender);
		
		$result = mysql_insert("chats_messages", array("chat"=>$chat, "sender"=>$sender, "action"=>$action, "message"=>$message, 'extras'=>$extras, 'timestamp'=>$timestamp));
		
		$results = mysql_select("chats", array("id"=>$chat), array("creator", "participants"));
		$creator = $results[0]["creator"];
		$participants = $results[0]["participants"];
		
		$participants_array = array_filter(array_unique(explode("!new_user!", $participants.$creator)));
		
		if (!in_array($sender, $participants_array)){
			return 0;
		}
		
		
		// CHECK IF BLOCKED
		for ($i=0; $i<sizeof($participants_array); $i++){
			$participant = $participants_array[$i];
			
			$follow = "0";
			$results = mysql_select("follow_requests", array("follower"=>$sender, "followed"=>$participant), array('status'));
			if (sizeof($results)>0){
				$follow = $results[0]['status'];
			}
			
			if ($follow == "blocked"){
				unset($participants_array[$i]);
			}
		}
		
		
			
		$results = mysql_select("members", array("id"=>$sender), array("username", "first_name", "last_name", "profile_image"));
		$sender_name = $results[0]['first_name']." ".$results[0]['last_name'];
		if ($sender_name==" "){
			$sender_name = $results[0]['username'];
		}
		
		$data = array(
			'id'=>$result,
			'chat'=>$chat,
			'sender'=>$sender,
			'sender_name'=>$sender_name,
			'sender_profile_image'=>$results[0]['profile_image'],
			'action'=>$action,
			'message'=>$message,
			'extras'=>$extras,
			'timestamp'=>$timestamp
		);
			
		sendPushNotification($participants_array, "102", "", $data);
		
		
		return $result;
    }
	
	public function updateChat($chat, $name, $image, $about, $uploaded){
		$results = mysql_select("chats", array("id"=>$chat), array("creator", "name", "image", "participants"));
		$creator = $results[0]["creator"];
		$former_name = $results[0]["name"];
		$former_image = $results[0]["image"];
		$participants = $results[0]["participants"];
		$participants_array = array_filter(array_unique(explode("!new_user!", $participants)));
		
		$results = mysql_select("members", array("id"=>$creator), array("username", "first_name", "last_name", "profile_image"));
		$sender_name = $results[0]['first_name']." ".$results[0]['last_name'];
		if ($sender_name==" "){
			$sender_name = $results[0]['username'];
		}
		$timestamp = time();
		
		// CHECK IF BLOCKED
		for ($i=0; $i<sizeof($participants_array); $i++){
			$participant = $participants_array[$i];
			
			$follow = "0";
			$results = mysql_select("follow_requests", array("follower"=>$creator, "followed"=>$participant), array('status'));
			if (sizeof($results)>0){
				$follow = $results[0]['status'];
			}
			
			if ($follow == "blocked"){
				unset($participants_array[$i]);
			}
		}
		
		
		
		if ($former_name != $name){
			
			$id = mysql_insert("chats_messages", array("chat"=>$chat, "sender"=>$creator, "action"=>"change_name", "message"=>$name, "timestamp"=>$timestamp)); 
		
			$data = array(
				'id'=>$id,
				'chat'=>$chat,
				'sender'=>$creator,
				'sender_name'=>$sender_name,
				'sender_profile_image'=>$results[0]['profile_image'],
				'action'=>"change_name",
				'message'=>$name,
				'extras'=>"",
				'timestamp'=>$timestamp
			);
			
			sendPushNotification($participants_array, "102", "", $data);
		}
		
		if ($uploaded == "true" || $former_image != $image){
		
			$id = mysql_insert("chats_messages", array("chat"=>$chat, "sender"=>$creator, "action"=>"change_image", "message"=>"", "timestamp"=>$timestamp)); 
		
			$data = array(
				'id'=>$id,
				'chat'=>$chat,
				'sender'=>$creator,
				'sender_name'=>$sender_name,
				'sender_profile_image'=>$results[0]['profile_image'],
				'action'=>"change_image",
				'message'=>"",
				'extras'=>"",
				'timestamp'=>$timestamp
			);
			
			sendPushNotification($participants_array, "102", "", $data);
		
		}
	
		$results = mysql_update("chats", array("id"=>$chat), array("name"=>$name, "image"=>$image, "about"=>$about, "timestamp"=>$timestamp));
		
		return "success";	
			
	}
	
	public function addToChat($chat, $user){
	
		$timestamp = time();


		// CHECK IF USER EXISTS

		$results = mysql_select("members", array("username"=>$user), array("id"));
		if (sizeof($results)==0){
			return "no-user";
		}
		

		$user = $results[0]["id"];
		
		$results = mysql_select("chats", array("id"=>$chat), array("creator", "participants"));
		$creator = $results[0]["creator"];
		$participants = $results[0]["participants"];
		$participants_array = array_filter(array_unique(explode("!new_user!", $participants.$creator)));
		
		
		// CHECK IF BLOCKED

			
		$follow = "0";
		$results = mysql_select("follow_requests", array("follower"=>$creator, "followed"=>$user), array('status'));
		if (sizeof($results)>0){
			$follow = $results[0]['status'];
		}
		
		if ($follow == "blocked"){
			return "no-user";
		}
		
		
		// CHECK IF ALREADY ADDED
		if (in_array($user, explode("!new_user!", $participants))){
			return "success";
		}
		
		$participants .= $user."!new_user!";
		
		mysql_update("chats", array("id"=>$chat), array("participants"=>$participants));
		
		// PUSH NOTIFICATION
		
		$participants_array = array_filter(array_unique(explode("!new_user!", $participants.$creator)));
		
		
		// CHECK IF BLOCKED
		for ($i=0; $i<sizeof($participants_array); $i++){
			$participant = $participants_array[$i];
			
			$follow = "0";
			$results = mysql_select("follow_requests", array("follower"=>$creator, "followed"=>$participant), array('status'));
			if (sizeof($results)>0){
				$follow = $results[0]['status'];
			}
			
			if ($follow == "blocked"){
				unset($participants_array[$i]);
			}
		}
		
		$results = mysql_select("members", array("id"=>$creator), array("username", "first_name", "last_name", "profile_image"));
		$sender_name = $results[0]['first_name']." ".$results[0]['last_name'];
		if ($sender_name==" "){
			$sender_name = $results[0]['username'];
		}
		$sender_profile_image = $results[0]['profile_image'];
		
		
		$results = mysql_select("members", array("id"=>$user), array("username", "first_name", "last_name"));
		$participant_name = $results[0]['first_name']." ".$results[0]['last_name'];
		if ($participant_name==" "){
			$participant_name = $results[0]['username'];
		}
	
	
		$id = mysql_insert("chats_messages", array("chat"=>$chat, "sender"=>$creator, "action"=>"add", "message"=>$user, "timestamp"=>$timestamp));
	
	
		$data = array(
			'id'=>$id,
			'chat'=>$chat,
			'sender'=>$creator,
			'sender_name'=>$sender_name,
			'sender_profile_image'=>$sender_profile_image,
			'action'=>"add",
			'message'=>$participant_name,
			'extras'=>"",
			'timestamp'=>$timestamp
		);
		
		sendPushNotification($participants_array, "102", "", $data);
			
		
		return "success";
			
			
	}
	
	public function removeFromChat($chat, $user){
	
		$timestamp = time();
	

		// CHECK IF USER EXISTS

		$results = mysql_select("members", array("username"=>$user), array("id"));
		if (sizeof($results)==0){
			return "no-user";
		}
		
		$user = $results[0]["id"];
		
		$results = mysql_select("chats", array("id"=>$chat), array("creator", "participants"));
		$creator = $results[0]["creator"];
		$participants = $results[0]["participants"];
		$participants_array = array_filter(array_unique(explode("!new_user!", $participants.$creator)));
		
		
		if (($key = array_search($user, $participants_array)) !== false) {
		    unset($participants_array[$key]);
		}
		
		
		$new_participants = "!new_user!";
		foreach(array_filter(array_unique(explode("!new_user!", $participants))) as $new_participant){
			if (!empty($new_participant) && $new_participant != $user){
				$new_participants .= $new_participant."!new_user!";	
			}
		}

		
		mysql_update("chats", array("id"=>$chat), array("participants"=>$new_participants));
		
		
		// PUSH NOTIFICATION
		
		// CHECK IF BLOCKED
		for ($i=0; $i<sizeof($participants_array); $i++){
			$participant = $participants_array[$i];
			
			$follow = "0";
			$results = mysql_select("follow_requests", array("follower"=>$creator, "followed"=>$participant), array('status'));
			if (sizeof($results)>0){
				$follow = $results[0]['status'];
			}
			
			if ($follow == "blocked"){
				unset($participants_array[$i]);
			}
		}
		
		$results = mysql_select("members", array("id"=>$creator), array("username", "first_name", "last_name", "profile_image"));
		$sender_name = $results[0]['first_name']." ".$results[0]['last_name'];
		if ($sender_name==" "){
			$sender_name = $results[0]['username'];
		}
		$sender_profile_image = $results[0]['profile_image'];
		
		
		$results = mysql_select("members", array("id"=>$user), array("username", "first_name", "last_name"));
		$participant_name = $results[0]['first_name']." ".$results[0]['last_name'];
		if ($participant_name==" "){
			$participant_name = $results[0]['username'];
		}
	
	
		$id = mysql_insert("chats_messages", array("chat"=>$chat, "sender"=>$creator, "action"=>"remove", "message"=>$user, "timestamp"=>$timestamp));
	
	
		$data = array(
			'id'=>$id,
			'chat'=>$chat,
			'sender'=>$creator,
			'sender_name'=>$sender_name,
			'sender_profile_image'=>$sender_profile_image,
			'action'=>"remove",
			'message'=>$participant_name,
			'extras'=>"",
			'timestamp'=>$timestamp
		);
		
		sendPushNotification($participants_array, "102", "", $data);
		
		return "success";	
	}
	
	public function leaveChat($chat, $user){
		$this->setLastSeen($user);
		
		$timestamp = time();
	

		// CHECK IF USER EXISTS

		$results = mysql_select("members", array("id"=>$user), array("id"));
		if (sizeof($results)==0){
			return "no-user";
		}
		
		$user = $results[0]["id"];
		
		$results = mysql_select("chats", array("id"=>$chat), array("type", "creator", "participants"));
		$type = $results[0]["type"];
		
		if ($type!="group"){
			return "success";
		}
		
		
		$creator = $results[0]["creator"];
		$participants = $results[0]["participants"];
		$participants_array = array_filter(array_unique(explode("!new_user!", $participants.$creator)));
		
		
		if (($key = array_search($user, $participants_array)) !== false) {
		    unset($participants_array[$key]);
		}
		
		
		$new_participants = "";
		foreach(array_filter(array_unique(explode("!new_user!", $participants))) as $new_participant){
			if (!empty($new_participant) && $new_participant != $user){
				$new_participants .= $new_participant."!new_user!";	
			}
		}
		
		mysql_update("chats", array("id"=>$chat), array("participants"=>$new_participants));
		
		
		// PUSH NOTIFICATION
		
		// CHECK IF BLOCKED
		for ($i=0; $i<sizeof($participants_array); $i++){
			$participant = $participants_array[$i];
			
			$follow = "0";
			$results = mysql_select("follow_requests", array("follower"=>$user, "followed"=>$participant), array('status'));
			if (sizeof($results)>0){
				$follow = $results[0]['status'];
			}
			
			if ($follow == "blocked"){
				unset($participants_array[$i]);
			}
		}
		
		$results = mysql_select("members", array("id"=>$user), array("username", "first_name", "last_name", "profile_image"));
		$sender_name = $results[0]['first_name']." ".$results[0]['last_name'];
		if ($sender_name==" "){
			$sender_name = $results[0]['username'];
		}
		$sender_profile_image = $results[0]['profile_image'];
	
	
		$id = mysql_insert("chats_messages", array("chat"=>$chat, "sender"=>$user, "action"=>"leave_group", "message"=>"", "timestamp"=>$timestamp));
	
	
		$data = array(
			'id'=>$id,
			'chat'=>$chat,
			'sender'=>$user,
			'sender_name'=>$sender_name,
			'sender_profile_image'=>$sender_profile_image,
			'action'=>"leave_group",
			'message'=>"",
			'extras'=>"",
			'timestamp'=>$timestamp
		);
		
		sendPushNotification($participants_array, "102", "", $data);
		
		if ($creator == $user){
			mysql_delete("chats", array("id"=>$chat));
		}
		
		return "success";		
	}
	
}
?>