<?php
require_once dirname(__FILE__).'/inc/dbhandler.php';
$mDBHandler = new DBHandler();

$user = $_GET['user'];
$amount = $_GET['amount'];
$timestamp = 0;
if (isset($_GET['timestamp'])){
	$timestamp = $_GET['timestamp'];
}

$response = array();

try {
	$mDBHandler = new DBHandler();
	$records = $mDBHandler->getHistory($user, $amount, $timestamp);
	
	if (count($records)==0){
		$response['history'] = '-1';
	} else {
	
		for($i=0; $i<count($records); $i++) {
			$record = $records[$i];
			$posts[] = array(			
		
			'id'=> $record['id'], 
			'author'=> $record['author'],
			'author_profile_image'=> $record['author_profile_image'],
			'author_name'=> $record['author_name'],
			'author_timestamp'=> $record['author_timestamp'],
			'action'=> $record['action'],
			'timestamp'=> $record['timestamp'],
			
	
			);
		
			$response['history'] = $posts;
		}
	}	
	
} catch (Exception $e) {
	$response['history'] = '-1'; // When there is no record in database
	//die("There was a problem: " . $e->getMessage());
}

if ($amount == 0){
	$response['history'] = '-1';
}

echo json_encode($response);
			
?>