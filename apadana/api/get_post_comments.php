<?php
require_once dirname(__FILE__).'/inc/dbhandler.php';
$mDBHandler = new DBHandler();

$id = $_GET['id'];
$user = "";
if (isset($_GET['user'])){
	$user = $_GET['user'];
}
$start = $_GET['start'];
$amount = $_GET['amount'];

$response = array();

try {
	$mDBHandler = new DBHandler();
	$records = $mDBHandler->getPostComments($id, $user, $start, $amount);
	
	for($i=0; $i<count($records); $i++) {
		$record = $records[$i];
		$posts[] = array(			
	
		'id'=> $record['id'], 
		'author'=> $record['author'],
		'author_name'=> $record['author_name'],
		'author_profile_image'=> $record['author_profile_image'],
		'comment'=> $record['comment'],
		'timestamp'=> $record['timestamp'],
		'author_timestamp'=> $record['author_timestamp'],

		);
	
	$response['post_comments'] = $posts;
}
	
	
	
} catch (Exception $e) {
	$response['post_comments'] = '-1'; // When there is no record in database
	//die("There was a problem: " . $e->getMessage());
}

echo json_encode($response);
			
?>