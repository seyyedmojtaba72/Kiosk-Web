<?php

require_once(dirname(__FILE__)."/../inc/class/imageCropSquare.class.php");
//require_once 'config.php';
//require_once 'db_connect.php';
//require_once 'information.php';

$directory = $_GET['directory'];
if (!isset($directory)){ $directory = "uploads"; }

if (!file_exists(dirname(__FILE__)."/../files/".$directory."/")) {
    mkdir(dirname(__FILE__)."/../files/".$directory."/", 0755);
}

$replace = $_GET['replace'];

//$replace = $_POST['replace'];

//$file_types = $_POST['FILE_TYPES'];
//$max_file_size = $_POST['MAX_FILE_SIZE'];

$tmp = $_FILES["uploaded_file"]["tmp_name"];
$full_name = $_FILES["uploaded_file"]["name"];
$name = substr($full_name,0,strpos($full_name,"."));
$tmpp = explode(".", $full_name);
$ext = end($tmpp);

/*
if ($directory=="profile_images"){
	$idd = $_POST['idd'];
	$model = $_POST['model'];
	$value = $_POST['value'];

	$name = $value;
}
*/

$size = $_FILES["uploaded_file"]["size"];
$type = $_FILES["uploaded_file"]["type"];
$error = $_FILES["uploaded_file"]["error"];

$path = dirname(__FILE__)."/../files/".$directory."/".$name.".".$ext;



if ($replace=="FALSE"){
	$i=0;
	while(file_exists($path)){
		$i++;
		$path = dirname(__FILE__)."/../files/".$directory."/".$name."_".$i.".".$ext;
	}
} else {
	if (file_exists($path)){
		unlink($path);
	}
}

/*

if (empty($_FILES["uploaded_file"]["name"])){
	header('Location: ../'.$redirect.'&err=empty');
	exit;
}

if (isset($max_file_size) && ($size > $max_file_size)) { header('Location: ../'.$redirect.'&err=size'); exit;}

if (isset($file_types) && (strpos($file_types,$type)===false)){ header('Location: ../'.$redirect.'&err=type'); exit;}


if (move_uploaded_file($tmp,$path)){
	
	if ($directory=="profile_images"){
		$crop = new Crop_Image_To_Square;
		$crop->source_image = utf8_encode($path);
		
		$crop->save_to_folder = '../files/profile_images/';
		
		$process = $crop->crop('center');
		
		
		$insert_stmt = $mysqli->prepare("UPDATE ".$model."s SET profile_image=? WHERE ".$idd."=?");
		$profile_image = utf8_encode($name).".".$ext;
		$insert_stmt->bind_param('ss', $profile_image, $value);
		$insert_stmt->execute();
		
		header('Location: ../'.$redirect.'&suc=upload&profile_image='.$profile_image);
		exit;
	} else {
		header('Location: ../'.$redirect.'&suc=upload&path='.str_replace('..',$options["url"],$path));
		exit;
	}
 }

*/

if(move_uploaded_file($_FILES['uploaded_file']['tmp_name'], $path)) {
	// FIX PERMISSIONS
	chmod($path, 0755);
	
	
	
	// CONVERT TO JPG IMAGE
	$temp = explode('.', $path);
	$ext  = array_pop($temp);
	$new_path = implode('.', $temp).".jpg";

	$imageTmp = "";
	if (preg_match('/jpg|jpeg/i',$ext))
		$imageTmp=imagecreatefromjpeg($path);
	else if (preg_match('/png/i',$ext))
		$imageTmp=imagecreatefrompng($path);
	else if (preg_match('/gif/i',$ext))
		$imageTmp=imagecreatefromgif($path);
	else if (preg_match('/bmp/i',$ext))
		$imageTmp=imagecreatefrombmp($path);
		
	if ($imageTmp == ""){
		return;
	}
	
	list( $width,$height ) = getimagesize( $path );
	unlink($path);
	
	$tmp = imagecreatetruecolor($width, $height);
	imagecopyresampled($tmp, $imageTmp, 0, 0, 0, 0,
					   $width, $height, $width, $height);
					   
	imagejpeg($tmp, $new_path, 75);
	imagedestroy($imageTmp);
	imagedestroy($tmp);
	
	$path = $new_path;
	
	
	
	// CREATE THUMBNAIL IF IS A JPG IMAGE
	
	$max = 300;
	$quality = 75;
	
	$a = getimagesize($path);
	$image_type = $a[2];
	
	if(in_array($image_type , array(IMAGETYPE_JPEG))){
	
		$thumb_path = $path . "_small.jpg";

		list( $width,$height ) = getimagesize( $path );
		$ratio = $width/$height;
		
		if ($width >= $height){
			$new_width = $max;
			$new_height = $new_width / $ratio;
		} else {
			$new_height = $max;
			$new_width = $new_height * $ratio;
		}

		$thumb_img = imagecreatetruecolor( $new_width, $new_height );
		$source_img = imagecreatefromjpeg( $path );
		
		imagecopyresampled($thumb_img, $source_img, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
		imagejpeg( $thumb_img, $thumb_path, $quality );  // QUALITY	 
	}
	
	
	if ($directory=="profile_images"){
		// CROP
		
		$crop = new Crop_Image_To_Square;
		$crop->source_image = utf8_encode($path);
		$crop->save_to_folder = '../files/profile_images/';
		$process = $crop->crop('center');
	}
	
	echo basename($path);
	
} else{
	echo "fail";
}
?>