<?php

require_once dirname(__FILE__).'/inc/dbhandler.php';

$response = array();

$user = "";
if (isset($_GET['user'])){
	$user = $_GET['user'];
}
$device_id = $_GET['device_id'];
$version = $_GET['version'];
$platform = $_GET['platform'];

$response['timestamp'] = "success;".substr(time(), 0, 10);

try {
	$mDBHandler = new DBHandler();
	
	$mDBHandler->getOnline($user, $device_id, $version, $platform);
	
	$posts = array();
	
	foreach ($parameters as $variable => $value) {

		$posts[] = array(
		
			'variable'=> $variable, 
			'value'=> $value,
			);
		
		$response['preferences'] = $posts;
	}
	
	
	$posts = array();
	
	$records = $mDBHandler->getVersioningOptions();
	
	for($i=0; $i<count($records); $i++) {
		$record = $records[$i];
		$posts[] = array(
			'variable'=> $record['variable'], 
			'value'=> $record['value']);
		
		$response['versioning_options'] = $posts;
	}
	
	
} catch (Exception $e) {
	echo '-1'; // When there is no record in database
	//die("There was a problem: " . $e->getMessage());
}

echo json_encode($response);

?>