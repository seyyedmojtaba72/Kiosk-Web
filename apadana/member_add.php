<?php 
require_once('header.php'); 
?>

<?php if ($role != "admin"){header("Location: login.php?redirect=".urlencode("http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']));} ?>

<?php
if (!empty($err)) {
	switch ($err) {
	
	case "invalid-username" : $err_msg = "نام کاربری باید شامل حروف انگلیسی و اعداد باشد!"; break;
	case "mobile_number-length" : $err_msg = "طول شماره موبایل باید 10 حرف باشد!"; break;
	case "fill" : $err_msg = "موارد الزامی را پر کنید!"; break;
	case "insert" : $err_msg = "خطا در نوشتن دیتابیس!"; break;
	case "database" : $err_msg = "خطا در دیتابیس!"; break;
	case "email-exists" : $err_msg = "کاربری با این ایمیل وجود دارد!"; break;
	case "invalid-email" : $err_msg = "ایمیل وارد شده معتبر نیست!"; break;
	case "invalid-password" : $err_msg = "گذرواژه نامعتبر است!"; break;
	case "username-exists" : $err_msg = "کاربری با این نام کاربری وجود دارد!"; break;
	case "mobile_number-exists" : $err_msg = "کاربری با این شماره موبایل وجود دارد!"; break;
	case "min-length" : $err_msg = "طول نام کاربری و گذواژه باید حداقل 6 کاراکتر باشد!"; break;
	case "max-username-length" : $err_msg = "طول نام کاربری باید حداکثر 50 کاراکتر باشد!"; break;
	case "max-email-length" : $err_msg = "طول آدرس ایمیل باید حداکثر 50 کاراکتر باشد!"; break;
	
	default : $err_msg = "خطای غیر منتظره‌ای پیش آمده!"; break;
	}
	
}
?> 
<?php
if (!empty($suc)) {
	switch ($suc) {

	case "insert" : $suc_msg = "کاربر اضافه شد!"; break;

	default : $suc_msg = "عملیات با موفقیت انجام شد!"; break;
	}
	
}
?> 
<?php
if (!empty($err_msg)) {
	echo '<div class="alert alert-error no-print"><a href="'.str_replace('err='.$err,"",implode('&',array_unique(explode('&',$_SERVER['REQUEST_URI'])))).'"><button type="button" class="close">&times;</button></a><p>'. $err_msg .'</p></div>';
}
?>
<?php
if (!empty($suc_msg)) {
	echo '<div class="alert alert-success no-print"><a href="'.str_replace('suc='.$suc,"",implode('&',array_unique(explode('&',$_SERVER['REQUEST_URI'])))).'"><button type="button" class="close">&times;</button></a><p>'. $suc_msg .'</p></div>';
}
?>
<?php
if (!isset($_SESSION['members_redirect'])){$_SESSION['members_redirect'] = "members.php";}
?>
<div class="container">
	<div class="pull-left">
		<a href="<?php echo $_SESSION['members_redirect'] ?>"><button class='btn'><span>بازگشت</span> <i class="icon-chevron-left"></i></button></a>
	</div>
	<button class="btn disabled"><span id="subtitle">اضافه‌کردن عضو</span></button><br /><br />
	<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert">&times;</button>
	<p>مواردی که با علامت ستاره‌ی قرمز مشخص شده‌ است، الزامی است.</p></div>
	<div id="right" class="span4 pull-right text-right">
		<form action="<?php echo $options["url"] ?>/inc/add_member.php" method="post">
        	<input type="hidden" name="redirect" id="redirect" value="member_add.php?" />
			<table class="table table-striped table-right">
			<tr>
				<td class="span2"><h5 class="normal">نام کاربری <span class="red">*</span></h5></td>
					<td style="padding: 5px 0 0 0;"><input type="text" maxlength="30" style="font: normal 11px tahoma; width:100px;" value="" name="username" id="username"/></td>
				</tr>
				<tr>
					<td><h5 class="normal">ایمیل <span class="red">*</span></h5></td>
					<td style="padding: 5px 0 0 0;"><input type="email" style="font: normal 11px tahoma; width:100px;" value="" name="email" id="email"/></td>
				</tr>
                <script type="text/javascript" src="<?php echo $options['url'] ?>/js/pwstrength.js"></script>
				<script type="text/javascript">
				jQuery(document).ready(function () {
					"use strict";
					var options = {};
					options.common = {
						onLoad: function () {
							$('#messages').text('Start typing password');
						}
					};
					options.ui = {
						showPopover: true,
						bootstrap2: true,
						showErrors: false
					};
					$('#password').pwstrength(options);
				});
				</script> 
				<tr>
					<td><h5 class="normal">گذرواژه <span class="red">*</span></h5></td>
					<td style="padding: 5px 0 0 0;"><input type="password" name="password" id="password" style="font: normal 11px tahoma; width:100px;" placeholder="گذرواژه" /><br /></td>
				</tr>
				<tr>
					<td><h5 class="normal">تأیید گذرواژه <span class="red">*</span></h5></h5></td>
					<td style="padding: 5px 0 0 0;"><input type="password" name="confirm_password" id="confirm_password" style="font: normal 11px tahoma; width:100px;" placeholder="تأیید گذرواژه" /></td>
				</tr>
                <tr>
					<td><h5 class="normal">نقش</h5></td>
					<td style="padding: 5px 0 0 0;">
						<select class="tahoma size-11" style="width: 115px;" name="role" id="role">
							<?php
							foreach ($member_roles as $role1=>$role1_value){
								echo '<option ';
								//if ($role1==$role){ echo 'selected="selected" ';}
								echo 'value="'.$role1.'">'.$role1_value.'</option>';
							}
							?>
							<?php
		
							$stmt = $mysqli->prepare("select count(1) FROM members_roles");
											
							$stmt->execute();
							$stmt->bind_result($rows);
							$stmt->fetch();
							$stmt->close();
		
							for($i=0;$i<$rows;$i++){
								
								$stmt = $mysqli->prepare('SELECT slug, role_title FROM members_roles LIMIT 1 OFFSET '.$i);
								$stmt->execute();	
								$stmt->bind_result($slug, $role_title);
								$stmt->fetch();
								$stmt->close();
								
								echo '<option ';
								if ($role==$slug){echo 'selected="selected" '; }
								echo 'value="'.$slug.'">'.$role_title.'</option>';
			
							}
							?>
						</select>
					</td>
				</tr>
                <tr>
                    <td><h5 class="normal">سطح</h5></td>
                    <td style="padding-top: 5px;">
                        <select class="tahoma size-11" style="width: 100px;" name="level" id="level">
                            <?php
                            foreach ($member_levels as $level1=>$level1_value){
                                echo '<option ';
                                //if ($level1==$level){ echo 'selected="selected" ';}
                                echo 'value="'.$level1.'">'.$level1_value.'</option>';
                            }
                            ?>
                        </select>
                    </td> 
                </tr>
                <tr>
					<td><h5 class="normal">وضعیت</h5></td>
					<td style="padding: 5px 0 0 0;">
						<select class="tahoma size-11" style="width: 115px;" name="status" id="status">
							<?php
							foreach ($member_statuses as $status1=>$status1_value){
								echo '<option ';
								//if ($status1==$status){ echo 'selected="selected" ';}
								echo 'value="'.$status1.'">'.$status1_value.'</option>';
							}
							?>
						</select>
					</td> 
				</tr>
				<tr>
					<td><h5 class="normal">نام</h5></td>
					<td style="padding: 5px 0 0 0;"><input type="text" maxlength="100" style="font: normal 11px tahoma; width:100px;" value="" name="first_name" id="first_name"/></td>
				</tr>
				<tr>
					<td><h5 class="normal">نام خانوادگی</h5></td>
					<td style="padding: 5px 0 0 0;"><input type="text" maxlength="100" style="font: normal 11px tahoma; width:100px;" value="" name="last_name" id="last_name"/></td>
				</tr>
				<tr>
					<td><h5 class="normal">شماره موبایل</h5></td>
					<td style="padding: 5px 0 0 0;"><input type="number" placeholder="" maxlength="10" value="" style="font: normal 11px tahoma; width:100px;" id="mobile_number" name="mobile_number"/> 98+</td>
				</tr>
                <tr>
					<td><h5 class="normal">وضعیت شماره موبایل</h5></td>
					<td style="padding: 5px 0 0 0;">
						<select class="tahoma size-11" style="width: 115px;" name="mobile_number_status" id="mobile_number_status">
							<?php
							foreach ($mobile_number_statuses as $status1=>$status1_value){
								echo '<option ';
								//if ($status1==$status){ echo 'selected="selected" ';}
								echo 'value="'.$status1.'">'.$status1_value.'</option>';
							}
							?>
						</select>
					</td> 
				</tr>
                <tr>
					<td><h5 class="normal">شماره منزل</h5></td>
					<td style="padding: 5px 0 0 0;"><input type="number" placeholder="" maxlength="10" value="" style="font: normal 11px tahoma; width:100px;" id="home_number" name="home_number"/> 98+</td>
				</tr>
                <tr>
					<td><h5 class="normal">شماره فکس</h5></td>
					<td style="padding: 5px 0 0 0;"><input type="number" placeholder="" maxlength="10" value="" style="font: normal 11px tahoma; width:100px;" id="fax_number" name="fax_number"/> 98+</td>
				</tr>
                <tr>
					<td><h5 class="normal">شرکت</h5></td>
					<td style="padding: 5px 0 0 0;"><input type="text" placeholder="شرکت" maxlength="100" value="" class="normal text-right span2" id="office" name="office" style="font: normal 12px 'WYekan',B Yekan; width:100px;" /></td>
				</tr>
                <tr>
                    <td><h5 class="normal">شماره شبا</h5></td>
                    <td style="padding: 5px 0 0 0;"><input type="text" placeholder="شماره شبا" maxlength="100" value="<?php echo $shaba ?>" class="normal text-right span2" id="shaba" name="shaba" style="font: normal 12px 'WYekan',B Yekan; width:100px;" /></td>
                </tr>
                <tr>
					<td><h5 class="normal">آدرس</h5></td>
					<td style="padding: 5px 0 0 0;"><textarea maxlength="500" class="normal text-right span2" id="address" name="address" style="font: normal 12px 'WYekan',B Yekan; width:150px;"></textarea></td>
				</tr>
                <tr>
					<td><h5 class="normal">معرف</h5></td>
					<td style="padding: 5px 0 0 0;">
                    	<select class="chosen-select chosen-rtl" name="presenter" id="presenter" data-placeholder="کاربری را انتخاب کنید.">
                            <option value="" selected="selected"></option>
                            <?php
                            
                            $result = $mysqli->query("SELECT * FROM members");
                            $rows = $result->num_rows;
                            
                            for ($i=0;$i<$rows;$i++){
                                $stmt = $mysqli->prepare("SELECT id, role, username, first_name, last_name FROM members LIMIT 1 OFFSET ?");
                                $stmt->bind_param('s', $i);
                                $stmt->execute();
                                $stmt->store_result();
                         
                                $stmt->bind_result($presenter_id, $presenter_role, $presenter_username, $presenter_first_name, $presenter_last_name);
                                $stmt->fetch();
                                $stmt->close();
                                
                                $presenter_display_name = $presenter_first_name." ".$presenter_last_name;
								if ($presenter_display_name == " "){
									$presenter_display_name = $presenter_username;
								}
								echo '<option value="'.$presenter_id.'" ';
								//if ($presenter == $presenter_id){echo 'selected="selected"';}
								echo '>';
								echo $presenter_display_name.'</option>';
                            }
                            ?>
                        </select>
                        <script>
                        $("#presenter").chosen({
                            width: "165px !important",
                            allow_single_deselect: true,
                            no_results_text : "کاربری یافت نشد.",
                        });
                        </script>
                    </td>
				</tr>
                <tr>
					<td><h5 class="normal">موجودی</h5></td>
					<td style="padding: 5px 0 0 0;"><input type="number" placeholder="" value="" style="font: normal 11px tahoma; width:100px;" id="balance" name="balance"/> ریال</td>
				</tr>
                <tr>
					<td><h5 class="normal">درصد جایزه</h5></td>
					<td style="padding: 5px 0 0 0;"><input type="number" placeholder="" maxlength="3" value="<?php echo $options['reward_percentage']; ?>" style="font: normal 11px tahoma; width:50px;" id="reward_percentage" name="reward_percentage"/></td>
				</tr>
                <tr>
					<td><h5 class="normal">درباره</h5></td>
					<td style="padding: 5px 0 0 0;"><textarea maxlength="500" class="normal text-right span2" id="about" name="about" style="font: normal 12px 'WYekan',B Yekan; width:150px;"></textarea></td>
				</tr>
                <tr>
                	<td><h5 class="normal">تنظیمات اضافی</h5></td>
                    <td style="padding: 5px 0 0 0;">
                        <?php
                        foreach ($member_extras as $extra=>$extra_text){
                            echo '<h6 class="normal" id="'.$extra.'"><input id="'.$extra.'-check" name="extras[]" type="checkbox" value="'.$extra.'" ';
                            //if (in_array($extra, explode(";", $extras))){ echo 'checked="checked"'; }
                            echo '>&ensp;'.$extra_text.'</option></h6>';
                        }
                        ?>
                    </td>
                </tr>
			</table>
			<button class='btn btn-info pull-left' onclick="return regformhash(this.form,this.form.username,this.form.email,this.form.password,this.form.confirm_password);" type="submit"><span>اضافه کردن</span> <i class="icon-plus icon-white"></i></button>
		</form>
	</div>
	<div style="clear:both"></div>
</div>
<?php require_once('footer.php'); ?>