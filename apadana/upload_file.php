<?php 		
require_once('header.php');
?>

<?php if (if_has_permission($role,"upload_file")){} else{header("Location: login.php?redirect=".urlencode("http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']));} ?>

<?php
if (!empty($err)) {
	switch ($err) {
	
	case "invalid" : $err_msg = "فرمت فایل قابل پشتیبانی نیست!"; break;
	case "fill" : $err_msg = "موارد الزامی را پر کنید!"; break;
	case "empty" : $err_msg = "فایلی را انتخاب کنید!"; break;
	case "size" : $err_msg = "حجم فایل رعایت نشده است.!"; break;
	case "type" : $err_msg = "پسوند فایل رعایت نشده است!"; break;
	case "upload" : $err_msg = "خطا در آپلود فایل. مجوزها را بررسی کنید!"; break;
	case "database" : $err_msg = "خطای پایگاه داده!"; break;
	case "insert" : $err_msg = "خطا در ویرایش پایگاه داده!"; break;
	
	default : $err_msg = "خطای غیر منتظره‌ای پیش آمده!"; break;
	}
	
}
?> 
<?php
if (!empty($suc)) {
	switch ($suc) {
		
	case "upload" : $path = filter_input(INPUT_GET, 'path', $filter = FILTER_SANITIZE_STRING); $suc_msg = "فایل آپلود شد! مسیر: ".'<br>'.$path; break;
	
	default : $suc_msg = "عملیات با موفقیت انجام شد!"; break;
	}
	
}
?> 
<?php
if (!empty($err_msg)) {
	echo '<div class="alert alert-error no-print"><a href="'.str_replace('err='.$err,"",implode('&',array_unique(explode('&',$_SERVER['REQUEST_URI'])))).'"><button type="button" class="close">&times;</button></a><p>'. $err_msg .'</p></div>';
}
?>
<?php
if (!empty($suc_msg)) {
	echo '<div class="alert alert-success no-print"><a href="'.str_replace('suc='.$suc,"",implode('&',array_unique(explode('&',$_SERVER['REQUEST_URI'])))).'"><button type="button" class="close">&times;</button></a><p>'. $suc_msg .'</p></div>';
}
?>

<div class="container" style="width: 350px;">
	<div class="pull-left">
		<a href="upload_dropbox.php"><button class='btn btn-primary'><span>نسخه جدید</span> <i class="icon-upload icon-white"></i></button></a>
		<a href="panel.php"><button class='btn'><span>بازگشت</span> <i class="icon-chevron-left"></i></button></a>
	</div>
	<div id="main-content">
	<button class="btn disabled"><span id="subtitle">آپلود فایل</span></button><br /><br />
		<div class="alert text-right">
			<ul>
				<li>حجم فایل باید کمتر از <?php echo $options['upload_max_file_size'] ?> کیلوبایت باشد.</li>
			</ul>
		</div>
		<form action="<?php echo $options["url"] ?>/inc/upload_file.php" method="post" enctype="multipart/form-data">
			<input type="hidden" value="upload_file.php?" name="redirect" />
            <input type="hidden" value="uploads" name="directory" />
			<input type="hidden" name="file_types" value="" />
			<input type="hidden" value="FALSE" name="replace" />
			<input type="hidden" name="upload_max_file_size" value="<?php echo ($options['upload_max_file_size']*1024) ?>" />						
			<input type="file" name="file" class="Tahoma pull-right"><br /><br />
			<button type="submit" name="submit" class="pull-left btn btn-primary" onclick="$('#loading_small').css('visibility','visible');"><span>آپلود</span> <i class="icon-upload icon-white"></i></button>
			<img id="loading_small" style="float: left; visibility: hidden; margin-top: 5px; margin-left: 5px;" src="<?php echo $options["url"]?>/img/loading_small.gif" border=0/>
		</form>
	</div>		
</div>
<?php require_once('footer.php'); ?>