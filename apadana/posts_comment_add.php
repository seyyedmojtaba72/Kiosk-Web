<?php 
require_once('header.php'); 
?>

<?php if ($role == "admin" | if_has_permission($role,"can_edit_posts")){} else{header("Location: login.php");} ?>


<?php
if (!empty($err)) {
	switch ($err) {
	case "insert" : $err_msg = "خطا در نوشتن دیتابیس!"; break;
	case "fill" : $err_msg = "موارد الزامی را پر کنید!"; break;
	case "exists" : $err_msg = "نظر مطالب وجود دارد!"; break;
	
	default : $err_msg = "خطای غیر منتظره‌ای پیش آمده!"; break;
	}
	
}
?> 
<?php
if (!empty($suc)) {
	switch ($suc) {
	case "insert" : $suc_msg = "نظر مطالب اضافه شد!"; break;
	
	default : $suc_msg = "عملیات با موفقیت انجام شد!"; break;
	}
	
}
?> 
<?php
if (!empty($err_msg)) {
	echo '<div class="alert alert-error no-print"><a href="'.str_replace('err='.$err,"",implode('&',array_unique(explode('&',$_SERVER['REQUEST_URI'])))).'"><button type="button" class="close">&times;</button></a><p>'. $err_msg .'</p></div>';
}
?>
<?php
if (!empty($suc_msg)) {
	echo '<div class="alert alert-success no-print"><a href="'.str_replace('suc='.$suc,"",implode('&',array_unique(explode('&',$_SERVER['REQUEST_URI'])))).'"><button type="button" class="close">&times;</button></a><p>'. $suc_msg .'</p></div>';
}
?>
<?php if (empty($_SESSION['posts_comments_redirect'])){$_SESSION['posts_comments_redirect']="posts_comments.php";} ?>
<div class="container">
	<a href="<?php echo $_SESSION['posts_comments_redirect'] ?>"><button class='pull-left btn'><span>بازگشت</span> <i class="icon-chevron-left"></i></button></a>
	<h4 id="subtitle" class="page-title normal">اضافه‌کردن نظر مطالب</h4><br />
	<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert">&times;</button>
	<p>مواردی که با علامت ستاره‌ی قرمز مشخص شده‌ است، الزامی است.</p></div>
	<div id="right" class="span6 pull-right">
		<form action="<?php echo $options["url"] ?>/inc/add_posts_comment.php" method="post">
			<table class="table table-striped table-right">
				<tr>
					<td><h5 class="normal">کاربر <span class="red">*</span></h5></td>
					<td style="padding-top: 5px;">
						<select class="tahoma size-11" style="width:150px;" name="author" id="author">	
							<?php
							//echo '<option value="" selected="selected"></option>';	
							
							$result = $mysqli->query("SELECT id FROM members");
							$rows = $result->num_rows;
							
							for ($i=0;$i<$rows;$i++){
								$stmt = $mysqli->prepare("SELECT id, username, first_name, last_name FROM members LIMIT 1 OFFSET ?");
								$stmt->bind_param('s', $i);
								$stmt->execute();
								$stmt->store_result();
						 
								$stmt->bind_result($member_id, $member_username, $member_first_name, $member_last_name);
								$stmt->fetch();
								$stmt->close();
								
								$member_name = $member_first_name." ".$member_last_name;
								if ($member_name == " "){
									$member_name = $member_username;
								}
							
								echo '<option value="'.$member_id.'" ';
								//if ($auhor==$member_id){echo 'selected="selected"';}
								echo '>'.$member_name.'</option>';
							}
							?>
						</select>
					</td>
				</tr>
				<tr>
					<td><h5 class="normal">مطلب <span class="red">*</span></h5></td>
					<td style="padding-top: 5px;">
						<select class="tahoma size-11" style="width:200px;" name="post" id="post">
						<?php
							
						$result = $mysqli->query("SELECT * FROM posts");
						$rows = $result->num_rows;
						
						for ($i=0;$i<$rows;$i++){
							$stmt = $mysqli->prepare("SELECT id, name FROM posts LIMIT 1 OFFSET ?");
							$stmt->bind_param('s', $i);
							$stmt->execute();
							$stmt->store_result();
					 
							$stmt->bind_result($id, $post_name);
							$stmt->fetch();
							$stmt->close();
							
						
							echo '<option value="'.$id.'" ';
							//if ($post==$id){echo 'selected="selected"';}
							echo '>'.$post_name.'</option>';
						}
						?>
					</select>
					</td>
				</tr>
				<tr>
					<td><h5 class="normal">نظر <span class="red">*</span></h5></td>
					<td style="padding-top: 5px;"><textarea style="font: normal 11px tahoma; width:200px;" name="comment" id="comment" maxlength="1000"></textarea></td>
				</tr>
				<tr>
					<td><h5 class="normal">تاریخ <span class="red">*</span></h5></td>
					<td style="padding-top: 5px;"><input type="text" style="font: normal 11px tahoma; width:100px;" value="<?php echo $date ?>" name="date" id="date" maxlength="20" /></td>
				</tr>
				<tr>
					<td><h5 class="normal">زمان <span class="red">*</span></h5></td>
					<td style="padding-top: 5px;"><input type="text" style="font: normal 11px tahoma; width:100px;" value="<?php echo date("H:i:s");?>" name="time" id="time" maxlength="20" /></td>
				</tr>
				<tr>
					<td><h5 class="normal">وضعیت <span class="red">*</span></h5></td>
					<td style="padding-top: 5px;">
						<select class="tahoma size-11" style="width: 100px;" name="status" id="status">
							<?php
							foreach ($comment_statuses as $status1=>$status1_value){
								echo '<option ';
								//if ($status1==$status){ echo 'selected="selected" ';}
								echo 'value="'.$status1.'">'.$status1_value.'</option>';
							}
							?>
						</select>
					</td> 
				</tr>
			</table>
			<button class='btn btn-info pull-left' type="submit"><span>اضافه کردن</span> <i class="icon-plus icon-white"></i></button>
		</form>
	</div>
</div>
<?php require_once('footer.php'); ?>