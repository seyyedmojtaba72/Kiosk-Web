<?php 
include('header.php'); 
?>

<?php
if (!empty($err)) {
	switch ($err) {
	
	default : $err_msg = "خطای غیر منتظره‌ای پیش آمده!"; break;
	}
	
}
?> 
<?php
if (!empty($suc)) {
	switch ($suc) {
	
	default : $suc_msg = "عملیات با موفقیت انجام شد!"; break;
	}
	
}
?> 
<?php
if (!empty($err_msg)) {
	echo '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">&times;</button><p>'. $err_msg .'</p></div>';
}
?>
<?php
if (!empty($suc_msg)) {
	echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button><p>'. $suc_msg .'</p></div>';
}
?>
	
<?php

if (!isset($_SESSION['posts_redirect'])){$_SESSION['posts_redirect'] = "posts.php";}

// ------

$id = filter_input(INPUT_GET, 'id', $filter = FILTER_SANITIZE_STRING);

// ------

$stmt = 'SELECT author, name, description, image, price, download_link, download_count, date, time, status FROM posts WHERE id="'.$id.'"';

if ($mysqli->query($stmt)->num_rows<1){ header('Location: posts.php?err=not-exists'); exit; }

$stmt = $mysqli->prepare($stmt);
$stmt->execute();
$stmt->store_result();

$stmt->bind_result($author, $name, $description, $image, $price, $download_link, $download_count, $date, $time, $status);
$stmt->fetch();
$stmt->close();

if ($status != "published"){
	header('Location: posts.php?err=not-exists');
	exit;
}

/* MEMBER NAME */
			
$stmt = $mysqli->prepare('SELECT username, first_name, last_name FROM members WHERE id="'.$author.'"');
$stmt->execute();
$stmt->store_result();

$stmt->bind_result($member_username, $member_first_name, $member_last_name);
$stmt->fetch();
$stmt->close();

$member_name = $member_first_name." ".$member_last_name;
if ($member_name==" "){
	$member_name = $member_username;
}

?>

<div class="container">
	<div class="pull-left">
    	<?php if ($role == "admin" | if_has_permission($role,"can_edit_posts")){ ?>
			<a href="post_delete.php?<?php echo 'id='.$id ?>"><button class='btn btn-danger'><span>حذف</span> <i class="icon-trash icon-white"></i></button></a>
			<a href="post_edit.php?<?php echo 'id='.$id ?>"><button class='btn btn-primary'><span>ویرایش</span> <i class="icon-edit icon-white"></i></button></a>
		<?php } ?>
		<a href="<?php echo $_SESSION['posts_redirect'] ?>"><button class='btn'><span>بازگشت</span> <i class="icon-chevron-left"></i></button></a>
	</div>
	<button class="btn disabled"><span id="subtitle">مشاهده‌ی مطلب</span></button><br /><br />
    <div class="pull-right span4" id="sidebar">
		<div id="images" class="img-polaroid pull-right span4" style="margin-top: 5px;">
			<a id="image_link" class="fancybox" href="<?php if (empty($image)){echo 'img/no_image.png';} else { echo $image; } ?>">
			<img id="image" style="width: 100%;" src="<?php if (empty($image)){echo 'img/no_image.png';} else { echo $image; } ?>" /></a>
            <div style="clear:both;"></div>
		</div>
        <div style="clear:both;"></div>
        <br />
        <div id="like" class="img-polaroid span4	pull-right">
			<?php

			$likes = ($mysqli->query('SELECT id FROM posts_likes WHERE post="'.$id.'"'))->num_rows;
			
			?>
            
            <h5 class="normal"><img id="image" src="img/like.png" width="32" height="32" />&ensp;<?php echo $likes; ?> پسند</h5>
		</div>
		<br />
	</div>
	<div id="main" class="span6 pull-right">
        <table class="table table-striped table-right">
            <tr>
                <td><h5 class="pull-right span1 normal">نام </h5></td>
                <td style="padding: 5px 0 0 0;"><?php echo $emoji->replaceEmojiWithImages($name); ?></td>
            </tr>
            <tr>
                <td><h5 class="normal">نویسنده</h5></td>
                <td style="padding: 5px 0 0 0;"><?php echo $emoji->replaceEmojiWithImages($member_name); ?></td>
            </tr>
            <tr>
                <td><h5 class="normal">نام</h5></td>
                <td style="padding: 5px 0 0 0;"><?php echo $emoji->replaceEmojiWithImages($name); ?></td>
            </tr>
            <tr>
                <td><h5 class="normal">توضیحات</h5></td>
                <td style="padding-top: 5px;"><?php echo $emoji->replaceEmojiWithImages(nl2br($description)); ?></td>
            </tr>
            <tr>
                <td><h5 class="normal">قیمت </h5></td>
                <td style="padding: 5px 0 0 0;"><?php echo $price; ?> ریال</td>
            </tr>
            <tr>
                <td><h5 class="normal">تعداد دریافت </h5></td>
                <td style="padding: 5px 0 0 0;"><?php echo $download_count; ?></td>
            </tr>
            <tr>
                <td><h5 class="normal">تاریخ </h5></td>
                <td style="padding: 5px 0 0 0;"><?php echo $date; ?></td>
            </tr>
            <tr>
                <td><h5 class="normal">زمان </h5></td>
                <td style="padding: 5px 0 0 0;"><?php echo $time; ?></td>
            </tr>
        </table>
	</div>
</div>
<?php include('footer.php'); ?>