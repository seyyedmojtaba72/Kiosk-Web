<?php 
require_once('header.php'); 

if (!empty($err)) {
	switch ($err) {

	default : $err_msg = "خطای غیر منتظره‌ای پیش آمده!"; break;
	}
	
}
?>
<?php
if (!empty($suc)) {
	switch ($suc) {
			
	default : $suc_msg = "عملیات با موفقیت انجام شد!"; break;
	}
	
}
?>
<?php
if (!empty($err_msg)) {
	echo '<div class="alert alert-error tahoma size-11"><button type="button" class="close" data-dismiss="alert">&times;</button><p>'. $err_msg .'</p></div>';
}
?>
<?php
if (!empty($suc_msg)) {
	echo '<div class="alert alert-success tahoma size-11"><button type="button" class="close" data-dismiss="alert">&times;</button><p>'. $suc_msg .'</p></div>';
}
?>
<?php 
$action = filter_input(INPUT_GET, 'action', $filter = FILTER_SANITIZE_STRING);

if ($action == "download"){
	$platform = $_GET['platform'];
	if ($platform == "android"){	
		$stmt = $mysqli->prepare('UPDATE options SET value = "'. ($options["android_download_count"]+1).'" WHERE variable = "android_download_count"');
	} else {
		$stmt = $mysqli->prepare('UPDATE options SET value = "'. ($options["ios_download_count"]+1).'" WHERE variable = "ios_download_count"');
	}
	$stmt->execute(); 
	$stmt->close();
	if ($platform == "android"){
		$options["android_download_count"]++;
		header('Location: download.php?file='.$options['android_download_link']);
	} else {
		$direct = $_GET['direct'];
		$options["ios_download_count"]++;
		if ($direct == "true"){
			header('Location: download.php?file='.$options['ios_download_link']);
		} else {
			header('Location: itms-services://?action=download-manifest&url=https://dl.dropboxusercontent.com/s/b54m1l1q2zrxbl3/tarfandestan-ios-manifest.plist');
		}	
	}
	
}

$android_users = $mysqli->query('SELECT id FROM users WHERE platform = "android"')->num_rows;
$android_uptodate_users = $mysqli->query('SELECT id FROM users WHERE version="'.$options["android_last_version"].'" AND platform = "android"')->num_rows;

$ios_users = $mysqli->query('SELECT id FROM users WHERE platform = "ios"')->num_rows;
$ios_uptodate_users = $mysqli->query('SELECT id FROM users WHERE version="'.$options["ios_last_version"].'" AND platform = "ios"')->num_rows;

?>
<?php $subtitle = 'خانه'; ?>
<div class="container" style="width: 90%; min-width: 300px;">
	<div style="margin: 0 auto;">
        <div class="pull-right" id="content-android" style="margin-left: 2%; min-width: 300px; width: 48%; max-width: 100%">
            <div style="margin: auto;">
            	<div style="height: 25px;">
            	<img class="pull-right" style="margin-left: 10px; width:25px; height: 25px; vertical-align:middle" src="img/android.png"  /><h2 class="normal" style="vertical-align: middle">اندروید</h2>
                </div>
                <div class="line clearfix"></div><br />
                <img class="pull-right" style="margin-left: 10px; max-width:128px; width: 25%; height: auto;" src="img/android_icon.png"  />
                <!--<div class="pull-right" id="slider">
                    <iframe src="<?php echo $options["url"]; ?>/fragment/Bootstrap-Slider/index.html" width="128" height="250px" frameborder="0" scrolling="no"></iframe>
                </div>-->
                <h5 class="normal green pull-right">نسخه‌ی <?php echo $options["android_last_version"]; ?></h5>
                <h5 class="normal green pull-left">&ensp;<?php echo get_file_size($options["android_download_link"], "MB"); ?> مگابایت</h5><p><br /><br /></p>
                <h5 class="normal green pull-right">آخرین به‌روزرسانی: &ensp;<?php echo $persian_date->to_date(date("Y-m-d", get_file_last_modified($options["android_download_link"])), "Y/m/d"); ?></h5>
                <h5 class="normal green pull-left"><?php echo date("H:m", get_file_last_modified($options["android_download_link"])); ?></h5>
            </div>
            <div class="clearfix"></div>
            <div style="margin: auto; margin-top: 50px; width: 100%;">
                <div style="margin: auto; max-width: 100%;">
                    <div style="max-width: 100%;">
                        <a href="<?php echo $options["url"]; ?>/home.php?action=download&platform=android"><button class="button3d large green" style="width: 100%; height: 75px;"><img class="pull-right" src="img/download.png" style="margin-left: 10px;" width="50" height="50" /><h1 class="normal pull-right" style="color: #ffffff;">دانلود نرم‌افزار</h1></button></a>
                        <div class="clearfix"></div><br />
			<a href="<?php echo $options["url"]; ?>/home.php?action=download&platform=android"><button class="button3d blue" style="width: 48.5%; height: 55px;"><img class="pull-right" src="img/googleplay.png" width="32" height="32" /><h5 class="normal" style="color: #ffffff;">دانلود از گوگل پلی</h6></button></a>
                        <a href="http://iranapps.ir/app/com.kiosk.android"><button class="button3d orange pull-left" style="width: 48.5%; height: 55px;"><img class="pull-right" src="img/iranapps.png"  width="32" height="32" /><h5 class="normal" style="color: #ffffff;">دانلود از ایران اپس</h6></button></a>
			<!--<div class="clearfix"></div><br />
                        <a href="<?php echo $options["url"]; ?>/home.php?action=download&platform=android"><button class="button3d orange" style="width: 44%; height: 55px;"><img class="pull-right" src="img/cando.png" style="margin-left: 10px;" width="32" height="32" /><h1 class="YekanBold" style="color: #ffffff; font-size: 18px;">دانلود از کندو</h1></button></a>
                        <a href="http://iranapps.ir/app/com.kiosk.android"><button class="button3d blue pull-left" style="width: 52%; height: 55px;"><img class="pull-right" src="img/iranapps.png" style="margin-left: 10px;" width="32" height="32" /><h1 class="YekanBold" style="color: #ffffff; font-size: 18px;">دانلود از ایران اپس</h1></button></a>-->
                        <br /><br />
                    </div>
                    <div style="background: #ddd; border-radius: 10px; padding: 2%; width: 96%; height: 125px;">
                        <h6 class="normal" style="line-height: normal;"><?php echo nl2br($options["android_last_version_details"]); ?></h6>
                    </div>
                </div>         
            </div>
        </div>
        <!--<div class="pull-right" id="content-ios" style="margin: auto; min-width: 300px; width: 48%; max-width: 100%;">
            <div style="margin: auto;">
            	<div style="height: 25px;">
            	<img class="pull-right" style="margin-left: 10px; width:25px; height: 25px; vertical-align:middle" src="img/apple.png"  /><h2 class="normal" style="vertical-align: middle">آی.او.اس</h2>
                </div>
                <div class="line clearfix"></div><br />
                <img class="pull-right" style="margin-left: 10px; max-width:128px; width: 25%; height: auto;" src="img/ios_icon.png"  />
                <h5 class="normal green pull-right">نسخه‌ی <?php echo $options["ios_last_version"]; ?></h5>
                <h5 class="normal green pull-left">&ensp;<?php echo get_file_size($options["ios_download_link"], "MB"); ?> مگابایت</h5><p><br /><br /></p>
                <h5 class="normal green pull-right">آخرین به‌روزرسانی: &ensp;<?php echo $persian_date->to_date(date("Y-m-d", get_file_last_modified($options["ios_download_link"])), "Y/m/d"); ?></h5>
                <h5 class="normal green pull-left"><?php echo date("H:m", get_file_last_modified($options["ios_download_link"])); ?></h5>    
            </div>
            <div class="clearfix"></div>
            <div style="margin: auto; margin-top: 50px; width: 100%;">
                <div style="margin: auto; max-width: 100%;">
                    <div style="max-width: 100%;">
                        <a href="<?php echo $options["url"]; ?>/home.php?action=download&platform=ios"><button class="button3d large orange" style="width: 100%; height: 75px;"><img class="pull-right" src="img/install.png" width="50" height="50" style="margin-left: 10px;" /><h1 class="YekanBold pull-right" style="color: #ffffff;">نصب نرم افزار</h1></button></a>
                        <div class="clearfix"></div><br />
                        <a href="<?php echo $options["url"]; ?>/home.php?action=download&platform=ios&direct=true"><button class="button3d large green" style="width: 100%; height: 75px;"><img class="pull-right" src="img/download.png" width="50" height="50" style="margin-left: 10px;" /><h1 class="YekanBold pull-right" style="color: #ffffff;">دانلود نرم‌افزار</h1></button></a>
                        <div class="clearfix"></div><br />
                        <a href="<?php echo $options["url"]; ?>/home.php?action=download&platform=ios"><button class="button3d blue" style="width: 44%; height: 55px;"><img class="pull-right" src="img/sibche.png" width="32" height="32" style="margin-left: 10px;" /><h1 class="YekanBold" style="color: #ffffff; font-size: 18px;">دانلود از سیبچه</h1></button></a>
                        <a href="<?php echo $options["url"]; ?>/home.php?action=download&platform=ios"><button class="button3d blue pull-left" style="width: 52%; height: 55px;"><img class="pull-right" src="img/appstore.png" width="32" height="32" style="margin-left: 10px;" /><h1 class="YekanBold" style="color: #ffffff; font-size: 18px;">دانلود از اپ استور</h1></button></a>
                        <br /><br />
                    </div>
                    <div style="background: #ddd; border-radius: 10px; padding: 2%; width: 96%; height: 125px;">
                        <h6 class="normal" style="line-height: normal;"><?php echo nl2br($options["ios_last_version_details"]); ?></h6>
                    </div>
                </div>
                            
            </div>
        </div>-->
        
    </div>
        
    <div class="clearfix"></div><br />
    
    <div id="statistics" style="min-width: 300px; width: 100%; max-width: 100%;">
        <!--<h5 class="normal" style="color: red;"><i class="pull-right icon-info-sign"></i>&ensp;در صورتی که قبلاً نسخه اندرویدی این نرم افزار را نصب کرده بودید؛ آن را حذف کرده و مجددا اقدام به نصب نسخه جدید کنید.</span></h5>
        <h5 class="normal" style="color: red;"><i class="pull-right icon-info-sign"></i>&ensp;در صورتی ک در دستگاه iOS خود پیام Untrusted Developer را مشاهده کردید به آدرس Settings-General-Profile-akramzade72@gmail.com رفته و روی Trust کلیک کنید.</span></h5>-->
        <br />
        <h5 class="normal"><i class="pull-right icon-info-sign"></i>&ensp;این اپلیکیشن هم‌اکنون &ensp;<span class="red normal"><?php echo $mysqli->query('SELECT id FROM members')->num_rows; ?></span>&ensp; عضو و &ensp;<span class="red normal"><?php echo $online_visitors->count_users(); ?></span>&ensp; کاربر آنلاین دارد.</h5>
        <h5 class="normal"><i class="pull-right icon-info-sign"></i>&ensp;تعداد دریافت اندروید: &ensp;<span class="red normal"><?php echo $options["android_download_count"]; ?></span></h5>
        <h5 class="normal"><i class="pull-right icon-info-sign"></i>&ensp;تعداد کاربران اندروید: &ensp;<span class="red normal"><?php echo $android_users; ?></span></h5>
        <h5 class="normal"><i class="pull-right icon-info-sign"></i>&ensp;کاربران به‌روز اندروید: &ensp;<span class="red normal"><?php echo $android_uptodate_users; ?></span></h5>
        <!--<h5 class="normal"><i class="pull-right icon-info-sign"></i>&ensp;تعداد دریافت آی.او.اس: &ensp;<span class="red normal"><?php echo $options["ios_download_count"]; ?></span></h5>
        <h5 class="normal"><i class="pull-right icon-info-sign"></i>&ensp;تعداد کاربران آی.او.اس: &ensp;<span class="red normal"><?php echo $ios_users; ?></span></h5>
        <h5 class="normal"><i class="pull-right icon-info-sign"></i>&ensp;کاربران به‌روز آی.او.اس: &ensp;<span class="red normal"><?php echo $ios_uptodate_users; ?></span></h5>--><br />
    </div>	

    <div class="line text-center hidden" id="progress">
        <br />
        <h4 class="normal">موقعیت پروژه</h4>
        <div class="pull-right" style="width: 50%;">
            <h5 class="normal">نمای کلی برنامه</h5>
            <div class="progress progress-inverse progress-striped active" style="width: 90%; margin: auto;">
                <div class="bar bar-primary" style="width: 100%;"><span class="Yekan h5 normal">95%</span></div>
            </div>
        </div>
        <div class="pull-right" style="width: 50%;">
            <h5 class="normal">فروشگاه‌ها</h5>
            <div class="progress progress-inverse progress-striped active" style="width: 90%; margin: auto;">
                <div class="bar bar-success" style="width: 100%;"><span class="Yekan h5 normal">95%</span></div>
            </div>
        </div>
        <div class="pull-right" style="width: 50%;">
            <h5 class="normal">کافه‌آگهی</h5>
            <div class="progress progress-inverse progress-striped active" style="width: 90%; margin: auto;">
                <div class="bar bar-warning" style="width: 100%;"><span class="Yekan h5 normal">95%</span></div>
            </div>
        </div>
        <div class="pull-right" style="width: 50%;">
            <h5 class="normal">ویترین</h5>
            <div class="progress progress-inverse progress-striped active" style="width: 90%; margin: auto;">
                <div class="bar bar-danger" style="width: 100%;"><span class="Yekan h5 normal">95%</span></div>
            </div>
        </div>
    </div>
    
    <div class="line"></div>
    <div id="models" style="width: 100%; padding: 0;">
        <div class="model">
            <a href="posts.php"><img src="<?php echo $options["url"] ?>/img/posts.png" /></a>
            <h4 class="normal">مطالب</h4>
        </div>
    </div>
</div>
<?php require_once('footer.php'); ?>