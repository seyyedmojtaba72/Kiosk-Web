<script type="text/javascript">
$(document).ready(function() {
	var divider = ' '+ '<?php if (isset($title_divider_character)){ echo $title_divider_character; } ?>' + ' ';
	var title = '';
	var subtitle = '<?php if (isset($subtitle)){ echo $subtitle; } ?>';
	if (subtitle.length>0){
		title += subtitle + divider;
	} else if ($('#subtitle').text().length>0){
		title += $('#subtitle').text() + divider;
	}
	
	title += '<?php echo $options["title"]; ?>';
	document.title = title;
});
</script>
<script >
$(document).ready(function() {
	$('.fancybox').fancybox();
});
</script>
<script type="text/javascript">
var config = {
	'.chosen-select' : {
		allow_single_deselect: true,
		no_results_text : "نتیجه‌ای یافت نشد.",
		placeholder_text_multiple: "چند گزینه را انتخاب کنید.",
		placeholder_text_single: "گزینه‌ای را انتخاب کنید.",
	},
}
for (var selector in config) {
	$(selector).chosen(config[selector]);
}
</script>
<script type="text/javascript">     
	$("#date").persianDatepicker({
		//months: ["فروردین", "اردیبهشت", "خرداد", "تیر", "مرداد", "شهریور", "مهر", "آبان", "آذر", "دی", "بهمن", "اسفند"],
		//dowTitle: ["شنبه", "یکشنبه", "دوشنبه", "سه شنبه", "چهارشنبه", "پنج شنبه", "جمعه"],
		//shortDowTitle: ["ش", "ی", "د", "س", "چ", "پ", "ج"],
		//showGregorianDate: false,
		persianNumbers: true,
		formatDate: "YYYY/MM/DD",
		//prevArrow: '\u25c4',
		//nextArrow: '\u25ba',
		theme: 'default',
		//alwaysShow: false,
		//selectableYears: null,
		//selectableMonths: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
		//cellWidth: 25, 
		//cellHeight: 20, 
		//fontSize: 11,             
		//isRTL: false,
		//calendarPosition: {
		//	x: 0,
		//	y: 0,
		//},
		//onShow: function(calendar) {
			//calendar.show();
		//},
		//onHide: function(calendar) {
			//calendar.hide();
		//
		//startDate: '0000-00-00',
		//startDate: '0000-00-00',
	
	});
</script>
<script type="text/javascript">
	$("#loading").hide();
</script>
<div style="clear:both"></div>
<div class="no-print" style="height: 50px;"></div>
<div class="footer text-center no-print">
	<div class="copyright">
		<img src="<?php echo $options["url"]; ?>/img/copyright.png" />&emsp;
		<?php echo $options["copyright_text"]; ?>
	</div>
    <div class="designer hidden">
    	<a href="http://www.developars.ir/apadana" target="_blank" title="آپادانا"><img alt="آپادانا" src="<?php echo $options["url"]; ?>/img/apadana.png" /></a>&ensp;
    	<a href="http://www.developars.ir" target="_blank" title="دولوپارس"><img alt="دولوپارس" src="<?php echo $options["url"]; ?>/img/developars.png" /></a>
    </div>
    <div class="links">
    	<a href="http://blog.kioskapp.ir/help">راهنما</a>&emsp;
    	<a href="http://blog.kioskapp.ir/terms">شرایط استفاده</a>&emsp;
	<a href="<?php echo $options["url"]; ?>/home.php">دانلود</a>
    	<a href="http://blog.kioskapp.ir/about">درباره ما</a>&emsp;
    	<a href="http://blog.kioskapp.ir/contact">ارتباط با ما</a>
    </div>
</div>
</div>
</body>
</html>