<?php require_once('header.php'); ?>
<?php if ($logged == "in"){header("Location: panel.php");} ?>

<?php
if (!empty($err)) {
	switch ($err) {
	case "safe" : $err_msg = "خطا در ورود مطمئن به سامانه!"; break;
	case "invalid-activation" : $err_msg = "درخواست نامعتبر است!"; break;
	case "invalid-reset" : $err_msg = "درخواست نامعتبر است!"; break;
	case "user-pass" : $err_msg = "نام کاربری یا گذرواژه اشتباه است!"; break;
	case "inactive" :
	{
		$username = filter_input(INPUT_GET, 'username', $filter = FILTER_SANITIZE_STRING);
		$password = filter_input(INPUT_GET, 'password', $filter = FILTER_SANITIZE_STRING);
		$err_msg = 'حساب شما غیرفعال است! <a style="margin-right:50px;" href="'.$options["url"].'/inc/send_activation.php?username='.$username.'&password='.$password.'"><button class="btn Yekan normal">درخواست فعال‌سازی</button></a>'; 
	};
	break;
	
	default : $err_msg = "خطای غیر منتظره‌ای پیش آمده!"; break;
	}
	
}
?> 
<?php
if (!empty($suc)) {
	switch ($suc) {
	case "register" : $suc_msg = "ثبت نام شما انجام شد. لطفاً وارد شوید!"; break;
	case "password" : $suc_msg = "گذرواژه تغییر کرد. لطفاً دوباره وارد شوید!"; break;
	case "already-active" : $suc_msg = "حساب شما در حال حاضر فعال است!"; break;
	case "active" : $suc_msg = "حساب شما فعال شد. لطفاً وارد شوید!"; break;
	case "check-email" : $suc_msg = "درخواست فرستاده شد. لطفاً ایمیل خود و پوشه‌ی Spam یا Bulk را بررسی کنید!"; break;
	case "reset" : {
		$password = filter_input(INPUT_GET, 'password', $filter = FILTER_SANITIZE_STRING);
		$suc_msg = "گذرواژه‌ی جدید شما <b>".$password."</b> است. لطفاً آن را در جایی امن یادداشت کنید!"; break;
	}
	
	default : $suc_msg = "عملیات با موفقیت انجام شد!"; break;
	}
	
}
?> 
<?php
if (!empty($err_msg)) {
	echo '<div class="alert alert-error tahoma size-11"><button type="button" class="close" data-dismiss="alert">&times;</button><p>'. $err_msg .'</p></div>';
}
?>
<?php
if (!empty($suc_msg)) {
	echo '<div class="alert alert-success tahoma size-11"><button type="button" class="close" data-dismiss="alert">&times;</button><p>'. $suc_msg .'</p></div>';
}
?>
<?php
$redirect = filter_input(INPUT_GET, 'redirect', $filter = FILTER_SANITIZE_STRING);
if (!empty($redirect)) {
	echo '<div class="alert tahoma size-11"><button type="button" class="close" data-dismiss="alert">&times;</button><p>برای ادامه ابتدا باید وارد شوید.</p></div>';
}
?>
<?php
if (empty($redirect)){ $redirect = $options["url"]."/panel.php"; } ?>
<div class="container" style="width: 350px;">
	<div class="text-center">
    	<button class="btn disabled"><span id="subtitle">ورود</span></button><br /><br />
		<div class="text-left" style="width:210px; margin-right:50px;">
			<form action="<?php echo $options["url"] ?>/inc/login.php" method="post" name="login_form">
            	<input type="hidden" name="redirect" value="<?php echo urldecode($redirect); ?>" />                      
				<input type="text" placeholder="نام کاربری" class="span2" id="username" name="username" style="font: normal 12px 'WYekan';" />
				<input type="password" class="span2" name="password" id="password" style="font: normal 12px 'WYekan';" placeholder="گذرواژه" />
				
				<button class="btn btn-primary normal" type="submit" onClick="return loginformhash(this.form, this.form.username, this.form.password);"><span>ورود</span> <i class="icon-user icon-white"></i></button>
			   
			</form>
			<p><a href="<?php echo $options["url"] ?>/register.php"><button class="btn btn-success normal" style="width:170px;" type="button"><span>ثبت‌نام کاربر جدید</span> <i class="icon-plus icon-white"></i></button></a></p>
			<p><a href="<?php echo $options["url"] ?>/forget.php"><button class="btn btn-warning normal" style="width:170px;" type="button"><span>گذرواژه را فراموش کردم</span> <i class="icon-lock icon-white"></i></button></a>
			
		</div>
	</div>

	
</div>
<?php require_once('footer.php'); ?>