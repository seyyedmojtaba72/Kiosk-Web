﻿<?php

require_once 'inc/db_connect.php';
require_once 'inc/functions.php';
require_once 'inc/config.php';
require_once 'inc/information.php';

$secret = $_GET['secret'];
if (!isset($secret)){ $secret = ""; }

if ($secret == "phpinfo"){
	phpinfo();
}

if ($secret == "utf8-encode"){
	$text = $_GET['text'];
	echo mb_convert_encoding($text, "UTF-8", "cp1252");
}

if ($secret == "utf8-decode"){
	$text = $_GET['text'];
	echo mb_convert_encoding($text, "cp1252", "UTF-8");
}
	
if ($secret == "save_shops"){
	$ST = 'SELECT id FROM shops';
	$result = $mysqli->query($ST);
	$rows = $result->num_rows;
	$array = array();
	while($row = mysql_fetch_array($result)){
		$array[] = $row['id'];
	}
	
	set_time_limit(0); 
	
	printf("saving ".$rows." files"."<br><br>");
	
	$i=0;
	while ($i<$rows){
		
		printf("saving ".($i+1)."/".$rows." => ".$array[$i].".json"."<br>");
		file_put_contents(dirname(__FILE__)."/temp/".$array[$i].".json", file_get_contents($options['url'].'/api/get_shop.php?id='.$array[$i]));
		
		$i++;
	
	}
} 

function listFolderFiles($dir, &$array){
	$ffs = scandir($dir);
    foreach($ffs as $ff){
        if($ff != '.' && $ff != '..'){
            if(is_dir($dir.'/'.$ff)){
				listFolderFiles($dir.'/'.$ff, $array);
			} else {
				array_push($array, $dir.'/'.$ff);
			}
        }
    }
	return $array;
}

if ($secret == "create_thumb"){
	$dir = $_GET['dir'];
	$max = $_GET['max'];
	$quality = $_GET['quality'];
	$suffix = $_GET['suffix'];

	
	$array = array();
	listFolderFiles($dir, $array);
	
	foreach($array as $path){	

		$thumb_path = $path . $suffix;
		if (file_exists($thumb_path)){
			unlink($thumb_path);
		}
		
		if (($key = array_search($thumb_path, $array)) !== false) {
			unset($array[$key]);
		}
		
	}
		
	foreach($array as $path){	

		
		// CREATE THUMBNAIL IF IS A JPG IMAGE
		
		$a = getimagesize($path);
		$image_type = $a[2];
		
		if(in_array($image_type , array(IMAGETYPE_JPEG))){
		
			$thumb_path = $path . $suffix;
			
			list( $width,$height ) = getimagesize( $path );
			$ratio = $width/$height;
			
			if ($width >= $height){
				$new_width = $max;
				$new_height = $new_width / $ratio;
			} else {
				$new_height = $max;
				$new_width = $new_height * $ratio;
			}

			$thumb_img = imagecreatetruecolor( $new_width, $new_height );
			$source_img = imagecreatefromjpeg( $path );
			
			imagecopyresampled($thumb_img, $source_img, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
			imagejpeg( $thumb_img, $thumb_path, $quality );  // QUALITY	 
		}
		
	}

	
}


?>