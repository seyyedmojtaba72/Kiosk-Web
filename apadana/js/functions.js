function today(){
	week= new Array("يک‌شنبه","دوشنبه","سه‌شنبه","چهارشنبه","پنج‌شنبه","جمعه","شنبه")
	a = new Date();
	d= a.getDay();
	day= a.getDate();
	month = a.getMonth()+1;
	year= a.getYear();
	year = (year== 0)?2000:year;
	(year<1000)? (year += 1900):true;
	year -= ( (month < 3) || ((month == 3) && (day < 21)) )? 622:621;
	switch (month) {
	case 1: (day<21)? (month=10, day+=10):(month=11, day-=20); break;
	case 2: (day<20)? (month=11, day+=11):(month=12, day-=19); break;
	case 3: (day<21)? (month=12, day+=9):(month=1, day-=20); break;
	case 4: (day<21)? (month=1, day+=11):(month=2, day-=20); break;
	case 5:
	case 6: (day<22)? (month-=3, day+=10):(month-=2, day-=21); break;
	case 7:
	case 8:
	case 9: (day<23)? (month-=3, day+=9):(month-=2, day-=22); break;
	case 10:(day<23)? (month=7, day+=8):(month=8, day-=22); break;
	case 11:
	case 12:(day<22)? (month-=3, day+=9):(month-=2, day-=21); break;
	default: break;
	}
	if (month<10){month="0"+month;}
	if (day<10){day="0"+day;}
	return week[d]+" "+year+"/"+month+"/"+ day;
}


function now(){
	var now = new Date();
	var hours = now.getHours();
	var minutes = now.getMinutes();
	var seconds = now.getSeconds();
	
	var timeValue = "";
	timeValue += ((hours < 10) ? "0" : "") + hours;
	timeValue += ((minutes < 10) ? ":0" : ":") + minutes;
	
	return timeValue;
}

function validate_num(evt) {
	var theEvent = evt || window.event;
	var key = theEvent.keyCode || theEvent.which;
	key = String.fromCharCode( key );
	var regex = /[0-9]|\./;
	if( !regex.test(key) ) {
		theEvent.returnValue = false;
		if(theEvent.preventDefault) theEvent.preventDefault();
	}
}

function go_to_page(pre_link,id){
	var link = pre_link+'&page='+(document.getElementById(id)).value;
	window.location.href=link;
}

function load_popup_image(id,link,place){
	$(id).popover({ placement: place, html: 'true', content: '<img width="128" height="128" src="'+link+'">'});
	$(id).popover('show');
}

function dismiss_popup_image(id,link){
	$(id).popover('hide');
}

function show_small_loading(id, pre_link){
	$("#"+id).html('<img id="small_loader_'+id+'" style="margin: 0 5px;" src="'+pre_link+'/img/loading_small.gif" border=0/>');
}

function hide_small_loading(id, pre_link){
	$('#small_loader_'+id).remove();
}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function escapeHtml(unsafe) {
    return unsafe
         .replace(/&/g, "&amp;")
         .replace(/</g, "&lt;")
         .replace(/>/g, "&gt;")
         .replace(/"/g, "&quot;")
         .replace(/'/g, "&#039;");
}

function fancyAlert(msg) {
	var ret;
    jQuery.fancybox({
		modal: true,
        'content' : "<div style=\"margin:1px;width:240px;\"><h5 class=\"normal\">"+msg+"</h5><br /><br /><button class=\"btn btn-primary pull-left\" onclick=\"jQuery.fancybox.close();\"><span>باشه</span></button></div>"
	});
}

	 
function fancyConfirm(msg,callback) {
    var ret;
    jQuery.fancybox({
		modal: true,
        content : "<div style=\"margin:1px;width:240px;\"><h5 class=\"normal\">"+msg+"</h5><div style=\"text-align:right;margin-top:10px;\"><br /><br /><div class=\"pull-left\"><button class=\"btn btn-primary\" onclick=\"jQuery.fancybox.close();\" id=\"fancyConfirm_cancel\"><span>انصراف</span></button>&ensp;<button class=\"btn btn-primary\" onclick=\"jQuery.fancybox.close();\" id=\"fancyConfirm_ok\"><span>باشه</span></button></div></div>",
        onComplete : function() {
            jQuery("#fancyConfirm_cancel").click(function() {
                ret = false; 
                jQuery.fancybox.close();
            })
            jQuery("#fancyConfirm_ok").click(function() {
                ret = true; 
                jQuery.fancybox.close();
            })
        },
        onClosed : function() {
            callback.call(this,ret);
        }
    });
}

function fancyPopup(msg) {
    jQuery.fancybox({
        'content' : msg,
	});
}