// LOGIN FORM
function loginformhash(form, username, password) {
	 // Check each field has a value
    if (username.value == '' || password.value == '') {
 
        fancyAlert('شما باید تمام مشخصات را پر کنید!');
        return false;
    } else {
    	form.submit();
	}
}
 
// REGISTER FORM
function regformhash(form, uid, email, password, conf) {
     // Check each field has a value
    if (uid.value == '' || email.value == '' || password.value == ''  || conf.value == '') {
 
        fancyAlert('شما باید تمام مشخصات را پر کنید!');
        return false;
    }
 
    // Check the username
 
    re = /^\w+$/; 
    if(!re.test(form.username.value)) { 
        fancyAlert("نام کاربری تنها باید شامل حروف انگلیسی و اعداد باشد!"); 
        form.username.focus();
        return false; 
    }
 
 
 	if (uid.value.length < 6) {
        fancyAlert('طول نام کاربری باید حداقل 6 کاراکتر باشد!');
        form.uid.focus();
        return false;
    }
	
    // Check that the password is sufficiently long (min 6 chars)
    // The check is duplicated below, but this is included to give more
    // specific guidance to the user
    if (password.value.length < 6) {
        fancyAlert('طول گذرواژه باید حداقل 6 کاراکتر باشد!');
        form.password.focus();
        return false;
    }
 
    // At least six characters 
 
 
    // Check password and confirmation are the same
    if (password.value != conf.value) {
        fancyAlert('گذرواژه و تأیید گذرواژه مطابقت ندارد.');
        form.password.focus();
        return false;
    }
 
    // Finally submit the form. 
    form.submit();
    return true;
}

// FORGET PASSWORD FORM
function forgetformhash(form,email) {
     // Check each field has a value
    if (email.value == '') {
        fancyAlert('شما باید تمام مشخصات را پر کنید!');
        return false;
    }
 
    // Finally submit the form. 
    form.submit();
    return true;
}


// CHANGE PASSWORD FORM
function passformhash(form, old_password, password, confirm_password) {
    
	// Check each field has a value
	if (old_password.value == '' || password.value == '' || confirm_password.value == '') {
	  
        fancyAlert('شما باید تمام مشخصات را پر کنید!');
        return false;
    }
 
 
    // Check that the password is sufficiently long (min 6 chars)
    // The check is duplicated below, but this is included to give more
    // specific guidance to the user
    if (password.value.length < 6) {
        fancyAlert('طول گذرواژه باید حداقل 6 کاراکتر باشد!');
        form.password.focus();
        return false;
    }
 
    // At least six characters 

    // Check password and confirmation are the same
    if (password.value != confirm_password.value) {
        fancyAlert('گذرواژه و تأیید گذرواژه مطابقت ندارد.');
        form.password.focus();
        return false;
    }
 
 
    // Finally submit the form. 
    form.submit();
    return true;
}