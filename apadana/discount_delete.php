<?php 
include('header.php'); 
?>

<?php if (if_has_permission($role,"edit_discounts")){} else{header("Location: login.php?redirect=".urlencode("http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']));} ?>

<?php
if (!empty($err)) {
	switch ($err) {
	
	default : $err_msg = "خطای غیر منتظره‌ای پیش آمده!"; break;
	}
	
}
?> 
<?php
if (!empty($suc)) {
	switch ($suc) {
	
	default : $suc_msg = "عملیات با موفقیت انجام شد!"; break;
	}
	
}
?> 
<?php
if (!empty($err_msg)) {
	echo '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">&times;</button><p>'. $err_msg .'</p></div>';
}
?>
<?php
if (!empty($suc_msg)) {
	echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button><p>'. $suc_msg .'</p></div>';
}
?>
	
<?php

if (!isset($_SESSION['discounts_redirect'])){$_SESSION['discounts_redirect'] = "discounts.php";}

// ------

$id = filter_input(INPUT_GET, 'id', $filter = FILTER_SANITIZE_STRING);

// ------

$stmt = 'SELECT code, title, percentage, number, used, min_price, start_date, end_date, description, status FROM discounts WHERE id="'.$id.'"';

if ($mysqli->query($stmt)->num_rows<1){ header('Location: discounts.php?err=not-exists'); exit; }

$stmt = $mysqli->prepare($stmt);
$stmt->execute();
$stmt->store_result();

$stmt->bind_result($code, $title, $percentage, $number, $used, $min_price, $start_date, $end_date, $description, $status);
$stmt->fetch();
$stmt->close();

?>

<div class="container">
	<div class="pull-left">
    	<a href="discount_edit.php?<?php echo 'id='.$id ?>"><button class='btn btn-primary'><span>ویرایش</span> <i class="icon-edit icon-white"></i></button></a>
		<a href="<?php echo $_SESSION['discounts_redirect'] ?>"><button class='btn'><span>بازگشت</span> <i class="icon-chevron-left"></i></button></a>
	</div>
	<button class="btn disabled"><span id="subtitle">حذف تخفیف</span></button><br /><br />
	<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>
		<p>آیا شما مطمئنید؟
		<form action="<?php echo $options["url"] ?>/inc/delete_discount.php" method="post">
			<input type="hidden" value="discounts.php?" name="redirect" id="redirect"/>
			<input type="hidden" value="<?php echo $id ?>" name="id" id="id"/>
			<button style="margin-right:50px;" type="submit" class="btn btn-danger Yekan normal">بله</button>
			<a href="<?php echo $_SESSION['discounts_redirect'] ?>" type="button" class="btn Yekan normal">خیر</a>
		</form>
		</p>
	</div>
	<div id="main" class="span7 pull-right">
		<table class="table table-striped table-right">
			<tr>
				<td class="span2"><h5 class="normal">کد </h5></td>
				<td style="padding: 5px 0 0 0;"><?php echo $code; ?></td>
			</tr>
			<tr>
				<td><h5 class="normal">عنوان </h5></td>
				<td style="padding: 5px 0 0 0;"><?php echo $title; ?></td>
			</tr>
            <tr>
				<td><h5 class="normal">درصد </h5></td>
				<td style="padding: 5px 0 0 0;"><?php echo $percentage; ?></td>
			</tr>
            <tr>
                <td><h5 class="normal">تعداد </h5></td>
                <td style="padding: 5px 0 0 0;"><?php echo $number; ?></td>
            </tr>
            <tr>
                <td><h5 class="normal">استفاده‌شده </h5></td>
                <td style="padding: 5px 0 0 0;"><?php echo $used; ?></td>
            </tr>
            <tr>
                <td><h5 class="normal">حداقل قیمت </h5></td>
                <td style="padding: 5px 0 0 0;"><?php echo $min_price; ?> ریال</td>
            </tr>
            <tr>
				<td><h5 class="normal">تاریخ شروع </h5></td>
				<td style="padding: 5px 0 0 0;"><?php echo $start_date; ?></td>
			</tr>
			<tr>
				<td><h5 class="normal">تاریخ پایان </h5></td>
				<td style="padding: 5px 0 0 0;"><?php echo $end_date; ?></td>
			</tr>
            <tr>
				<td><h5 class="normal">توضیحات </h5></td>
				<td style="padding: 5px 0 0 0;"><?php echo $description; ?></td>
			</tr>
            <tr>
				<td><h5 class="normal">وضعیت </h5></td>
				<td style="padding: 5px 0 0 0;"><?php echo $discount_statuses[$status]; ?></td>
			</tr>
		</table>
	</div>
</div>
<?php include('footer.php'); ?>