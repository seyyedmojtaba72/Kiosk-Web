<?php 	

$action = filter_input(INPUT_GET, 'action', $filter = FILTER_SANITIZE_STRING);

if ($action == "send_confirmation"){
	require_once 'inc/db_connect.php';
	require_once 'inc/functions.php';
	ob_start();
	sec_session_start();
	require_once 'inc/information.php';
	require_once('inc/statistics.php');
	
	$mobile_number = filter_input(INPUT_GET, 'mobile_number', $filter = FILTER_SANITIZE_STRING);
	$code = filter_input(INPUT_GET, 'code', $filter = FILTER_SANITIZE_STRING);
	
	$today_sms_sent = mysql_select('text_messages', array('date'=>$date, 'receiver'=>$mobile_number), array('id'));

	if (sizeof($today_sms_sent) >= 3){
		echo "تعداد دفعات مجاز برای امروز به حداکثر رسیده است.";
	} else{
		// SMS
			
		$to = $mobile_number;		
		$sms_body = str_replace("%CODE%", $code, $options['confirmation_sms_text']);
		send_sms($to, $sms_body);

		echo "کد فعال‌سازی ارسال شد.";
	} 
	return;
} else if ($action == "confirm_mobile_number"){
	require_once 'inc/db_connect.php';
	require_once 'inc/functions.php';
	ob_start();
	sec_session_start();
	require_once 'inc/information.php';
	require_once('inc/statistics.php');
	
	$email = filter_input(INPUT_GET, 'email', $filter = FILTER_SANITIZE_STRING);
	$mobile_number = filter_input(INPUT_GET, 'mobile_number', $filter = FILTER_SANITIZE_STRING);
	
	global $mysqli;
	$stmt = $mysqli->prepare("UPDATE members SET mobile_number_status=1 WHERE email=\"".$email."\"");
	
	$username = $_SESSION["username"];
	
	$name = $_SESSION["first_name"]." ".$_SESSION[$last_name];
	if ($name == " "){
		$name = $username;
	}
		
	if ($stmt->execute()){
		// SMS
			
		$to = $mobile_number;		
		$sms_body = str_replace("%NAME%", $name, str_replace("%USERNAME%", $username, $options['welcome_sms_text']));
		send_sms($to, $sms_body);
		
		echo "شماره موبایل شما تایید شد.";
	} else{
		echo "fail";
	}
	

	$_SESSION['mobile_number_status'] = 1;
	
	$stmt->fetch();
	$stmt->close();

	return;
}

require_once('header.php');
?>

<?php if ($logged != 'in'){header("Location: login.php?redirect=".urlencode("http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']));} ?>	

<?php
if (!empty($err)) {
	switch ($err) {
	
	case "invalid-username" : $err_msg = "نام کاربری باید شامل حروف انگلیسی و اعداد باشد!"; break;
	case "username-exists" : $err_msg = "کاربری با این نام کاربری وجود دارد!"; break;
	case "min-length" : $err_msg = "طول نام کاربری و گذواژه باید حداقل 6 کاراکتر باشد!"; break;
	case "max-username-length" : $err_msg = "طول نام کاربری باید حداکثر 50 کاراکتر باشد!"; break;
	case "max-email-length" : $err_msg = "طول آدرس ایمیل باید حداکثر 50 کاراکتر باشد!"; break;
	case "mobile_number-length" : $err_msg = "طول شماره موبایل باید 10 حرف باشد!"; break;
	case "invalid" : $err_msg = "فرمت تصویر قابل پشتیبانی نیست!"; break;
	case "fill" : $err_msg = "موارد الزامی را پر کنید!"; break;
	case "empty" : $err_msg = "تصویری را انتخاب کنید!"; break;
	case "size" : $err_msg = "حجم فایل رعایت نشده است!"; break;
	case "type" : $err_msg = "پسوند فایل رعایت نشده است!"; break;
	case "upload" : $err_msg = "خطا در آپلود فایل. مجوزها را بررسی کنید!"; break;
	case "con-oldpsw" : $err_msg = "گذرواژه قبلی شما درست نیست!"; break;
	case "invalid-email" : $err_msg = "ایمیل وارد شده معتبر نیست!"; break;

	case "email-exists" : $err_msg = "کاربری با این ایمیل وجود دارد!"; break;
	case "database" : $err_msg = "خطای پایگاه داده!"; break;
	case "insert" : $err_msg = "خطا در ویرایش پایگاه داده!"; break;
	
	default : $err_msg = "خطای غیر منتظره‌ای پیش آمده!"; break;
	}
	
}
?> 
<?php
if (!empty($suc)) {
	switch ($suc) {
		
	case "upload" : $suc_msg = "تصویر آپلود شد!"; break;
	case "delete" : $suc_msg = "تصویر حذف شد!"; break;
	case "update" : $suc_msg = "نمایه به‌روز شد!"; break;
	case "pay" : $suc_msg = "پرداخت با موفقیت انجام شد!"; $transaction_id = filter_input(INPUT_GET, 'transaction_id', $filter = FILTER_SANITIZE_STRING); if (!empty($transaction_id)){$suc_msg.=" شماره تراکنش: ".$transaction_id;} break;
	
	default : $suc_msg = "عملیات با موفقیت انجام شد!"; break;
	}
	
}
?> 
<?php
if (!empty($err_msg)) {
	echo '<div class="alert alert-error no-print"><a href="'.str_replace('err='.$err,"",implode('&',array_unique(explode('&',$_SERVER['REQUEST_URI'])))).'"><button type="button" class="close">&times;</button></a><p>'. $err_msg .'</p></div>';
}
?>
<?php
if (!empty($suc_msg)) {
	echo '<div class="alert alert-success no-print"><a href="'.str_replace('suc='.$suc,"",implode('&',array_unique(explode('&',$_SERVER['REQUEST_URI'])))).'"><button type="button" class="close">&times;</button></a><p>'. $suc_msg .'</p></div>';
}
?>
<script type="text/javascript" src="<?php echo $options['url'] ?>/js/pwstrength.js"></script>
<script type="text/javascript">
jQuery(document).ready(function () {
	"use strict";
	var options = {};
	options.common = {
		onLoad: function () {
			$('#messages').text('Start typing password');
		}
	};
	options.ui = {
		showPopover: true,
		bootstrap2: true,
		showErrors: false
	};
	$('#password').pwstrength(options);
});
</script> 
<div class="container" style="width: 350px;">
	<div class="pull-left">
		<a href="panel.php"><button class='btn'><span>بازگشت</span> <i class="icon-chevron-left"></i></button></a>
	</div>
	<button class="btn disabled"><span id="subtitle">نمایه</span></button><br /><br />
	<div id="main-content" style="margin-right: 20px;">
		<div class="text-left" style="width:300px;">
			<ul class="nav nav-tabs">
				<li class="pull-right active"><a href="#basic-data" data-toggle="tab">اطلاعات پایه</a></li>
				<li class="pull-right"><a href="#change-password" data-toggle="tab">گذرواژه</a></li>
                <li class="pull-right"><a href="#profile-picture" data-toggle="tab">تصویر</a></li>
                <li class="pull-right"><a href="#wallet" data-toggle="tab">کیف پول</a></li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active" id="basic-data">
					<form action="<?php echo $options["url"] ?>/inc/change_profile.php" method="post" name="profile_form">
						<input type="hidden" value="profile.php?" id="redirect" name="redirect" />
                        <input type="hidden" value="<?php echo $_SESSION; ?>" id="session" name="session[]" />
						<input type="hidden" value="<?php echo $email ?>" id="former_email" name="former_email" />
                        <input type="hidden" value="<?php echo $mobile_number ?>" id="former_mobile_number" name="former_mobile_number" />
						<h6 class="normal span1 pull-right text-right">نام کاربری</h6>
						<input type="text" placeholder="" maxlength="100" value="<?php echo $username ?>" class="normal text-right span2" id="username" name="username" style="font: normal 12px 'WYekan',B Yekan"  />
						<h6 class="normal span1 text-right pull-right">ایمیل <span class="red">*</span></h6>
						<input type="email" placeholder="" value="<?php echo $email ?>" class="normal text-right span2 Yekan" id="email" name="email" style="font: normal 12px 'WYekan',B Yekan" />
                        <h6 class="normal span1 pull-right text-right">نقش</h6>
						<?php
						if (isset($member_roles[$role])){
							$role_title = $member_roles[$role];
						} else {
							$stmt = $mysqli->prepare('SELECT role_title FROM members_roles WHERE slug="'.$role.'"');
							$stmt->execute();	
							$stmt->bind_result($role_title);
							$stmt->fetch();
							$stmt->close();
						} ?>
						<input type="text" placeholder="" maxlength="100" value="<?php echo $role_title ?>" class="normal text-right span2" id="role" name="role" style="font: normal 12px 'WYekan',B Yekan" readonly />
                        <h6 class="normal span1 pull-right text-right">نام</h6>
                        <input type="text" placeholder="" maxlength="100" value="<?php echo $first_name ?>" class="normal text-right span2 Yekan" id="first_name" name="first_name" style="font: normal 12px 'WYekan',B Yekan" />
                        <h6 class="normal span1 pull-right text-right">نام خانوادگی</h6>
                        <input type="text" placeholder="" maxlength="100" value="<?php echo $last_name ?>" class="normal text-right span2" id="last_name" name="last_name" style="font: normal 12px 'WYekan',B Yekan" />
                        <h6 class="normal span1 pull-right text-right">شماره موبایل</h6>
						<h6 class="normal pull-left">98+</h6>
						<input type="number" placeholder="" maxlength="10" value="<?php echo $mobile_number ?>" class="normal text-right" style="width: 124px; margin-left: 12px; font: normal 12px 'WYekan',B Yekan" id="mobile_number" name="mobile_number"/>
						<?php if ($mobile_number_status == "0"){ ?>
                        <div id="code_section">
                            <h6 class="normal span1 pull-right text-right"></h6>
                            <div id="send-code">
                                <button type="button" class="btn btn-primary" onClick="send_code();"><span>ارسال کد فعال‌سازی</span> <i class="icon-envelope icon-white"></i></button> 
                            </div>
                            <div id="confirm-code">
                                <input type="text" placeholder="کد فعال‌سازی" maxlength="10" value="" class="normal text-right" style="width: 80px; font: normal 12px 'WYekan',B Yekan; margin: 5px;" id="code" name="code"/>
                                <button type="button" class="btn btn-success" onClick="confirm_code();"><span>تایید</span> <i class="icon-ok icon-white"></i></button>
                            </div><br />
                            <div class="clearfix"></div>
                        </div>
                        <?php } ?>
                        <h6 class="normal span1 pull-right text-right">شماره منزل</h6>
						<h6 class="normal pull-left">98+</h6>
						<input type="number" placeholder="" maxlength="10" value="<?php echo $home_number ?>" class="normal text-right" style="width: 124px; margin-left: 12px; font: normal 12px 'WYekan',B Yekan" id="home_number" name="home_number"/>
                        <h6 class="normal span1 pull-right text-right">شماره فکس</h6>
						<h6 class="normal pull-left">98+</h6>
						<input type="number" placeholder="" maxlength="10" value="<?php echo $fax_number ?>" class="normal text-right" style="width: 124px; margin-left: 12px; font: normal 12px 'WYekan',B Yekan" id="fax_number" name="fax_number"/>
                        <h6 class="normal span1 pull-right text-right">شرکت</h6>
                        <input type="text" placeholder="" maxlength="100" value="<?php echo $office ?>" class="normal text-right span2" id="office" name="office" style="font: normal 12px 'WYekan',B Yekan" />
                        <h6 class="normal span1 pull-right text-right">شماره شبا</h6>
                        <input type="text" placeholder="" maxlength="100" value="<?php echo $shaba ?>" class="normal text-right span2" id="shaba" name="shaba" style="font: normal 12px 'WYekan',B Yekan" />
                        <h6 class="normal span1 pull-right text-right">آدرس</h6>
                        <textarea maxlength="500" class="normal text-right span2" id="address" name="address" style="font: normal 12px 'WYekan',B Yekan"><?php echo $address ?></textarea>
                        <h6 class="normal span1 pull-right text-right">درباره</h6>
                        <textarea maxlength="500" class="normal text-right span2" id="about" name="about" style="font: normal 12px 'WYekan',B Yekan"><?php echo $about ?></textarea>
                        <h6 class="normal span1 pull-right text-right">تنظیمات اضافی</h6>
                         
                        <h6 class="normal span2 pull-right text-right" id="private"><input id="private-check" name="extras[]" type="checkbox" value="private" <?php if (in_array("private", explode(";", $extras))){ echo 'checked="checked"'; } ?> style="font: normal 12px 'WYekan',B Yekan">&ensp;حساب خصوصی</option></h6>
                           
						<button type="submit" name="submit" class="btn btn-info"><span>ذخیره</span> <i class="icon-edit icon-white"></i></button>
					</form>
				</div>
                <div class="tab-pane" id="change-password">
					<form action="<?php echo $options["url"] ?>/inc/change_password.php" method="post">
						<input type="hidden" class="span2" name="email" id="email" value="<?php echo $email; ?>" />
						<input type="hidden" class="span2" name="salt" id="salt" value="<?php echo $salt; ?>" />
						<input type="hidden" class="span2" name="former_password" id="former_password" value="<?php echo $password; ?>" />
						<h6 class="normal span1 pull-right text-right">گذرواژه قبلی</h6>
						<input type="password" class="normal text-right span2" style="font: normal 12px 'WYekan',B Yekan" name="old_password" id="old_password" placeholder="" />
						<h6 class="normal span1 pull-right text-right">گذرواژه</h6>
						<input type="password" class="normal text-right span2" style="font: normal 12px 'WYekan',B Yekan" name="password" id="password" placeholder="" />
						<h6 class="normal span1 pull-right text-right">تأیید گذرواژه</h6>
						<input type="password" class="normal text-right span2" style="font: normal 12px 'WYekan',B Yekan" name="confirm_password" id="confirm_password" placeholder="" />
						
						<button type="submit" name="submit" class="btn btn-info" onClick="return passformhash(this.form,this.form.old_password,this.form.password,this.form.confirm_password);"><span>تغییر</span> <i class="icon-edit icon-white"></i></button> 
					</form>
				</div>
				<div class="tab-pane" id="profile-picture">
					<div class="alert text-right">
						<ul>
							<li>حجم تصویر باید کمتر از <?php echo $options['profile_image_max_file_size'] ?> کیلوبایت باشد.</li>
							<li>فرمت تصویر باید یکی از موارد زیر باشد:
							<ul>
								<li>jpg, jpeg, png, gif</li>
							</ul>	
						</ul>
					</div>
					<a class="fancybox" href="<?php if (if_file_exists('files/profile_images/'.$profile_image)){ echo 'files/profile_images/'.$profile_image; } else{ echo 'img/profile_image.png'; } ?>">
						<img class="img-circle span2 pull-left" style="box-shadow: 0px 0px 5px #000;" src="<?php if (if_file_exists('files/profile_images/'.$profile_image)){ echo 'files/profile_images/'.$profile_image; } else{ echo 'img/profile_image.png'; } ?>" >
					</a>
					<form action="<?php echo $options["url"] ?>/inc/upload_file.php" method="post" enctype="multipart/form-data">
						<input type="hidden" value="profile.php?" name="redirect"  />
						<input type="hidden" value="profile_images" name="directory" />
						<input type="hidden" value="TRUE" name="replace" />
						
						<input type="hidden" name="file_types" value="jpg jpeg png gif" />
						<input type="hidden" name="upload_max_file_size" value="<?php echo ($options['profile_image_max_file_size']*1024) ?>" />
						
						<input type="hidden" value="id" name="idd"  />
						<input type="hidden" value="member" name="model"  />
						<input type="hidden" value="<?php echo $id ?>" name="value"  />
                        
						<input type="file" name="file" id="file" class="pull-right">
						
						<button type="submit" name="submit" class="pull-right btn btn-info"><span>آپلود</span> <i class="icon-upload icon-white"></i></button>
					
					</form>
					<?php if (if_file_exists('files/profile_images/'.$profile_image)){?>
						<form action="<?php echo $options["url"] ?>/inc/delete_file.php" method="post">
							<input type="hidden" value="profile.php?" id="redirect" name="redirect" />
							<input type="hidden" value="profile_images" name="directory" />
							<input type="hidden" value="<?php echo $profile_image ?>" id="file" name="file"  />
							
							<input type="hidden" value="username" id="idd" name="idd"  />
							<input type="hidden" value="member" id="model" name="model"  />
							<input type="hidden" value="<?php echo $username ?>" id="value" name="value"  />
							
							<button type="submit" name="submit" class="pull-left btn btn-danger"><span>حذف</span> <i class="icon-trash icon-white"></i></button>
						</form>
					<?php } ?>
					
				</div>
                <div class="tab-pane" id="wallet">
					<h6 class="normal span1 pull-right text-right">موجودی</h6>
						<input type="number" placeholder="" maxlength="10" value="<?php echo $balance ?>" class="normal text-right span2" id="balance" name="balance" style="width: 126px; margin-left: 12px; font: normal 12px 'WYekan',B Yekan;" readonly />
                        <h6 class="normal pull-left">ریال</h6>
                        <input type="number" placeholder="مبلغ" maxlength="10" value="" class="normal text-right" style="width: 80px; font: normal 12px 'WYekan',B Yekan; margin: 5px;" id="price" name="price"/>
                        <button type="button" class="btn btn-success" onClick="charge();"><span>شارژ</span> <i class="icon-plus icon-white"></i></button>
				</div>
			</div>
			
		</div>	
	</div>	
</div>
<script>
$("#confirm-code").hide();
var code = "";
function send_code(){
	$("#send-code").hide();
	$("#confirm-code").show();
	code = '<?php echo randomNumberPassword(5); ?>';
	$.get("profile.php?action=send_confirmation&mobile_number=<?php echo $mobile_number; ?>&code="+code, function(data, status){
		fancyAlert(data);
	});
}

function confirm_code(){
	if ($("#code").val() == code){
		$.get("profile.php?action=confirm_mobile_number&mobile_number=<?php echo $mobile_number; ?>&email=<?php echo $email; ?>", function(data, status){
			fancyAlert(data);
		});	
		$("#code_section").hide();
		
	} else{
		fancyAlert("کد وارد شده اشتباه است.");
	}
}

function charge(){
	var price = $("#price").val();
	if (price >= 1000){
		window.location.href = "online_payment.php?action=charge_balance&customer="+"<?php echo $_SESSION['id']; ?>"+"&amount="+price+"&redirect=profile.php?";		
	} else{
		fancyAlert("مبلغ باید حداقل 1000 ریال باشد.");
	}
}
</script>
<?php require_once('footer.php'); ?>