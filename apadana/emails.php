<?php include('header.php'); ?>

<?php if ($role == "admin" | if_has_permission($role,"edit_emails")){} else{header("Location: login.php?redirect=".urlencode("http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']));} ?>

<?php
if (!empty($err)) {
	switch ($err) {
	case "not-exists" : $err_msg = "ایمیل وجود ندارد! ممکن است حذف شده باشد."; break;
	case "delete" : $err_msg = "خطا در حذف!"; break;
	case "no-match" : $err_msg = "جستجو نتیجه‌ای نداشت! <a style='margin-right:50px;' href='emails.php'><button class='btn Yekan normal'>نمایش همه</button></a>"; break;
	
	default : $err_msg = "خطای غیر منتظره‌ای پیش آمده!"; break;
	}
	
}
?> 
<?php
if (!empty($suc)) {
	switch ($suc) {
	case "delete" : $suc_msg = "ایمیل حذف شد!"; break;
	case "search" : $suc_msg = "جستجو با موفقیت انجام شد! <a style='margin-right:50px;' href='emails.php'><button class='btn Yekan normal'>نمایش همه</button></a>"; break;
	
	default : $suc_msg = "عملیات با موفقیت انجام شد!"; break;
	}
	
}
?> 
<?php
if (!empty($err_msg)) {
	echo '<div class="alert alert-error no-print"><a href="'.str_replace('err='.$err,"",implode('&',array_unique(explode('&',$_SERVER['REQUEST_URI'])))).'"><button type="button" class="close">&times;</button></a><p>'. $err_msg .'</p></div>';
}
?>
<?php
if (!empty($suc_msg)) {
	echo '<div class="alert alert-success no-print"><a href="'.str_replace('suc='.$suc,"",implode('&',array_unique(explode('&',$_SERVER['REQUEST_URI'])))).'"><button type="button" class="close">&times;</button></a><p>'. $suc_msg .'</p></div>';
}
?>
<?php

$_SESSION['emails_redirect'] = 
str_replace(basename($_SERVER['PHP_SELF']),basename($_SERVER['PHP_SELF'])."?",str_replace("?","",implode('&',array_unique(explode('&', $_SERVER['REQUEST_URI'])))));

// ------

$list_amount = $options['list_rows_per_page'];
$search = filter_input(INPUT_GET, 'search', $filter = FILTER_SANITIZE_STRING);
$order_by = filter_input(INPUT_GET, 'order_by', $filter = FILTER_SANITIZE_STRING);
$mode = filter_input(INPUT_GET, 'mode', $filter = FILTER_SANITIZE_STRING);
$page = filter_input(INPUT_GET, 'page', $filter = FILTER_SANITIZE_STRING);

if (empty($search)){$search = "false";}
if (empty($order_by)){$order_by = 'row';}
if (empty($mode)){$mode = 'DESC';}
if (empty($page)){$page = 1;}

// ------

$receiver = filter_input(INPUT_GET, 'receiver', $filter = FILTER_SANITIZE_STRING);
$subject = filter_input(INPUT_GET, 'subject', $filter = FILTER_SANITIZE_STRING);
$date = filter_input(INPUT_GET, 'date', $filter = FILTER_SANITIZE_STRING);
$time = filter_input(INPUT_GET, 'time', $filter = FILTER_SANITIZE_STRING);

function get_link($order_by,$mode,$page){
	global $options, $list_amount, $search;
	global $receiver, $subject, $date, $time;

	$link="emails.php?";
	if ($search=="true"){
		
		$link.='suc=search&search=true&receiver='.$receiver.'&subject='.$subject.'&date='.$date.'&time='.$time.'&';		
	} 
	
	$link.="order_by=".$order_by."&mode=".$mode;
	if ($page!=0){$link.="&page=".$page;}
	
	return $link;
}
?>
	
	
<div class="container">
	<div class="pull-left no-print">
		<button class='btn btn-success' onclick="print();"><span>چاپ</span> <i class="icon-print icon-white"></i></button>
		<a href="email_add.php"><button class='btn btn-primary'><span>اضافه‌کردن</span> <i class="icon-plus icon-white"></i></button></a>
		<a href="panel.php"><button class='btn'><span>بازگشت</span> <i class="icon-chevron-left"></i></button></a>
	</div>
    <button class="btn disabled"><span id="subtitle">ایمیل‌ها</span></button>
    <a href="send_email.php"><button class='btn btn-primary'><span>ارسال ایمیل</span> <i class="icon-envelope icon-white"></i></button></a><br />
	<div class="pull-right">
		<?php
		if ($search=="true"){
	
			$statement = 'SELECT * FROM emails WHERE';
			
			if (!empty($receiver)){ $statement .= ' receiver LIKE "%'.$receiver .'%" AND'; }
			if (!empty($subject)){ $statement .= ' subject LIKE "%'.$subject.'%" AND'; }
			if (!empty($date)){ $statement .= ' date LIKE "%'.$date .'%" AND'; }
			if (!empty($time)){ $statement .= ' time = "'.$time .'" AND'; }
			
			
			if (substr($statement,strlen($statement)-3,strlen($statement))=="AND"){
				$statement = substr($statement,0,strlen($statement)-3);
			}
			
			if (substr($statement,strlen($statement)-5,strlen($statement))=="WHERE"){
				$statement = substr($statement,0,strlen($statement)-5);
			}
						
			
			$result = $mysqli->query($statement);
		
			$mutch = $result->num_rows;
		} else {
			$statement = 'SELECT * FROM emails';
			
			$result = $mysqli->query($statement);
		
			$mutch = $result->num_rows;
		}
		$pages = floor(($mutch-1)/$list_amount)+1;
		?>
		<div style="margin: 10px 0;">
            <?php if ($mutch>0){ ?>
                <a class="no-print" href="<?php echo get_link($order_by,$mode,$page-1); ?>"><button class="btn btn-small btn-inverse <?php if ($page==1) { echo 'disabled'; } ?>" <?php if ($page==1) { echo 'disabled="disabled"'; } ?>><i class="icon-chevron-right icon-white"></i></button></a>
                &emsp;
                <span class="h5">
                    <?php echo $mutch ?> مورد یافت شد
                    &emsp;///&emsp;
                    نمایش موارد <?php echo (($page-1)*$list_amount+1).' تا '.min($page*$list_amount,$mutch) ?>
                    &emsp;///&emsp;
                    صفحه‌ی
                    &ensp;
                    <select class="tahoma size-11" style="margin-top: 10px; width: 50px; height: 25px;" name="pagg" id="pagg" onChange='go_to_page("<?php echo get_link($order_by,$mode,0); ?>","pagg");'>
                        <?php
                        for ($i=0;$i<$pages;$i++){
                            echo '<option ';
                            if ($page==$i+1){ echo 'selected="selected" '; }
                            echo 'value='.($i+1).'>'.($i+1).'</option>';
                        }
                        ?>
                    </select>
                    &ensp;
                    <?php echo ' از '.$pages ?>
                </span>
                &emsp;
                <a class="no-print" href="<?php echo get_link($order_by,$mode,$page+1); ?>"><button class="btn btn-small btn-inverse <?php if ($page==$pages) { echo 'disabled'; } ?>" <?php if ($page==$pages) { echo 'disabled="disabled"'; } ?>><i class="icon-chevron-left icon-white"></i></button></a>
          	<?php } ?>
    	</div>
	</div>
    <div id="batch" class="no-print pull-left" style="margin: 15px 0; display: none;">
		<button class="btn btn-danger" onClick="batchDelete();"><span>حذف</span> <i class="icon-trash icon-white"></i></button>
		<button class="btn btn-info" onClick="batchEdit();"><span>ويرايش</span> <i class="icon-edit icon-white"></i></button>
	</div>
	<div class="clearfix"></div>
	<form action="<?php echo $options["url"] ?>/inc/search_email.php" method="post">
		<input type="hidden" value="emails.php?order_by=<?php echo $order_by ?>&mode=<?php echo $mode ?>&" name="redirect" id="redirect" />
		<table class="table table-striped table-hover text-center" style="margin-bottom:-20px;">
			<td style="width: 50px;"><h5><input id="batch_check" type="checkbox" style="margin: 5px 0 0px 5px;" onChange="batchCheck();" /></h5></td>
            <td style="width: 170px;"><h5>
                <select class="tahoma size-11" style="width:150px;" name="receiver" id="receiver">
                    <option value="" selected="selected"></option>
                    <?php
                    
                    $result = $mysqli->query("SELECT * FROM members");
                    $rows = $result->num_rows;
                    
                    for ($i=0;$i<$rows;$i++){
                        $stmt = $mysqli->prepare("SELECT email, first_name, last_name FROM members LIMIT 1 OFFSET ?");
                        $stmt->bind_param('s', $i);
                        $stmt->execute();
                        $stmt->store_result();
                 
                        $stmt->bind_result($email, $receiver_first_name, $receiver_last_name);
                        $stmt->fetch();
                        $stmt->close();
                        
                        $display_name = $receiver_first_name." ".$receiver_last_name;
                        if ($display_name == " "){
                            $display_name = $email;
                        }
                        echo '<option value="'.$email.'" ';
                        if ($receiver == $email){echo 'selected="selected"';}
                        echo '>';
                        echo $display_name.'</option>';
                    }
                    ?>
                </select>
            </h5></td>
			<td style="width: 150px;"><h5><input type="text" style="font: normal 11px tahoma; width:130px;" 
			value="<?php echo $subject; ?>" name="subject" id="subject" maxlength="300" /></h5></td>
            <td style="width: 100px;"><h5>
            	<input type="text" style="font: normal 11px tahoma; width:80px;" value="<?php echo $date ?>" name="date" id="date" maxlength="20" />
            </h5></td>
            <td style="width: 100px;"><h5>
            	<input type="text" style="font: normal 11px tahoma; width:80px;" value="<?php echo $time ?>" name="time" id="time" maxlength="20" />
            </h5></td>
			<td class="no-print" style="width: 80px;"><h4 class='normal'>
				<button class='btn btn-info' type="submit"><i class="icon-search"></i></button>
			</h4></td>
		</table>
	</form>
	<table class="table table-striped table-hover text-center">
		<tr>
			<td style="width: 50px;"><h5 class="normal ">ردیف <br />
			<a href="<?php echo get_link('row','ASC',$page); ?>" class="<?php if ($order_by=="row" && $mode=="ASC"){ echo "red_outline "; } ?>icon-chevron-up no-print"></a><a href="<?php echo get_link('row','DESC',$page); ?>" class="<?php if ($order_by=="row" && $mode=="DESC"){ echo "red_outline "; } ?>icon-chevron-down no-print"></a></h5></td>
			<td style="width: 170px;"><h5 class="normal">دریافت‌کننده <br />
			<a href="<?php echo get_link('receiver','ASC',$page); ?>" class="<?php if ($order_by=="receiver" && $mode=="ASC"){ echo "red_outline "; } ?>icon-chevron-up no-print"></a><a href="<?php echo get_link('receiver','DESC',$page); ?>" class="<?php if ($order_by=="receiver" && $mode=="DESC"){ echo "red_outline "; } ?>icon-chevron-down no-print"></a></h5></td>
			<td style="width: 150px;"><h5 class="normal">موضوع <br />
			<a href="<?php echo get_link('subject','ASC',$page); ?>" class="<?php if ($order_by=="subject" && $mode=="ASC"){ echo "red_outline "; } ?>icon-chevron-up no-print"></a><a href="<?php echo get_link('subject','DESC',$page); ?>" class="<?php if ($order_by=="subject" && $mode=="DESC"){ echo "red_outline "; } ?>icon-chevron-down no-print"></a></h5></td>
            <td style="width: 100px;"><h5 class="normal">تاریخ <br />
			<a href="<?php echo get_link('date','ASC',$page); ?>" class="<?php if ($order_by=="date" && $mode=="ASC"){ echo "red_outline "; } ?>icon-chevron-up no-print"></a><a href="<?php echo get_link('date','DESC',$page); ?>" class="<?php if ($order_by=="date" && $mode=="DESC"){ echo "red_outline "; } ?>icon-chevron-down no-print"></a></h5></td>
            <td style="width: 100px;"><h5 class="normal">زمان <br />
			<a href="<?php echo get_link('time','ASC',$page); ?>" class="<?php if ($order_by=="time" && $mode=="ASC"){ echo "red_outline "; } ?>icon-chevron-up no-print"></a><a href="<?php echo get_link('time','DESC',$page); ?>" class="<?php if ($order_by=="time" && $mode=="DESC"){ echo "red_outline "; } ?>icon-chevron-down no-print"></a></h5></td>
			<td class="no-print" style="width: 80px;"><h5 class="normal">عملیات</h5></td>
		</tr>
		
		<?php
		
		if ($order_by == "row"){ $order_by = "id"; }
		
		if ($search=="true"){
			
			$statement .= ' ORDER BY '.$order_by.' '.$mode;
				
			$result = $mysqli->query($statement.' LIMIT '.(($page-1)*$list_amount).' , '.$list_amount);
		
			$rows = $result->num_rows;
			
			for ($i=0;$i<$rows;$i++){
			$statement2 = str_replace("SELECT *","SELECT id, receiver, subject, date, time",$statement);
			$statement2 .= ' LIMIT '.((($page-1)*$list_amount+$i).' , 1');

			$stmt = $mysqli->prepare($statement2);
			$stmt->execute();
			$stmt->store_result();
	 
			$stmt->bind_result($id, $receiver, $subject, $date, $time);
			$stmt->fetch();
			$stmt->close();
			
			/* RECEIVER */
			
			$stmt = $mysqli->prepare("SELECT id, email, first_name, last_name FROM members WHERE email='".$receiver."'");
			$stmt->execute();
			$stmt->store_result();
			$stmt->bind_result($receiver_id, $receiver_email, $receiver_first_name, $receiver_last_name);
			$stmt->fetch();
			$nums = $stmt->num_rows;
			$stmt->close();
			
			
			$receiver_display_name = $receiver_first_name." ".$receiver_last_name;
			if ($receiver_display_name == " "){
				$receiver_display_name = $receiver_email;
			}
			
			$receiver_link = '<a href="member_edit.php?id='.$receiver_id.'">'.$receiver_display_name.'</a>';
			
			if ($nums<=0){
				$receiver_link = $receiver;
			}
			
			?>
			
			<tr>
				<?php echo '
				<td><input name="checkbox[]" type="checkbox" style="margin: -1px 0 -1px 5px;" value="'.$id.'" onChange="loadBatch();" />'.(($page-1)*$list_amount+$i+1).'</td>
				<td>'.$receiver_link.'</td>
				<td>'.$subject.'</td>
				<td>'.$date.'</td>
				<td>'.$time.'</td>
				<td class="no-print">
				<a href="email_delete.php?id='.$id.'"><button class="btn btn-danger" style=" padding:0 2px;"><i class="icon-trash"></i></button></a>
				<a href="email_edit.php?id='.$id.'"><button class="btn btn-info" style=" padding:0 2px;"><i class="icon-edit"></i></button></a>
				</td>
				';
				?>
			</tr>
			<?php
			}
	
	 
		} else{

		$statement .= ' ORDER BY '.$order_by.' '.$mode;
		$statement .= ' LIMIT '.(($page-1)*$list_amount).' , '.$list_amount;
		
		$result = $mysqli->query($statement);
	
		$rows = $result->num_rows;

		for ($i=0;$i<$rows;$i++){
			$statement2 = 'SELECT id, receiver, subject, date, time FROM emails';
			$statement2 .= ' ORDER BY '.$order_by.' '.$mode;
			$statement2 .= ' LIMIT '.((($page-1)*$list_amount+$i).' , 1');
		
			$stmt = $mysqli->prepare($statement2);
			$stmt->execute();
			$stmt->store_result();
	 
			$stmt->bind_result($id, $receiver, $subject, $date, $time);
			$stmt->fetch();
			$stmt->close();
			
			/* RECEIVER */
			
			$stmt = $mysqli->prepare("SELECT id, email, first_name, last_name FROM members WHERE email='".$receiver."'");
			$stmt->execute();
			$stmt->store_result();
			$stmt->bind_result($receiver_id, $receiver_email, $receiver_first_name, $receiver_last_name);
			$stmt->fetch();
			$nums = $stmt->num_rows;
			$stmt->close();
			
			
			$receiver_display_name = $receiver_first_name." ".$receiver_last_name;
			if ($receiver_display_name == " "){
				$receiver_display_name = $receiver_email;
			}
			
			$receiver_link = '<a href="member_edit.php?id='.$receiver_id.'">'.$receiver_display_name.'</a>';
			
			if ($nums<=0){
				$receiver_link = $receiver;
			}
			
			?>
			
			<tr>
				<?php echo '
				<td><input name="checkbox[]" type="checkbox" style="margin: -1px 0 -1px 5px;" value="'.$id.'" onChange="loadBatch();" />'.(($page-1)*$list_amount+$i+1).'</td>
				<td>'.$receiver_link.'</td>
				<td>'.$subject.'</td>
				<td>'.$date.'</td>
				<td>'.$time.'</td>
				<td class="no-print">
				<a href="email_delete.php?id='.$id.'"><button class="btn btn-danger" style=" padding:0 2px;"><i class="icon-trash"></i></button></a>
				<a href="email_edit.php?id='.$id.'"><button class="btn btn-info" style=" padding:0 2px;"><i class="icon-edit"></i></button></a>
				</td>
				';
				?>
			</tr>
			<?php
			}
		}
		?>
	</table>
	<?php if ($mutch == 0){
		echo '<div class="alert alert-warning no-print"><p>موردی یافت نشد!</i></p></div>';
	} ?>
</div>
<script>
function loadBatch(){
	var values = new Array();
	$.each($("input[name='checkbox[]']:checked"), function() {
		values.push($(this).val());
	});
	
	if (values.length > 0){
		$("#batch").fadeIn();
	} else {
		$("#batch").fadeOut();
	}
}

function batchEdit(){
	$.each($("input[name='checkbox[]']:checked"), function() {
		window.open('email_edit.php?id='+$(this).val(), '_blank');
	});

}

function batchDelete(){
	$.each($("input[name='checkbox[]']:checked"), function() {
			window.open('email_delete.php?id='+$(this).val(), '_blank');
	});
}

function batchCheck(){
	if ($("#batch_check").is(":checked")){
		$.each($("input[name='checkbox[]']"), function() {
			$(this).prop("checked", true);
		});
	} else{
		$.each($("input[name='checkbox[]']"), function() {
			$(this).prop("checked", false);
		});
	}
	
	loadBatch();
}
</script>
<?php include('footer.php'); ?>