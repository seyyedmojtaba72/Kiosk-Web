<?php 
require_once('header.php'); 
?>

<?php if ($role != "admin"){header("Location: login.php?redirect=".urlencode("http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']));} ?>

<?php
if (!empty($err)) {
	switch ($err) {	
	case "not-exists" : $err_msg = "کاربر وجود ندارد! ممکن است حذف شده باشد."; break;
	case "delete" : $err_msg = "خطا در حذف!"; break;
	case "no-match" : $err_msg = "جستجو نتیجه‌ای نداشت! <a style='margin-right:50px;' href='members.php'><button class='btn Yekan normal'>نمایش همه</button></a>"; break;
	
	default : $err_msg = "خطای غیر منتظره‌ای پیش آمده!"; break;
	}
	
}
?> 
<?php
if (!empty($suc)) {
	switch ($suc) {	
	case "delete" : $suc_msg = "کاربر حذف شد!"; break;
	case "search" : $suc_msg = "جستجو با موفقیت انجام شد! <a style='margin-right:50px;' href='members.php'><button class='btn Yekan normal'>نمایش همه</button></a>"; break;
	
	default : $suc_msg = "عملیات با موفقیت انجام شد!"; break;
	}
	
}
?> 
<?php
if (!empty($err_msg)) {
	echo '<div class="alert alert-error no-print"><a href="'.str_replace('err='.$err,"",implode('&',array_unique(explode('&',$_SERVER['REQUEST_URI'])))).'"><button type="button" class="close">&times;</button></a><p>'. $err_msg .'</p></div>';
}
?>
<?php
if (!empty($suc_msg)) {
	echo '<div class="alert alert-success no-print"><a href="'.str_replace('suc='.$suc,"",implode('&',array_unique(explode('&',$_SERVER['REQUEST_URI'])))).'"><button type="button" class="close">&times;</button></a><p>'. $suc_msg .'</p></div>';
}
?>
<?php

$_SESSION['members_redirect'] = 
str_replace(basename($_SERVER['PHP_SELF']),basename($_SERVER['PHP_SELF'])."?",str_replace("?","",implode('&',array_unique(explode('&', $_SERVER['REQUEST_URI'])))));

// ------

$amount = $options['list_rows_per_page'];
$search = filter_input(INPUT_GET, 'search', $filter = FILTER_SANITIZE_STRING);
$order_by = filter_input(INPUT_GET, 'order_by', $filter = FILTER_SANITIZE_STRING);
$mode = filter_input(INPUT_GET, 'mode', $filter = FILTER_SANITIZE_STRING);
$page = filter_input(INPUT_GET, 'page', $filter = FILTER_SANITIZE_STRING);

if (empty($search)){$search = "false";}
if (empty($order_by)){$order_by = 'row';}
if (empty($mode)){$mode = 'DESC';}
if (empty($page)){$page = 1;}

// ------

$id = filter_input(INPUT_GET, 'id', $filter = FILTER_SANITIZE_STRING);
$username = filter_input(INPUT_GET, 'username', $filter = FILTER_SANITIZE_STRING);
$email = filter_input(INPUT_GET, 'email', $filter = FILTER_SANITIZE_STRING);
$first_name = filter_input(INPUT_GET, 'first_name', $filter = FILTER_SANITIZE_STRING);
$last_name = filter_input(INPUT_GET, 'last_name', $filter = FILTER_SANITIZE_STRING);
$mobile_number = filter_input(INPUT_GET, 'mobile_number', $filter = FILTER_SANITIZE_STRING);
$role = filter_input(INPUT_GET, 'role', $filter = FILTER_SANITIZE_STRING);
$status = filter_input(INPUT_GET, 'status', $filter = FILTER_SANITIZE_STRING);
$balance = filter_input(INPUT_GET, 'balance', $filter = FILTER_SANITIZE_STRING);

// ------

function get_link($order_by,$mode,$page){
	global $options, $amount, $search;
	global $id, $username, $email, $first_name, $last_name, $mobile_number, $role, $status, $balance;

	$link="members.php?";
	if ($search=="true"){
		$link.='suc=search&search=true&username='.$username.'&email='.$email.'&first_name='.$first_name.'&last_name='.$last_name.'&mobile_number='.$mobile_number.'&role='.$role.'&status='.$status.'&balance='.$balance.'&';		
	} 
	
	$link.="order_by=".$order_by."&mode=".$mode;
	if ($page!=0){$link.="&page=".$page;}
	
	return $link;
}
?>
	
	
<div class="container">
	<div class="pull-left no-print">
		<button class='btn btn-success' onClick="print();"><span>چاپ</span> <i class="icon-print icon-white"></i></button>
		<a href="member_add.php"><button class='btn btn-primary'><span>اضافه‌کردن</span> <i class="icon-plus icon-white"></i></button></a>
		<a href="panel.php"><button class='btn'><span>بازگشت</span> <i class="icon-chevron-left"></i></button></a>
	</div>
    <button class="btn disabled"><span id="subtitle">اعضا</span></button>
    <a href="members_roles.php"><button class='btn btn-primary'><span>نقش‌ها</span> <i class="icon-user icon-white"></i></button></a><br />
	<div class="pull-right">
		<?php
		if ($search=="true"){
	
			$statement = 'SELECT * FROM members WHERE';
			
			if (!empty($id)){ $statement .= ' id ="'.$id .'" AND'; }
			if (!empty($username)){ $statement .= ' username LIKE "%'.$username .'%" AND'; }
			if (!empty($email)){ $statement .= ' email LIKE "%'.$email.'%" AND'; }
			if (!empty($first_name)){ $statement .= ' first_name LIKE "%'.$first_name.'%" AND'; }
			if (!empty($last_name)){ $statement .= ' last_name LIKE "%'.$last_name.'%" AND'; }
			if (!empty($mobile_number)){ $statement .= ' mobile_number LIKE "%'.$mobile_number.'%" AND'; }
			if (!empty($role)){ $statement .= ' role LIKE "%'.$role.'%" AND'; }
			if (!empty($status)){ $statement .= ' status LIKE "%'.$status.'%" AND'; }
			if (!empty($balance)){ $statement .= ' balance ="'.$balance.'" AND'; }
			
			if (substr($statement,strlen($statement)-3,strlen($statement))=="AND"){
				$statement = substr($statement,0,strlen($statement)-3);
			}
			
			if (substr($statement,strlen($statement)-5,strlen($statement))=="WHERE"){
				$statement = substr($statement,0,strlen($statement)-5);
			}
						
			
			$result = $mysqli->query($statement);
		
			$mutch = $result->num_rows;
		} else {
			$statement = 'SELECT * FROM members';
			
			$result = $mysqli->query($statement);
		
			$mutch = $result->num_rows;
		}
		$pages = floor(($mutch-1)/$amount)+1;
		?>
		<div style="margin: 10px 0;">
            <?php if ($mutch>0){ ?>
                <a class="no-print" href="<?php echo get_link($order_by,$mode,$page-1); ?>"><button class="btn btn-small btn-inverse <?php if ($page==1) { echo 'disabled'; } ?>" <?php if ($page==1) { echo 'disabled="disabled"'; } ?>><i class="icon-chevron-right icon-white"></i></button></a>
                &emsp;
                <span class="h5">
                    <?php echo $mutch ?> مورد یافت شد
                    &emsp;///&emsp;
                    نمایش موارد <?php echo (($page-1)*$amount+1).' تا '.min($page*$amount,$mutch) ?>
                    &emsp;///&emsp;
                    صفحه‌ی
                    &ensp;
                    <select class="tahoma size-11" style="margin-top: 10px; width: 50px; height: 25px;" name="pagg" id="pagg" onChange='go_to_page("<?php echo get_link($order_by,$mode,0); ?>","pagg");'>
                        <?php
                        for ($i=0;$i<$pages;$i++){
                            echo '<option ';
                            if ($page==$i+1){ echo 'selected="selected" '; }
                            echo 'value='.($i+1).'>'.($i+1).'</option>';
                        }
                        ?>
                    </select>
                    &ensp;
                    <?php echo ' از '.$pages ?>
                </span>
                &emsp;
                <a class="no-print" href="<?php echo get_link($order_by,$mode,$page+1); ?>"><button class="btn btn-small btn-inverse <?php if ($page==$pages) { echo 'disabled'; } ?>" <?php if ($page==$pages) { echo 'disabled="disabled"'; } ?>><i class="icon-chevron-left icon-white"></i></button></a>
          	<?php } ?>
    	</div>	
	</div>
    <div id="batch" class="no-print pull-left" style="margin: 15px 0; display: none;">
        <button class="btn btn-danger" onClick="batchDelete();"><span>حذف</span> <i class="icon-trash icon-white"></i></button>
        <button class="btn btn-info" onClick="batchEdit();"><span>ویرایش</span> <i class="icon-edit icon-white"></i></button>
	</div>
    
	<div class="clearfix"></div>
	<form action="<?php echo $options["url"] ?>/inc/search_member.php" method="post">
		<input type="hidden" value="members.php?order_by=<?php echo $order_by ?>&mode=<?php echo $mode ?>&" name="redirect" id="redirect" />
		<table class="table table-striped table-hover text-center" style="margin-bottom:-20px;">
			<td style="width: 50px;"><h5><input id="batch_check" type="checkbox" style="margin: 7px 0 0px 5px;" onChange="batchCheck();" /></h5></td>
            <td style="width: 70px;"><h5><input type="number" maxlength="11" style="font: normal 11px tahoma; width:50px;" 
			value="<?php echo $id; ?>" name="id" id="id"/></h5></td>
			<td style="width: 90px;"><h5><input type="text" maxlength="10" style="font: normal 11px tahoma; width:70px;" 
			value="<?php echo $username; ?>" name="username" id="username"/></h5></td>
			<td style="width: 170px;"><h5><input type="email" style="font: normal 11px tahoma; width:150px;" 
			value="<?php echo $email; ?>" name="email" id="email"/></h5></td>
			<td style="width: 80px;"><h5><input type="text" style="font: normal 11px tahoma; width:60px;" 
			value="<?php echo $first_name; ?>" maxlength="100" name="first_name" id="first_name"/></h5></td>
			<td style="width: 100px;"><h5><input type="text" style="font: normal 11px tahoma; width:80px;" 
			value="<?php echo $last_name; ?>" maxlength="100" name="last_name" id="last_name"/></h5></td>
			<td style="width: 100px;"><h5><input type="text" style="font: normal 11px tahoma; width:80px;" 
			value="<?php echo $mobile_number; ?>" maxlength="10" name="mobile_number" id="mobile_number"/></h5></td>
			<td style="width: 100px;"><h5>
				<select class="tahoma size-11" style="width: 80px;" name="role" id="role">
					<option selected="selected" ></option>
					<?php
					foreach ($member_roles as $role1=>$role1_value){
						echo '<option ';
						if ($role1==$role){echo 'selected="selected" '; }
						echo 'value="'.$role1.'">'.$role1_value.'</option>';
					}
					?>
					<?php

					$stmt = $mysqli->prepare("select count(1) FROM members_roles");
									
					$stmt->execute();
					$stmt->bind_result($rows);
					$stmt->fetch();
					$stmt->close();

					for($i=0;$i<$rows;$i++){
						
						$stmt = $mysqli->prepare('SELECT slug, role_title FROM members_roles LIMIT 1 OFFSET '.$i);
						$stmt->execute();	
						$stmt->bind_result($slug, $role_title);
						$stmt->fetch();
						$stmt->close();
						
						echo '<option ';
						if ($role==$slug){echo 'selected="selected" '; }
						echo 'value="'.$slug.'">'.$role_title.'</option>';
	
					}
					?>
				</select>
			</h5></td>
			<td style="width: 100px;"><h5>
				<select class="tahoma size-11" style="width: 80px;" name="status" id="status">
					<option <?php if ($status == ""){ echo "selected='selected'"; } ?> value=""></option>
					<?php
					foreach ($member_statuses as $status1=>$status1_value){
						echo '<option ';
						if ($status1==$status){echo 'selected="selected" '; }
						echo 'value="'.$status1.'">'.$status1_value.'</option>';
					}
					?>
				</select>
			</h5></td>
            
			<td class="no-print" style="width: 50px;"><h4 class='normal'>
				<button class='btn btn-info' type="submit"><i class="icon-search"></i></button>
			</h4></td>
		</table>
	</form>
	<table class="table table-striped table-hover text-center">
		<tr>
			<td style="width: 50px;"><h5 class="normal ">ردیف <br />
			<a href="<?php echo get_link('row','ASC',$page); ?>" class="<?php if ($order_by=="row" && $mode=="ASC"){ echo "red_outline "; } ?>icon-chevron-up no-print"></a><a href="<?php echo get_link('row','DESC',$page); ?>" class="<?php if ($order_by=="row" && $mode=="DESC"){ echo "red_outline "; } ?>icon-chevron-down no-print"></a></h5></td>
            <td style="width: 70px;"><h5 class="normal">شناسه <br>
			<a href="<?php echo get_link('id','ASC',$page); ?>" class="<?php if ($order_by=="id" && $mode=="ASC"){ echo "red_outline "; } ?>icon-chevron-up no-print"></a><a href="<?php echo get_link('id','DESC',$page); ?>" class="<?php if ($order_by=="id" && $mode=="DESC"){ echo "red_outline "; } ?>icon-chevron-down no-print"></a></h5></td>
			<td style="width: 90px;"><h5 class="normal">نام کاربری <br>
			<a href="<?php echo get_link('username','ASC',$page); ?>" class="<?php if ($order_by=="username" && $mode=="ASC"){ echo "red_outline "; } ?>icon-chevron-up no-print"></a><a href="<?php echo get_link('username','DESC',$page); ?>" class="<?php if ($order_by=="username" && $mode=="DESC"){ echo "red_outline "; } ?>icon-chevron-down no-print"></a></h5></td>
			<td style="width: 170px;"><h5 class="normal">ایمیل <br>
			<a href="<?php echo get_link('email','ASC',$page); ?>" class="<?php if ($order_by=="email" && $mode=="ASC"){ echo "red_outline "; } ?>icon-chevron-up no-print"></a><a href="<?php echo get_link('email','DESC',$page); ?>" class="<?php if ($order_by=="email" && $mode=="DESC"){ echo "red_outline "; } ?>icon-chevron-down no-print"></a></h5></td>
			<td style="width: 80px;"><h5 class="normal">نام <br>
			<a href="<?php echo get_link('first_name','ASC',$page); ?>" class="<?php if ($order_by=="first_name" && $mode=="ASC"){ echo "red_outline "; } ?>icon-chevron-up no-print"></a><a href="<?php echo get_link('first_name','DESC',$page); ?>" class="<?php if ($order_by=="first_name" && $mode=="DESC"){ echo "red_outline "; } ?>icon-chevron-down no-print"></a></h5></td>
			<td style="width: 100px;"><h5 class="normal">نام خانوادگی <br>
			<a href="<?php echo get_link('last_name','ASC',$page); ?>" class="<?php if ($order_by=="last_name" && $mode=="ASC"){ echo "red_outline "; } ?>icon-chevron-up no-print"></a><a href="<?php echo get_link('last_name','DESC',$page); ?>" class="<?php if ($order_by=="last_name" && $mode=="DESC"){ echo "red_outline "; } ?>icon-chevron-down no-print"></a></h5></td>
			<td style="width: 100px;"><h5 class="normal">شماره موبایل <br>
			<a href="<?php echo get_link('mobile_number','ASC',$page); ?>" class="<?php if ($order_by=="mobile_number" && $mode=="ASC"){ echo "red_outline "; } ?>icon-chevron-up no-print"></a><a href="<?php echo get_link('mobile_number','DESC',$page); ?>" class="<?php if ($order_by=="mobile_number" && $mode=="DESC"){ echo "red_outline "; } ?>icon-chevron-down no-print"></a></h5></td>
			<td style="width: 100px;"><h5 class="normal">نقش <br>
			<a href="<?php echo get_link('role','ASC',$page); ?>" class="<?php if ($order_by=="role" && $mode=="ASC"){ echo "red_outline "; } ?>icon-chevron-up no-print"></a><a href="<?php echo get_link('role','DESC',$page); ?>" class="<?php if ($order_by=="role" && $mode=="DESC"){ echo "red_outline "; } ?>icon-chevron-down no-print"></a></h5></td>
			<td style="width: 100px;"><h5 class="normal">وضعیت <br>
			<a href="<?php echo get_link('status','ASC',$page); ?>" class="<?php if ($order_by=="status" && $mode=="ASC"){ echo "red_outline "; } ?>icon-chevron-up no-print"></a><a href="<?php echo get_link('status','DESC',$page); ?>" class="<?php if ($order_by=="status" && $mode=="DESC"){ echo "red_outline "; } ?>icon-chevron-down no-print"></a></h5></td>
			<!--<a href="<?php echo get_link('balance','ASC',$page); ?>" class="<?php if ($order_by=="balance" && $mode=="ASC"){ echo "red_outline "; } ?>icon-chevron-up no-print"></a><a href="<?php echo get_link('balance','DESC',$page); ?>" class="<?php if ($order_by=="balance" && $mode=="DESC"){ echo "red_outline "; } ?>icon-chevron-down no-print"></a>--></h5></td>
			<td class="no-print" style="width: 50px;"><h5 class="normal">عملیات</h5></td>
		</tr>
		
		<?php
		
		if ($order_by == "row"){ $order_by = "id"; }
		
		if ($search=="true"){
	
			$statement .= ' ORDER BY '.$order_by.' '.$mode;
			
			$result = $mysqli->query($statement.' LIMIT '.(($page-1)*$amount).' , '.$amount);
		
			$rows = $result->num_rows;
			
			for ($i=0;$i<$rows;$i++){
			$statement2 = str_replace("SELECT *","SELECT id, profile_image, username, email, first_name, last_name, mobile_number, mobile_number_status, role, status, balance",$statement);
			$statement2 .= ' LIMIT '.((($page-1)*$amount+$i).' , 1');

			$stmt = $mysqli->prepare($statement2);
			$stmt->execute();
			$stmt->store_result();
	 
			$stmt->bind_result($id, $member_profile_image, $username, $email, $first_name, $last_name, $mobile_number, $mobile_number_status, $role, $status, $balance);
			$stmt->fetch();
			$stmt->close();
			
			
			?>
			<tr<?php if ($level=="vip"){ echo ' class="info"'; } ?> onMouseOver="load_popup_image('<?php echo '#member_'.$i ?>','<?php if (if_file_exists("files/profile_images/".$member_profile_image)){ echo "files/profile_images/".$member_profile_image; } else{ echo "img/profile_image.png"; } ?>');" onMouseOut="dismiss_popup_image('<?php echo '#member_'.$i ?>');">
				<?php echo '
				<td><input id="member_'.$i.'" name="checkbox[]" type="checkbox" style="margin: -1px 0 -1px 5px;" value="'.$id.'" onChange="loadBatch();" />'.(($page-1)*$amount+$i+1).'</td>
				<td>'.$id.'</td>
				<td>'.$username.'</td>
				<td>'.$email.'</td>
				<td>'.$emoji->replaceEmojiWithImages($first_name).'</td>
				<td>'.$emoji->replaceEmojiWithImages($last_name).'</td>
				<td><span'; if ($mobile_number_status){ echo ' class="green"'; } echo '>98'.$mobile_number.'+</span></td>
				<td>';
				
				if (isset($member_roles[$role])){
					echo $member_roles[$role];
				} else {
					$stmt = $mysqli->prepare('SELECT role_title FROM members_roles WHERE slug="'.$role.'"');
					$stmt->execute();	
					$stmt->bind_result($role_title);
					$stmt->fetch();
					$stmt->close();
					
					echo $role_title;
				}
					
				echo '</td>
				<td>'.$member_statuses[$status].'</td>
				<td class="no-print">
				<a href="member_delete.php?id='.$id.'"><button class="btn btn-danger" style="padding:0 2px;"><i class="icon-trash"></i></button></a>
				<a href="member_edit.php?id='.$id.'"><button class="btn btn-info" style="padding:0 2px;"><i class="icon-edit"></i></button></a>
				</td>
				';
				?>
			</tr>
			<?php
			}
	
	 
		} else{
			
		$statement .= ' ORDER BY '.$order_by.' '.$mode;
		$statement .= ' LIMIT '.(($page-1)*$amount).' , '.$amount;
		
		$result = $mysqli->query($statement);
	
		$rows = $result->num_rows;

		for ($i=0;$i<$rows;$i++){
			$statement2 = 'SELECT id, profile_image, username, email, first_name, last_name, mobile_number, mobile_number_status, role, level, status, balance FROM members ';
			$statement2 .= ' ORDER BY '.$order_by.' '.$mode;
			$statement2 .= ' LIMIT '.((($page-1)*$amount+$i).' , 1');
		
			$stmt = $mysqli->prepare($statement2);
			$stmt->execute(); 
			$stmt->store_result();
	 
			$stmt->bind_result($id, $member_profile_image, $username, $email, $first_name, $last_name, $mobile_number, $mobile_number_status, $role, $level, $status, $balance);
			$stmt->fetch();
			$stmt->close();
				
			?>
			
			<tr<?php if ($level=="vip"){ echo ' class="info"'; } ?> onMouseOver="load_popup_image('<?php echo '#member_'.$i ?>','<?php if (if_file_exists("files/profile_images/".$member_profile_image)){ echo "files/profile_images/".$member_profile_image; } else{ echo "img/profile_image.png"; } ?>');" onMouseOut="dismiss_popup_image('<?php echo '#member_'.$i ?>');">
				<?php echo '
				<td><input id="member_'.$i.'" name="checkbox[]" type="checkbox" style="margin: -1px 0 -1px 5px;" value="'.$id.'" onChange="loadBatch();" />'.(($page-1)*$amount+$i+1).'</td>
				<td>'.$id.'</td>
				<td>'.$username.'</td>
				<td>'.$email.'</td>
				<td>'.$emoji->replaceEmojiWithImages($first_name).'</td>
				<td>'.$emoji->replaceEmojiWithImages($last_name).'</td>
				<td><span'; if ($mobile_number_status){ echo ' class="green"'; } echo '>98'.$mobile_number.'+</span></td>
				<td>';
				
				if (isset($member_roles[$role])){
					echo $member_roles[$role];
				} else {
					$stmt = $mysqli->prepare('SELECT role_title FROM members_roles WHERE slug="'.$role.'"');
					$stmt->execute();	
					$stmt->bind_result($role_title);
					$stmt->fetch();
					$stmt->close();
					
					echo $role_title;
				}
					
				echo '</td>
				<td>'.$member_statuses[$status].'</td>
				<td class="no-print">
				<a href="member_delete.php?id='.$id.'"><button class="btn btn-danger" style="padding:0 2px;"><i class="icon-trash"></i></button></a>
				<a href="member_edit.php?id='.$id.'"><button class="btn btn-info" style="padding:0 2px;"><i class="icon-edit"></i></button></a>
				</td>
				';
				?>
			</tr>
			<?php
			}
		}
		?>
	</table>
	<?php if ($mutch == 0){
		echo '<div class="alert alert-warning no-print"><p>موردی یافت نشد!</i></p></div>';
	} ?>
</div>
<script>
function loadBatch(){
	var values = new Array();
	$.each($("input[name='checkbox[]']:checked"), function() {
		values.push($(this).val());
	});
	
	if (values.length > 0){
		$("#batch").fadeIn();
	} else {
		$("#batch").fadeOut();
	}
}

function batchEdit(){
	$.each($("input[name='checkbox[]']:checked"), function() {
		window.open('member_edit.php?id='+$(this).val(), '_blank');
	});

}

function batchDelete(){
	$.each($("input[name='checkbox[]']:checked"), function() {
			window.open('member_delete.php?id='+$(this).val(), '_blank');
	});
}

function batchCheck(){
	if ($("#batch_check").is(":checked")){
		$.each($("input[name='checkbox[]']"), function() {
			$(this).prop("checked", true);
		});
	} else{
		$.each($("input[name='checkbox[]']"), function() {
			$(this).prop("checked", false);
		});
	}
	
	loadBatch();
}
</script>
<?php require_once('footer.php'); ?>