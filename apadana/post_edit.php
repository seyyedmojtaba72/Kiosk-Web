<?php 
include('header.php'); 
?>

<?php if (if_has_permission($role,"edit_posts")){} else{header("Location: login.php?redirect=".urlencode("http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']));} ?>

<?php
if (!empty($err)) {
	switch ($err) {
	case "edit" : $err_msg = "خطا در نوشتن دیتابیس!"; break;
	case "fill" : $err_msg = "موارد الزامی را پر کنید!"; break;
	case "exists" : $err_msg = "مطلب‌ی با این نامک وجود دارد!"; break;
	
	default : $err_msg = "خطای غیر منتظره‌ای پیش آمده!"; break;
	}
	
}
?> 
<?php
if (!empty($suc)) {
	switch ($suc) {
	case "edit" : $suc_msg = "مطلب ویرایش شد!"; break;
	
	default : $suc_msg = "عملیات با موفقیت انجام شد!"; break;
	}
	
}
?> 
<?php
if (!empty($err_msg)) {
	echo '<div class="alert alert-error no-print"><a href="'.str_replace('err='.$err,"",implode('&',array_unique(explode('&',$_SERVER['REQUEST_URI'])))).'"><button type="button" class="close">&times;</button></a><p>'. $err_msg .'</p></div>';
}
?>
<?php
if (!empty($suc_msg)) {
	echo '<div class="alert alert-success no-print"><a href="'.str_replace('suc='.$suc,"",implode('&',array_unique(explode('&',$_SERVER['REQUEST_URI'])))).'"><button type="button" class="close">&times;</button></a><p>'. $suc_msg .'</p></div>';
}
?>
<?php

if (!isset($_SESSION['posts_redirect'])){$_SESSION['posts_redirect'] = "posts.php";}

// ------

$id = filter_input(INPUT_GET, 'id', $filter = FILTER_SANITIZE_STRING);

// ------

$stmt = 'SELECT author, name, description, image, price, download_link, download_count, date, time, status FROM posts WHERE id="'.$id.'"';

if ($mysqli->query($stmt)->num_rows<1){ header('Location: posts.php?err=not-exists'); exit; }

$stmt = $mysqli->prepare($stmt);
$stmt->execute();
$stmt->store_result();

$stmt->bind_result($author, $name, $description, $image, $price, $download_link, $download_count, $date, $time, $status);
$stmt->fetch();
$stmt->close();

?>
<div class="container">
	<div class="pull-left">
		<a href="post_delete.php?<?php echo 'id='.$id ?>"><button class='btn btn-danger'><span>حذف</span> <i class="icon-trash icon-white"></i></button></a>
		<a href="<?php echo $_SESSION['posts_redirect'] ?>"><button class='btn'><span>بازگشت</span> <i class="icon-chevron-left"></i></button></a>
	</div>
	<button class="btn disabled"><span id="subtitle">ویرایش مطلب</span></button><br /><br />
	<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert">&times;</button>
	<p>مواردی که با علامت ستاره‌ی قرمز مشخص شده‌ است، الزامی است.</p></div>
    <div class="pull-right span4" id="sidebar">
		<div id="images" class="img-polaroid pull-right span4" style="margin-top: 5px;">
			<a id="image_link" class="fancybox" href="<?php if (empty($image)){echo 'img/no_image.png';} else { echo $image; } ?>">
			<img id="image" style="width: 100%;" src="<?php if (empty($image)){echo 'img/no_image.png';} else { echo $image; } ?>" /></a>
            <div style="clear:both;"></div>
		</div>
        <div style="clear:both;"></div>
        <br />
        <div id="like" class="img-polaroid span4	pull-right">
			<?php

			$likes = ($mysqli->query('SELECT id FROM posts_likes WHERE post="'.$id.'"'))->num_rows;
			
			?>
            
            <h5 class="normal"><img id="image" src="img/like.png" width="32" height="32" />&ensp;<?php echo $likes; ?> پسند</h5>
		</div>
		<br />
	</div>
	<div id="main" class="span6 pull-right">
		<form action="<?php echo $options["url"] ?>/inc/edit_post.php" method="post">
			<input type="hidden" value="post_edit.php?id=<?php echo $id ?>&" name="redirect" id="redirect"/>
			<input type="hidden" value="<?php echo $id ?>" name="id" id="id"/>
			<table class="table table-striped table-right">
            	<tr>
                	<td><h5 class="span1 pull-right normal">نویسنده <span class="red">*</span></h5></td>
                	<td style="padding: 5px 0 0 0;"><input type="text" style="font: normal 11px tahoma; width:150px;" value="<?php echo $author; ?>" name="author" id="author" /></td>
				</tr>
                <tr>
                    <td><h5 class="normal">نام</h5></td>
                    <td style="padding: 5px 0 0 0;"><input type="text" style="font: normal 11px tahoma; width:150px;" value="<?php echo $name ?>" name="name" id="name"/></td>
                </tr>
                <tr>
					<td><h5 class="normal">توضیحات</h5></td>
					<td style="padding-top: 5px;"><textarea style="font: normal 11px tahoma; width:200px;" name="description" id="description"><?php echo $description; ?></textarea></td>
				</tr>
                <tr>
					<td><h5 class="normal">تصویر <span class="red">*</span></h5></td>
					<td style="padding: 5px 0 0 0;"><input type="text" style="font: normal 11px tahoma; width:100px;" value="<?php echo $image ?>" name="image" id="image"/></td>
				</tr>
                <tr>
					<td><h5 class="normal">قیمت <span class="red">*</span></h5></td>
					<td style="padding: 5px 0 0 0;"><input type="number" style="font: normal 11px tahoma; width:100px;" value="<?php echo $price; ?>" name="price" id="price"/> ریال</td>
				</tr>
                <tr>
					<td><h5 class="normal">لینک دانلود</h5></td>
					<td style="padding: 5px 0 0 0;"><input type="text" style="font: normal 11px tahoma; width:200px;" value="<?php echo $download_link; ?>" name="download_link" id="download_link"/></td>
				</tr>
                <tr>
					<td><h5 class="normal">تعداد دانلود</h5></td>
					<td style="padding: 5px 0 0 0;"><input type="number" maxlength="11" style="font: normal 11px tahoma; width:100px;" value="<?php echo $download_count; ?>" name="download_count" id="download_count"/></td>
				</tr>
                <tr>
					<td><h5 class="normal">تاریخ</h5></td>
					<td style="padding: 5px 0 0 0;"><input type="text" style="font: normal 11px tahoma; width:100px;" value="<?php echo $date ?>" name="date" id="date"/></td>
				</tr>
                <tr>
					<td><h5 class="normal">زمان</h5></td>
					<td style="padding: 5px 0 0 0;"><input type="text" style="font: normal 11px tahoma; width:100px;" value="<?php echo $time ?>" name="time" id="time"/></td>
				</tr>
                <tr>
					<td><h5 class="normal">وضعیت <span class="red">*</span></h5></td>
					<td style="padding-top: 5px;">
						<select class="tahoma size-11" style="width: 100px;" name="status" id="status">
							<?php
							foreach ($post_statuses as $status1=>$status1_value){
								echo '<option ';
								if ($status1==$status){ echo 'selected="selected" ';}
								echo 'value="'.$status1.'">'.$status1_value.'</option>';
							}
							?>
						</select>
					</td> 
				</tr>
			</table>
			<button class='btn btn-info pull-left' type="submit"><span>ویرایش</span> <i class="icon-edit icon-white"></i></button>
		</form>
	</div>
</div>
<?php include('footer.php'); ?>