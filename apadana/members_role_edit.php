<?php 
include('header.php'); 
?>

<?php if ($role != "admin"){header("Location: login.php?redirect=".urlencode("http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']));} ?>

<?php
if (!empty($err)) {
	switch ($err) {
	case "edit" : $err_msg = "خطا در نوشتن دیتابیس!"; break;
	case "fill" : $err_msg = "موارد الزامی را پر کنید!"; break;
	case "exists" : $err_msg = "نقش عضوی با این نامک وجود دارد!"; break;
	
	default : $err_msg = "خطای غیر منتظره‌ای پیش آمده!"; break;
	}
	
}
?> 
<?php
if (!empty($suc)) {
	switch ($suc) {
	case "edit" : $suc_msg = "نقش عضو ویرایش شد!"; break;
	
	default : $suc_msg = "عملیات با موفقیت انجام شد!"; break;
	}
	
}
?> 
<?php
if (!empty($err_msg)) {
	echo '<div class="alert alert-error no-print"><a href="'.str_replace('err='.$err,"",implode('&',array_unique(explode('&',$_SERVER['REQUEST_URI'])))).'"><button type="button" class="close">&times;</button></a><p>'. $err_msg .'</p></div>';
}
?>
<?php
if (!empty($suc_msg)) {
	echo '<div class="alert alert-success no-print"><a href="'.str_replace('suc='.$suc,"",implode('&',array_unique(explode('&',$_SERVER['REQUEST_URI'])))).'"><button type="button" class="close">&times;</button></a><p>'. $suc_msg .'</p></div>';
}
?>
<?php

if (!isset($_SESSION['members_roles_redirect'])){$_SESSION['members_roles_redirect'] = "members_roles.php";}

// ------

$id = filter_input(INPUT_GET, 'id', $filter = FILTER_SANITIZE_STRING);

// ------

$stmt = 'SELECT slug, role_title, permissions FROM members_roles WHERE id="'.$id.'"';

if ($mysqli->query($stmt)->num_rows<1){ header('Location: members_roles.php?err=not-exists'); exit; }

$stmt = $mysqli->prepare($stmt);
$stmt->execute();
$stmt->store_result();

$stmt->bind_result($slug, $role_title, $permissions);
$stmt->fetch();
$stmt->close();

?>
<div class="container">
	<div class="pull-left">
		<a href="members_role_delete.php?<?php echo 'id='.$id ?>"><button class='btn btn-danger'><span>حذف</span> <i class="icon-trash icon-white"></i></button></a>
		<a href="<?php echo $_SESSION['members_roles_redirect'] ?>"><button class='btn'><span>بازگشت</span> <i class="icon-chevron-left"></i></button></a>
	</div>
	<button class="btn disabled"><span id="subtitle">ویرایش نقش عضو</span></button><br /><br />
	<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert">&times;</button>
	<p>مواردی که با علامت ستاره‌ی قرمز مشخص شده‌ است، الزامی است.</p></div>
	<h5 class="normal">مجوز‌های موجود:</h5>
	<div style="background: #ddd; margin-bottom: 10px; padding: 5px;">
		<?php
		foreach($member_permissions as $permission_slug => $permission_title){ ?>
		<button class="btn tahoma size-11" style="margin: 0 0 5px 10px;" onclick='a = "<?php echo $permission_slug ?>"; b= "<?php echo $permission_title ?>"; add_permission(a,b);'><?php echo $permission_title ?></button>
		<?php } ?>
	</div>
	<div id="left" class="span6 pull-right">
		<form action="<?php echo $options["url"] ?>/inc/edit_members_role.php" method="post">
			<input type="hidden" value="members_role_edit.php?id=<?php echo $id ?>&" name="redirect" id="redirect"/>
			<input type="hidden" value="<?php echo $slug ?>" name="former_slug" id="former_slug"/>
			<input type="hidden" value="<?php echo $permissions ?>" maxlength="1000" style="font: normal 11px tahoma; width:550px;" name="permissions" id="permissions" />
			<table class="table table-striped table-right">
			<tr>
				<td class="span2"><h5 class="normal">نامک <span class="red">*</span></h5></td>
					<td style="padding: 5px 0 0 0;"><input type="text" maxlength="100" style="font: normal 11px tahoma; width:100px;" value="<?php echo $slug ?>" name="slug" id="slug" readonly="readonly" /></td>
				</tr>
				<tr>
					<td><h5 class="normal">عنوان <span class="red">*</span></h5></td>
					<td style="padding: 5px 0 0 0;"><input type="text" style="font: normal 11px tahoma; width:150px;" value="<?php echo $role_title ?>" name="role_title" id="role_title"/></td>
				</tr>
				<tr>
					<td><h5 class="normal">مجوزها <span class="red">*</span></h5></td>
					<td style="padding: 5px 0; width: 400px;">
						<div class="normal tahoma size-11" id="permissions_span">
							<?php
							foreach($member_permissions as $permission_slug => $permission_title){
								if (stripos($permissions, $permission_slug.";")!==false){ ?>
									<button class="btn tahoma size-11" id="p_<?php echo $permission_slug ?>" style="margin: 0 0 5px 10px;" type="button" onclick='a = "<?php echo $permission_slug ?>"; b= "<?php echo $permission_title ?>"; delete_permission(a,b);'><?php echo $permission_title ?></button>
							<?php } } ?>
						</div>
					</td>
				</tr>
			</table>
			<button class='btn btn-info pull-left' type="submit"><span>ویرایش</span> <i class="icon-edit icon-white"></i></button>
		</form>
	</div>
</div>
<script type="text/javascript">

function add_permission(slug, title){
	
	var permissions = $("#permissions").val();
	var permission_span = '<button id="p_'+slug+'" class="btn tahoma size-11" style="margin: 0 0 5px 10px;">'+title+'</button>';

	if ((permissions).indexOf(slug) < 0){
			$("#permissions").val(permissions+slug+";");
			$("#permissions_span").append(permission_span).button().click();
			$("#p_"+slug).click(function(){delete_permission(slug,title)});
	};
}

</script>
<script type="text/javascript">

function delete_permission(slug, title){
	
	$("#permissions").val($("#permissions").val().replace(slug+";",""));
	$("#p_"+slug).remove();
	
}

</script>
<?php include('footer.php'); ?>