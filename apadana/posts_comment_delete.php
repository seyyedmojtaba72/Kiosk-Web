<?php 
require_once('header.php'); 
?>

<?php if ($role == "admin" | if_has_permission($role,"can_edit_posts")){} else{header("Location: login.php");} ?>

<?php
if (!empty($err)) {
	switch ($err) {
	
	default : $err_msg = "خطای غیر منتظره‌ای پیش آمده!"; break;
	}
	
}
?> 
<?php
if (!empty($suc)) {
	switch ($suc) {
	
	default : $suc_msg = "عملیات با موفقیت انجام شد!"; break;
	}
	
}
?> 
<?php
if (!empty($err_msg)) {
	echo '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">&times;</button><p>'. $err_msg .'</p></div>';
}
?>
<?php
if (!empty($suc_msg)) {
	echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button><p>'. $suc_msg .'</p></div>';
}
?>

<?php

if (!isset($_SESSION['posts_comments_redirect'])){$_SESSION['posts_comments_redirect'] = "posts_comments.php";}

// ------

$id = filter_input(INPUT_GET, 'id', $filter = FILTER_SANITIZE_STRING);

// ------

$stmt = 'SELECT author, post, comment, date, time, status FROM posts_comments WHERE id="'.$id.'"';

if ($mysqli->query($stmt)->num_rows<1){ header('Location: post_comments.php?err=not-exists'); exit; }

$stmt = $mysqli->prepare($stmt);
$stmt->execute();
$stmt->store_result();

$stmt->bind_result($author, $post, $comment, $date, $time, $status);
$stmt->fetch();
$stmt->close();

$stmt = $mysqli->prepare('SELECT username, first_name, last_name FROM members WHERE id="'.$author.'"');
$stmt->execute();
$stmt->store_result();

$stmt->bind_result($member_username, $member_first_name, $member_last_name);
$stmt->fetch();
$stmt->close();

$name = $member_first_name." ".$member_last_name;
if ($name==" "){
	$name = $member_username;
}
					
?>
<div class="container">
	<div class="pull-left">
		<a href="<?php echo $_SESSION['posts_comments_redirect'] ?>"><button class='btn'><span>بازگشت</span> <i class="icon-chevron-left"></i></button></a>
	</div>
	<h4 id="subtitle" class="page-title normal">حذف نظر مطالب</h4><br>
	<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>
		<p>آیا شما مطمئنید؟
		<form action="<?php echo $options["url"] ?>/inc/delete_posts_comment.php" method="post">
			<input type="hidden" value="posts_comments.php?" name="redirect" id="redirect"/>
			<input type="hidden" value="<?php echo $id ?>" name="id" id="id"/>
			<button style="margin-right:50px;" type="submit" class="btn btn-danger Yekan normal">بله</button>
			<a href="<?php echo $_SESSION['posts_comments_redirect'] ?>" type="button" class="btn Yekan normal">خیر</a>
		</form>
		</p>
	</div>
	<div id="right" class="span6 pull-right">
		<table class="table table-striped table-right">
			<tr>
				<td><h5 class="normal">کاربر</h5></td>
				<td style="padding-top: 5px;"><?php echo $name ?></td>
			</tr>
			<tr>
				<td><h5 class="normal">مطلب</h5></td>
				<td style="padding-top: 5px;">
					<?php
					$stmt = $mysqli->prepare('SELECT name FROM posts WHERE id="'.$post.'"');
					$stmt->execute();
					$stmt->store_result();
			 
					$stmt->bind_result($post_name);
					$stmt->fetch();
					$stmt->close();
					
				
					echo $post_name;

					?>
				</td>
			</tr>
			<tr>
				<td><h5 class="normal">نظر</h5></td>
				<td style="padding-top: 5px;"><?php echo $comment ?></td>
			</tr>
			<tr>
				<td><h5 class="normal">تاریخ</h5></td>
				<td style="padding-top: 5px;"><?php echo $date ?></td>
			</tr>
			<tr>
				<td><h5 class="normal">زمان</h5></td>
				<td style="padding-top: 5px;"><?php echo $time;?></td>
			</tr>
			<tr>
				<td><h5 class="normal">وضعیت</h5></td>
				<td style="padding-top: 5px;"><?php echo $comment_statuses[$status];?></td>
			</tr>
		</table>
	</div>
 
</div>
<?php require_once('footer.php'); ?>