<?php 

include('header.php'); 
?>

<?php if (if_has_permission($role,"edit_payments")){} else{header("Location: login.php?redirect=".urlencode("http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']));} ?>

<?php
if (!empty($err)) {
	switch ($err) {
	case "edit" : $err_msg = "خطا در نوشتن دیتابیس!"; break;
	case "fill" : $err_msg = "موارد الزامی را پر کنید!"; break;
	case "exists" : $err_msg = "پرداخت وجود دارد!"; break;
	
	default : $err_msg = "خطای غیر منتظره‌ای پیش آمده!"; break;
	}
	
}
?> 
<?php
if (!empty($suc)) {
	switch ($suc) {
	case "edit" : $suc_msg = "پرداخت ویرایش شد!"; break;
	
	default : $suc_msg = "عملیات با موفقیت انجام شد!"; break;
	}
	
}
?> 
<?php
if (!empty($err_msg)) {
	echo '<div class="alert alert-error no-print"><a href="'.str_replace('err='.$err,"",implode('&',array_unique(explode('&',$_SERVER['REQUEST_URI'])))).'"><button type="button" class="close">&times;</button></a><p>'. $err_msg .'</p></div>';
}
?>
<?php
if (!empty($suc_msg)) {
	echo '<div class="alert alert-success no-print"><a href="'.str_replace('suc='.$suc,"",implode('&',array_unique(explode('&',$_SERVER['REQUEST_URI'])))).'"><button type="button" class="close">&times;</button></a><p>'. $suc_msg .'</p></div>';
}
?>
<?php

if (!isset($_SESSION['payments_redirect'])){$_SESSION['payments_redirect'] = "payments.php";}

// ------

$id = filter_input(INPUT_GET, 'id', $filter = FILTER_SANITIZE_STRING);

// ------

$stmt = 'SELECT customer, transaction_id, amount, action, date, time, status FROM payments WHERE id="'.$id.'"';

if ($mysqli->query($stmt)->num_rows<1){ header('Location: payments.php?err=not-exists'); exit; }

$stmt = $mysqli->prepare($stmt);
$stmt->execute();
$stmt->store_result();

$stmt->bind_result($customer, $transaction_id, $amount, $action, $date, $time, $status);
$stmt->fetch();
$stmt->close();

?>

<div class="container">
	<div class="pull-left">
		<a href="payment_delete.php?<?php echo 'id='.$id ?>"><button class='btn btn-danger'><span>حذف</span> <i class="icon-trash icon-white"></i></button></a>
        <a href="<?php echo $_SESSION['payments_redirect'] ?>"><button class='btn'><span>بازگشت</span> <i class="icon-chevron-left"></i></button></a>
	</div>
	<button class="btn disabled"><span id="subtitle">ویرایش پرداخت</span></button>
    <br /><br />
	<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert">&times;</button>
	<p>مواردی که با علامت ستاره‌ی قرمز مشخص شده‌ است، الزامی است.</p></div>
	<div id="main" class="span8 pull-right">
		<form action="<?php echo $options["url"] ?>/inc/edit_payment.php" method="post">
			<input type="hidden" value="payment_edit.php?id=<?php echo $id ?>&" name="redirect" id="redirect"/>
			<input type="hidden" value="<?php echo $id ?>" name="id" id="id"/>
            <input type="hidden" value="<?php echo time(); ?>" name="timestamp" id="timestamp" />
			<table class="table table-striped table-right">
			<tr>
				<td class="span2"><h5 class="normal">مشتری <span class="red">*</span></h5></td>
					<td style="padding: 5px 0 0 0;"><input type="number" maxlength="11" style="font: normal 11px tahoma; width:150px;" value="<?php echo $customer; ?>" name="customer" id="customer"/></td>
				</tr>
				<tr>
					<td><h5 class="normal">شماره تراکنش <span class="red">*</span></h5></td>
					<td style="padding: 5px 0 0 0;"><input type="number" maxlength="11" style="font: normal 11px tahoma; width:150px;" value="<?php echo $transaction_id; ?>" name="transaction_id" id="transaction_id"/></td>
				</tr>
                <tr>
					<td><h5 class="normal">مبلغ <span class="red">*</span></h5></td>
					<td style="padding: 5px 0 0 0;"><input type="number" style="font: normal 11px tahoma; width:150px;" value="<?php echo $amount; ?>" name="amount" id="amount"/> ریال</td>
				</tr>
                <tr>
					<td><h5 class="normal">عملیات <span class="red">*</span></h5></td>
					<td style="padding: 5px 0 0 0;">
                        <select class="tahoma size-11" style="width: 100px;" name="action" id="action">
                            <?php
                            foreach ($payment_actions as $action1=>$action_text){
                                echo '<option ';
                                if ($action1==$action){ echo 'selected="selected" ';}
                                echo 'value="'.$action1.'">'.$action_text.'</option>';
                            }
                            ?>
                        </select>
                	</td>
				</tr>    
                <tr>
					<td><h5 class="normal">تاریخ <span class="red">*</span></h5></td>
					<td style="padding: 5px 0 0 0;">
                    	<input type="text" style="font: normal 11px tahoma; width:100px;" value="<?php echo $date ?>" name="date" id="date" maxlength="20" />
                    </td>
				</tr>
                <tr>
					<td><h5 class="normal">زمان <span class="red">*</span></h5></td>
					<td style="padding: 5px 0 0 0;">
                    	<input type="text" style="font: normal 11px tahoma; width:100px;" value="<?php echo $time ?>" name="time" id="time" maxlength="20" />
                    </td>
				</tr>
                <tr>
					<td><h5 class="normal">وضعیت</h5></td>
					<td style="padding: 5px 0 0 0;">
                    	<select class="tahoma size-11" style="width: 115px;" name="status" id="status">
							<?php
                            foreach ($payment_statuses as $status1=>$status_text){
                                echo '<option ';
                                if ($status1==$status){ echo 'selected="selected" ';}
                                echo 'value="'.$status1.'">'.$status_text.'</option>';
                            }
                            ?>
                    	</select>
                	</td>
				</tr>
			</table>
            <button class='btn btn-info pull-left' type="submit"><span>ویرایش</span> <i class="icon-edit icon-white"></i></button>
		</form>
	</div>
</div>
<?php include('footer.php'); ?>