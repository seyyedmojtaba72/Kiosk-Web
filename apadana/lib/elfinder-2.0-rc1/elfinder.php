<?php require_once dirname(__FILE__).'/../../inc/information.php'; ?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>elFinder 2.0</title>

		<!-- jQuery and jQuery UI (REQUIRED) -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo $options["url"]; ?>/lib/elfinder-2.0-rc1/css/jquery-ui.css">
		<script src="<?php echo $options["url"] ?>/lib/elfinder-2.0-rc1/js/jquery.min.js"></script>
		<script src="<?php echo $options["url"] ?>/lib/elfinder-2.0-rc1/js/jquery-ui.min.js"></script>

		<!-- elFinder CSS (REQUIRED) -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo $options["url"]; ?>/lib/elfinder-2.0-rc1/css/elfinder.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo $options["url"]; ?>/lib/elfinder-2.0-rc1/css/theme.css">

		<!-- elFinder JS (REQUIRED) -->
		<script type="text/javascript" src="<?php echo $options["url"] ?>/lib/elfinder-2.0-rc1/js/elfinder.min.js"></script>

		<!-- elFinder translation (OPTIONAL) -->
		<script type="text/javascript" src="<?php echo $options["url"] ?>/lib/elfinder-2.0-rc1/js/i18n/elfinder.ru.js"></script>

		<!-- elFinder initialization (REQUIRED) -->
		<script type="text/javascript" charset="utf-8">
			$().ready(function() {
				var elf = $('#elfinder').elfinder({
					url : '<?php echo $options["url"] ?>/lib/elfinder-2.0-rc1/php/connector.php'  // connector URL (REQUIRED)
					//lang: 'fa',             // language (OPTIONAL)
				}).elfinder('instance');
			});
		</script>
	</head>
	<body>

		<!-- Element where elFinder will be created (REQUIRED) -->
		<div id="elfinder"></div>

	</body>
</html>
