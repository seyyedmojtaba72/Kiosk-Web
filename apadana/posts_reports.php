<?php 
require_once('header.php'); 
?>

<?php if ($role == "admin" | if_has_permission($role,"can_edit_posts")){} else{header("Location: login.php");} ?>

<?php
if (!empty($err)) {
	switch ($err) {
	case "not-exists" : $err_msg = "گزارش مطالب وجود ندارد! ممکن است حذف شده باشد."; break;
	case "delete" : $err_msg = "خطا در حذف!"; break;
	case "no-match" : $err_msg = "جستجو نتیجه‌ای نداشت! <a style='margin-right:50px;' href='posts_reports.php'><button class='btn Yekan normal'>نمایش همه</button></a>"; break;
	
	default : $err_msg = "خطای غیر منتظره‌ای پیش آمده!"; break;
	}
	
}
?> 
<?php
if (!empty($suc)) {
	switch ($suc) {
	case "delete" : $suc_msg = "گزارش مطالب حذف شد!"; break;
	case "search" : $suc_msg = "جستجو با موفقیت انجام شد! <a style='margin-right:50px;' href='posts_reports.php'><button class='btn Yekan normal'>نمایش همه</button></a>"; break;
	
	default : $suc_msg = "عملیات با موفقیت انجام شد!"; break;
	}
	
}
?> 
<?php
if (!empty($err_msg)) {
	echo '<div class="alert alert-error no-print"><a href="'.str_replace('err='.$err,"",implode('&',array_unique(explode('&',$_SERVER['REQUEST_URI'])))).'"><button type="button" class="close">&times;</button></a><p>'. $err_msg .'</p></div>';
}
?>
<?php
if (!empty($suc_msg)) {
	echo '<div class="alert alert-success no-print"><a href="'.str_replace('suc='.$suc,"",implode('&',array_unique(explode('&',$_SERVER['REQUEST_URI'])))).'"><button type="button" class="close">&times;</button></a><p>'. $suc_msg .'</p></div>';
}
?>
<?php

$_SESSION['posts_reports_redirect'] = 
str_replace(basename($_SERVER['PHP_SELF']),basename($_SERVER['PHP_SELF'])."?",str_replace("?","",implode('&',array_unique(explode('&', $_SERVER['REQUEST_URI'])))));

// ------

$amount = $options['list_rows_per_page'];
$search = filter_input(INPUT_GET, 'search', $filter = FILTER_SANITIZE_STRING);
$order_by = filter_input(INPUT_GET, 'order_by', $filter = FILTER_SANITIZE_STRING);
$mode = filter_input(INPUT_GET, 'mode', $filter = FILTER_SANITIZE_STRING);
$page = filter_input(INPUT_GET, 'page', $filter = FILTER_SANITIZE_STRING);

if (empty($search)){$search = "false";}
if (empty($order_by)){$order_by = 'id';}
if (empty($mode)){$mode = 'DESC';}
if (empty($page)){$page = 1;}

// ------

$author = filter_input(INPUT_GET, 'author', $filter = FILTER_SANITIZE_STRING);
$post = filter_input(INPUT_GET, 'post', $filter = FILTER_SANITIZE_STRING);
$report = filter_input(INPUT_GET, 'report', $filter = FILTER_SANITIZE_STRING);
$date = filter_input(INPUT_GET, 'date', $filter = FILTER_SANITIZE_STRING);
$time = filter_input(INPUT_GET, 'time', $filter = FILTER_SANITIZE_STRING);

// ------

function get_link($order_by,$mode,$page){
	global $options, $amount, $search;
	global $author, $post, $report, $date, $time;
	
	$link="posts_reports.php?";
	if ($search=="true"){

		$link.='suc=search&search=true&author='.$author.'&post='.$post.'&report='.$report.'&date='.$date.'&time='.$time.'&';		
	} 
	
	$link.="order_by=".$order_by."&mode=".$mode;
	if ($page!=0){$link.="&page=".$page;}
	
	return $link;	
}


				
?>
<div class="container">
	<div class="pull-left no-print">
		<button class='btn btn-success' onClick="print();"><span>چاپ</span> <i class="icon-print icon-white"></i></button>
		<?php if ($role == "admin" | if_has_permission($role,"can_control_posts")){ ?>
			<a href="posts_report_add.php"><button class='btn btn-primary'><span>اضافه‌کردن</span> <i class="icon-plus icon-white"></i></button></a>
		<?php } ?>
		<a href="panel.php"><button class='btn'><span>بازگشت</span> <i class="icon-chevron-left"></i></button></a>
	</div>
	<h3 id="subtitle" class="page-title normal">‌گزارشات مطالب</h3>
	<div class="pull-right">
		<?php
			if ($search=="true"){

				$statement = 'SELECT * FROM posts_reports WHERE';
				
				if (!empty($author)){ $statement .= ' author LIKE "%'.$author .'%" AND'; }
				if (!empty($post)){ $statement .= ' post ="'.$post .'" AND'; }
				if (!empty($report)){ $statement .= ' report LIKE "%'.$report .'%" AND'; }
				if (!empty($date)){ $statement .= ' date LIKE "%'.$date .'%" AND'; }
				if (!empty($discount)){ $statement .= ' time LIKE "%'.$time .'%" AND'; }
				
				if (substr($statement,strlen($statement)-3,strlen($statement))=="AND"){
					$statement = substr($statement,0,strlen($statement)-3);
				}
				
				if (substr($statement,strlen($statement)-5,strlen($statement))=="WHERE"){
					$statement = substr($statement,0,strlen($statement)-5);
				}
							

				$result = $mysqli->query($statement);
			
				$mutch = $result->num_rows;
			} else {
				$statement = 'SELECT * FROM posts_reports';

				$result = $mysqli->query($statement);
			
				$mutch = $result->num_rows;
			}
			$pages = floor(($mutch-1)/$amount)+1;
		?>
		<?php if ($mutch>0){ ?>
		<div class="pull-right">
			<a class="no-print" href="<?php echo get_link($order_by,$mode,$page-1); ?>"><button class="btn btn-small btn-inverse <?php if ($page==1) { echo 'disabled'; } ?>" <?php if ($page==1) { echo 'disabled="disabled"'; } ?>><i class="icon-chevron-right icon-white"></i></button></a>
			&emsp;
			<span class="h5">
			<?php echo $mutch ?> مورد یافت شد
			&emsp;///&emsp;
			نمایش موارد <?php echo (($page-1)*$amount+1).' تا '.min($page*$amount,$mutch) ?>
			&emsp;///&emsp;
			صفحه‌ی
			&ensp;
			<select class="tahoma size-11" style="width: 50px; height: 25px; margin-top: 10px;" name="pagg" id="pagg" onChange='go_to_page("<?php echo get_link($order_by,$mode,0); ?>","pagg");'>
				<?php
				for ($i=0;$i<$pages;$i++){
					echo '<option ';
					if ($page==$i+1){ echo 'selected="selected" '; }
					echo 'value='.($i+1).'>'.($i+1).'</option>';
				}
				?>
			</select>
			&ensp;
			<?php echo ' از '.$pages ?>
			</span>
			&emsp;
			<a class="no-print" href="<?php echo get_link($order_by,$mode,$page+1); ?>"><button class="btn btn-small btn-inverse <?php if ($page==$pages) { echo 'disabled'; } ?>" <?php if ($page==$pages) { echo 'disabled="disabled"'; } ?>><i class="icon-chevron-left icon-white"></i></button></a>
		</div>
		<?php } ?>

	</div>
	<div class="clearfix"></div>
	<form action="<?php echo $options["url"] ?>/inc/search_posts_report.php" method="post">
		<input type="hidden" value="posts_reports.php?order_by=<?php echo $order_by ?>&mode=<?php echo $mode ?>&" name="redirect" id="redirect" />
		<table class="table table-striped table-hover text-center" style="margin-bottom:-20px;">
		
			<tr>
				<td style="width: 30px;"></td>
				<td style="width: 170px;"><h5><input type="text" style="font: normal 11px tahoma; width:150px;" value="<?php echo $author; ?>" name="author" id="author" />
				</h5></td>
				<td style="width: 170px;"><h5>
					<select class="tahoma size-11" style="width:150px;" name="post" id="post">
						<option value="" selected="selected"></option>
						<?php
							
						$result = $mysqli->query("SELECT * FROM posts");
						$rows = $result->num_rows;
						
						for ($i=0;$i<$rows;$i++){
							$stmt = $mysqli->prepare("SELECT id, name FROM posts LIMIT 1 OFFSET ?");
							$stmt->bind_param('s', $i);
							$stmt->execute();
							$stmt->store_result();
					 
							$stmt->bind_result($id, $name);
							$stmt->fetch();
							$stmt->close();
							
						
							echo '<option value="'.$id.'" ';
							if ($post==$id){echo 'selected="selected"';}
							echo '>'.$name.'</option>';
						}
						?>
					</select>
				</h5></td>
				<td style="width: 220px;"><h5><input type="text" style="font: normal 11px tahoma; width:200px;" value="<?php echo $report; ?>" name="report" id="report" maxlength="1000" />
				</h5></td>
				<td style="width: 120px;"><h5><input type="text" style="font: normal 11px tahoma; width:100px;" value="<?php echo $date; ?>" name="date" id="date" maxlength="20" />
				</h5></td>
				<td style="width: 120px;"><h5><input type="text" style="font: normal 11px tahoma; width:100px;" value="<?php echo $time; ?>" name="time" id="time" maxlength="20" />
				</h5></td>
				<td class="no-print" style="width: 50px;"><h4 class='normal'>
					<button class='btn btn-info' type="submit"><i class="icon-search"></i></button>
				</h4></td>
			</tr>
		</table>
	</form>
	<table class="table table-striped table-hover text-center">
		<tr>
			<td style="width: 30px;"><h5 class="normal">ردیف <br />
			<a href="<?php echo get_link('id','ASC',$page); ?>" class="<?php if ($order_by=="id" && $mode=="ASC"){ echo "red_outline "; } ?>icon-chevron-up no-print"></a><a href="<?php echo get_link('id','DESC',$page); ?>" class="<?php if ($order_by=="id" && $mode=="DESC"){ echo "red_outline "; } ?>icon-chevron-down no-print"></a></h5></td>
			<td style="width: 170px;"><h5 class="normal">کاربر <br />
			</h5></td>
			<td style="width: 170px;"><h5 class="normal">مطلب <br />
			</h5></td>
			<td style="width: 220px;"><h5 class="normal">گزارش <br />
			</h5></td>
			<td style="width: 120px;"><h5 class="normal">تاریخ <br />
			</h5></td>
			<td style="width: 120px;"><h5 class="normal">زمان <br />
			</h5></td>
			<td class="no-print" style="width: 50px;"><h5 class="normal">عملیات</h5></td>
		</tr>
		<?php
		
		if ($search=="true"){
			$result = $mysqli->query($statement.' LIMIT '.(($page-1)*$amount).' , '.$amount);
		
			$rows = $result->num_rows;
			
			for ($i=0;$i<$rows;$i++){
				$statement2 = str_replace("SELECT *","SELECT id, author, post, report, date, time",$statement);
				$statement2 .= ' LIMIT '.((($page-1)*$amount+$i).' , 1');
	
				$stmt = $mysqli->prepare($statement2);
				$stmt->execute();
				$stmt->store_result();
		 
				$stmt->bind_result($id, $author, $post, $report, $date, $time);
				$stmt->fetch();
				$stmt->close();
				

				$stmt = $mysqli->prepare('SELECT id, username, first_name, last_name FROM members WHERE id="'.$author.'"');
				$stmt->execute();
				$stmt->store_result();
		 
				$stmt->bind_result($member_id, $member_username, $member_first_name, $member_last_name);
				$stmt->fetch();
				$stmt->close();
				
				$member_name = $member_first_name." ".$member_last_name;
				if ($member_name==" "){
					$member_name = $member_username;
				}
				
				$post_name = "";
				
				$stmt = $mysqli->prepare('SELECT name FROM posts WHERE id="'.$post.'"');
				$stmt->execute();
				$stmt->store_result();
		 
				$stmt->bind_result($post_name);
				$stmt->fetch();
				$stmt->close();
				
				if (empty($post_name)){
					$post_name = $post;
				}

				?>
				<tr>
					<?php echo '
					<td>'.(($page-1)*$amount+$i+1).'</td>
					<td><a href="member_edit.php?id='.$member_id.'">'.$member_name.'</a></td>
					<td><a href="post_edit.php?id='.$post.'">'.$emoji->replaceEmojiWithImages($post_name).'</a></td>
					<td>'.$report.'</td>
					<td>'.$date.'</td>
					<td>'.$time.'</td>
					<td class="no-print">';
					if ($role == "admin" | if_has_permission($role,"can_control_posts")){
						echo '
						<a href="posts_report_delete.php?id='.$id.'"><button class="btn btn-danger" style="padding: 0 2px;"><i class="icon-trash"></i></button></a>
						<a href="posts_report_edit.php?id='.$id.'"><button class="btn btn-info" style="padding: 0 2px;"><i class="icon-edit"></i></button></a>
						';
					} 
					echo '</td>';
					?>
				</tr>
			<?php
			}
	
		} else{
				
			$statement .= ' ORDER BY '.$order_by.' '.$mode;
			$statement .= ' LIMIT '.(($page-1)*$amount).' , '.$amount;
			
			$result = $mysqli->query($statement);
		
			$rows = $result->num_rows;
	
			for ($i=0;$i<$rows;$i++){
				$statement2 = 'SELECT id, author, post, report, date, time FROM posts_reports';
				$statement2 .= ' ORDER BY '.$order_by.' '.$mode;
				$statement2 .= ' LIMIT '.((($page-1)*$amount+$i).' , 1');

	
				$stmt = $mysqli->prepare($statement2);
				$stmt->execute();
				$stmt->store_result();
		 
				$stmt->bind_result($id, $author, $post, $report, $date, $time);
				$stmt->fetch();
				$stmt->close();
				

				$stmt = $mysqli->prepare('SELECT id, username, first_name, last_name FROM members WHERE id="'.$author.'"');
				$stmt->execute();
				$stmt->store_result();
		 
				$stmt->bind_result($member_id, $member_username, $member_first_name, $member_last_name);
				$stmt->fetch();
				$stmt->close();
				
				$member_name = $member_first_name." ".$member_last_name;
				if ($member_name==" "){
					$member_name = $member_username;
				}
				
				$post_name = "";
				
				$stmt = $mysqli->prepare('SELECT name FROM posts WHERE id="'.$post.'"');
				$stmt->execute();
				$stmt->store_result();
		 
				$stmt->bind_result($post_name);
				$stmt->fetch();
				$stmt->close();
				
				if (empty($post_name)){
					$post_name = $post;
				}

				?>
				<tr>
					<?php echo '
					<td>'.(($page-1)*$amount+$i+1).'</td>
					<td><a href="member_edit.php?id='.$member_id.'">'.$member_name.'</a></td>
					<td><a href="post_edit.php?id='.$post.'">'.$emoji->replaceEmojiWithImages($post_name).'</a></td>
					<td>'.$report.'</td>
					<td>'.$date.'</td>
					<td>'.$time.'</td>
					<td class="no-print">';
					if ($role == "admin" | if_has_permission($role,"can_control_posts")){
						echo '
						<a href="posts_report_delete.php?id='.$id.'"><button class="btn btn-danger" style="padding: 0 2px;"><i class="icon-trash"></i></button></a>
						<a href="posts_report_edit.php?id='.$id.'"><button class="btn btn-info" style="padding: 0 2px;"><i class="icon-edit"></i></button></a>
						';
					} 
					echo '</td>';
					?>
				</tr>
				<?php
				}
			}
		?>
	</table>  
	<?php if ($mutch == 0){
		echo '<div class="alert alert-warning no-print"><p>موردی یافت نشد!</i></p></div>';
	} ?>     	
</div>
<?php require_once('footer.php'); ?>