<?php 
require_once('header.php'); 
?>

<?php if (if_has_permission($role,"view_statistics")){} else{header("Location: login.php?redirect=".urlencode("http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']));} ?>

<?php
if (!empty($err)) {
	switch ($err) {
	case "delete" : $err_msg = "خطا در حذف!"; break;
	case "no-match" : $err_msg = "جستجو نتیجه‌ای نداشت! <a style='margin-right:50px;' href='statistics.php'><button class='btn Yekan normal'>نمایش همه</button></a>"; break;
	
	default : $err_msg = "خطای غیر منتظره‌ای پیش آمده!"; break;
	}
	
}
?> 
<?php
if (!empty($suc)) {
	switch ($suc) {
	case "search" : $suc_msg = "جستجو با موفقیت انجام شد! <a style='margin-right:50px;' href='statistics.php'><button class='btn Yekan normal'>نمایش همه</button></a>"; break;
	
	default : $suc_msg = "عملیات با موفقیت انجام شد!"; break;
	}
	
}
?> 
<?php
if (!empty($err_msg)) {
	echo '<div class="alert alert-error no-print"><a href="'.str_replace('err='.$err,"",implode('&',array_unique(explode('&',$_SERVER['REQUEST_URI'])))).'"><button type="button" class="close">&times;</button></a><p>'. $err_msg .'</p></div>';
}
?>
<?php
if (!empty($suc_msg)) {
	echo '<div class="alert alert-success no-print"><a href="'.str_replace('suc='.$suc,"",implode('&',array_unique(explode('&',$_SERVER['REQUEST_URI'])))).'"><button type="button" class="close">&times;</button></a><p>'. $suc_msg .'</p></div>';
}
?>
<?php

$_SESSION['slider_redirect'] = 
str_replace(basename($_SERVER['PHP_SELF']),basename($_SERVER['PHP_SELF'])."?",str_replace("?","",implode('&',array_unique(explode('&', $_SERVER['REQUEST_URI'])))));

// ------

$amount = $options['list_rows_per_page'];
$search = filter_input(INPUT_GET, 'search', $filter = FILTER_SANITIZE_STRING);
$order_by = filter_input(INPUT_GET, 'order_by', $filter = FILTER_SANITIZE_STRING);
$mode = filter_input(INPUT_GET, 'mode', $filter = FILTER_SANITIZE_STRING);
$page = filter_input(INPUT_GET, 'page', $filter = FILTER_SANITIZE_STRING);

if (empty($search)){$search = "false";}
if (empty($order_by)){$order_by = 'id';}
if (empty($mode)){$mode = 'DESC';}
if (empty($page)){$page = 1;}

// ------

$ip = filter_input(INPUT_GET, 'ip', $filter = FILTER_SANITIZE_STRING);
$location = filter_input(INPUT_GET, 'location', $filter = FILTER_SANITIZE_STRING);
$date = filter_input(INPUT_GET, 'date', $filter = FILTER_SANITIZE_STRING);
$time = filter_input(INPUT_GET, 'time', $filter = FILTER_SANITIZE_STRING);
$os = filter_input(INPUT_GET, 'os', $filter = FILTER_SANITIZE_STRING);
$browser = filter_input(INPUT_GET, 'browser', $filter = FILTER_SANITIZE_STRING);

// -------

function get_link($order_by,$mode,$page){
	global $options, $amount, $search;
	global $ip, $location, $date, $time, $os, $browser;
	
	$link="statistics.php?";
	if ($search=="true"){
		
		$link.='suc=search&search=true&ip='.$ip.'&location='.$location.'&date='.$date.'&time='.$time.'&os='.$os.'&browser='.$browser.'&';		
	} 
	
	$link.="order_by=".$order_by."&mode=".$mode;
	if ($page!=0){$link.="&page=".$page;}
	
	return $link;	
}

?>

<!------ Jquery jqPlot v1.0.8r1250 ------>
<script type="text/javascript" src="<?php echo $options['url'] ?>/lib/jquery.jqplot-1.0.8r1250/jquery.jqplot.js"></script>
<script type="text/javascript" src="<?php echo $options['url'] ?>/lib/jquery.jqplot-1.0.8r1250/plugins/jqplot.pieRenderer.min.js"></script>
<script type="text/javascript" src="<?php echo $options['url'] ?>/lib/jquery.jqplot-1.0.8r1250/plugins/jqplot.cursor.min.js"></script>
<script type="text/javascript" src="<?php echo $options['url'] ?>/lib/jquery.jqplot-1.0.8r1250/plugins/jqplot.pointLabels.min.js"></script>
<script type="text/javascript" src="<?php echo $options['url'] ?>/lib/jquery.jqplot-1.0.8r1250/plugins/jqplot.dateAxisRenderer.min.js"></script>
<script type="text/javascript" src="<?php echo $options['url'] ?>/lib/jquery.jqplot-1.0.8r1250/plugins/jqplot.logAxisRenderer.min.js"></script>
<script type="text/javascript" src="<?php echo $options['url'] ?>/lib/jquery.jqplot-1.0.8r1250/plugins/jqplot.canvasTextRenderer.min.js"></script>
<script type="text/javascript" src="<?php echo $options['url'] ?>/lib/jquery.jqplot-1.0.8r1250/plugins/jqplot.canvasAxisTickRenderer.min.js"></script>
<script type="text/javascript" src="<?php echo $options['url'] ?>/lib/jquery.jqplot-1.0.8r1250/plugins/jqplot.highlighter.min.js"></script>
<!--------------------------------------->
	
	
<div class="container">
	<div class="pull-left no-print">
		<button class='btn btn-success' onClick="print();"><span>چاپ</span> <i class="icon-print icon-white"></i></button>
		<a href="panel.php"><button class='btn'><span>بازگشت</span> <i class="icon-chevron-left"></i></button></a>
	</div>
    <button class="btn disabled"><span id="subtitle">آمار</span></button><br />
	<div class="pull-right">
		<?php
			if ($search=="true"){
				$statement = 'SELECT DISTINCT ip FROM statistics WHERE';

				if (!empty($ip)){ $statement .= ' ip LIKE "%'.$ip .'%" AND'; }
				if (!empty($location)){ $statement .= ' location LIKE "%'.$location .'%" AND'; }
				if (!empty($date)){ $statement .= ' date LIKE "%'.$date .'%" AND'; }
				if (!empty($time)){ $statement .= ' time LIKE "%'.$time .'%" AND'; }
				if (!empty($os)){ $statement .= ' os LIKE "%'.$os .'%" AND'; }
				if (!empty($browser)){ $statement .= ' browser LIKE "%'.$browser .'%" AND'; }
				
				
				if (substr($statement,strlen($statement)-3,strlen($statement))=="AND"){
					$statement = substr($statement,0,strlen($statement)-3);
				}
				
				if (substr($statement,strlen($statement)-5,strlen($statement))=="WHERE"){
					$statement = substr($statement,0,strlen($statement)-5);
				}
							

				$result = $mysqli->query($statement);
			
				$mutch = $result->num_rows;
			} else {
				$statement = 'SELECT DISTINCT ip FROM statistics';

				$result = $mysqli->query($statement);
			
				$mutch = $result->num_rows;
			}
			$pages = floor(($mutch-1)/$amount)+1;
		?>
		<div style="margin: 10px 0;">
            <?php if ($mutch>0){ ?>
                <a class="no-print" href="<?php echo get_link($order_by,$mode,$page-1); ?>"><button class="btn btn-small btn-inverse <?php if ($page==1) { echo 'disabled'; } ?>" <?php if ($page==1) { echo 'disabled="disabled"'; } ?>><i class="icon-chevron-right icon-white"></i></button></a>
                &emsp;
                <span class="h5">
                    <?php echo $mutch ?> مورد یافت شد
                    &emsp;///&emsp;
                    نمایش موارد <?php echo (($page-1)*$amount+1).' تا '.min($page*$amount,$mutch) ?>
                    &emsp;///&emsp;
                    صفحه‌ی
                    &ensp;
                    <select class="tahoma size-11" style="margin-top: 10px; width: 50px; height: 25px;" name="pagg" id="pagg" onChange='go_to_page("<?php echo get_link($order_by,$mode,0); ?>","pagg");'>
                        <?php
                        for ($i=0;$i<$pages;$i++){
                            echo '<option ';
                            if ($page==$i+1){ echo 'selected="selected" '; }
                            echo 'value='.($i+1).'>'.($i+1).'</option>';
                        }
                        ?>
                    </select>
                    &ensp;
                    <?php echo ' از '.$pages ?>
                </span>
                &emsp;
                <a class="no-print" href="<?php echo get_link($order_by,$mode,$page+1); ?>"><button class="btn btn-small btn-inverse <?php if ($page==$pages) { echo 'disabled'; } ?>" <?php if ($page==$pages) { echo 'disabled="disabled"'; } ?>><i class="icon-chevron-left icon-white"></i></button></a>
          	<?php } ?>
    	</div>
	</div>
	<div class="clearfix"></div>
	<form action="<?php echo $options["url"] ?>/inc/search_statistic.php" method="post">
		<input type="hidden" value="statistics.php?order_by=<?php echo $order_by ?>&mode=<?php echo $mode ?>&" name="redirect" id="redirect" />
		<table class="table table-striped table-hover text-center" style="margin-bottom:-20px;">
		
			<tr>
				<td style="width: 50px;"></td>
				<td style="width: 100px;"><h5><input type="text" maxlength="20" style="font: normal 11px tahoma; width:100px;" 
				value="<?php echo $ip; ?>" name="ip" id="ip"/></h5></td>
				<td style="width: 100px;"><h5><input type="text" maxlength="20" style="font: normal 11px tahoma; width:100px;" 
				value="<?php echo $location; ?>" name="location" id="location"/></h5></td>
				<td style="width: 100px;"><h5><input type="text" maxlength="20" style="font: normal 11px tahoma; width:100px;" 
				value="<?php echo $date; ?>" name="date" id="date"/></h5></td>
				<td style="width: 100px;"><h5><input type="text" maxlength="20" style="font: normal 11px tahoma; width:100px;" 
				value="<?php echo $time; ?>" name="time" id="time"/></h5></td>
				<td style="width: 100px;"><h5><input type="text" maxlength="20" style="font: normal 11px tahoma; width:100px;" 
				value="<?php echo $os; ?>" name="os" id="os"/></h5></td>
				<td style="width: 100px;"><h5><input type="text" maxlength="20" style="font: normal 11px tahoma; width:100px;" 
				value="<?php echo $browser; ?>" name="browser" id="browser"/></h5></td>
				
				<td class="no-print" style="width: 50px;"><h4 class='normal'>
					<button class='btn btn-info' type="submit"><i class="icon-search"></i></button>
				</h4></td>
			</tr>
		</table>
	</form>
	<table class="table table-striped table-hover text-center">
		<tr>
			<td style="width: 50px;"><h5 class="normal">ردیف <br />
			<a href="<?php echo get_link('id','ASC',$page); ?>" class="<?php if ($order_by=="id" && $mode=="ASC"){ echo "red_outline "; } ?>icon-chevron-up no-print"></a><a href="<?php echo get_link('id','DESC',$page); ?>" class="<?php if ($order_by=="id" && $mode=="DESC"){ echo "red_outline "; } ?>icon-chevron-down no-print"></a></h5></td>
			<td style="width: 100px;"><h5 class="normal">آدرس آی.پی <br />
			<a href="<?php echo get_link('ip','ASC',$page); ?>" class="<?php if ($order_by=="ip" && $mode=="ASC"){ echo "red_outline "; } ?>icon-chevron-up no-print"></a><a href="<?php echo get_link('ip','DESC',$page); ?>" class="<?php if ($order_by=="ip" && $mode=="DESC"){ echo "red_outline "; } ?>icon-chevron-down no-print"></a></h5></td>
			<td style="width: 100px;"><h5 class="normal">موقعیت <br />
			<a href="<?php echo get_link('location','ASC',$page); ?>" class="<?php if ($order_by=="location" && $mode=="ASC"){ echo "red_outline "; } ?>icon-chevron-up no-print"></a><a href="<?php echo get_link('location','DESC',$page); ?>" class="<?php if ($order_by=="location" && $mode=="DESC"){ echo "red_outline "; } ?>icon-chevron-down no-print"></a></h5></td>
			<td style="width: 100px;"><h5 class="normal">تاریخ <br />
			<a href="<?php echo get_link('date','ASC',$page); ?>" class="<?php if ($order_by=="date" && $mode=="ASC"){ echo "red_outline "; } ?>icon-chevron-up no-print"></a><a href="<?php echo get_link('date','DESC',$page); ?>" class="<?php if ($order_by=="date" && $mode=="DESC"){ echo "red_outline "; } ?>icon-chevron-down no-print"></a></h5></td>
			<td style="width: 100px;"><h5 class="normal">زمان <br />
			<a href="<?php echo get_link('time','ASC',$page); ?>" class="<?php if ($order_by=="time" && $mode=="ASC"){ echo "red_outline "; } ?>icon-chevron-up no-print"></a><a href="<?php echo get_link('time','DESC',$page); ?>" class="<?php if ($order_by=="time" && $mode=="DESC"){ echo "red_outline "; } ?>icon-chevron-down no-print"></a></h5></td>
			<td style="width: 100px;"><h5 class="normal">سیستم عامل <br />
			<a href="<?php echo get_link('os','ASC',$page); ?>" class="<?php if ($order_by=="os" && $mode=="ASC"){ echo "red_outline "; } ?>icon-chevron-up no-print"></a><a href="<?php echo get_link('os','DESC',$page); ?>" class="<?php if ($order_by=="os" && $mode=="DESC"){ echo "red_outline "; } ?>icon-chevron-down no-print"></a></h5></td>
			<td style="width: 100px;"><h5 class="normal">مرورگر <br />
			<a href="<?php echo get_link('browser','ASC',$page); ?>" class="<?php if ($order_by=="browser" && $mode=="ASC"){ echo "red_outline "; } ?>icon-chevron-up no-print"></a><a href="<?php echo get_link('browser','DESC',$page); ?>" class="<?php if ($order_by=="browser" && $mode=="DESC"){ echo "red_outline "; } ?>icon-chevron-down no-print"></a></h5></td>
			<td style="width: 50px;"><h5 class='normal'>بازدید <br />
			</h5></td>
		</tr>
		<?php
		
		if ($search=="true"){

			$result = $mysqli->query($statement.' LIMIT '.(($page-1)*$amount).' , '.$amount);
		
			$rows = $result->num_rows;
			
			for ($i=0;$i<$rows;$i++){
				
				$statement2 = $statement;
				$statement2 .= ' ORDER BY '.$order_by.' '.$mode;
				$statement2 .= ' LIMIT '.((($page-1)*$amount+$i).' , 1');
				
				$stmt = $mysqli->prepare($statement2);
				$stmt->execute();
				$stmt->store_result();
		 
				$stmt->bind_result($ipp);
				$stmt->fetch();
				$stmt->close();
				
				
				$statement2 = 'SELECT DISTINCT location, date, time, os, browser FROM statistics WHERE ip="'.$ipp.'"';
				$statement2 .= ' ORDER BY '.$order_by.' '.$mode;
				$statement2 .= ' LIMIT '.((($page-1)*$amount+$i).' , 1');
			
				$stmt = $mysqli->prepare($statement2);
				$stmt->execute();
				$stmt->store_result();
		 
				$stmt->bind_result($location, $visit_date, $time, $os, $browser);
				$stmt->fetch();
				$stmt->close();
				
				
				$statement3 = 'SELECT * FROM statistics WHERE ip="'.$ipp.'"';
				if (!empty($date)){ $statement3.=' AND date="'.$date.'"'; }
				$stmt = $mysqli->prepare($statement3);
				$stmt->execute();
				$stmt->store_result();
				$visits = $stmt->num_rows();
				$stmt->fetch();
				$stmt->close();
				
				
				?>
				<tr>
					<?php echo '
					<td>'.(($page-1)*$amount+$i+1).'</td>
					<td>'.$ipp.'</td>
					<td>'.$location.'</td>
					<td>'.$visit_date.'</td>
					<td>'.$time.'</td>
					<td>'.$os.'</td>
					<td>'.$browser.'</td>
					<td>'.$visits.'</td>';
					?>
				</tr>
			<?php
			}
	
		} else{
				
			$statement .= ' ORDER BY '.$order_by.' '.$mode;
			$statement .= ' LIMIT '.(($page-1)*$amount).' , '.$amount;
			$result = $mysqli->query($statement);
		
			$rows = $result->num_rows;
	
			for ($i=0;$i<$rows;$i++){
				
				$statement2 = 'SELECT DISTINCT ip FROM statistics';
				$statement2 .= ' ORDER BY '.$order_by.' '.$mode;
				$statement2 .= ' LIMIT '.((($page-1)*$amount+$i).' , 1');
			
				$stmt = $mysqli->prepare($statement2);
				$stmt->execute();
				$stmt->store_result();
		 
				$stmt->bind_result($ipp);
				$stmt->fetch();
				$stmt->close();				
				
				$statement2 = 'SELECT DISTINCT location, date, time, os, browser FROM statistics WHERE ip="'.$ipp.'"';
				$statement2 .= ' ORDER BY '.$order_by.' '.$mode;
				$statement2 .= ' LIMIT '.((($page-1)*$amount+$i).' , 1');
			
				$stmt = $mysqli->prepare($statement2);
				$stmt->execute();
				$stmt->store_result();
		 
				$stmt->bind_result($location, $visit_date, $time, $os, $browser);
				$stmt->fetch();
				$stmt->close();
				
				
				$statement3 = 'SELECT * FROM statistics WHERE ip="'.$ipp.'"';
				if (!empty($date)){ $statement3.=' AND date="'.$date.'"'; }
				$stmt = $mysqli->prepare($statement3);
				$stmt->execute();
				$stmt->store_result();
				$visits = $stmt->num_rows();
				$stmt->fetch();
				$stmt->close();
				
				
				?>
				<tr>
					<?php echo '
					<td>'.(($page-1)*$amount+$i+1).'</td>
					<td>'.$ipp.'</td>
					<td>'.$location.'</td>
					<td>'.$visit_date.'</td>
					<td>'.$time.'</td>
					<td>'.$os.'</td>
					<td>'.$browser.'</td>
					<td>'.$visits.'</td>';
					?>
				</tr>
				<?php
				}
			}
		?>
	</table>
	<?php if ($mutch == 0){
		echo '<div class="alert alert-warning no-print"><p>موردی یافت نشد!</i></p></div>';
	} ?>
	<br />
	<h4 class="normal">‌نمای کلی</h4>
	<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert">&times;</button>
	<p>برای بزرگنمایی قسمتی از نمودار را انتخاب کنید و برای بازگشت دابل کلیک کنید.</p></div>
	<?php 
	echo '
	<script>
	$(document).ready(function () {
		var s1 = [';
		for ($i=0;$i<30;$i++){
			$date = date('Y-m-d',strtotime("-".$i." days"));
			$statement = 'SELECT * FROM statistics WHERE date="'.$persian_date->to_date($date,"Y/m/d").'"';
			$stmt = $mysqli->prepare($statement);
			$stmt->execute();
			$stmt->store_result();
			$num = $stmt->num_rows();
			$stmt->fetch();
			$stmt->close();
			echo "['".$date."', ".$num."], ";
		}
		echo '];
		var s2 = [';
		for ($i=0;$i<30;$i++){
			$date = date('Y-m-d',strtotime("-".$i." days"));
			$statement = 'SELECT DISTINCT ip FROM statistics WHERE date="'.$persian_date->to_date($date,"Y/m/d").'"';
			$stmt = $mysqli->prepare($statement);
			$stmt->execute();
			$stmt->store_result();
			$num = $stmt->num_rows();
			$stmt->fetch();
			$stmt->close();
			echo "['".$date."', ".$num."], ";
		}
		echo '];
	 
		plot1 = $.jqplot("overview", [s2, s1], {
			// Turns on animatino for all series in this plot.
			animate: true,
			// Will animate plot on calls to plot1.replot({resetAxes:true})
			animateReplot: true,
			cursor: {
				show: true,
				zoom: true,
				looseZoom: true,
				showTooltip: false,
			},
			series:[
				{
                label: "  بازدید کننده  ",
					yaxis: "yaxis",
					rendererOptions: {
						// speed up the animation a little bit.
						// This is a number of milliseconds.
						// Default for a line series is 2500.
						animation: {
							speed: 2000
						}
					}
				},
				{
					label: "  بازدید  ",
					rendererOptions: {
						// speed up the animation a little bit.
						// This is a number of milliseconds.
						// Default for a line series is 2500.
						animation: {
							speed: 2000
						}
					}
				}
			],
			axesDefaults: {
				pad: 0
			},
			axes: {
				// These options will set up the x axis like a category axis.
				xaxis: {
					renderer: $.jqplot.DateAxisRenderer,
					tickRenderer: $.jqplot.CanvasAxisTickRenderer,
					tickOptions: {
						formatString: "%b %e",
						angle: -30,
						textColor: "#dddddd"
					},
					min: "'.date('Y-m-d',strtotime("-30 days")).'",
					max: "'.date('Y-m-d').'",
					tickInterval: "1 days",
					drawMajorGridlines: false
				},
				yaxis: {
					tickOptions: {
						formatString: "%\'d"
					},
					rendererOptions: {
						forceTickAt0: true
					}
				},
				y2axis: {
					tickOptions: {
						formatString: "%\'d"
					},
					rendererOptions: {
						// align the ticks on the y2 axis with the y axis.
						alignTicks: true,
						forceTickAt0: true
					}
				}
			},
			highlighter: {
				show: true,
				showLabel: true,
				tooltipAxes: "y",
				sizeAdjust: 7.5 , tooltipLocation : "ne"
			},
			legend: {
            show: true,
            placement: "inside"
        },
		});
	    $(".jqplot-highlighter-tooltip").addClass("ui-corner-all")
	});
	</script>';
	?>
	<div id="overview" style="font: normal 15px 'WYekan', B Yekan, Tahoma; direction: ltr; height: 500px;"></div>
	<br />
	<br />
	<table class="table table-striped table-hover text-center">
		<tr>
			<td><h5 class="normal">افراد آنلاین</h5></td>
			<td><h5 class="normal">بازدید امروز</h5></td>
			<td><h5 class="normal">بازدید دیروز</h5></td>
			<td><h5 class="normal">بازدید هفته</h5></td>
			<td><h5 class="normal">بازدید ماه</h5></td>
			<td><h5 class="normal">بازدید سال</h5></td>
			<td><h5 class="normal">بازدید کل</h5></td>
		</tr>
		<tr>
			<td><h5 class="normal"><?php echo $online_visitors->count_users(); ?></h5></td>
			<td><h5 class="normal">
				<?php
				$statement = 'SELECT * FROM statistics WHERE date="'.$persian_date->date('Y/m/d').'"';
				$stmt = $mysqli->prepare($statement);
				$stmt->execute();
				$stmt->store_result();
				$num = $stmt->num_rows();
				$stmt->fetch();
				$stmt->close();
				echo $num;
				?>
			</h5></td>
			<td><h5 class="normal">
				<?php
				$date = $persian_date->to_date(date('Y-m-d',strtotime("-1 days")),'Y/m/d');
				$statement = 'SELECT * FROM statistics WHERE date="'.$date.'"';
				$stmt = $mysqli->prepare($statement);
				$stmt->execute();
				$stmt->store_result();
				$num = $stmt->num_rows();
				$stmt->fetch();
				$stmt->close();
				echo $num;
				?>
			</h5></td>
			<td><h5 class="normal">
				<?php
				$num=0;
				for ($i=0;$i<7;$i++){
					$date = $persian_date->to_date(date('Y-m-d',strtotime("-".$i." days")),'Y/m/d');
					$statement = 'SELECT * FROM statistics WHERE date="'.$date.'"';
					$stmt = $mysqli->prepare($statement);
					$stmt->execute();
					$stmt->store_result();
					$num += $stmt->num_rows();
					$stmt->fetch();
					$stmt->close();
				}
				echo $num;
				?>
			</h5></td>
			<td><h5 class="normal">
				<?php
				$num=0;
				for ($i=0;$i<30;$i++){
					$date = $persian_date->to_date(date('Y-m-d',strtotime("-".$i." days")),'Y/m/d');
					$statement = 'SELECT * FROM statistics WHERE date="'.$date.'"';
					$stmt = $mysqli->prepare($statement);
					$stmt->execute();
					$stmt->store_result();
					$num += $stmt->num_rows();
					$stmt->fetch();
					$stmt->close();	
				}
				echo $num;
				?>
			</h5></td>
			<td><h5 class="normal">
				<?php
				$num=0;
				for ($i=0;$i<365;$i++){
					$date = $persian_date->to_date(date('Y-m-d',strtotime("-".$i." days")),'Y/m/d');
					$statement = 'SELECT * FROM statistics WHERE date="'.$date.'"';
					$stmt = $mysqli->prepare($statement);
					$stmt->execute();
					$stmt->store_result();
					$num += $stmt->num_rows();
					$stmt->fetch();
					$stmt->close();	
				}
				echo $num;
				?>
				</h5></td>
			<td><h5 class="normal">
				<?php
				$statement = 'SELECT * FROM statistics';
				$stmt = $mysqli->prepare($statement);
				$stmt->execute();
				$stmt->store_result();
				$num = $stmt->num_rows();
				$stmt->fetch();
				$stmt->close();
				
				echo $num;
				?>
			</h5></td>
		</tr>
	</table>
	<br />
	<h4 class="normal">‌بر حسب موقعیت</h4>
	<?php
	echo '
	<script>
	$(document).ready(function(){
	  var data1 = [
		';
		$statement = 'SELECT DISTINCT location FROM statistics ORDER BY "id" LIMIT 0,30';
		$stmt = $mysqli->prepare($statement);
		$stmt->execute();
		$stmt->store_result();
		$nums = $stmt->num_rows();
		$stmt->fetch();
		$stmt->close();
		
		for($i=0;$i<$nums;$i++){
			$statement = 'SELECT DISTINCT location FROM statistics ORDER BY "id" LIMIT '.$i.',1';
			$stmt = $mysqli->prepare($statement);
			$stmt->execute();
			$stmt->store_result();
			$stmt->bind_result($location);
			$stmt->fetch();
			$stmt->close();
			
			$statement = 'SELECT * FROM statistics WHERE location="'.$location.'"';
			$stmt = $mysqli->prepare($statement);
			$stmt->execute();
			$stmt->store_result();
			$num = $stmt->num_rows();
			$stmt->fetch();
			$stmt->close();			
			
			echo '["'.$location.'", '.$num.'],';
			
		}
			
	  echo '];
			
	  var plot1 = jQuery.jqplot ("chart1", [data1],
		{
			
		  seriesDefaults: {
			// Make this a pie chart.
			renderer: jQuery.jqplot.PieRenderer,
			rendererOptions: {
			  // Put data labels on the pie slices.
			  // By default, labels show the percentage of the slice.
			  showDataLabels: true
			}
		  },
		  legend: { show:true, location: "e" }
		}
	  );
	  
	});
	</script>
	';
	?>
	<div id="chart1" style="font: normal 15px 'WYekan', B Yekan, Tahoma; direction: ltr; height: 500px;"></div>
	<table class="table table-bordered table-hover text-center">
		<tr>
			<td style="width: 150px;"><h5 class="normal">موقعیت</h5></td>
			<td style="width: 100px;"><h5 class="normal">تعداد</h5></td>
			<td style="width: 150px;"><h5 class="normal">موقعیت</h5></td>
			<td style="width: 100px;"><h5 class="normal">تعداد</h5></td>
			<td style="width: 150px;"><h5 class="normal">موقعیت</h5></td>
			<td style="width: 100px;"><h5 class="normal">تعداد</h5></td>
			<td style="width: 150px;"><h5 class="normal">موقعیت</h5></td>
			<td style="width: 100px;"><h5 class="normal">تعداد</h5></td>
		</tr>
		<tr>
			<?php
			$i=0;
			
			$statement = 'SELECT DISTINCT location FROM statistics ORDER BY "id" LIMIT 0,30';
			$stmt = $mysqli->prepare($statement);
			$stmt->execute();
			$stmt->store_result();
			$nums = $stmt->num_rows();
			$stmt->fetch();
			$stmt->close();
			
			for($i2=0;$i2<$nums;$i2++){
				$i++;
				$statement = 'SELECT DISTINCT location FROM statistics ORDER BY "id" LIMIT '.$i2.',1';
				$stmt = $mysqli->prepare($statement);
				$stmt->execute();
				$stmt->store_result();
				$stmt->bind_result($location);
				$stmt->fetch();
				$stmt->close();
				
				$statement = 'SELECT * FROM statistics WHERE location="'.$location.'"';
				$stmt = $mysqli->prepare($statement);
				$stmt->execute();
				$stmt->store_result();
				$num = $stmt->num_rows();
				$stmt->fetch();
				$stmt->close();			
				
				echo '<td><h5 class="normal">'.$location.'</h5></td><td><h5 class="normal">'.$num.'</h5></td>';
				
				if ($i%4==0){ echo "</tr><tr>"; }
				
			}
			
			for ($i2=0;$i2<4-$i%4;$i2++){ echo "<td></td><td></td>"; }
			?>
		</tr>
	</table>
	<br />
	<h4 class="normal">‌بر حسب سیستم عامل</h4>
	<?php
	echo '
	<script>
	$(document).ready(function(){
	  var data2 = [
		';
		foreach ($user_agent->os_array as $os=>$os_value){
			echo '["'.$os_value.'", ';
			
			$statement = 'SELECT * FROM statistics WHERE os="'.$os_value.'"';
			$stmt = $mysqli->prepare($statement);
			$stmt->execute();
			$stmt->store_result();
			$num = $stmt->num_rows();
			$stmt->fetch();
			$stmt->close();
			echo $num;
			
			echo '],	';
		}
	  echo '];
			
	  var plot2 = jQuery.jqplot ("chart2", [data2],
		{
		  seriesDefaults: {
			// Make this a pie chart.
			renderer: jQuery.jqplot.PieRenderer,
			rendererOptions: {
			  // Put data labels on the pie slices.
			  // By default, labels show the percentage of the slice.
			  showDataLabels: true
			}
		  },
		  legend: { show:true, location: "e" }
		}
	  );
	  
	});
	</script>
	';
	?>
	<div id="chart2" style="font: normal 15px 'WYekan', B Yekan, Tahoma; direction: ltr; height: 700px;"></div>
	<table class="table table-bordered table-hover text-center">
		<tr>
			<td style="width: 150px;"><h5 class="normal">سیستم عامل</h5></td>
			<td style="width: 100px;"><h5 class="normal">تعداد</h5></td>
			<td style="width: 150px;"><h5 class="normal">سیستم عامل</h5></td>
			<td style="width: 100px;"><h5 class="normal">تعداد</h5></td>
			<td style="width: 150px;"><h5 class="normal">سیستم عامل</h5></td>
			<td style="width: 100px;"><h5 class="normal">تعداد</h5></td>
			<td style="width: 150px;"><h5 class="normal">سیستم عامل</h5></td>
			<td style="width: 100px;"><h5 class="normal">تعداد</h5></td>
		</tr>
		<tr>
			<?php
			$i=0;
			foreach ($user_agent->os_array as $os=>$os_value){
				$i++;
				echo '<td><h5 class="normal">'.$os_value.'</h5></td>';
				
				$statement = 'SELECT * FROM statistics WHERE os="'.$os_value.'"';
				$stmt = $mysqli->prepare($statement);
				$stmt->execute();
				$stmt->store_result();
				$num = $stmt->num_rows();
				$stmt->fetch();
				$stmt->close();
				echo '<td><h5 class="normal">'.$num.'</h5></td>';
				
				if ($i%4==0){ echo "</tr><tr>"; }
			} 
			for ($i2=0;$i2<4-$i%4;$i2++){ echo "<td></td><td></td>"; }
			?>
		</tr>
	</table>
	<br />
	<br />
	<h4 class="normal">‌بر حسب مرورگر</h4>
	<?php
	echo '
	<script>
	$(document).ready(function(){
	  var data3 = [
		';
		foreach ($user_agent->browser_array as $browser=>$browser_value){
			echo '["'.$browser_value.'", ';
			
			$statement = 'SELECT * FROM statistics WHERE browser="'.$browser_value.'"';
			$stmt = $mysqli->prepare($statement);
			$stmt->execute();
			$stmt->store_result();
			$num = $stmt->num_rows();
			$stmt->fetch();
			$stmt->close();
			echo $num;
			
			echo '],	';
		}
	  echo '];
			
	  var plot3 = jQuery.jqplot ("chart3", [data3],
		{
		  seriesDefaults: {
			// Make this a pie chart.
			renderer: jQuery.jqplot.PieRenderer,
			rendererOptions: {
			  // Put data labels on the pie slices.
			  // By default, labels show the percentage of the slice.
			  showDataLabels: true
			}
		  },
		  legend: { show:true, location: "e" }
		}
	  );
	  
	});
	</script>
	';
	?>
	<div id="chart3" style="font: normal 15px 'WYekan', B Yekan, Tahoma; direction: ltr; height: 500px;"></div>
	<table class="table table-bordered table-hover text-center">
		<tr>
			<td style="width: 150px;"><h5 class="normal">مرورگر</h5></td>
			<td style="width: 100px;"><h5 class="normal">تعداد</h5></td>
			<td style="width: 150px;"><h5 class="normal">مرورگر</h5></td>
			<td style="width: 100px;"><h5 class="normal">تعداد</h5></td>
			<td style="width: 150px;"><h5 class="normal">مرورگر</h5></td>
			<td style="width: 100px;"><h5 class="normal">تعداد</h5></td>
			<td style="width: 150px;"><h5 class="normal">مرورگر</h5></td>
			<td style="width: 100px;"><h5 class="normal">تعداد</h5></td>
		</tr>
		<tr>
			<?php
			$i=0;
			foreach ($user_agent->browser_array as $browser=>$browser_value){
				$i++;
				echo '<td><h5 class="normal">'.$browser_value.'</h5></td>';
				
				$statement = 'SELECT * FROM statistics WHERE browser="'.$browser_value.'"';
				$stmt = $mysqli->prepare($statement);
				$stmt->execute();
				$stmt->store_result();
				$num = $stmt->num_rows();
				$stmt->fetch();
				$stmt->close();
				echo '<td><h5 class="normal">'.$num.'</h5></td>';
				
				if ($i%4==0){ echo "</tr><tr>"; }
			} 
			for ($i2=0;$i2<4-$i%4;$i2++){ echo "<td></td><td></td>"; }
			?>
		</tr>
	</table>
</div>
<?php require_once('footer.php'); ?>