<?php 
require_once('header.php');  ?>
<?php if ($logged == "in"){header("Location: profile.php");} ?>

<?php $err = filter_input(INPUT_GET, 'err', $filter = FILTER_SANITIZE_STRING); ?>
<?php $suc = filter_input(INPUT_GET, 'suc', $filter = FILTER_SANITIZE_STRING); ?>

<?php
if (!empty($err)) {
	switch ($err) {

	case "invalid-username" : $err_msg = "نام کاربری باید شامل حروف انگلیسی و اعداد باشد!"; break;
	case "mobile_number-length" : $err_msg = "طول شماره موبایل باید 10 حرف باشد!"; break;
	case "fill" : $err_msg = "موارد الزامی را پر کنید!"; break;
	case "insert" : $err_msg = "خطا در نوشتن دیتابیس!"; break;
	case "database" : $err_msg = "خطا در دیتابیس!"; break;
	case "email-exists" : $err_msg = "کاربری با این ایمیل وجود دارد!"; break;
	case "mobile_number-exists" : $err_msg = "کاربری با این شماره موبایل وجود دارد!"; break;
	case "invalid-email" : $err_msg = "ایمیل وارد شده معتبر نیست!"; break;
	case "invalid-password" : $err_msg = "گذرواژه نامعتبر است!"; break;
	case "incorrect-key" : $err_msg = "کد وارد شده صحیح نیست!"; break;
	case "check-rules" : $err_msg = "شما باید قوانین را قبول کنید!"; break;
	case "username-exists" : $err_msg = "کاربری با این نام کاربری وجود دارد!"; break;
	case "min-length" : $err_msg = "طول نام کاربری و گذواژه باید حداقل 6 کاراکتر باشد!"; break;
	case "max-username-length" : $err_msg = "طول نام کاربری باید حداکثر 50 کاراکتر باشد!"; break;
	case "max-email-length" : $err_msg = "طول آدرس ایمیل باید حداکثر 50 کاراکتر باشد!"; break;
	
	default : $err_msg = "خطای غیر منتظره‌ای پیش آمده!"; break;
	}
	
}
?> 
<?php
if (!empty($suc)) {
	switch ($suc) {
	
	case "check-email" : $suc_msg = "ثبت‌نام انجام شد. لطفاً ایمیل خود و پوشه‌ی Spam یا Bulk را بررسی کنید!"; break;
			
	default : $suc_msg = "عملیات با موفقیت انجام شد!"; break;
	}
	
}
?> 
<?php
if (!empty($err_msg)) {
	echo '<div class="alert alert-error tahoma size-11"><button type="button" class="close" data-dismiss="alert">&times;</button><p>'. $err_msg .'</p></div>';
}
?>
<?php
if (!empty($suc_msg)) {
	echo '<div class="alert alert-success tahoma size-11"><button type="button" class="close" data-dismiss="alert">&times;</button><p>'. $suc_msg .'</p></div>';
}
?>
<script type="text/javascript" src="<?php echo $options['url'] ?>/js/pwstrength.js"></script>
<script type="text/javascript">
jQuery(document).ready(function () {
	"use strict";
	var options = {};
	options.common = {
		onLoad: function () {
			$('#messages').text('Start typing password');
		}
	};
	options.ui = {
		showPopover: true,
		bootstrap2: true,
		showErrors: false
	};
	$('#password').pwstrength(options);
});
</script> 
<script type="text/javascript">
var RecaptchaOptions = {
   theme : 'white'
};
</script>
<div class="container" style="width: 350px;">   
	<div class="text-center">        
    	<button class="btn disabled"><span id="subtitle">ثبت‌نام</span></button><br /><br />
		<div class="alert text-right tahoma size-11">
			<ul>
				<li>نام کاربری و گذرواژه باید به زبان انگلیسی وارد شوند.</li>
				<li>طول نام کاربری و گذرواژه باید حداقل 6 حرف باید باشد.</li>
			</ul>
		</div>
		<div class="text-left" style="width:310px; margin-right:20px;">
			<form action="<?php echo $options["url"] ?>/inc/register.php" method="post" name="registration_form">
				<h6 class="normal span1 pull-right text-right">نام کاربری</h6>
				<input type="text" class="span2" name="username" id="username" style="font: normal 12px 'WYekan',B Yekan;" maxlength="30" placeholder="نام کاربری" />
				<h6 class="normal span1 pull-right text-right">ایمیل</h6>
				<input type="email" placeholder="ایمیل" class="span2" id="email" style="font: normal 12px 'WYekan',B Yekan;" maxlength="50" name="email" />
				<h6 class="normal span1 pull-right text-right">شماره موبایل</h6>
				<h6 class="normal pull-left Yekan">98+</h6>
				<input type="number" placeholder="" maxlength="10" value="" class="normal text-right" style="width: 124px; margin-left: 12px; font: normal 12px 'WYekan',B Yekan" id="mobile_number" name="mobile_number"/>
				<h6 class="normal span1 pull-right text-right">گذرواژه</h6>
				<input type="password" class="span2" name="password" id="password" style="font: normal 12px 'WYekan',B Yekan;" placeholder="گذرواژه" />
				<h6 class="normal span1 pull-right text-right">تأیید گذرواژه</h6>
				<input type="password" class="span2" name="confirmpwd" id="confirmpwd" style="font: normal 12px 'WYekan',B Yekan;" placeholder="تأیید گذرواژه" />
                <!--<h6 class="normal span1 pull-right text-right">معرف</h6>
				<input type="text" class="span2" name="presenter" id="presenter" style="font: normal 12px 'WYekan',B Yekan;" placeholder="" maxlength="11" />-->
				<div style="direction:ltr;">
					<?php
					require_once('inc/lib/recaptcha.lib.php');
					$publickey = "6LdFOPASAAAAAM6VjrKTDuP1hHXYV3uiKTAT4lxs";
					echo recaptcha_get_html($publickey);
					?>
				</div><br />
				<h6 class="normal text-center"><input type="checkbox" name="rules"> با <a href="<?php echo $options["url"]; ?>/terms.php">شرایط استفاده از نرم افزار</a> موافقم.</h6><br />
				<button class="btn btn-primary normal" type="submit" onClick="return regformhash(this.form,this.form.username,this.form.email,this.form.password,this.form.confirmpwd);"><span>ثبت‌نام</span> <i class="icon-plus icon-white"></i></button>
			</form>
		</div>
	</div>
</div>
<?php require_once('footer.php'); ?>