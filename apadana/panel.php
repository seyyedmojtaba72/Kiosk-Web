<?php
require_once('header.php'); 
?>
<?php if ($role != "admin" && $logged == 'in'){header("Location: member_panel.php");} ?>
<?php if ($logged != 'in'){header("Location: login.php");} ?>
<?php
if (!empty($err)) {
	switch ($err) {
	case "insert" : $err_msg = "خطا در نوشتن دیتابیس!"; break;
	
	default : $err_msg = "خطای غیر منتظره‌ای پیش آمده!"; break;
	}
	
}
?> 
<?php
if (!empty($suc)) {
	switch ($suc) {

	default : $suc_msg = "عملیات با موفقیت انجام شد!"; break;
	}
	
}
?> 
<?php
if (!empty($err_msg)) {
	echo '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">&times;</button><p>'. $err_msg .'</p></div>';
}
?>
<?php
if (!empty($suc_msg)) {
	echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button><p>'. $suc_msg .'</p></div>';
}
?>
<div class="container" style="min-height: 450px;">
	<div id="main-content">
    	<button class="btn disabled"><span id="subtitle">محیط کاربری</span></button><br />
		<div id="models">
        	<?php if (if_has_permission($role,"edit_apps")) { ?>
            <div class="model2">
                <img src="<?php echo $options["url"] ?>/img/posts.png" /><br />
                <a href="posts.php"><button class='btn' style="width: 100%; margin: 1px 0;"><span>مطالب</span> <i class="icon-list"></i></button></a>
                <a href="post_add.php"><button class='btn btn-primary' style="width: 100%; margin: 1px 0;"><span>اضافه‌کردن</span> <i class="icon-plus icon-white"></i></button></a>
                <a href="posts_likes.php"><button class='btn btn-success' style="width: 100%; margin: 1px 0;"><span>لایک‌ها</span> <i class="icon-heart icon-white"></i></button></a>
            	<a href="posts_comments.php"><button class='btn btn-warning' style="width: 100%; margin: 1px 0;"><span>نظرات</span> <i class="icon-comment icon-white"></i></button></a>
            	<a href="posts_reports.php"><button class='btn btn-danger' style="width: 100%; margin: 1px 0;"><span>گزارشات</span> <i class="icon-book icon-white"></i></button></a>
            </div>
            <?php } ?>
            <?php if (if_has_permission($role,"edit_members_statistics") || if_has_permission($role,"edit_checkout_requests")) { ?>
            <div class="model2">
                <img src="<?php echo $options["url"] ?>/img/members_statistics.png" /><br />
                <?php if (if_has_permission($role,"edit_checkout_requests")) { ?>
                <a href="checkout_requests.php"><button class='btn' style="width: 100%; margin: 1px 0;"><span>درخواست‌های تصفیه</span> <i class="icon-list"></i></button></a>
                <?php } ?>
                <?php if (if_has_permission($role,"edit_members_statistics")) { ?>
                <a href="follow_requests.php"><button class='btn btn-primary' style="width: 100%; margin: 1px 0;"><span>درخواست‌های دنبال</span> <i class="icon-list icon-white"></i></button></a>
                <a href="purchase_history.php"><button class='btn btn-success' style="width: 100%; margin: 1px 0;"><span>تاریخچه خرید</span> <i class="icon-list icon-white"></i></button></a>
                <?php } ?>
                <?php if (if_has_permission($role,"edit_user_reports")) { ?>
                <a href="user_reports.php"><button class='btn btn-warning' style="width: 100%; margin: 1px 0;"><span>گزارشات کاربران</span> <i class="icon-list icon-white"></i></button></a>
                <?php } ?>
            </div>
            <?php } ?>
            <div class="clearfix"></div>
            <div class="model2">
				<img src="<?php echo $options["url"] ?>/img/member.png" /><br />
				<a href="members.php"><button class='btn' style="width: 100%; margin: 1px 0;"><span>اعضا</span> <i class="icon-list"></i></button></a>
				<a href="member_add.php"><button class='btn btn-primary' style="width: 100%; margin: 1px 0;"><span>اضافه‌کردن</span> <i class="icon-plus icon-white"></i></button></a>
				<a href="members_roles.php"><button class='btn btn-success' style="width: 100%; margin: 1px 0;"><span>نقش‌ها</span> <i class="icon-user icon-white"></i></button></a>
			</div>
            <div class="model2">
				<img src="<?php echo $options["url"] ?>/img/emails.png" /><br />
                <a href="emails.php"><button class='btn' style="width: 100%; margin: 1px 0;"><span>ایمیل‌ها</span> <i class="icon-list"></i></button></a>
				<a href="email_add.php"><button class='btn btn-primary' style="width: 100%; margin: 1px 0;"><span>اضافه‌کردن</span> <i class="icon-plus icon-white"></i></button></a>
                <a href="send_email.php"><button class='btn btn-success' style="width: 100%; margin: 1px 0;"><span>ارسال ایمیل</span> <i class="icon-envelope icon-white"></i></button></a>
			</div>
            <div class="model2">
				<img src="<?php echo $options["url"] ?>/img/text_messages.png" /><br />
                <a href="text_messages.php"><button class='btn' style="width: 100%; margin: 1px 0;"><span>پیامک‌ها</span> <i class="icon-list"></i></button></a>
				<a href="text_message_add.php"><button class='btn btn-primary' style="width: 100%; margin: 1px 0;"><span>اضافه‌کردن</span> <i class="icon-plus icon-white"></i></button></a>
                <a href="send_sms.php"><button class='btn btn-success' style="width: 100%; margin: 1px 0;"><span>ارسال پیامک</span> <i class="icon-comment icon-white"></i></button></a>
			</div>
            <div class="model2">
				<img src="<?php echo $options["url"] ?>/img/payments.png" /><br />
                <a href="payments.php"><button class='btn' style="width: 100%; margin: 1px 0;"><span>پرداخت‌ها</span> <i class="icon-list"></i></button></a>
				<a href="payment_add.php"><button class='btn btn-primary' style="width: 100%; margin: 1px 0;"><span>اضافه‌کردن</span> <i class="icon-plus icon-white"></i></button></a>
                <a href="discounts.php"><button class='btn btn-success' style="width: 100%; margin: 1px 0;"><span>تخفیف‌ها</span> <i class="icon-gift icon-white"></i></button></a>
			</div>
			<div class="clearfix"></div>
			<div class="model">
				<a href="file_manager.php"><img src="<?php echo $options["url"] ?>/img/file_manager.png" /></a>
				<h4 class="normal">مدیریت فایل</h4>
			</div>
			<div class="model">
				<a href="upload_dropbox.php"><img src="<?php echo $options["url"] ?>/img/upload.png" /></a>
				<h4 class="normal">آپلود فایل</h4>
			</div>
			<div class="model">
				<a href="statistics.php"><img src="<?php echo $options["url"] ?>/img/statistics.png" /></a>
				<h4 class="normal">آمار</h4>
			</div>
			<div class="model">
				<a href="options.php"><img src="<?php echo $options["url"] ?>/img/options.png" /></a>
				<h4 class="normal">تنظیمات</h4>
			</div>
			<div class="model">
				<a href="profile.php"><img src="<?php echo $options["url"] ?>/img/profile.png" /></a>
				<h4 class="normal">نمایه</h4>
			</div>
		</div>
	</div>
</div>
<?php require_once('footer.php'); ?>