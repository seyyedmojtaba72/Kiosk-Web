<?php 		
require_once('header.php');
?>

<?php if (if_has_permission($role,"upload_file")){} else{header("Location: login.php?redirect=".urlencode("http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']));} ?>

<?php
if (!empty($err)) {
	switch ($err) {
	
	default : $err_msg = "خطای غیر منتظره‌ای پیش آمده!"; break;
	}
	
}
?> 
<?php
if (!empty($suc)) {
	switch ($suc) {
	
	default : $suc_msg = "عملیات با موفقیت انجام شد!"; break;
	}
	
}
?> 
<?php
if (!empty($err_msg)) {
	echo '<div class="alert alert-error no-print"><a href="'.str_replace('err='.$err,"",implode('&',array_unique(explode('&',$_SERVER['REQUEST_URI'])))).'"><button type="button" class="close">&times;</button></a><p>'. $err_msg .'</p></div>';
}
?>
<?php
if (!empty($suc_msg)) {
	echo '<div class="alert alert-success no-print"><a href="'.str_replace('suc='.$suc,"",implode('&',array_unique(explode('&',$_SERVER['REQUEST_URI'])))).'"><button type="button" class="close">&times;</button></a><p>'. $suc_msg .'</p></div>';
}
?>

<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
<link rel="stylesheet" href="<?php echo $options["url"] ?>/fragment/jQuery-File-Upload-9.9.2/css/jquery.fileupload.css">
<div class="container">
	<div class="pull-left">
		<a href="upload_file.php"><button class='btn btn-primary'><span>نسخه قدیمی</span> <i class="icon-upload icon-white"></i></button></a>
		<a href="panel.php"><button class='btn'><span>بازگشت</span> <i class="icon-chevron-left"></i></button></a>
	</div>
	<div id="main-content">
		<button class="btn disabled"><span id="subtitle">آپلود فایل</span></button><br /><br />
		
		<div class="alert text-right">
			<ul>
				<li>حجم فایل باید کمتر از <?php echo $options['upload_max_file_size'] ?> کیلوبایت باشد.</li>
			</ul>
		</div>
		<!-- The fileinput-button span is used to style the file input field as button -->
        <span class="btn btn-success fileinput-button">
            <span>انتخاب</span> <i class="icon-folder-open icon-white"></i>
            <!-- The file input field used as target for the file upload widget -->
            <input id="fileupload" type="file" name="files[]" multiple>
        </span>
        <br />
        <!-- The container for the uploaded files -->
        <div id="dropbox" class="files"></div>
    </div>
</div>
<script src="<?php echo $options["url"] ?>/fragment/jQuery-File-Upload-9.9.2/js/vendor/jquery.ui.widget.js"></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="<?php echo $options["url"] ?>/js/load-image.all.min.js"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="<?php echo $options["url"] ?>/js/canvas-to-blob.min.js"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="<?php echo $options["url"] ?>/fragment/jQuery-File-Upload-9.9.2/js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="<?php echo $options["url"] ?>/fragment/jQuery-File-Upload-9.9.2/js/jquery.fileupload.js"></script>
<!-- The File Upload processing plugin -->
<script src="<?php echo $options["url"] ?>/fragment/jQuery-File-Upload-9.9.2/js/jquery.fileupload-process.js"></script>
<!-- The File Upload image preview & resize plugin -->
<script src="<?php echo $options["url"] ?>/fragment/jQuery-File-Upload-9.9.2/js/jquery.fileupload-image.js"></script>
<!-- The File Upload audio preview plugin -->
<script src="<?php echo $options["url"] ?>/fragment/jQuery-File-Upload-9.9.2/js/jquery.fileupload-audio.js"></script>
<!-- The File Upload video preview plugin -->
<script src="<?php echo $options["url"] ?>/fragment/jQuery-File-Upload-9.9.2/js/jquery.fileupload-video.js"></script>
<!-- The File Upload validation plugin -->
<script src="<?php echo $options["url"] ?>/fragment/jQuery-File-Upload-9.9.2/js/jquery.fileupload-validate.js"></script>
<script>
/*jslint unparam: true, regexp: true */
/*global window, $ */
$(function () {
    'use strict';
    // Change this to the location of your server-side upload handler:
    var url = '<?php echo $options["url"] ?>/fragment/jQuery-File-Upload-9.9.2/server/php/',
	uploadButton = $('<button/>')
		.addClass('btn btn-primary')
		.prop('disabled', true)
		.html('<span>...در حال پردازش</span>')
		.on('click', function () {
			var $this = $(this),
				data = $this.data();
			$this
				.off('click')
				.html('<span>انصراف</span> <i class="icon-trash icon-white"></i>')
				.on('click', function () {
					$this.remove();
					data.abort();
				});
			data.submit().always(function () {
				$this.remove();
			});
		});
    $('#fileupload').fileupload({
        url: url,
        dataType: 'json',
        autoUpload: false,
       // acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
        maxFileSize: <?php echo $options['upload_max_file_size'] * 1024 ?>,
        // Enable image resizing, except for Android and Opera,
        // which actually support image resizing, but fail to
        // send Blob objects via XHR requests:
        disableImageResize: /Android(?!.*Chrome)|Opera/
            .test(window.navigator.userAgent),
        previewMaxWidth: 100,
        previewMaxHeight: 100,
        previewCrop: true
    }).on('fileuploadadd', function (e, data) {
        data.context = $('<div/>').addClass('file-cell').appendTo('#dropbox');
        $.each(data.files, function (index, file) {
            var node = $('<div/>')
                    .append($('<span/>').text(file.name));
            if (!index) {
                node
                    .append('<br>')
                    .append(uploadButton.clone(true).data(data));
            }
            node.appendTo(data.context);
        });
    }).on('fileuploadprocessalways', function (e, data) {
        var index = data.index,
            file = data.files[index],
            node = $(data.context.children()[index]);
        if (file.preview) {
            node
                .prepend('<br>')
                .prepend(file.preview);
        }
        if (file.error) {
            node
                .append('<br>')
                .append($('<span class="text-danger"/>').text(file.error));
        }
        if (index + 1 === data.files.length) {
            data.context.find('button')
                .html('<span>آپلود</span> <i class="icon-upload icon-white"></i>')
                .prop('disabled', !!data.files.error);
        }
    }).on('fileuploadprogressall', function (e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
        $('#progress .progress-bar').css(
            'width',
            progress + '%'
        );
    }).on('fileuploaddone', function (e, data) {
        $.each(data.result.files, function (index, file) {
            if (file.url) {
                var link = $('<a>')
                    .attr('target', '_blank')
                    .prop('href', file.url);
                $(data.context.children()[index])
                    .wrap(link);
            } else if (file.error) {
                var error = $('<span class="text-danger"/>').text(file.error);
                $(data.context.children()[index])
                    .append('<br>')
                    .append(error);
            }
        });
    }).on('fileuploadfail', function (e, data) {
        $.each(data.files, function (index) {
            var error = $('<span class="text-danger"/>').text('آپلود شکست خورد.');
            $(data.context.children()[index])
                .append('<br>')
                .append(error);
        });
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
});
</script>
<?php require_once('footer.php'); ?>