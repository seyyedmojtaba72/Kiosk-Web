<?php 
include('header.php'); 
?>

<?php
if (!empty($err)) {
	switch ($err) {
	case "not-exists" : $err_msg = "مطلب وجود ندارد! ممکن است حذف شده باشد."; break;
	case "delete" : $err_msg = "خطا در حذف!"; break;
	case "no-match" : $err_msg = "جستجو نتیجه‌ای نداشت! <a style='margin-right:50px;' href='posts.php'><button class='btn Yekan normal'>نمایش همه</button></a>"; break;
	
	default : $err_msg = "خطای غیر منتظره‌ای پیش آمده!"; break;
	}
	
}
?> 
<?php
if (!empty($suc)) {
	switch ($suc) {
	case "delete" : $suc_msg = "مطلب حذف شد!"; break;
	case "search" : $suc_msg = "جستجو با موفقیت انجام شد! <a style='margin-right:50px;' href='posts.php'><button class='btn Yekan normal'>نمایش همه</button></a>"; break;
	
	default : $suc_msg = "عملیات با موفقیت انجام شد!"; break;
	}
	
}
?> 
<?php
if (!empty($err_msg)) {
	echo '<div class="alert alert-error no-print"><a href="'.str_replace('err='.$err,"",implode('&',array_unique(explode('&',$_SERVER['REQUEST_URI'])))).'"><button type="button" class="close">&times;</button></a><p>'. $err_msg .'</p></div>';
}
?>
<?php
if (!empty($suc_msg)) {
	echo '<div class="alert alert-success no-print"><a href="'.str_replace('suc='.$suc,"",implode('&',array_unique(explode('&',$_SERVER['REQUEST_URI'])))).'"><button type="button" class="close">&times;</button></a><p>'. $suc_msg .'</p></div>';
}
?>
<?php

$_SESSION['posts_redirect'] = 
str_replace(basename($_SERVER['PHP_SELF']),basename($_SERVER['PHP_SELF'])."?",str_replace("?","",implode('&',array_unique(explode('&', $_SERVER['REQUEST_URI'])))));

// ------

$amount = $options['list_rows_per_page'];
$search = filter_input(INPUT_GET, 'search', $filter = FILTER_SANITIZE_STRING);
$order_by = filter_input(INPUT_GET, 'order_by', $filter = FILTER_SANITIZE_STRING);
$mode = filter_input(INPUT_GET, 'mode', $filter = FILTER_SANITIZE_STRING);
$page = filter_input(INPUT_GET, 'page', $filter = FILTER_SANITIZE_STRING);

if (empty($search)){$search = "false";}
if (empty($order_by)){$order_by = 'row';}
if (empty($mode)){$mode = 'DESC';}
if (empty($page)){$page = 1;}

// ------

$author = filter_input(INPUT_GET, 'author', $filter = FILTER_SANITIZE_STRING);
$name = filter_input(INPUT_GET, 'name', $filter = FILTER_SANITIZE_STRING);
$price = filter_input(INPUT_GET, 'price', $filter = FILTER_SANITIZE_STRING);
$download_count = filter_input(INPUT_GET, 'download_count', $filter = FILTER_SANITIZE_STRING);
$date = filter_input(INPUT_GET, 'date', $filter = FILTER_SANITIZE_STRING);
$time = filter_input(INPUT_GET, 'time', $filter = FILTER_SANITIZE_STRING);
$status = filter_input(INPUT_GET, 'status', $filter = FILTER_SANITIZE_STRING);

function get_link($order_by,$mode,$page){
	global $options, $amount, $search;
	global $author, $name, $price, $download_count, $date, $time, $status;

	$link="posts.php?";
	if ($search=="true"){
		
		$link.='suc=search&search=true&author='.$author.'&name='.$name.'&price='.$price.'&download_count='.$download_count.'&date='.$date.'&time='.$time.'&status='.$status.'&';		
	} 
	
	$link.="order_by=".$order_by."&mode=".$mode;
	if ($page!=0){$link.="&page=".$page;}
	
	return $link;
}
?>
	
	
<div class="container">
	<div class="pull-left no-print">
		<button class='btn btn-success' onclick="print();"><span>چاپ</span> <i class="icon-print icon-white"></i></button>
		<?php if ($role == "admin" | if_has_permission($role,"can_edit_posts")){ ?>
			<a href="post_add.php"><button class='btn btn-primary'><span>اضافه‌کردن</span> <i class="icon-plus icon-white"></i></button></a>
        <?php } ?>
		<a href="panel.php"><button class='btn'><span>بازگشت</span> <i class="icon-chevron-left"></i></button></a>
	</div>
    <button class="btn disabled"><span id="subtitle">مطالب</span></button>
    <br />
	<div class="pull-right">
		<?php
		if ($search=="true"){
	
			$statement = 'SELECT * FROM posts WHERE';
			
			if (!empty($author)){ $statement .= ' author = "'.$author.'" AND'; }
			if (!empty($name)){ $statement .= ' name LIKE "%'.$name.'%" AND'; }
			if (!empty($price)){ $statement .= ' price = "'.$price .'" AND'; }
			if (!empty($download_count)){ $statement .= ' download_count = "'.$download_count .'" AND'; }
			if (!empty($date)){ $statement .= ' date LIKE "%'.$date .'%" AND'; }
			if (!empty($time)){ $statement .= ' time LIKE "%'.$time .'%" AND'; }
			if (!empty($status)){ $statement .= ' status LIKE "%'.$status .'%" AND'; }
			
			
			
			
			if (substr($statement,strlen($statement)-3,strlen($statement))=="AND"){
				$statement = substr($statement,0,strlen($statement)-3);
			}
			
			if (substr($statement,strlen($statement)-5,strlen($statement))=="WHERE"){
				$statement = substr($statement,0,strlen($statement)-5);
			}
			
			$result = $mysqli->query($statement);
		
			$mutch = $result->num_rows;
		} else {
			$statement = 'SELECT * FROM posts';
			
			$result = $mysqli->query($statement);
		
			$mutch = $result->num_rows;
		}
		$pages = floor(($mutch-1)/$amount)+1;
		?>
		<div style="margin: 10px 0;">
            <?php if ($mutch>0){ ?>
                <a class="no-print" href="<?php echo get_link($order_by,$mode,$page-1); ?>"><button class="btn btn-small btn-inverse <?php if ($page==1) { echo 'disabled'; } ?>" <?php if ($page==1) { echo 'disabled="disabled"'; } ?>><i class="icon-chevron-right icon-white"></i></button></a>
                &emsp;
                <span class="h5">
                    <?php echo $mutch ?> مورد یافت شد
                    &emsp;///&emsp;
                    نمایش موارد <?php echo (($page-1)*$amount+1).' تا '.min($page*$amount,$mutch) ?>
                    &emsp;///&emsp;
                    صفحه‌ی
                    &ensp;
                    <select class="tahoma size-11" style="margin-top: 10px; width: 50px; height: 25px;" name="pagg" id="pagg" onChange='go_to_page("<?php echo get_link($order_by,$mode,0); ?>","pagg");'>
                        <?php
                        for ($i=0;$i<$pages;$i++){
                            echo '<option ';
                            if ($page==$i+1){ echo 'selected="selected" '; }
                            echo 'value='.($i+1).'>'.($i+1).'</option>';
                        }
                        ?>
                    </select>
                    &ensp;
                    <?php echo ' از '.$pages ?>
                </span>
                &emsp;
                <a class="no-print" href="<?php echo get_link($order_by,$mode,$page+1); ?>"><button class="btn btn-small btn-inverse <?php if ($page==$pages) { echo 'disabled'; } ?>" <?php if ($page==$pages) { echo 'disabled="disabled"'; } ?>><i class="icon-chevron-left icon-white"></i></button></a>
          	<?php } ?>
    	</div>
	</div>
    <div id="batch" class="no-print pull-left" style="margin: 15px 0; display: none;">
		<button class="btn btn-danger" onClick="batchDelete();"><span>حذف</span> <i class="icon-trash icon-white"></i></button>
		<button class="btn btn-info" onClick="batchEdit();"><span>ويرايش</span> <i class="icon-edit icon-white"></i></button>
	</div>
	<div class="clearfix"></div>
	<form action="<?php echo $options["url"] ?>/inc/search_post.php" method="post">
		<input type="hidden" value="posts.php?order_by=<?php echo $order_by ?>&mode=<?php echo $mode ?>&" name="redirect" id="redirect" />
		<table class="table table-striped table-hover text-center" style="margin-bottom:-20px;">
			<td style="width: 50px;"><h5><input id="batch_check" type="checkbox" style="margin: 5px 0 0px 5px;" onChange="batchCheck();" /></h5></td>
			<td style="width: 170px;"><h5><input type="text" style="font: normal 11px tahoma; width:150px;" 
			value="<?php echo $author; ?>" name="author" id="author"/></h5></td>
            <td style="width: 170px;"><h5><input type="text" style="font: normal 11px tahoma; width:150px;" 
			value="<?php echo $name; ?>" name="name" id="name"/></h5></td>
            <td style="width: 120px;"><h5><input type="number" style="font: normal 11px tahoma; width:100px;" 
			value="<?php echo $price; ?>" name="price" id="price"/></h5></td>
            <td style="width: 120px;"><h5><input type="number" maxlength="11" style="font: normal 11px tahoma; width:100px;" 
			value="<?php echo $download_count ?>" name="download_count" id="download_count"/></h5></td>
            <td style="width: 120px;"><h5><input type="text" style="font: normal 11px tahoma; width:100px;" 
			value="<?php echo $date; ?>" name="date" id="date"/></h5></td>
            <td style="width: 120px;"><h5><input type="text" style="font: normal 11px tahoma; width:100px;" 
			value="<?php echo $time; ?>" name="time" id="time"/></h5></td>
            <td style="width: 120px;"><h5>
                <select class="tahoma size-11" style="width: 100px;" name="status" id="status">
                    <option value="" selected="selected"></option>
                    <?php
                    foreach ($post_statuses as $status1=>$status1_value){
                        echo '<option ';
                        if ($status1==$status){ echo 'selected="selected" ';}
                        echo 'value="'.$status1.'">'.$status1_value.'</option>';
                    }
                    ?>
                </select>
			</h5></td>
			<td class="no-print" style="width: 70px;"><h4 class='normal'>
				<button class='btn btn-info' type="submit"><i class="icon-search"></i></button>
			</h4></td>
		</table>
	</form>
	<table class="table table-striped table-hover text-center">
		<tr>
			<td style="width: 50px;"><h5 class="normal ">ردیف <br />
			<a href="<?php echo get_link('row','ASC',$page); ?>" class="<?php if ($order_by=="row" && $mode=="ASC"){ echo "red_outline "; } ?>icon-chevron-up no-print"></a><a href="<?php echo get_link('row','DESC',$page); ?>" class="<?php if ($order_by=="row" && $mode=="DESC"){ echo "red_outline "; } ?>icon-chevron-down no-print"></a></h5></td>
			<td style="width: 170px;"><h5 class="normal">نویسنده <br />
			<a href="<?php echo get_link('author','ASC',$page); ?>" class="<?php if ($order_by=="author" && $mode=="ASC"){ echo "red_outline "; } ?>icon-chevron-up no-print"></a><a href="<?php echo get_link('author','DESC',$page); ?>" class="<?php if ($order_by=="author" && $mode=="DESC"){ echo "red_outline "; } ?>icon-chevron-down no-print"></a></h5></td>
            <td style="width: 170px;"><h5 class="normal">نام <br />
			<a href="<?php echo get_link('name','ASC',$page); ?>" class="<?php if ($order_by=="name" && $mode=="ASC"){ echo "red_outline "; } ?>icon-chevron-up no-print"></a><a href="<?php echo get_link('name','DESC',$page); ?>" class="<?php if ($order_by=="name" && $mode=="DESC"){ echo "red_outline "; } ?>icon-chevron-down no-print"></a></h5></td>
            <td style="width: 120px;"><h5 class="normal">قیمت <br />
			<a href="<?php echo get_link('price','ASC',$page); ?>" class="<?php if ($order_by=="price" && $mode=="ASC"){ echo "red_outline "; } ?>icon-chevron-up no-print"></a><a href="<?php echo get_link('price','DESC',$page); ?>" class="<?php if ($order_by=="price" && $mode=="DESC"){ echo "red_outline "; } ?>icon-chevron-down no-print"></a></h5></td>
            <td style="width: 120px;"><h5 class="normal">تعداد دانلود <br />
			<a href="<?php echo get_link('download_count','ASC',$page); ?>" class="<?php if ($order_by=="download_count" && $mode=="ASC"){ echo "red_outline "; } ?>icon-chevron-up no-print"></a><a href="<?php echo get_link('download_count','DESC',$page); ?>" class="<?php if ($order_by=="download_count" && $mode=="DESC"){ echo "red_outline "; } ?>icon-chevron-down no-print"></a></h5></td>					            <td style="width: 120px;"><h5 class="normal">تاریخ <br />
			<a href="<?php echo get_link('date','ASC',$page); ?>" class="<?php if ($order_by=="date" && $mode=="ASC"){ echo "red_outline "; } ?>icon-chevron-up no-print"></a><a href="<?php echo get_link('date','DESC',$page); ?>" class="<?php if ($order_by=="date" && $mode=="DESC"){ echo "red_outline "; } ?>icon-chevron-down no-print"></a></h5></td>
            <td style="width: 120px;"><h5 class="normal">زمان <br />
			<a href="<?php echo get_link('time','ASC',$page); ?>" class="<?php if ($order_by=="time" && $mode=="ASC"){ echo "red_outline "; } ?>icon-chevron-up no-print"></a><a href="<?php echo get_link('time','DESC',$page); ?>" class="<?php if ($order_by=="time" && $mode=="DESC"){ echo "red_outline "; } ?>icon-chevron-down no-print"></a></h5></td>
            <td style="width: 120px;"><h5 class="normal">وضعیت <br />
            <a href="<?php echo get_link('status','ASC',$page); ?>" class="<?php if ($order_by=="status" && $mode=="ASC"){ echo "red_outline "; } ?>icon-chevron-up no-print"></a><a href="<?php echo get_link('status','DESC',$page); ?>" class="<?php if ($order_by=="status" && $mode=="DESC"){ echo "red_outline "; } ?>icon-chevron-down no-print"></a></h5></td>
			<td class="no-print" style="width: 70px;"><h5 class="normal">عملیات</h5></td>
		</tr>
		
		<?php
		
		if ($order_by == "row"){ $order_by = "id"; }
		
		if ($search=="true"){
			
			$statement .= ' ORDER BY '.$order_by.' '.$mode;
				
			$result = $mysqli->query($statement.' LIMIT '.(($page-1)*$amount).' , '.$amount);
		
			$rows = $result->num_rows;
			
			for ($i=0;$i<$rows;$i++){
			$statement2 = str_replace("SELECT *","SELECT id, author, name, image, price, download_link, download_count, date, time, status",$statement);
			$statement2 .= ' LIMIT '.((($page-1)*$amount+$i).' , 1');

			$stmt = $mysqli->prepare($statement2);
			$stmt->execute();
			$stmt->store_result();
	 
			$stmt->bind_result($id, $author, $name, $image, $price, $download_link, $download_count, $date, $time, $status);
			$stmt->fetch();
			$stmt->close();
			
			if (!if_has_permission($role,"can_edit_posts") && $status != "published"){
				continue;
			}
			
			/* MEMBER NAME */
			
			$stmt = $mysqli->prepare('SELECT username, first_name, last_name FROM members WHERE id="'.$author.'"');
			$stmt->execute();
			$stmt->store_result();
	 
			$stmt->bind_result($member_username, $member_first_name, $member_last_name);
			$stmt->fetch();
			$stmt->close();
			
			$member_name = $member_first_name." ".$member_last_name;
			if ($member_name==" "){
				$member_name = $member_username;
			}
			
			$member_link = '<a href="member_edit.php?id='.$author.'">'.$member_name.'</a>';
			
			
			
			?>
			<tr class="<?php if($status=="deleted"){ echo "error"; } ?>" onMouseOver="load_popup_image('<?php echo '#post_'.$i ?>','<?php if (!empty($image)){echo $image; } else{ echo "img/no_image.png"; } ?>');" onMouseOut="dismiss_popup_image('<?php echo '#post_'.$i ?>');">
				<?php echo '
				<td><input id="post_'.$i.'" name="checkbox[]" type="checkbox" style="margin: -1px 0 -1px 5px;" value="'.$id.'" onChange="loadBatch();" />'.(($page-1)*$amount+$i+1).'</td>
				<td>'.$member_link.'</td>
				<td>'.$emoji->replaceEmojiWithImages($name).'</td>
				<td>'.$price.'</td>
				<td>'.$download_count.'</td>
				<td>'.$date.'</td>
				<td>'.$time.'</td>
				<td>'.$post_statuses[$status].'</td>
				<td class="no-print">';
				if ($role == "admin" | if_has_permission($role,"can_edit_posts")){
					echo '
					<a href="post_delete.php?id='.$id.'"><button class="btn btn-danger" style=" padding:0 2px;"><i class="icon-trash"></i></button></a>
					<a href="post_edit.php?id='.$id.'"><button class="btn btn-info" style=" padding:0 2px;"><i class="icon-edit"></i></button></a>
					';
				} else {
					echo '
					<a href="post_view.php?id='.$id.'"><button class="btn btn-warning" style="padding: 0 2px;"><i class="icon-eye-open"></i></button></a>';
					}
				echo '</td>';
				?>
			</tr>
			<?php
			}
	
	 
		} else{

		$statement .= ' ORDER BY '.$order_by.' '.$mode;
		$statement .= ' LIMIT '.(($page-1)*$amount).' , '.$amount;
		
		$result = $mysqli->query($statement);
	
		$rows = $result->num_rows;

		for ($i=0;$i<$rows;$i++){
			
			$statement2 = 'SELECT id, author, name, image, price, download_link, download_count, date, time, status FROM posts ';
			$statement2 .= ' ORDER BY '.$order_by.' '.$mode;
			$statement2 .= ' LIMIT '.((($page-1)*$amount+$i).' , 1');
		
			$stmt = $mysqli->prepare($statement2);
			$stmt->execute();
			$stmt->store_result();
	 
			$stmt->bind_result($id, $author, $name, $image, $price, $download_link, $download_count, $date, $time, $status);
			$stmt->fetch();
			$stmt->close();
			
			if (!if_has_permission($role,"can_edit_posts") && $status != "published"){
				continue;
			}
			
			/* MEMBER NAME */
			
			$stmt = $mysqli->prepare('SELECT username, first_name, last_name FROM members WHERE id="'.$author.'"');
			$stmt->execute();
			$stmt->store_result();
	 
			$stmt->bind_result($member_username, $member_first_name, $member_last_name);
			$stmt->fetch();
			$stmt->close();
			
			$member_name = $member_first_name." ".$member_last_name;
			if ($member_name==" "){
				$member_name = $member_username;
			}
			
			$member_link = '<a href="member_edit.php?id='.$author.'">'.$member_name.'</a>';
			
			
			
			?>
			<tr class="<?php if($status=="deleted"){ echo "error"; } ?>" onMouseOver="load_popup_image('<?php echo '#post_'.$i ?>','<?php if (!empty($image)){echo $image; } else{ echo "img/no_image.png"; } ?>');" onMouseOut="dismiss_popup_image('<?php echo '#post_'.$i ?>');">
				<?php echo '
				<td><input id="post_'.$i.'" name="checkbox[]" type="checkbox" style="margin: -1px 0 -1px 5px;" value="'.$id.'" onChange="loadBatch();" />'.(($page-1)*$amount+$i+1).'</td>
				<td>'.$member_link.'</td>
				<td>'.$emoji->replaceEmojiWithImages($name).'</td>
				<td>'.$price.'</td>
				<td>'.$download_count.'</td>
				<td>'.$date.'</td>
				<td>'.$time.'</td>
				<td>'.$post_statuses[$status].'</td>
				<td class="no-print">';
				if ($role == "admin" | if_has_permission($role,"can_edit_posts")){
					echo '
					<a href="post_delete.php?id='.$id.'"><button class="btn btn-danger" style=" padding:0 2px;"><i class="icon-trash"></i></button></a>
					<a href="post_edit.php?id='.$id.'"><button class="btn btn-info" style=" padding:0 2px;"><i class="icon-edit"></i></button></a>
					';
				} else {
					echo '
					<a href="post_view.php?id='.$id.'"><button class="btn btn-warning" style="padding: 0 2px;"><i class="icon-eye-open"></i></button></a>';
					}
				echo '</td>';
				?>
			</tr>
			<?php
			}
		}
		?>
	</table>
	<?php if ($mutch == 0){
		echo '<div class="alert alert-warning no-print"><p>موردی یافت نشد!</i></p></div>';
	} ?>
</div>
<script>
function loadBatch(){
	var values = new Array();
	$.each($("input[name='checkbox[]']:checked"), function() {
		values.push($(this).val());
	});
	
	if (values.length > 0){
		$("#batch").fadeIn();
	} else {
		$("#batch").fadeOut();
	}
}


function batchEdit(){
	$.each($("input[name='checkbox[]']:checked"), function() {
		window.open('post_edit.php?id='+$(this).val(), '_blank');
	});

}

function batchDelete(){
	$.each($("input[name='checkbox[]']:checked"), function() {
			window.open('post_delete.php?id='+$(this).val(), '_blank');
	});
}

function batchCheck(){
	if ($("#batch_check").is(":checked")){
		$.each($("input[name='checkbox[]']"), function() {
			$(this).prop("checked", true);
		});
	} else{
		$.each($("input[name='checkbox[]']"), function() {
			$(this).prop("checked", false);
		});
	}
	
	loadBatch();
}
</script>
<?php include('footer.php'); ?>